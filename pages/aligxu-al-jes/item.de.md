---
title: 'Anmelden zum JES!'
media_order: alighu.png
date: '12:45 24-10-2020'
taxonomy:
    category:
        - Renkontiĝoj
    tag:
        - JES
        - esperanto
        - junularaesemajno
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Melde dich an zum kostenlosen JES - 1.-3. Januar 2021, online und vor Ort in vielen Regionen!

Für dieses JES werden wir ein virtuelles Treffen auf hopin.to mit kleineren Treffen vor Ort verbinden - JESeroj.

Es erwarten dich verschiedene Programmpunkte wie Kennenlernspiele, eine Internacia Vespero, Vorträge, Gesprächsrunden, Gufujo, Drinkejo und natürlich Konzerte. Wir laden dich herzlich dazu ein, bei deiner Anmeldung selbst einen Programmpunkt einzubringen 😉 Wenn du nicht alleine feiern willst, helfen wir dir, andere Menschen zum Mitfeiern zu finden! Also schlag ruhig dein eigenes JESero vor, um ein kleines Treffen nach deinen Vorstellungen vor Ort auf die Beine zu stellen, oder schließe dich einfach einem anderen JESero an!

Weitere Informationen sowie das Anmeldeformular findest du unter [https://jes.pej.pl](https://jes.pej.pl)

![](alighu.png)
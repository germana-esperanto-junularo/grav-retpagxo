---
title: 'Aliĝu al JES!'
date: '12:45 24-10-2020'
taxonomy:
    category:
        - Renkontiĝoj
    tag:
        - JES
        - esperanto
        - junularaesemajno
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Aliĝu al senpaga JES - 1a-3a de januaro 2021, virtuale kaj surloke en multaj lokoj!

Dum tiu ĉi JES ni kunigos retan eventon ĉe hopin.to kaj ĉeestajn negrandajn renkontiĝojn - JESerojn.

Vin atendas diversaj programeroj kiel interkona kaj internacia vesperoj, prelegoj, babilejoj, gufujo, drinkejo kaj memkompreneble koncertoj. Ne forgesu proponi vian programeron en la aliĝilo 😉 Se vi ne volas festi sole, ni helpas al vi trovi kunfestantojn! Do proponu vian JESeron por krei renkontiĝeton laŭ via imago aŭ aliĝu al jam kreita JESero!

Pli da informoj kaj la aliĝilon vi trovos ĉe [https://jes.pej.pl](https://jes.pej.pl)

![](alighu.png)

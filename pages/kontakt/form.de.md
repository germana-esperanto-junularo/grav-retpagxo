---
title: Kontaktformular
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
    recipient: m.vrazitulis@yahoo.de
    subject: '[Kontaktformular esperantojugend.de] '
form:
    name: contact
    fields:
        -
            name: name
            label: 'Name oder Spitzname/Nomo aŭ kromnomo'
            placeholder: 'Dein Name'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                required: true
        -
            name: email
            label: Email/Retpoŝtadreso
            placeholder: 'Deine Email/Via Retpoŝtadreso'
            type: email
            validate:
                required: true
        -
            name: message
            label: Nachricht/Mesaĝo
            placeholder: 'Deine Nachricht an uns/Via Mesaĝo al ni'
            type: textarea
            validate:
                required: true
        -
            name: g-recaptcha-response
            label: Captcha
            type: captcha
            recaptcha_site_key: 6LdnH18aAAAAAH23Nrr9NvZmweWc043BiHS-4-RO
            recaptcha_not_validated: 'Captcha not valid!'
            validate:
                required: true
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            captcha:
                recaptcha_secret: 6LdnH18aAAAAALMIxHdRz_1vyWnXXjxBM4RtSPCk
        -
            email:
                from: '{{ config.plugins.email.from }}'
                to:
                    - '{{ config.plugins.email.to }}'
                    - '{{ form.value.email }}'
                subject: '[Kontaktformular] {{ form.value.name|e }}'
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Danke für deine Nachricht! Wir melden uns bald bei dir!'
---

### Kontaktformular



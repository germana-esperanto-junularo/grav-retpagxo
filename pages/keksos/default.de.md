---
title: Keksos
date: '00:27 09-11-2021'
hide_git_sync_repo_link: false
cache_enable: false
recaptchacontact:
    enabled: false
---

# Vergangene KEKSOj

[map-leaflet lat=51.165 lng=10.455278 zoom=6 mapname=neighbourhood variant=neighbourhood scale ]
[a-markers icon=""]
[  {"text":"1", "title":"1. KEKSO am 01.04.2006 in Stuttgart", "lat": 48.775845, "lng": 9.182932 },
{"text":"2", "title":"2. KEKSO am 24.11.2006 in Frankfurt am Main", "lat": 50.110924, "lng": 8.682127 },
{"text":"3", "title":"3. KEKSO am 03.04.2007 in Bonn", "lat": 50.737431, "lng": 7.098207 },
{"text":"4", "title":"4. KEKSO am 09.11.2007 in Heidelberg", "lat": 49.398750, "lng": 8.672434 },
{"text":"5", "title":"5. KEKSO am 25.05.2009 in Soest", "lat": 51.571148, "lng": 8.105754 },
{"text":"6", "title":"6. KEKSO am 25.09.2009 in Dessau", "lat": 51.838880, "lng": 12.245160 },
{"text":"7", "title":"7. KEKSO am 02.04.2010 in Hamburg", "lat": 53.551086, "lng": 9.993682 },
{"text":"8", "title":"8. KEKSO am 22.10.2010 in Karlsruhe", "lat": 49.006889, "lng": 8.403653 },
{"text":"9", "title":"9. KEKSO am 29.04.2011 in Rostock", "lat": 54.092442, "lng": 12.099147 },
{"text":"10", "title":"10. KEKSO am 30.08.2012 in Potsdam", "lat": 52.396149, "lng": 13.058540 },
{"text":"11", "title":"11. KEKSO am 05.06.2013 in Nürnberg", "lat": 49.452103, "lng": 11.076665 },
{"text":"12", "title":"12. KEKSO am 21.05.2014 in Erfurt", "lat": 50.984768, "lng": 11.029880 },
{"text":"13", "title":"13. KEKSO am 14.11.2014 in Bielefeld", "lat": 52.023071, "lng": 8.533210 },
{"text":"14", "title":"14. KEKSO am 23.05.2015 in Hameln", "lat": 52.108273, "lng": 9.362171 },
{"text":"15", "title":"15. KEKSO am 01.06.2016 in Wuppertal", "lat": 51.256214, "lng": 7.150764 },
{"text":"16", "title":"16. KEKSO am 01.11.2016 in Weinstadt bei Stuttgart", "lat": 48.783920, "lng": 9.271820 },
{"text":"17", "title":"17. KEKSO am 01.06.2017 in Freiburg", "lat": 47.999008, "lng": 7.842104 },
{"text":"18", "title":"18. KEKSO am 01.10.2017 in Herzberg", "lat": 51.685661, "lng": 13.218010 },
{"text":"19", "title":"19. KEKSO am 18.05.2018 in Zweibrücken", "lat": 49.250511, "lng": 7.361140 },
{"text":"20", "title":"20. KEKSO am 03.10.2018 in Heidelberg", "lat": 49.398750, "lng": 8.672434 },
{"text":"21", "title":"21. KEKSO am 10.07.2019 in Neumünster", "lat": 54.074100, "lng": 9.984670 },
{"text":"22", "title":"22. KEKSO am 20.10.2019 in Görlitz", "lat": 51.153141, "lng": 14.9753002 }
]
[/a-markers]
[/map-leaflet]

KEKSOs fanden in folgenden Städten statt:

* Stuttgart am 04.2006        
* Frankfurt am Main am 11.2006        
* Bonn am 04.2007        
* Heidelberg am 11.2007        
* Soest am 05.2009        
* Dessau am 09.2009        
* Hamburg am 04.2010        
* Karlsruhe am 10.2010        
* Rostock am 04.2011        
* Potsdam am 08.2012        
* Nürnberg am 06.2013        
* [Erfurt](https://esperantojugend.de/retpagxo/de/kune/kekso-17) am 05.2014        
* Bielefeld am 11.2014        
* Hameln am 05.2015        
* Wuppertal am 06.2016        
* Weinstadt bei Stuttgart am 11.2016        
* [Freiburg](https://esperantojugend.de/retpagxo/de/kune/kekso-21) am 06.2017        
* [Herzberg](https://esperantojugend.de/retpagxo/de/kune/kekso-22) am 10.2017        
* [Zweibrücken](https://esperantojugend.de/retpagxo/de/kune/kekso-23) am 05.2018        
* Heidelberg am 10.2018        
* [Neumünster](https://esperantojugend.de/retpagxo/de/kune/kekso-25) am 07.2019        
* [Görlitz](https://esperantojugend.de/retpagxo/de/kune/kekso-27) am 10.2019
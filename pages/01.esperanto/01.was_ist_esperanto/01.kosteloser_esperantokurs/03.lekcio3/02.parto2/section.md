---
title: "Grammatik"
---

###  Zeit-, Längen- und Gewichtsangaben

Alle Zeit-, Längen- und Gewichtsangaben erhalten die Akkusativendung -n:
Beispiele:

* la sekvan tagon – am nächsten Tag
* Mi konas vin nur du tagojn. – Ich kenne dich erst zwei Tage.

Nur wenn sie selbst Subjekt des Satzes sind oder wenn sie durch eine Präposition (z.B.
en, dum, post, antaŭ) eingeleitet werden, stehen sie im Nominativ:

Beispiele:

* La dua tago estis bela. – Der zweite Tag war schön.
* dum la sekva tago – am nächsten Tag (wörtlich: während des nächsten Tages)

###  Die Mehrzahl (Plural)

Die Mehrzahl (Plural) wird durch die Endung -j gekennzeichnet.
Sie wird an die Endungen der Adjektive, Substantive bzw. vieler Pronomen gehängt und
steht vor einer eventuellen Akkusativendung.

Beispiele:

* la ruĝaj T-ĉemizoj – die roten T-Shirts
* tiuj ĉi pomoj – diese Äpfel
* Ŝi aĉetis por mi du helajn ĉemizojn. – Sie kaufte für mich zwei helle Hemden.
* Kiun vi vidas? Mi vidas ĉiujn. – Wen siehst du? Ich sehe alle.

###  Die Aufforderungsform (Imperativ)

Die Aufforderungsform (Imperativ) der Verben wird durch die Endung -u ausgedrückt:

* iri – gehen → iru – geh, geht, gehen Sie
* pardoni – verzeihen → pardonu – verzeih, verzeiht, verzeihen Sie.

Die Endung -u kann auch an ein Verb mit Personalpronomen angehängt werden und
drückt dann die Absichtsform (Volitiv) aus, die sich im Deutschen gut mit „lass uns“ oder
„lass mich“ wiedergeben lässt:

* Mi pripensu – Lass mich nachdenken
* Ni iru – Lass uns gehen

###  Die Präposition „da“

Nach Adverbien, die gebraucht werden, um eine Menge eines unbestimmten Ganzen zu
bezeichnen, wie auch nach Nomen mit einer solchen Bedeutung, wird im Esperanto in der
Regel die nicht übersetzbare Präposition „da“ gebraucht.

* botelo da vino – eine Flasche Wein
*iom da butero – etwas Butter

Verben können ergänzt werden mit einem Adverb und einem präpositionalen Ausdruck,
eingeleitet mit „da“ (Partitative Konstruktion)

* Li trinkis malmulte da akvo. - Er trank wenig Wasser.
* Estas multe da sablo en la dezerto. - Es gibt viel Sand in der Wüste.

Das betrifft auch das Fragewort „kiom“ (= wieviel), falls das Bezugswort im Fragesatz
genannt wird:

* Kiom da mono? – Wieviel Geld?
* Aber: Kiom mi devas pagi? – Wieviel muss ich bezahlen?

Aus den obigen Beispielen ist ersichtlich, dass bei einem Adverb gefolgt von da eher die
Masse betont wird, die aus ähnlichen oder gleichen Dingen besteht. Ein Adjektiv mit einer
Mengenangabe dagegen betont mehr die verschiedenartigen Dinge, aus denen diese
Masse besteht.

###  Die Zahlen von 0 bis 9

* 0 nul
* 1 unu
* 2 du
* 3 tri
* 4 kvar
* 5 kvin
* 6 ses
* 7 sep
* 8 ok
* 9 naŭ

Zahlwörter werden nicht dekliniert. Sie erhalten kein Akkusativ-N und kein Plural-J.

###  Zusammengesetzte Wörter

Wie bereits angedeutet, können Wörter im Esperanto sehr einfach zusammengesetzt
werden. Dabei steht, wie im Deutschen, das Bestimmungswort zuerst. Es wird, meist ohne
Endung, vor das Grundwort gesetzt. Nur im Interesse der klanglichen Schönheit bzw.
Übersichtlichkeit kann die Endung -o (selten -a) des Bestimmungswortes bestehenbleiben:
eliri (el-iri) – hinausgehen
amrakonto (am-rakonto) – Liebesgeschichte
duonfrato (duon-frato) – Halbbruder
teskatolo (oder teo-skatolo) – Teeschachtel, Teebüchse.

###  Im Text haben wir folgende neue Wortbildungssilben kennengelernt:

Präfix:

* re- : wieder, noch einmal; zurück
* revidi – wiedersehen

Suffixe:

* -aĵ- macht aus abstrakten Begriffen konkrete Sachen; Gegenstand
  * vesti (jemanden anziehen) → vestaĵo (Kleidungsstück, Bekleidung)
  * trinki → trinkaĵo (Getränk)
  * blua (blau) → bluaĵo (etwas Blaues)
* -iĝ- zeigt einen Vorgang an: „zu etwas werden“
  * ruĝa (rot) → ruĝiĝi (erröten)
  * alta (hoch) → altiĝi (hoch werden, aufsteigen)
* -ist-: Berufsbezeichnung; Person, die eine Tätigkeit ständig ausführt
  * vendi (verkaufen) → vendisto (Verkäufer)
* -et-: schwächt ab, Verkleinerungsform; Gegenteil von -eg◦ libreto (Büchlein)
  * beleta (hübsch, „ein bisschen schön“)

Eine genauere Einführung und Übersicht der sogenannten Tabellwörter, wie kiom-iom-tiom, kiu-iu-tiu kommt in Lektion 6.

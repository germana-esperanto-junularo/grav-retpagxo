---
title: Übungen - Vi kondutas kvazaŭ infano!
---

![Ĉio havas alian guston](kek-3-2.mp3)

Post unu horo, ambaŭ eliras el la librovendejo. Ili nun iras al kafejo. Antaŭ la kafejo, iu
malalta, diketa viro demandas ilin:* Pardonu, karaj, mi estas fremda en tiu ĉi urbo, kaj tiu ega trafiko ĉi tie en la centro
tute ne plaĉas al mi. Mi serĉas la centran bibliotekon. Ĉu vi konas la vojon?

Peter jesas la demandon:* Mi pripensu... Iru unue en tiu ĉi direkto, ĝis la angulo. Prenu la straton dekstren kaj
iru rekte ĝis alta arbo. Tie vi trovos haltejon de la tramo. Entramiĝu (en-tram-iĝ-u),
veturu kvin staciojn, kaj eltramiĝu (el-tram-iĝ-u). Nun vi estos sur malgranda placo.
Tie ekveturos aŭtobuso. Vi veturos sep staciojn kaj elbusiĝos (el- bus-iĝ-os).
Dekstre de la aŭtobusa haltejo vi vidos grandan konstruaĵon. Tio estas la teatro.
Apud la teatro vi trovos la urban muzeon. Malantaŭ la muzeo estas malgranda
ponto. Transiru la ponton, maldekstre estos poŝtoficejo, sed vi iros dekstren ĝis
aŭtovendejo. Tie bonvolu demandi denove (de-nov-e).

Klara ruĝiĝas:* Sed mi proponas al vi iri en la alia direkto. Vi trovos la bibliotekon nur cent metrojn
de ĉi tie, malantaŭ vi. Tio estas simpla kaj tre proksima.

Kaj al Peter mallaŭte:* Ĉu vi ne kredas, ke vi kondutas kvazaŭ malbona infano?

La viro foriras.* Kaj nun ni iros en la kafejon, Peter, ĉu vi mendas por mi tason da kafo kun sukero
sen lakto?
* Jes, volonte. Cetere, hieraŭ mi prunte prenis lernolibron el la urba biblioteko kaj jam
lernis unu frazon en Esperanto!
* Kiun frazon?
* Ĝi konsistas el nur tri vortoj: Mi amas vin.

Nicht aufstöhnen! Wenden wir uns am besten wieder den neuen Vokabeln zu...


6. VOKABELN
Folgende Wörter erklären sich wohl selbst: centro, biblioteko, halti, placo, buso, aŭtobuso,
teatro, aŭto, punkto, kilometro, metro, laŭta, vorto.

* ambaŭ - beide
* mendi - bestellen
* ami - lieben
* muzeo - Museum
* angulo - Ecke
* ponto - Brücke
* apud - neben
* poŝtejo - Post (Gebäude)
* arbo - Baum
* proksima - nah
* cent - hundert
* proponi - vorschlagen
* dekstre - rechts
* prunte - leihweise
* dekstren - nach rechts
* serĉi - suchen
* denove - erneut, von neuem, wieder
* simpla - einfach
* direkto - Richtung
* stacio - Station
* frazo - Satz
* sukero - Zucker
* haltejo - Haltestelle
* taso - Tasse
* infano - Kind
* trafiko - Verkehr
* kafejo - Café, Kaffeehaus
* tramo - Straßenbahn
* konduti - sich benehmen
* trans - darüber hinweg; auf der anderen Seite
* konsisti el - bestehen aus
* konstruaĵo - Bauwerk, Gebäude
* trovi - finden
* konstrui - bauen
* unue - erstens; zuerst
* kredi - glauben
* veturi - fahren
* kvazaŭ - wie; so wie, gleichsam
* vojo - Weg
* lakto - Milch
* volonte - gern

### Lösungen zu 4.

4.1

1. roboj
2. la bluaj jupoj
3. Mi prenas tiujn ĉi ruĝajn pomojn.

4.2

1. la hieraŭan tagon
2. eliri
3. Mi vidas vin.
4. Vidu!
5. Pagu!
6. unu kilogramo da fromaĝo
7. Estu!
8. helbruna
9. pomkuko
10. Alltag
11. ganztägig
12. Besitz
13. erneut
14. Ding
15. klein

## KONTROLLÜBUNGEN FÜR DEINEN MENTOR

### Übersetze die folgenden Fragen:

1. Ĉu la prezo de la kafo altiĝis?
2. Kiel mi trovos la urban muzeon?
3. Kiu estas tiu ĉi interesa viro?
4. Kiu el tiuj ĉi virinoj estas via edzino?
5. Kiu aŭto estas nia?
6. Kiun vi renkontis hieraŭ en la kafejo?
7. Kio estas en la skatolo?
8. Kion vi skribis al tiu knabo?
9. Kiom da sukero vi deziras?
10. Wie geht es Ihnen?
11. Wer sind Sie?
12. Wen hast du gestern im Geschäft gesehen?
13. Was (Akkusativ!) hast du gestern gekauft?
14. Hast du Äpfel gekauft?
15. Wie viele Äpfel hast du gekauft?
16. Kiel vi nomiĝas?

### Übersetze folgende Wörter sinngemäß:

1. laktaĵo
2. enamiĝi (en-am-iĝ-i)
3. vorteto
4. elaŭtiĝi (el-aŭt-iĝ-i)
5. laŭtiĝi
6. mallaŭtiĝi
7. reheliĝi (re-hel-iĝ-i)
8. kontentiĝi

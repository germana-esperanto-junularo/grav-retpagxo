---
title: Teksto - Ĉio havas alian guston
---

![Ĉio havas alian guston](kek-3-1.mp3)

La sekvan tagon, Peter renkontas hazarde Klara en la strato. Ŝi portas grandan sakon.

* Bonan matenon, Klara! Kion vi havas en la sako?
* Saluton! Mi aĉetis manĝaĵojn: Du pecojn de kuko, kilogramon da pomoj, fromaĝon, panon, skatolon da kafo, nigran teon kaj unu botelon da vino.
* Ho, vi vere malsatas! Cetere, ĉu vi pardonis al mi, ke mi hieraŭ eliris (el-ir-is) el la kinejo dum la filmo?
* Ne, tute ne.
* Sed mi konas vin nur du tagojn, kaj vi jam invitis vian tutan familion al nia rendevuo.
* Jes, tion mi faris, ĉar ... ĉar en la parko vi estis tiel ...
* Kiel?
* Tiel rekta! Tio ne estas tre ĝentila. Almenaŭ oni ne tiel invitas fremdulon al kinejo.
* Pardonu tion al mi, mi petas vin.
* Peter, vi skribis en via letero, ke mia robo plaĉis al vi.
* Jes, ĝi estis treege (tre-eg-e) bela.
* Sed mi ne portis robon. Mi portis ruĝan T-ĉemizon kaj helbrunan jupon, kiel hodiaŭ.
* Aĥ …

Peter ruĝiĝas. Por ŝanĝi la temon li demandas:* Kiom da mono vi devis pagi por ĉio ĉi?
* Ĉio kostis dekkvin eŭrojn kaj dek cendojn. Peter, mi nun iros ankoraŭ en tiun
librovendejon.
* Kiun libron vi volas aĉeti?
* Mi volas aĉeti vortaron de Esperanto kaj sciencfikcian komikson.
* Mh, interese. Mi preferas amrakontojn.
* Nu, ĉiu havas alian guston.

## NEUE WÖRTER

* aĉeti - kaufen, einkaufen
* amrakonto - Liebesgeschichte
* apetito - Appetit
* botelo - Flasche
* ĉe - bei
* cendo - Eurocent
* ĉio - alles
* ĉiu - jeder
* ĉiuj - alle
* da - von (nach Maßangaben, s 3.4.)
* dekkvin - fünfzehn
* doni - geben
* du - zwei
* duona - halb
* eliri - hinausgehen
* eŭro - Euro
* fromaĝo - Käse
* gusto - Geschmack
* hela - hell
* inviti - einladen
* jen - hier ist, da ist; hier, da
* jupo - Rock
* kafo - Kaffee
* kilogramo - Kilogramm
* kiom - wieviel
* komikso - Comic, Bildergeschichte
* kuko - Kuchen
* laŭta - laut
* libro - Buch
* malsati - Hunger haben
* manĝaĵo - Essen
* mono - Geld
* nigra - schwarz
* nur - nur, erst
* oni - man
* pano - Brot
* peco - Stück
* pomo - Apfel
* poste - später
* rekta - direkt, gerade
* rendevuo - Rendezvous, Verabredung
* ruĝa - rot
* ruĝiĝi - erröten, rot werden
* sako - Tasche (zum Tragen)
* sata - satt
* sciencfikcio - Science-Fiction, Utopie
* sekva - folgende
* skatolo - Schachtel
* T-ĉemizo - T-Shirt (wörtlich: T-Hemd)
* temo - Thema
* teo - Tee
* tiel (ti-el) - so
* tri - drei
* trinkaĵo - Getränk
* trinki - trinken
* unu - eins, ein (Zahlwort)
* vendejo - Geschäft, Laden
* vino - Wein
* vortaro - Wörterbuch

Hinweis: Eŭro wird nicht wie „ojro“, sondern wie „eŭro“ ausgesprochen, also mit einem e
am Anfang.

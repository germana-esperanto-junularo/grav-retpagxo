---
title: "Grammatik"
---

In dieser Lektion lernst du zwei Abschnitte der Esperanto-Grammatik kennen, die auf den
ersten Blick ungewöhnlich kompliziert erscheinen, aber auf den zweiten Blick sehr
vielfältige und präzise Ausdrucksmöglichkeiten schaffen. Es geht um die Partizipien und
das Reflexipronomen si. Die Erklärungen sind in dieser Lektion umfangreicher als in den
anderen, um das Thema von möglichst vielen Seiten zu beleuchten und verständlich zu
machen. Zunächst schauen wir uns die Partizipien an:

#### Die Partizipien (Mittelwörter):

Partizipien sind Wörter, die eine Handlung als Eigenschaft oder Zustand beschreiben:

schreibend, geschlagen, geschlossen usw.

Im Esperanto gibt es sechs verschiedene Partizipien:

| Aktive Partizipien   |  Passive Partizipien    |     |
| :------------- | :------------- | :------------- |
|   -ant-    |    -at-     | geschieht jetzt |
|   -int-    |    -it-     | bereits geschehen |
|   -ont-    |    -ot-     | wird geschehen |

#### Aktive Partizipien

Die aktiven Partizipien drücken eine Eigenschaft desjenigen aus, der eine Handlung
vollzieht:

skribanta - schreibend
skribinta - geschrieben habend
skribonta - schreiben werdend

skribanta knabo - ein schreibender Junge
skribinta knabo - ein Junge, der geschrieben hat
skribonta knabo - ein Junge, der schreiben wird

Mi estis skribanta. - Ich schrieb gerade / war dabei, zu schreiben.
Ili estos skribantaj. - Sie werden schreiben / werden dabei sein, zu schreiben.
Ŝi estis skribonta. - Sie war kurz davor, zu schreiben, (in Zukunft, d.h. später) zu schreiben.

Partizipien können mit der Adverbendung -e auch als Umstandswort benutzt werden:

Skribante li pensis pri ŝi. - Schreibend dachte er an sie.

#### Partizip Vergangenheit Aktiv:

> atendinta/atendinte (Suffix: -int-)

Diese Form entspricht etwa der deutschen Formulierung: der/die gewartet hat (wörtlich:
wartend war). Die vom Partizip beschriebene Handlung verlief also vor der Handlung, die
das eigentliche Verb des Satzes beschreibt:

* La knabo estas atendinta. - Der Junge hat gewartet („ist wartend gewesen“).
* La atendinta knabo legas. - Der Junge, der gewartet hat, liest (jetzt).
* Atendinte, la knabo legas. - Nachdem der Junge gewartet hat, liest er (nun).

Partizipien mit Adverbendung werden der Verständlichkeit halber oft durch Teilsätze
übersetzt.

#### Partizip Zukunft Aktiv:

> atendonta/atendonte (Suffix: -ont-)

Die vom Partizip beschriebene Handlung verläuft nach der vom eigentlichen Verb des
Satzes beschriebenen Handlung. Beispiele:

* La knabo estas atendonta. - Der Junge wird warten (wörtlich: wird wartend sein).
* La atendonta knabo legas. - Der Junge, der (später) warten wird, liest (jetzt).
* Atendonte, la knabo legas. - Bevor er wartet, liest der Junge.

Was nicht geht: „Irante sur la strato venis ideo al mi en la kapon.“ Das
Subjekt des Prädikats venis ist ideo. Der Satz heißt also, dass die ideo iranta
war, was vermutlich nicht ausgedrückt werden soll.
Richtig heißt es: Irante sur la strato mi ekhavis ideon en la kapo.
Oder: Kiam mi promenis sur la strato, venis ideo al mi en la kapon.
Wichtig bei der Verwendung von Partizipien ist, dass die Zeitform des
Hauptverbs des Satzes als Grundlage gesehen wird und die Zeitform des Partizips dazu
ins Verhältnis gesetzt wird. So kann man alle im Deutschen vorhandenen Zeiten
ausdrücken und noch weitere dazu erfinden:

|      |      |
| :------------- | :------------- |
| Kiam Klara venis, li jam delonge estis atendinta.       | Als Klara kam, hatte Peter schon seit langem gewartet (wörtlich: war Peter schon lange wartend gewesen) Plusquamperfekt: estis atendinta.       |
| Ŝi estos leginta la libron.                             | Sie wird das Buch gelesen haben (wörtlich: sie wird lesend gewesen sein). Futur II: estos leginta. Ili estis irontaj kinejen.       |
| Ili estis irontaj kinejen.       | Sie waren kurz davor ins Kino zu gehen.       |

#### Passive Partizipien

Die passiven Partizipien drücken eine Eigenschaft dessen aus, der das Ziel einer
Handlung ist:

* skribata - geschrieben-werdend
* skribita - geschrieben
* skribota - geschrieben werden seiend

* skribata letero - Brief, der gerade geschrieben wird
* skribita letero - ein geschriebener Brief
* skribota letero - ein Brief, der geschrieben werden wird

Und in einem Satz beispielsweise:

* La letero estas skribata de mi. - Der Brief wird gerade von mir geschrieben.
* La letero estis skribata de ŝi. - Der Brief wurde gerade von ihr geschrieben.
* La letero estis skribita de li. - Der Brief war von ihm geschrieben worden.

Passiv-Partizipien sind also dadurch charakterisiert, dass das vom Partizip näher
bezeichnete Substantiv nicht Handlungsträger, sondern eigentlich nur das Objekt (Ziel) der
Handlung (passives „Objekt“) ist.
Passiv-Partizipien können nur von transitiven Verben, also solchen, die ein (Objekt-)Ziel
haben, gebildet werden.

Das Partizip Vergangenheit Passiv entspricht dem deutschen Partizip II: erwartet,
gelesen, usw. Es kennzeichnet eine (im Vergleich zum Hauptverb) abgeschlossene
Handlung: atendita/atendite

* La knabo estas atendita. - Der Junge ist erwartet worden. (Jetzt wartet niemand mehr.)
* La atendita knabo forkuris kun ŝi. - Der erwartete Junge ging mit ihr fort. (Er wurde erst erwartet, dann ging er mit ihr fort.)
* Neatendite ŝi venis en la dormoĉambron. - Unerwartet kam sie in das Schlafzimmer. (Das heisst, keiner hatte es, bevor sie hereinkam, erwartet.)

Die vom Partizip Vergangenheit Passiv beschriebenen Vorgänge sind in jedem Fall
abgeschlossen, auch wenn ihre Auswirkungen bis in die Gegenwart anhalten können:

* Mi estas okupita. - Ich bin beschäftigt. (wörtlich: okupi = besetzen: durch eine Idee, eine Aufgabe, eine Zielstellung usw. Der Vorgang der „Besetzung“ ist abgeschlossen, auch wenn der Zustand, der dadurch bewirkt wurde, noch andauert. Das lässt sich vielleicht einem beladenen Boot verdeutlichen: „Das Boot ist beladen.“ - das ist ein Zustand, der durch eine in der Vergangenheit abgeschlossene Handlung erreicht wurde.)

Das Partizip Gegenwart Passiv drückt einen Vorgang aus, der zur Zeit der Handlung des
Hauptverbes noch andauert oder mehrfach wiederholt wird: atendata/atendate (Suffix: -at-)

* La atendata knabo ĝis nun ne venis. - Der Junge, der (z.Z.) erwartet wird, ist noch nicht gekommen.
* ein mehrfach eingeladener Gast. plurfoje invitata gasto

Das Partizip Zukunft Passiv beschreibt einen Vorgang, der zur Zeit der Haupthandlung
noch gar nicht begonnen hat: atendota/atendote (Suffix: -ot-)

* La atendota knabo ankoraŭ dormas. - Der Junge, der erwartet (werden) wird, schläft noch. (Er wird in der Zukunft erwartet, z.Z. wartet noch niemand auf ihn.)

Die Endung -o bezeichnet bei aktiven und passiven Partizipien die Person, die eine
Handlung tätigt:

| Aktiv          | Passiv          |
| :------------- | :------------- |
| skribanto - Schreibender                     | gvidato – Geführter       |
| skribinto - Verfasser                        | perdito – Verlorener      |
| skribonto - Jemand, der schreiben wird       | naskoto – Fötus           |

So weit, so gut. Alles klar? Dann kommen wir zu einem weiteren grammatischen Element
im Esperanto, das präzises Ausdrücken ermöglicht:

#### Das Reflexivpronomen „si“

Um in dem Satz „Sie nimmt ihr Buch.“ eindeutig festzulegen, ob sie ihr eigenes Buch oder
das einer anderen Frau nimmt, wurde im Esperanto das rückbezügliche Pronomen
(Reflexivpronomen) „si/sia“ eingeführt. Es zeigt an, dass die mit diesem Reflexivpronomen
versehene Ergänzung sich auf das Satzsubjekt bezieht. Hier die drei goldenen Regeln zur
Verwendung von „si“:

1. Es kommt nur in der dritten Person Einzahl und Mehrzahl vor: li, ŝi, ĝi, oni und ili sowie entsprechende Substantive
2. Es ist nicht Teil des Subjekts: Li kaj lia frato vizitas min; nicht sia, weil der frato Teil des Subjekts ist.
3. Es bezieht sich auf das Subjekt des Verbes: Mia edzo vidis, ke la teknikisto riparis sian aŭton - das aŭto gehört dem teknikisto, nicht dem edzo, da das Verb ripari und nicht vidi ist.

Ein Pronomen ist reflexiv beziehungsweise heißt Reflexivpronomen (rückbezügliches
Fürwort), wenn es sich innerhalb des Satzes auf dieselbe Person oder dasselbe Ding wie
das Subjekt bezieht ohne selbst Subjekt zu sein. Im Deutschen kann es streng genommen
nur in der dritten Person Reflexivpronomen geben, allerdings werden gerne auch die
Pronomen der ersten und zweiten Person als reflexiv bezeichnet (mich, dich, uns, euch).

Die Personalpronomen der ersten und zweiten Person mi, ni und vi werden als
Reflexivpronomen der ersten und zweiten Person verwendet. Dabei ist unmissverständlich
festgelegt, dass das Reflexivpronomen der ersten Person sich nur auf den/die Sprecher,
und das der zweiten Person sich nur auf die angesprochene(n) Person(en) beziehen kann.

|      |     |     |
| :------------- | :------------- | :------------- |
| Mi vidas min.         | - Ich sehe mich.           | Die erste Person Singular sieht und sie wird gesehen (von sich selbst).       |
| Vi diras al vi.       | - Du sprichst zu dir.      | Die zweite Person Singular spricht und wird angesprochen (von sich selbst).       |
| Ni amuzas nin.        | - Wir amüsieren uns.       | Die erste Person Plural amüsiert und wird amüsiert (von sich selbst).       |

Anmerkung: Ins Deutsche übersetzt wird aus dem Reflexivpronomen ein
Personalpronomen im Dativ, wenn eine Präposition das Pronomen einleitet.

Wenn ein Verb in der dritten Person steht (egal ob Einzahl oder Mehrzahl!), kann ein
Pronomen der dritten Person sich auf das Subjekt beziehen oder auch nicht. Zum Beispiel:
Er sieht einen Vogel nahe ihm – entweder ist der Vogel nahe dem Seher (Subjekt) oder
einer anderen Person. Wenn solch ein Pronomen der dritten Person sich auf das Subjekt
des Verbes bezieht, benutzt man im Esperanto ein spezielles reflexives Pronomen si
(Akkusativ sin), welches sich (selbst) bedeutet.

* Li amuzas sin. - Er amüsiert sich
* Arturo vidis birdon super si. - Arthur sah einen Vogel über sich.
* Ŝi trovas florojn apud si. - Sie findet Blumen neben sich.


* La tapiŝo havas diversajn kolorojn en si. - Der Teppich hat verschiedene Farben in sich.
* La birdo kaŝas sin sub la folioj. - Der Vogel versteckt sich unter den Blättern.
* La viroj havas seĝojn apud si. - Die Männer haben Stühle neben sich.
* La virinoj trovas florojn apud si. - Die Frauen finden Blumen neben sich.
* Sub si la infanoj trovis molan tapiŝon. - Unter sich fanden die Kinder einen weichen Teppich.

Da si sich immer auf das Subjekt des Verbes bezieht, kann es niemals selber Subjekt
oder Teil des Subjektes des Verbes sein. Klara kaj sia frato parolas kun Peter (Klara kaj
ŝia frato parolas kun Peter)
Die Reflexivpronomen der Verben (levi sin usw.) werden manchmal verkürzt mit Hilfe des
Suffixes -iĝ-: Li leviĝas.
So, das waren die beiden harten Nüsse der Esperanto-Grammatik. Keine Sorge: In der
Praxis üben sie sich schnell ein.
Nun wie in jeder Lektion noch etwas Wortbildung:

#### Wortbildung:

Suffixe:

* -iĝ- In Ergänzung zu den bisherigen Erläuterungen wird -iĝ- auch oft als Ersatzform
für das Passiv verwendet:
  * konstrui (bauen): La domo estas rekonstruata (re-konstru-at-a). - Das Haus wird
wieder aufgebaut. = La domo rekonstruiĝas.
* -ig- Suffix zur Bildung transitiver Verben; Grundbedeutung: „machen, veranlassen“
  * bela (schön) - beligi - schön machen, verschönen
  * preta (bereit) - pretigi (bereit machen, zubereiten)
* -id- (Nachkomme): bovo (Rind) - bovido (Kalb)
* -ebl- (Möglichkeit):
  * vidi (sehen) - videbla (sichtbar)
  * manĝi (essen) - manĝebla (essbar)
* -aĵ- dient auch zur Bildung von Speisen: porko (Schwein) - porkaĵo (Schweinefleisch)

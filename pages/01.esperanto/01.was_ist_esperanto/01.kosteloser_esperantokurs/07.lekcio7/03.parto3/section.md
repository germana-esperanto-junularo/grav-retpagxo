---
title: Übungen - Proverboj kaj anekdotoj
---


Suche aus dem Text (1.) alle Partizipien heraus, bestimme sie und übersetze die
betreffenden Sätze!

#### PROVERBOJ EN ESPERANTO

Eble vi miras, ke en planlingvo kiel Esperanto ekzistas proverboj? La patro de Zamenhof
kolektis ĉirkaŭ dumil sescent proverbojn ekzistantajn en pluraj lingvoj. Zamenhof, la kreinto
de Esperanto, tradukis ilin al Esperanto kaj publikigis ilin kiel „Proverbaron“. La kompleta
listo legeblas en la interreto. Jen dek ekzemploj.

1. Komenci de Adamo.
2. Venis fino de mia latino.
3. Du militas - tria profitas.
4. Laŭ la fruktoj oni arbon ekkonas.
5. Mizero havas talentan kapon.
6. Granda nubo - eta pluvo.
7. Eĉ malgranda muŝo ne estas sen buŝo.
8. Akvo trankvila estas akvo danĝera.
9. Mano manon lavas.
10. Saĝa hundo post la vundo.

#### TRI ANEKDOTOJ

1. Iam Fernando invitis siajn geamikojn por komuna vespermanĝo. La gastoj interalie babilis pri la demando de ĝentileco. „Ĉu vi scias, kial la invitintoj akompanas siajn gastojn ĝis la pordo?“ demandis Jan. „Mi scias“, diris Fernando, „por certigi sin, ke ili efektive foriris.“
2. La pola tradukisto kaj verkisto Tadeusz Boy-Żeleński sidis en kafejo. Venis al li kelnero kaj diris: „Sinjoro, ĉi tiu tablo estas rezervita!“ „Do, bonvolu forpreni ĝin“ trankvile diris la verkisto „kaj alportu al mi alian...“
3. La patrino koleras ĉar la eta Joĉjo pasigas la tutan tagon ludante per komputilo:
  * Joĉjo! Vi ne ludu per komputilo dum tiom belega tago!
  * Sed panjo, estas tiom da mojosaj retejoj kun ludoj, kaj....
  * Ĉesu! Vi nun iros eksterdomen!
  Ŝi permane kondukas Joĉjon ĝis la pordo, malfermas ĝin, kaj deklaras:
    * Jen: Eksterdome.com !

#### NEUE WÖRTER

* efektive - wirklich, tatsächlich
* forpreni - wegnehmen
* frukto - Frucht
* kelnero - Kellner
* kolekti - sammeln
* kolera - wütend
* komputilo - Computer, Rechner
* konduki - führen
* latino - Latein
* milito - Krieg
* mizero - Not, Elend
* mojosa - cool, lässig
* muŝo - Fliege
* nubo - Wolke
* panjo - Mutti
* patrino - Mutter
* planlingvo - Plansprache
* pluraj - mehrere
* proverbo - Sprichwort
* rezervi - reservieren
* saĝa - weise, klug
* verki - etw. verfassen
* verkisto - Schriftsteller
* vundo - Wunde, Verletzung


### KONTROLLÜBUNGEN FÜR DEINEN MENTOR

#### Ergänze die Wörter

In den Wörtern fehlen funktionale Silben wie -ig und -iĝ, Verb- und Partizipienendungen
sowie Wortanfänge für Possessivpronomen (mia, via, lia, sia usw.)

1. Mi surbret_____ la aĉet____libron. - Ich stelle das gekaufte Buch auf das Brett.
2. En la ven____ semajno ŝi edz____ (wörtlich: wird sie zur Ehefrau). - In der kommenden Woche heiratet sie
3. Pro troa lac____ Maria liber____ hodiaŭ de a ofico. - Wegen zu starker Ermüdung nahm Maria sich heute vom Dienst frei (wörtlich: befreite Maria sich ...).
4. Inform____ ____an patrinon, Frank iris en la kinejon. - Nachdem er ihre Mutter informiert hatte, ging Frank ins Kino.
5. Ating la landlimon, ni ĝustajn horloĝojn. - Bevor wir die Grenze erreichten, stellten wir unsere Uhren richtig (wörtlich: berichtigten).

#### Versuche, für folgende Wörter Übersetzungen oder Umschreibungen zu finden:

Wenn du für einen Ausdruck kein einzelnes Wort auf Deutsch findest, umschreibe es mit
einer Wortgruppe.

1. malsatantoj
2. kontraŭuloj
3. dormanto
4. ĝustigo
5. remalsekiĝi
6. malplimultigi
7. pendigi
8. pendigita
9. respeguliĝi
10. plilarĝigebla
11. nepagebla
12. retrankviliĝinte
13. surteriĝi
14. egaligi
15. krome
16. ebla
17. homaro
18. revulo
19. malordigi
20. ordigita


#### Finde für die Sprichwörter im Text 5.1 eine deutsche Entsprechung!

Wenn du für eines keine Entsprechung findest, übersetze es wörtlich.

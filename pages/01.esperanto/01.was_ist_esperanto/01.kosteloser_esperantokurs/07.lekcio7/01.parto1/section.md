---
title: Teksto - Kiel satigi manĝegemulon?
---

Klara invitis Peter al sia loĝejo. Peter frapas kontraŭ la pordo kaj Klara malfermas ĝin.

* Saluton, Klara!
* Saluton, Peter! Envenu en mian ĉambron kaj sidiĝu! Mi nur finskribas mian bildkarton al Maria

En la mezo de la ĉambreto staras ronda tablo, ĉe kiu Klara reekskribas (re-ek-skribas) la
bildkarton, sidiĝinte. Peter rompas la silenton.

* Mi volas inviti vin al tagmanĝo.
* Peter, lasu min fini tiun ĉi lastan karton, mi petas. Ni tuj povos interparoli.

Dum Klara skribas la lastajn vortojn, malsekigas la poŝtmarkon kaj premas ĝin per la
dekstra dikfingro sur la karton, Peter rigardas ŝian ĉambron. Ĝi estas eta, sed hela kaj
bele aranĝita. Sur la tablo kuŝas jam kelkaj multkoloraj (mult-kolor-a-j), plenskribitaj
bildkartoj, apude videblas diverskoloraj (divers-kolor-a-j) krajonoj. Sur la muro pendas
pluraj bildoj, spegulo, malnova horloĝo kaj larĝa bretaro kun amaso da libroj. Ĉe la alia
flanko de la ĉambro staras lito, apude tableto (tabl-et-o) portanta vazon kun rozoj. Fininte
la lastan salutkarton, Klara metas la skribilon sur la tablon kaj ree petas Peter sidiĝi.

* Do, ŝajnas, ke vi malsatas, respondas Klara al la demando starigita de Peter antaŭ kelkaj minutoj. Mi proponas resti hejme, ĉe mi. Ni preparos bongustan (bon-gust-an) manĝaĵon. Ĉu vi konsentas?
* Bone, kion ni manĝu? Ĉu unue kokan supon kaj poste kolbason kun acidbrasiko kaj terpomoj?
* Ne, Peter, mi estas vegetarano kaj manĝas nek viandon nek fiŝon. Mi proponas picon kun freŝaj tomatoj kaj fromaĝo kaj poste glaciaĵon. Ĉu bone?
* Jes, tio sonas bone. Sed ĉu tio sufiĉos? Mi vere malsatas!
* Ne zorgu, ĝi sufiĉos kaj ankaŭ la preparado ne tro longe daŭros. Sed prefere ne helpu kuiri, sed metu jam la manĝilaron sur la tablon.
* Ĉu forkojn, tranĉilojn kaj kulerojn?
* Jes, vi trovas ilin en la dekstra tirkesto en la kuirejo.


#### NEUE WÖRTER

Was heißen wohl: bildo, karto, sekundo, fingro, breto?

* acidbrasiko - Sauerkraut
* bongusta - lecker
* bretaro - Regal
* fini - beenden
* fiŝo - Fisch
* forko - Gabel
* frapi - klopfen
* freŝa - frisch
* glaciaĵo - Eis
* koko - Huhn
* kolbaso - Wurst
* koloro - Farbe
* krajono - Stift
* kuirejo - Küche
* kulero - Löffel
* kuŝi - liegen
* lasi - lassen
* lasta - letzter
* manĝilaro - Besteck („Schar der Essgeräte“)
* mezo - Mitte
* pendi - hängen
* rompi - brechen
* silento - Stille
* spegulo - Spiegel
* sufiĉi - ausreichen
* terpomo - Kartoffel
* tirkesto - Schublade („Zugkasten“)
* tranĉilo - Messer („Schneidegerät“)
* vegetarano - Vegetarier („Anhänger des Gemüses“)
* viando - Fleisch
* zorgi - sich Sorgen machen; für etw. sorgen

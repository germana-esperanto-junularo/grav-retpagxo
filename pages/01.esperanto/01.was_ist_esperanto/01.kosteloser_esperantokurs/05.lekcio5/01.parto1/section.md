---
title:  Teksto - Ĉu ankaŭ vi?
---

> Poprad, la 18-an de majo

> Kara Peter!

> Multajn salutojn el Slovakujo sendas al vi Klara. La tagoj ĉi tie pasas tre rapide. Tial nur
hodiaŭ mi sukcesas skribi al vi. Mi esperas, ke mia bildkarto atingos vin antaŭ la fino de la
kurso. Ĉi tie ĉio plaĉas al mi. Ankaŭ la amasloĝejo, en kiu loĝas mi kun dudek junuloj:
Apud mi dormas Maria el Polujo kaj Aurelie el Francujo. Aurelie estas la plej aĝa el ni, 27
jarojn ŝi aĝas. La knabinoj estas tre gajaj. Ankaŭ la vetero bonegas. La suno brilas, la
ĉielo estas blua, sed vespere estas malvarma aero. De mateno ĝis vespero ni
konversacias en Esperanto. Ĉar mi estas la sola germanino ĉi tie, mi ne povas paroli
germane. Proksime al la kursejo troviĝas naĝejo kaj mi hieraŭ enakviĝis (en-akv-iĝ-is).
Ĉirkaŭ la kursejo estas granda ĝardeno. Antaŭhieraŭ nia grupo lernis dum la tuta tago
ekstere, en tiu ĉi ĝardeno. Apud la ĝardeno troviĝas la tendejo. Cetere, estas en nia grupo
du polinoj, unu francino, tio estas Aurelie, du italinoj, tri rusoj, unu bulgaro, unu aŭstro, du
rumanoj kaj kvar slovakoj. Do, kun mi entute (en-tut-e) dek sep personoj. En la alia grupo
estas svisoj, hispano, svedo, finno kaj usonanino. Unu el la rusoj enamiĝis en la
usonaninon. Nun ili ĉiam kune sidas kaj flirtas unu kun la alia. Miaj gepatroj sendis al mi
jam retpoŝton. Ili skribis, ke ili jam atendegas mian revenon (re-ven-on). Ĉu ankaŭ vi?


> Kore salutas vin

> Klara


### NEUE WÖRTER

Zum Selberherausfinden: Slovakujo, Italujo, bulgaro, teksto.

* aero - Luft
* aĝo - Alter
* akvo - Wasser
* aŭstro - Österreicher
* brili - scheinen, glänzen
* ĉiam - immer
* ĉirkaŭ - um ... herum
* eĉ - sogar
* ekscii (ek-sci-i) - erfahren
* enamiĝi en - sich verlieben in
* entute - insgesamt
* esperi - hoffen
* fermi - schließen
* finno - Finne
* flirti - flirten
* Francujo - Frankreich
* franco - Franzose
* ĝardeno - Garten
* gepatroj - Eltern
* germano - Deutscher
* hispano - Spanier
* instrui - lehren, unterrichten
* itala - italienisch
* italo - Italiener
* klaso - Klasse
* kompreni - verstehen
* konversacii - sich unterhalten
* kore - herzlich
* koro - Herz
* lago - See
* mateno - Morgen
* miri - sich wundern, staunen
* multa - viel
* parolado - Rede
* pasi - vorbeigehen
* Polujo - Polen
* polo - Pole (Einwohner Polens)
* por ke - so dass, damit
* proksime al - in der Nähe von
* rapide - schnell
* rumano - Rumäne
* ruso - Russe
* scii - wissen
* sidadi (sid-ad-i) lange sitzen, herumsitzen
* sukcesi - (es) schaffen, Erfolg haben, gelingen
* sukceso - Erfolg
* suno - Sonne
* svedo - Schwede
* sviso - Schweizer
* tendejo - Zeltplatz
* tial - darum
* unu el - einer von
* unu kun la alia - miteinander
* usonano - US-Amerikaner
* Usono - USA
* vespere - abends, am Abend
* vetero - Wetter

Redewendungen:

* la plej aĝa - der/die älteste
* la suno brilas - die Sonne scheint
* de mateno ĝis verspero - von morgens bis abends
* iomete - ein wenig
* kore salutas - (es) grüßt herzlich
* multajn salutojn - viele Grüße

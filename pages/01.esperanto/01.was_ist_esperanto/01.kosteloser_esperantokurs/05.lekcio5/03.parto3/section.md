---
title: Übungen - La programo de la renkontiĝo
---

### Übersetze!

1. übereinander, nebeneinander, beieinander
2. Worüber hat er gesprochen?
3. Ich weiß nicht worüber er gesprochen hat.
4. Mit welchem Stift hat sie den Brief geschrieben?
5. Das ist der Stift, mit dem sie den Brief geschrieben hat.
6. Hier ist das Buch, das ich gestern gelesen habe.
7. Weißt du, was er getan hat?
8. Sind das die Kinder, die du vorgestern getroffen hast?

### Bilde neue Wörter!

Bilde mit Hilfe der Wortbildungssilben, die du bisher gelernt hast, aus den folgenden
Wörtern neue und übersetze sie anschließend:
rumano, sviso, svedo, hispano, edzo, amiko, loĝi, ĉambro, gazeto, Berlino.

### La programo de la renkontiĝo

Jen vi vidas la programon de la renkontiĝo, en kiu Klara partoprenas:

| tempo     | Vendredo     | Sabato     | Dimanĉo     | Lundo     | Mardo     |
| :------------- | :------------- | :------------- | :------------- | :------------- | :------------- |
| 8-9h       | Alvenado       | Matenmanĝo     | Matenmanĝo     | Matenmanĝo     | Matenmanĝo     |
| 9 - 11 h   | ...            | Instruado      | Instruado      | Instruado      | Instruado      |
| 11 - 12 h  | ...            | Kafopaŭzo      | Kafopaŭzo      | Kafopaŭzo      | Kafopaŭzo      |
| 12 - 13 h  | ...            | Instruado      | Instruado      | Instruado      | Instruado      |
| 13 - 14 h  | Alvenado       | Tagmanĝo       | Tagmanĝo       | Tagmanĝo       | Tagmanĝo       |
| 14 - 15 h  | ...            | Item Two       | Item Two       | Historio de Esperanto | La Esperanto-movado |
| 15 - 16 h  | ...            | Item Two       | Item Two       | Esperanta literaturo  | Instruado de Salsa  |
| 16 - 17 h  | Alvenado       | Item Two       | Item Two       | La vivo de Zamenhof   | Improviza teatro    |
| 17 - 18 h  | Item Two       | Item Two       | Item Two       | Item Two       | Historio de Slovakujo      |
| 19 - 21 h  | Vespermanĝo    | Vespermanĝo    | Vespermanĝo    | Vespermanĝo    | Vespermanĝo                |
| 21 - 22 h  | Item Two       | Item Two       | Item Two       | Item Two       | Internacia vespero         |
| 22 h – …   | Interkona vespero | Diskejo | Filmo | Karaokeo | Dancado de Salsa |

Bicikla ekskurso
Japanajmanla boroj
Lingva testo
Kurso de la slovaka lingvo
Koktelista mini-kurso
Oficiala malfermo
Popola muziko
Koncerto
Teatraĵo
Merkredo
Kafopaŭzo
Naĝado en urba naĝejo
Promeno tra Poprad
Ekskurso al Bratislava
Rezultoj de la ekzameno
Fina ekzameno
Adiaŭo oficiala
Sportaj konkursoj
Forirado
Diskutrondo
Vingustumado
Koncerto
Diskejo
Vespermanĝo
Adiaŭa vespero

La lingvoinstruado komenciĝas je la naŭa horo. Tagmeze estas paŭzo de unu horo. La
posttagmeza (post-tag-mez-a) programo komenciĝas je la dekkvara kaj finiĝas je la
dekoka horo. Ĉiuvespere okazas kultura programo: Vi povos danci, kanti kaj tiel plu. Jaŭde
estas granda, tuttaga (tut-tag-a) ekskurso al Bratislava.

Krom tio kutime dum ĉiuj renkontiĝoj ekzistas:

* Libroservo, kie vi povas aĉeti librojn kaj kompaktdiskojn.
* Gufujo, trankvila loko sen alkoholo, kie vi povas trinki teon, manĝi keksojn kaj babili.
* Trinkejo, kie vi povas trinki ion aŭ drinki. Klarigo pri la vortoj: „Trinki“ oni uzas transitive por trinkaĵoj kun aŭ sen alkoholo, ekz. „mi trinkas akvon“. „Drinki“ oni uzas maltransitivie nur por alkoholaĵoj, ekz. „mi neniam drinkas“. En la trinkejo ofte ankaŭ estas bona muziko.
* Ludejo, ĉar multaj esperantistoj ŝatas tablo-ludojn.

### NEUE WÖRTER

Die folgenden Wörter kannst du dir bestimmt selbst herleiten: oficiala, testo, paŭzo, japana, teatro, karaokeo, improviza, koncerto, sporto, kekso, alkoholo, ofte

* babili - plaudern
* danci - tanzen
* diskejo - Disco
* ekskurso - Ausflug
* ekzameno - Prüfung
* gustumi - kosten
* interkona vespero - Kennenlernabend
* kie - wo
* koktelisto - Barkeeper, Barmann/-frau
* kompaktdisko - CD
* krom - außer
* kutime - gewöhnlich
* L.L. Zamenhof - Name des EsperantoErfinders
* labori - arbeiten
* ludi - spielen
* matenmanĝo - Frühstück
* movado - Bewegung
* popolo - Volk
* promeni - spazieren gehen
* rezulto - Ergebnis
* rondo - Runde
* ŝati - mögen
* tablo - Tisch
* tagmanĝo - Mittagessen
* teatraĵo - Theaterstück
* trankvila - ruhig
* uzi - benutzen
* vespero - Abend
* vingustumado - Weinverkostung

Vielleicht fragst du dich, welche Musik auf den Treffen gespielt wird? Mehr dazu erfährst
du im zweiten Text von Lektion 8.

Ein Treffen, das dem von Klara besuchten sehr ähnelt, ist übrigens die „Somera
Esperanto-studado“ (SES), die jedes Jahr im Juli stattfindet. Mehr Infos dazu unter
http://de.lernu.net/ses/ Für viele Treffen können DEJ-Mitglieder auch
Reisekostenunterstützung erhalten. Mehr dazu erfährst du auf den Internetseiten der
Deutschen Esperanto-Jugend – www.esperanto.de/dej.

### Lösungen zu 4.

1. unu super la alia, unu apud la alia, unu ĉe la alia
2. Pri kio li parolis?
3. Mi ne scias, pri kio li parolis.
4. Per kiu skribilo ŝi skribis la leteron?
5. Jen la skribilo, per kiu ŝi skribis la leteron.
6. Jen la libro, kiun mi legis hieraŭ.
7. Ĉu vi scias, kion li faris?
8. Ĉu tiuj estas la infanoj, kiujn vi renkontis antaŭhieraŭ?

### KONTROLLÜBUNGEN FÜR DEINEN MENTOR

Übersetze sinngemäß folgende zum Teil zusammengesetzte Wörter:

1. plimultiĝi
2. plirapidiĝi
3. vesperiĝas
4. mateniĝis
5. proksimiĝi
6. enlagiĝi
7. elakviĝi
8. multegaj
9. la kialo
10. pollingve
11. malpli
12. sunbrilo
13. ĉiama
14. remalfermo
15. instruado
16. la malkompreno
17. sunleviĝo
18. malleviĝo
19. studado
20. gratulanto
21. la plimultiĝo
22. restaĵo
23. kundancado
24. domano

### Sage sinngemäß,

* dass du am Dienstag angekommen bist,
* dass du noch Geschwister hast, die älter sind als Du,
* dass du an diesem Kurs teilnimmst, da du endlich Esperanto nicht nur lesen und schreiben, sondern auch sprechen willst,
* dass du im letzten Monat durch Frankreich und Italien gefahren bist,
* dass du beim Kennenlernabend einen netten Polen kennen gelernt hast,
* dass dir der Ausflug ins Schwimmbad und das Konzert sehr gefallen hat,
* dass du wieder auf ein Esperanto-Treffen fahren möchtest.

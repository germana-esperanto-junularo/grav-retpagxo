---
title: Teksto - Grava momento
---


Wahrscheinlich haben die anderen Menschen in deiner Stadt oder deinem Ort, die
Esperanto sprechen, neben ihren internationalen Kontakten wenig regelmäßige
Verbindungen untereinander. Oder es gibt einige junge Leute in etwa deinem Alter, die
sich ab und zu locker treffen. Vielleicht aber wohnst Du auch in einer Stadt, wo es einen
richtigen traditionellen Esperanto-Klub mit „Veteranen“ gibt, die die internationale Sprache
schon vor Jahrzehnten gelernt haben. Der folgende Text schildert in etwas ironischen
Worten, wie Peter zum ersten Mal den sehr traditionsbewussten Esperanto-Klub seiner
Stadt besucht.

### Grava momento

* Kara Peter, ni ĝojas, ke vi membriĝis en nia Esperanto-klubo. Verŝajne vi jam aŭdis de Klara, ke ni estas unu el la plej famaj kluboj de la lando, fondita antaŭ pli ol sep jardekoj (jar-dek-oj). Jam mia praavo staris antaŭ tiu ĉi nigra tabulo, kun blanka kreto en la mano, instruante Esperanton. Mi memoras la tagon, kiam li kun entuziasmo gvidis la klubon kaj agis por disvastigi Esperanton en nia urbo kaj la ĉirkaŭaj vilaĝoj. Bedaŭrinde li nun estas malsana kaj jam ne povas veni.

Klara turnas sin al Peter kaj flustras en lian orelon:

* Iam ni estis preskaŭ familia klubo, ĉar membris krom lia praavo eĉ lia bofrato kaj lia
eksedzino.

Ridetante la klubestro daŭrigas:* Mi scias, ke Klara nun fiflustris pri mia familio. Sed, karaj, vi ja scias, ke la persona
agado de unuopuloj ĉiam ludis gravan rolon en Esperantujo.

* Mi memoras la tempon, kiam ni sidis ĉi tie nur kvarope aŭ kvinope, sed dank' al la tiama fidela sindonemo de la membroj, al ilia mirinda entuziasmo, ni altiris tiun grandan nombron da homoj interesiĝantaj pri Esperanto kaj uzantaj nian lingvon dum vojaĝoj kaj en la korespondado kun landoj en ĉiuj anguloj de la mondo: en nordo, sudo, oriento kaj okcidento. Kiam vi envenis nian ĉambron, Peter, vi preteriris ŝrankon kun valoraj objektoj el nia kluba vivo, kaj super la ŝranko pendas malnova foto de doktoro Zamenhof. Tiujn ĉi valoraĵojn apartenantajn al la klubo ni montros al vi, poste. Nun, Peter, mi elkore bonvenigas vin en nia rondo kaj transdonas al vi tiun ĉi insignon: verdan stelon kun la litero 'E' en la mezo. Ĝi estas la signo de nia esperantista komuneco. Neniam forĵetu ĝin, ĉar laŭ ĝi, oni rekonas vin en la tuta mondo kiel esperantiston. Ĝoju fariĝi membro de amikeca internacia societo. Karaj samideanoj, levu la glasojn kaj trinku je la sano de nia novalveninto (nov-al-ven-into).

Klara prenas sian vinglason dirante al Peter:

* Li estas lerta parolanto kaj granda idealisto. Liaj sukcesoj montras, ke li ofte pravas. Kaj ĝuste pro tio ni reelektis lin kiel nian estron.

2. NEUE WÖRTER

Erschließe Dir: momento, entuziasmo, rolo, korespondi, nordo, sudo, ŝranko, objekto, doktoro, idealo.

* agi - handeln
* bofrato - Schwager
* ĉefo - Leiter, Chef
* dank' al - dank (+Dativ)
* daŭrigi - fortsetzen, fortfahren, weitermachen
* disvastigi - verbreiten
* eksedzino - geschiedene Frau, Ex-Frau
* elekti - wählen
* Esperantujo - Welt, in der Esperanto gesprochen wird (wörtlich: Esperantoland)
* estro - Leiter, Chef
* fidela - treu
* fiflustri - verächtlich flüstern
* flustri - flüstern
* fondi - gründen
* glaso - Glas
* ĝoji - sich freuen
* gvidi - leiten
* insigno - Abzeichen
* interesiĝi pri - sich interessieren für
* jam ne - nicht mehr
* jardeko - Jahrzehnt
* ĵeti - werfen
* klubestro - Klubleiter, Klubchef
* komuneco - Gemeinsamkeit
* kreto - Kreide
* kvarope - zu viert
* kvinope - zu fünft
* lerta - geschickt
* litero - Buchstabe
* membro - Mitglied
* memori - sich erinnern (an)
* mirinda - bewundernswert
* jam ne - (ab jetzt) nicht mehr
* Levu la glasojn! - Erhebt die Gläser!
* montri - zeigen
* nombro - Anzahl
* okcidento - Westen
* orelo - Ohr
* oriento - Osten
* praavo - Urgroßvater
* pravi - recht haben
* preter - vorbei (an)
* rideti - lächeln
* ridi - lachen
* samideano (sam-ide-an-o) - Anhänger der gleichen Idee (so sprechen sich sehr traditionsbewusste EsperantoSprecher untereinander an)
* sana - gesund
* signo - Zeichen
* sindonemo (sin-don-em-o) - Hingabe
* societo - Gesellschaft (Freundeskreis u.ä.)
* socio - Gesellschaft (eines Landes)
* stelo - Stern
* super - über
* tabulo - Tafel
* tiri - ziehen
* transdoni - Übergeben, überreichen
* unuopulo - einzelner
* uzi - nutzen
* valora - wertvoll
* vasta - weit, breit
* Zamenhof Ludoviko Lazaro Zamenhof (geb. 1859 in Bialystok, heute Polen, gest. 1917 in Warschau; jüdischer Augenarzt, Autor des Esperanto)

**Redewendungen**:

* Je via sano!
* Auf euer (dein, Ihr) Wohl!

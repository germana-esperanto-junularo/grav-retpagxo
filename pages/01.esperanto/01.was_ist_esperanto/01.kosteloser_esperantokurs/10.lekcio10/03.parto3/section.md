---
title: Übungen - La Esperanto mondo
---

####  Übersetze folgende Sätze. Unterscheide dabei die drei

Übersetzungsmöglichkeiten für das deutsche „über“: pri, super, trans

1. ein Buch über Berlin schreiben
2. über die Straße gehen
3. über dem Baum fliegen
4. über den Baum (hinweg-)fliegen
5. das Bild über das Schränkchen hängen (pendigi)
6. über seine Schwester sprechen

####  Übersetze die folgenden Sätze:

1. Das Haus wird im nächsten Jahr gebaut werden.
2. Das Zimmer ist nicht bewohnt.
3. Hier ist das versprochene Buch.
4. Die Tür wird erst in einer Stunde geöffnet werden.
5. Wird das Essen schon vorbereitet?
6. Wo ist der von Peter geschriebene Brief?

####  Nachsilben zur Bildung von Personen. Setze die richtigen Endungen ein!

-ulo ist Träger einer Eigenschaft, -isto macht etwas sehr häufig oder professionell
-anto macht gerade etwas, -ano ist Anhänger oder Mitglied von etwas

1. La skrib______ de la letero ne estas mia samland_______ nek samlingv________.
2. Ŝi estas simpatia jun______ el Burundujo, kiu lernas por esti fleg_______.
3. En Svedujon ŝi venis, ĉar ŝi dum milito estis disigita de siaj famili_______.
4. Ŝi sentis sin kiel fremd_______ en Svedujo, konante nek la landon, nek la lingvon.
5. Inter la dom______, ŝi havas multajn amikojn.

####  Unterstreiche alle Wortwurzeln im ersten Text, die auf -aŭ enden, und übersetze sie.

#### LA ESPERANTO-MONDO

Fine, ni parolu iomete pri la Esperanto-movado:

La UNIVERSALA ESPERANTO-ASOCIO (UEA) estas la internacia organizo por ĉiuj
Esperanto-parolantoj. Ĝiaj celoj estas:

a) disvastigi la uzadon de la internacia lingvo Esperanto
b) agadi por la solvo de la lingva problemo en internaciaj rilatoj kaj faciligi la internacian komunikadon;
c) plifaciligi ĉiujn rilatojn inter homoj, malgraŭ diferenco de nacieco, raso, sekso, religio, politiko aŭ lingvo;
ĉ) kreskigi inter siaj membroj fortikan senton de solidaro, kaj disvolvi ĉe ili la komprenon kaj estimon por aliaj popoloj.

La revuo „Esperanto“ estas la monata oficiala organo de UEA. Ĉiu
numero enhavas raportojn pri okazintaĵoj, artikolojn pri la movado
kaj rilataj temoj, resumojn kaj recenzojn pri laste aperintaj verkoj en
(aŭ pri) Esperanto, kaj multajn utilajn informojn por movadaj
aktivuloj.

La „Jarlibro“ estas ĉiujara manlibro pri la Esperanto-movado. Ĝi
vaste informas pri ties organiza strukturo kaj disponigas pli ol du mil
adresojn de la delegitoj de UEA tra la tuta mondo, kiuj respondos al
viaj demandoj pri fakaj temoj aŭ pri siaj hejmlandoj. Ĉio ĉi hodiaŭ
ankaŭ estas por membroj legebla en la interreto.

Esperanto ekzistas por internaciaj kontaktoj, kaj la plej ideala
aranĝo por tiu celo estas la Esperanto-kongresoj. Krom sennombraj amikiĝoj
kaj interparoloj en stimula etoso, okazas interesaj prelegoj, seminarioj, teatraĵoj
kaj koncertoj.

La libroservo de UEA vendas praktike ĉiujn Esperantajn librojn, gazetojn,
diskojn, kasedojn kaj aliajn varojn, kiuj estas akireblaj en la merkato. El
Germanio, Aŭstrio kaj Svisio estas tre facile mendi tie, sed ekzistas ankaŭ pliaj Esperantolibroservoj en tiuj landoj, pri kiuj via landa asocio certe scias.

Landaj Esperanto-asocioj ekzistas en pli ol sepdek landoj: Tiuj en
Germanio, Aŭstrio kaj Svisio estas inter la plej tradiciaj. La landaj
Esperanto-asocioj faras kongresojn kaj kursojn, eldonas gazetojn kaj informas pri
Esperanto al interesitoj, sed ankaŭ al gazetoj, radio- kaj televido-stacioj.

La TUTMONDA ESPERANTISTA JUNULARA ORGANIZO (TEJO) estas la
internacia organizo de la junaj esperantistoj, de ĉiuj, kiuj aĝas malpli ol 30
jarojn. Se vi estas juna, estas speciale interese paroli Esperanton, ĉar facile
eblas vojaĝi, ne nur tra Eŭropo, kaj ekkoni novajn interesajn homojn.

Bona okazo por tio estas ankaŭ la „Internaciaj Junularaj Kongresoj“, kiuj
okazas dum ĉiu somero en alia lando. Vintre en la semajno ĉirkaŭ silvestro
okazas la „Junulara E-Semajno“, organizita de la Germana kaj Pola
Esperanto-Junularoj. Krom tiuj plej grandaj junularaj renkontiĝoj ekzistas
sennombraj aliaj renkontiĝoj por junaj Esperanto-parolantoj en ĉiuj landoj, la
plej multaj dum pasko, pentekosto kaj la someraj monatoj - estas plej rekomendinde lerni
Esperanton tie!

Alia bona servo de TEJO estas la „Pasporta Servo“: interreta servo, kiu listigas la adresojn
de ĉirkaŭ mil personoj, kiuj invitas Esperanto-parolantojn senkoste tranokti ĉe ili. Se vi
vojaĝas tra Eŭropo (aŭ pli malproksimen) kaj ne volas paroli nur kun turistoj, vi povas tre
profiti de tiu servo! Vi povas informiĝi pri ĝi je www.pasporta-servo.net .

#### NEUE WÖRTER

Die folgenden Wörter kannst Du sicher selbst erraten:
problemo, komunikado, diferenco, raso, sekso, religio, politiko, solidaro, oficiala, organo,
raporto, artikolo, temo, resumo, recenzo, aktiva, strukturo, faka, stimula, seminario, teatro,
koncerto, kasedo, varo, silvestro, listo, turisto, profiti, kontakti, simpatia.

* akiri - erwerben
* asocio - Verband, Organisation
* celo - Ziel
* delegito - Delegierter
* disko - Platte; Schallplatte
* disponi (pri) - verfügen (über)
* disponigi - zur Verfügung stellen
* disvolvi - entwickeln
* enhavi (en-havi) - beinhalten
* estimi - achten, ehren
* etoso - Stimmung
* faka - fachlich
* fortika - kräftig, stark, fest
* hejmlando - Heimatland
* kunsido - Sitzung, Tagung
* merkato - Markt
* nacieco - Nationalität
* okazintaĵo - Ereignis (vergangenes)
* okazo - Gelegenheit
* organizo - Verband, Organisation
* pliaj (pli-aj) - weitere
* prelego - Vortrag
* rekomendi - empfehlen
* rekomendinde - empfehlenswert
* rilata - diesbezüglich, angrenzend
* sen-... - ...-los (z.B.: sennombra - zahllos)
* solvo - Lösung
* tradicia - traditionsreich
* tranokti - Übernachten
* tutmonda - weltweit

Lösungen zu 4.

4.1

1. skribi libron pri Berlin
3. flugi super la arbo
2. iri trans la strato
4. flugi trans la arbo
5. pendigi la bildon super la ŝrankon
6. paroli pri sia fratino

4.2

1. La domo estos konstruata sekvontjare.
2. Neniu loĝas en la ĉambro.
3. Jen la promesita libro.
4. La pordo estos malfermata nur post horo.
5. Ĉu la manĝaĵo jam estas preparata?
6. Kie estas la letero skribita de Peter?

4.3

1. La skribanto de la letero ne estas mia samlandano nek samlingvano.
2. Ŝi estas simpatia junul(in)o el Burundujo, kiu lernas por esti flegist(in)o.
3. En Svedujon ŝi venis, ĉar ŝi dum milito estis disigita de siaj familianoj.
4. Ŝi sentis sin kiel fremdulo en Svedujo, konante nek la landon, nek la lingvon.
5. Inter la domanoj de ŝia loĝejo, ŝi havas multajn amikojn.

4.4
adiaŭ – tschüss, almenaŭ – wenigstens, ambaŭ – beide, ankaŭ – auch, ankoraŭ – noch, anstataŭ – statt, antaŭ – vor,
apenaŭ – fast nicht, baldaŭ – bald, ĉirkaŭ – ungefähr, hieraŭ – gestern, hodiaŭ – heute, kontraŭ – gegen, kvazaŭ – als ob, morgaŭ – morgen, preskaŭ – fast

### KONTROLLÜBUNG FÜR DEINEN MENTOR

####  Übersetze die letzten drei Absätze des Textes 5 – möglichst nah am Original.

####  Finde unter Verwendung der unter 3.6 neu gelernten Grammatik den

Bedeutungsunterschied zwischen den beiden Sätzen. Trage dein Ergebnis in
die Tabelle ein.

1. La aŭto veturis po dek kilometrojn en kvin horoj.
2. La aŭto veturis dek kilometrojn en po kvin horoj.

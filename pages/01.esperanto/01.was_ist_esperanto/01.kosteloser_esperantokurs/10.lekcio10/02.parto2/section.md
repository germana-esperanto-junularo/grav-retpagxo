---
title: "Grammatik"
---

####  Die Partizipien:

* Aktiv Präsens: -ant (leganta - lesend)
* Aktiv Präteritum: -int (leginta - der lesend war
* Aktiv Futur: -ont (legonta - der lesend sein wird)
* Passiv Präsens: -at (legata - das jetzt gelesene)
* Passiv Präteritum: -it (legita - das zu Ende gelesene)
* Passiv Futur: -ot (legota - das gelesen werdende)

Beispiele:

* la leganta knabo - der lesende Junge
* la legita libro - das gelesene Buch
* Li estas legonta (= Li legos.) - Er wird lesend sein. (Er wird lesen.)

####  Das Passiv der Verben

Das Passiv der Verben wird durch das Hilfsverb esti + Passivpartizip des Vollverbs
ausgedrückt. Dabei geben die Partizipialsuffixe Auskunft über die Vollendung der
beschriebenen Handlung:

* -it- Handlung ist vollendet
* -at- Handlung ist nicht vollendet
* -ot- Handlung steht bevor

Beispiele:

* La libro estas legita. - Das Buch ist gelesen (worden).
* La libro estas legata. - Das Buch wird gelesen.
* La libro estas legota. - Das Buch wird gelesen werden.
* La libro estis legita. - Das Buch war gelesen (worden).
* La libro estis legata. - Das Buch wurde (gerade) gelesen.
* La libro estis legota. - Das Buch sollte gelesen werden.
* La libro estos legita. - Das Buch wird gelesen (worden) sein.
* La libro estos legata. - Das Buch wird (gerade) gelesen werden.
* La libro estos legoto. - Das Buch wird (später) gelesen werden.
* La libro estus legita. - Das Buch wäre gelesen worden.
* La libro estus legata. - Das Buch würde gelesen.
* La libro estus legota. - Das Buch würde (später) gelesen werden.

Der Handlungsträger kann dabei mit der Präposition „de“ beschrieben werden:

> La libro estas legita de via frato. - Das Buch ist von deinem Bruder gelesen worden.

Wenn Zweideutigkeit ausgeschlossen ist, kann das Suffix -iĝ- zur Bildung eines passiven
Satzes genutzt werden. Hierbei entfällt das Hilfsverb:

* La problemo solviĝis. (= La problemo estas solvita.) - Das Problem ist gelöst.
* La problemo solviĝas. (= La problemo estas solvata.) - Das Problem wird (gerade) gelöst.

####  Die Endung -n (an Adjektiven, Substantiven, Pronomen, Adverbien)

Die Endung -n (an Adjektiven, Substantiven, Pronomen, Adverbien) wird benutzt:

* zur Kennzeichnung des Akkusativs (wen-Fall): Mi skribas longan leteron.
* bei allen zeitlichen sowie Maßangaben, sofern sie nicht durch eine Präposition eingeleitet werden oder selbst Satzsubjekt sind:
* Ŝi havas du jarojn.
* La trian de oktobro li venos.
* La pano pezas du kilogramojn.
* La turo estas dudek metrojn alta.
* Aber:
  * Du jaroj estas longa tempo. (Subjekt)
  * Li legis la libron dum tri monatoj. (Präposition)
  * Je la tria de oktobro ili venos. (Präposition)
  * La turo estas alta je dudek metroj. (Präprosition)
* zur Angabe einer Richtung:
  * ohne Präposition:
    * Mi iras tien.
    * Ili venis hejmen.
    * Ŝi veturas Parizon.
  * mit Präposition:
    * Li iras en la ĉambron.
  * ohne Präposition, aber mit zusammengesetztem Verb (Präposition als Vorsilbe):
    * Peter eliras la ĉambron.
    * Ili priparolas la aferon.

#### Alle Präpositionen mit wichtigen Merkmalen:

* al: (hin) zu, an immer ohne nachfolgenden Akkusativ;
* anstataŭ: statt, anstelle
* malantaŭ vi - hinter dir
* malantaŭ vin - hinter dich
* apud: neben
* ĉirkaŭ la domo - rund um das Haus
* ĉirkaŭ dumil - ungefähr zweitausend
* da (Mengenpräp.): steht zwischen Mengenangabe und deren Bezugswort:
* unu litro da vino - ein Liter Wein
* de: von; durch; zusammengesetzte Wörter
* Li venas de la domo - Er kommt vom Haus
* Tion mi eksciis de li. - Das erfuhr ich durch ihn.
* dum: während
* ĉe la tablo - am Tisch, beim Tisch
* ĉirkaŭ um ... herum, ungefähr:
* apud la domo - neben dem Haus
* ĉe: bei, an
* antaŭ unu jaro - vor einem Jahr
* malantaŭ: hinter nur örtlich;
* anstataŭ vi - an deiner Stelle
* antaŭ: vor (örtlich und zeitlich);
* al la tabulo - an die Tafel
* dum tri tagoj - während dreier Tage (drei Tage lang)
* ekster: außerhalb von, aus... hinaus (mit Akk.)
* ekster la domo - außerhalb des Hauses
* ekster la domon - aus dem Haus hinaus (Richtung)
* el: aus
* el ligno - aus Holz
* el la ĝardeno - aus dem Garten
* en: in (Ort oder Richtung)
* en la domo - im Haus
* en la domon - ins Haus
* ĝis: bis (zeitich und örtlich, immer ohne Akkusativ)
* ĝis morgaŭ - bis morgen
* ĝis la fino - bis zum Ende
* inter: zwischen (Ort oder Richtung)
* inter la seĝoj - zwischen den Stühlen
* inter la seĝojn - zwischen die Stühle
* je (ohne feste Bedeutung)
* je la sepa horo - um sieben Uhr
* je via sano - auf dein Wohl
* kontraŭ: gegen
* krom: außer
* laŭ tiu ĉi vojo - diesen Weg entlang
* laŭ via peto - entsprechend deiner Bitte
* malgraŭ: trotz
* paroli pri ŝi - über sie sprechen
* pro: wegen
* preter la domo - am Haus vorbei
* pri: über (nicht örtlich);
* post du horoj - nach zwei Stunden
* preter: an ... vorbei
* por vi - für dich
* post: nach, hinter (zeitlich, selten örtlich)
* Donu al ĉiu knabo po du pomojn! - Gib jedem Jungen je zwei Äpfel!
* por: für
* skribi per krajono - mit Bleistift schreiben
* po: je(weils)
* malgraŭ mia peto - trotz meiner Bitte
* per: mit (Hilfe von)
* kun vi - mit dir
* laŭ: entlang, gemäß, entsprechend
* neniu krom mi – niemand außer mir
* kun: (gemeinsam) mit
* kontraŭ la malamiko - gegen den Feind
* pro ili - ihretwegen
* sen: ohne
* sen ŝi - ohne sie
* sub: unter
* sub la tablo - unter dem Tisch (Ort)
* sub la tablon - unter den Tisch (Richtung)
* super: über
* super la nuboj - über den Wolken (Ort)
* super la nubojn - über die Wolken (Richtung)
* sur: auf
* sur la tablo - auf dem Tisch
* sur la tablon - auf den Tisch
* tra: durch
* Ili promenas tra la parko. - Sie spazieren im Park (durch den Park, kreuz und quer).
* Ili iras tra la parkon. - Sie gehen durch den Park (hindurch)
* trans: über
* trans la strato - auf der anderen Seite der Strasse
* trans la straton - auf die andere Straßenseite (= über die Straße)

####  Affixe der Lektionen 7-9

a) Präfixe:
  * pra- (Ur-): praavo - Urgrossvater
  * dis- (auseinander): disiri - auseinandergehen
  * bo- (Verwandschaft durch Heirat): bofilino - Schwiegertochter
  * eks- (Ex-): eksoficiro - Offizier a.D.
  * fi- (moralisch abwertend): fiago - Gemeinheit
  * mis- (miss-): miskompreno - Missverständnis
b) Suffixe:
  * -iĝ- (auch zur Bildung des Passivs verwendbar): ĝi konstruiĝas - es wird aufgebaut
  * -ig- (machen, veranlassen): heligi - aufhellen, hell machen
  * -id- (Nachkomme): hundido - Welpe
  * -ebl- (Möglichkeit): legebla - lesbar
  * -aĵ- (auch zur Bildung von Speisen): fiŝaĵo - Fischgericht
  * -ĉj- (männliche Kosenamen): avĉjo - Opa, Opi
  * -nj- (weibliche Kosenamen): avinjo - Oma, Omi
  * -ind- (-würdig): aminda - liebenswert, liebenswürdig
  * -estr- (Leiter): grupestro - Gruppenleiter
  * -op- (zu wievielt): triope - zu dritt
  * -em- (Neigung, Lust):
  * malpaciĝema (mal-pac-iĝ-em-a) - streitsüchtig
  * -ec- (Eigenschaft): ĝentileco - Höflichkeit
  * -ism- (-ismus): kristanismo (krist-an-ism-o) - Christentum
  * -obl- (-fach): trioble - dreifach
  * -on- (Bruchteil): triono - Drittel
  * -um- (ohne feste Bedeutung): amindumi (am-ind-um-i) - flirten
  * -aĉ- (schlechte Form): hundaĉo - Köter
  * -end- (noch zu ...): lengenda libro - zu lesendes Buch
  * -er- (Bestandteil): sablero - Sandkorn
  * -ing- (Halter): kandelingo - Kerzenhalter
  * -uj- (Behältnisse, Ländernamen und Baumnamen): panujo - Brotkorb; pomujo Apfelbaum (besser: pomarbo)

####  Das Wörtchen po

Das Wort po drückt eine Verteilung von Mengen aus. Nach po muss immer eine
Mengenangabe kommen. Manchmal wird ein Zahlwort auch stillschweigend
angenommen, z.B. po leciono = po unu leciono. Po drückt einen Anteil von etwas aus, was
gleichmäßig über eine Anzahl von Personen, Dingen, Orte usw. verteilt ist.

* La gastoj trinkis po unu glason – Die Gäste tranken jeweils ein Glas
* Achtung: po heißt nicht „pro“ oder „je“: Tiu ŝtofo kostas dek eŭrojn po metro .
* Richtig: Tiu ŝtofo kostas po dek eŭrojn por metro

Wir sehen: Während im Deutschen nur ein Wort verwendet wird, um eine Verteilung
auszudrücken, nämlich „je“, werden im Esperanto dafür zwei Wörter verwendet, po und
(meistens, aber nicht ausschließlich) por.

Man kann po auch für Brüche nehmen:

* 20 km/h – po 20 kilometroj en/por horo
* 3 €/kg – po 3 eŭroj por kilogramo
* Und man kann an po auch Adverbien anschließen:
* 20 km/h – po 20 kilometroj hore

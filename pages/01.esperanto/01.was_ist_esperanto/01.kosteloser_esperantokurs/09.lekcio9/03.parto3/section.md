---
title: Übungen - Planoj
---

####  Übersetze mit Hilfe von Vor- oder Nachsilben:

1. das Vierfache:
2. noch zu lesendes Buch:
3. ein Drittel:
4. Kuchenkrümel (von: kuko):
5. zwei Fünftel:
6. tratschen (von: paroli):
7. fehlleiten (von: gvidi):
8. Geldbörse (von: mono):

####  Übertrage die folgende Wörter ins Deutsche
Versuche dabei, möglichst treffende Begriffe oder Umschreibungen zu finden!

1. ujeto
2. arano
3. aĉaĵo
4. disigi
5. eksigenda
6. fiega
7. etiĝi
8. estrino

### Planoj...

Nu, Peter, ĉu mi sukcesis fari la internacian Esperanto-kongreson interesa por vi?
Ĉu vi akompanos min en la sekva somero?
Peter ankoraŭ ne findecidis (fin-decid-is).

– Mi ja intencis ferii vintre, eble januare aŭ februare. Vi scias, ke mi volonte okupiĝaspri vintra sporto!
– Tion vi ankaŭ povos fari. La kongresoj daŭras nur iom pli ol unu semajno, tiel ke restos al vi ankoraŭ tri semajnoj da ferioj. Krome: Komparu vian vintran feriadon kun internacia kongreso! En la montaro vi estos sola kaj malvarmumos, sed dum la kongreso vi estos kune kun centoj da novaj amikoj el dekoj da landoj. Kaj tio estas ne iu seka kongreso, sed kultura festivalo kun multaj interesaj kaj gajaj programeroj!
– Kara Klanjo, unue, dum vintra feriado mi nek estas sola, nek malvarmumas, kaj due, mi ne sufiĉe bone komprenas Esperanton.
– En la sekva somero vi komprenos. Tion mi promesas al vi, Peĉjo.
– La venonta kongreso okazos en suda ameriko, ĉu ne? Nu, certe mi ĉiam volis vojaĝi tien. Sed pensu pri la mono...

Sonoras. Envenas Eva, amikino de Klara. Ili salutas unu la alian, kaj Peter diras:

– Mi volas inviti vin al promenado en la urbocentron. Hodiaŭ estas tiel bona vetero, ke estus domaĝe sidi hejme malantaŭ ŝlosita pordo. Ĉu mi ne pravas?
– Jes, vi pravas. Ĉu vi jam konas la plej novan novaĵon? Permesu prezenti al vi nian akompananton por la Internacia Junulara Kongreso. Jen li staras! Eva rigardas Peter, ridetante (rid-et-ant-e).
– Tio estas vere bona novaĵo! Peter provas protesti.
– Sed mi nenion promesis! Ĉu vi ne unue volas mian opinion pri tio? Sed ambaŭ knabinoj enkape (en-kap-e) jam preparas la vojaĝon:
– Ĉar la kongreso okazos en suda ameriko, ni vojaĝos unue per trajno, kaj trans la oceanon ni veturos per grandega ŝipo. Ni dormos unue en dormvagono, kaj poste en la malsupra etaĝo de la ŝipo. Tiel vojaĝi jam delonge estis mia revo!
– Ne, ni ne havos tiom da tempo. Ni flugos … Peter silentigas ilin:
– Mi proponas, ke ni nun ekiru al nia promenado. Ĉar hodiaŭ estas blua ĉielo, ni povus veturi al la televida turo kaj supreniĝi (supr-e-n-iĝ-i), kaj tie, en la kafejo, ni trankvile priparolos la aferojn de la vojaĝo. Ek!

6. NEUE WÖRTER
Erschließe Dir selber: sporto, festivalo, protesti, oceano, vagono.

* ĉielo - Himmel
* domaĝe - schade
* etaĝo - Etage
* ferio - Ferien
* flugi - fliegen
* montaro - Gebirge
* monto - Berg
* provi - versuchen
* revo - Traum (nicht im Schlaf)
* ŝipo - Schiff
* ŝlosi - verschließen
* sonĝo - Traum (im Schlaf)
* sonori - klingeln
* televido - Fernsehen
* vintro - Winter

Lösungen zu 4.

4.1

1. kvaroblo
2. legenda libro
3. triono
4. kukereto
5. du kvinonoj
6. parolaĉi

4.2

1. kleines Gefäß
2. Mitglied
3. hässliches Ding
4. scheiden, auseinander bringen
5. rauszuwerfend
6. total verdorben/schlecht
7. klein werden
8. Leiterin

### KONTROLLÜBUNGEN

####  Erkläre nachfolgende Wörter auf Esperanto!

Beispiel: panujo: Tio estas ujo, en kiu estas pano.

1. paperujo
2. hundaĉo
3. programero 4. sonorilo
5. besteto
6. taskaro
7. saĝulo
8. kuracilo

####  Setze die fehlenden Endungen ein!

1. Mi proponas, ke vi nun ir___ hejmen!
2. Li decidis, ke ŝi enven___ en la ĉambron.
3. Paul ŝajnas esti la plej malalt___.
4. Ĉu vi opinias la teatraĵon interes___?
5. Ili sentis sin lac___.

####  Schreibe einem Freund einen Brief über deinen bevorstehenden Urlaub!

Beachte dabei die Briefform (Anrede, Datum, Verabschiedung) und nutze dazu die
folgenden Fragen:

* Kiam vi intencas ferii?
* Kien vi veturos? Ĉu la unuan fojon tien?
* Kun kiu vi pasigos la feriojn?
* Kion vi planas fari dum la feriaj tagoj?
* Kiom da tagoj vi restos?
* Pri kio vi okupiĝas vespere?

---
title: Teksto - Klara monologas
---

Ĉu vi antaŭ nia interkonatiĝo (inter-kon-at-iĝ-o) en la
parko vere neniam aŭdis pri Esperanto, Peter? Ĝi
ekzistas jam ekde pli ol cent jaroj, kaj hodiaŭ parolas
ĝin inter centmil kaj du milionoj da homoj sur ĉiuj
kontinentoj: unue en Eŭropo, poste en Azio kaj
Ameriko, kaj hodiaŭ ankaŭ en Aŭstralio kaj Afriko! Mi
proponas, ke vi partoprenu kun mi la sekvan
Internacian Junularan Kongreson. Tie ni travivos
kelkajn entuziasmigajn tagojn, kaj vi vidos, ke
Esperanto estas eĉ pli ol nura lingvo. Ĝi estas
amikeco inter la homoj, ĝi estas sento de komuneco.
Tiun ĉi komunan senton oni nomas esperantismo. La
homaro povus interrilati multoble pli harmonie, se ĉiu
povus paroli unu komunan lingvon kiel duan lingvon
apud sia gepatra lingvo. Nur proksimume unu
kvinono el mia iama klaso en la lernejo sufiĉe bone
parolas fremdan lingvon, ĉiuj aliaj bezonas tradukistojn dum siaj vojaĝoj aŭ tute ne povus
interparoli kun eksterlandanoj.
Tiel pensis ankaŭ la kreinto de Esperanto, Ludoviko Lazaro Zamenhof. Laŭ profesio li estis
okulkuracisto, li estis judo kaj vivis unue en Bjalistoko kaj poste en Varsovio, en la tiama
Rusujo, hodiaŭ Polujo. En lia hejmurbo loĝis diversnaciaj homoj, ofte malpaciĝeme. Jam
kiel knabo, Zamenhof promesis al si ŝanĝi tiun ĉi situacion. Li pensis, ke, kreante unu
lingvon por ĉiuj, li povas servi al pacama mondo. Tiel naskiĝis la esperantismo, kaj pro tio
Zamenhof ne nur elpensis saĝajn gramatikajn regulojn kaj utilajn vortojn, sed ankaŭ multe
vojaĝis kaj per kortuŝaj paroladoj klopodis ligi sian lingvon kun la amo inter la homoj. Tion
li opiniis tasko de sia vivo. Al mi plej plaĉas frazo, kiun li diris dum la unua Universala
Kongreso de Esperanto en 1905 en la franca urbo Boulogne-sur-Mer:
„Ni konsciu bone la tutan gravecon de la hodiaŭa tago, ĉar hodiaŭ inter la gastamaj muroj
de Bulonjo-sur-Maro kunvenis ne francoj kun angloj, ne rusoj kun poloj, sed homoj kun
homoj.“

#### NEUE WÖRTER

Diese Wörter erklären sich sicher selbst:
monologo, miliono, kontinento, Eŭropo, Azio, Ameriko, Aŭstralio, Afriko, harmonie, situacio.

* Bjalistoko - Bialystok (Stadt in Polen)
* esperantismo - „Esperantismus“ (ideeller Anspruch der Esperanto-Bewegung)
* gepatra lingvo - Muttersprache
* gramatiko - Grammatik
* homaro - Menschheit
* iam - irgendwann, einst
* iama - ehemalig, einstig
* interkonatiĝi - Bekanntschaft schließen
* interkonatiĝo - Bekanntschaft
* interrilati - miteinander umgehen
* judo - Jude
* klopodi - sich bemühen
* kortuŝa - herzergreifend, anrührend
* krei - schaffen, zeugen
* kreinto - Schöpfer; Autor; Erzeuger
* kuraci - heilen
* kuracisto - Arzt
* kvinono - ein Fünftel
* laŭ profesio - von Beruf
* malpaciĝeme unfriedlich, streitsüchtig
* morti - sterben
* multoble - mehrfach, um ein Vielfaches
* neniam - nie, niemals
* okulo - Auge
* opinii - (die) Meinung haben, meinen;
* paco - Frieden
* profesio - Beruf
* proksimume - ungefähr, annähernd
* promesi - versprechen
* regulo - Regel
* rilato - Beziehung
* Rusio - Russland
* ŝanĝi - (ver-)ändern
* sento - Gefühl
* servi - dienen
* tasko - Aufgabe
* tiama - damalig; zu jener Zeit
* travivi - erleben
* tuŝi - berühren
* universala - universell; Welt-, weltumfassend
* utila - nützlich
* vere - wirklich, wahrhaftig

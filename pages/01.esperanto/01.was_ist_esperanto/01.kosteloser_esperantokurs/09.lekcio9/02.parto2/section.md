---
title: "Grammatik"
---

####  Imperativ in einem Nebensatz, der mit „ke“ eingeleitet wird:

* Li diris, ke ŝi venu en la ĉambron. - Er sagte, sie soll in das Zimmer kommen.
* Mi proponas, ke vi partoprenu la renkontiĝon. - Ich schlage vor, dass du an dem Treffen teilnimmst.
* La historio decidis, ke li mortu dum la milito. - Die Geschichte entschied, dass er während des Krieges sterben sollte.

In diesen Fällen wird durch die Imperativendung -u dem formulierten Wunsch, der
Entscheidung oder der Anordnung deutlicher Nachdruck verliehen (häufig übersetzbar mit
„sollen“).

Natürlich könnte der Imperativ in diesen Sätzen auch durch Modalverben oder einfach
andere Form des Verbs ersetzt werden. Dadurch würde aber der Satzinhalt neutraler,
emotionsloser zum Ausdruck gebracht. Zum Vergleich:

* Li diris, ke ŝi povas veni. - Er sagte, dass sie kommen kann.
* Li diris, ke ŝi venu. - Er sagte, dass sie kommen soll. (Er sagte, sie soll kommen.)

####  Die Verwendung des Akkusativs

Die Verwendung des Akkusativs kann helfen, Sätze zu verkürzen und damit
übersichtlicher zu gestalten. Oft ist es möglich, den Präpositiv indirekter Objekte zu
direkten Objekten zu verkürzen. Nicht selten klingt das in deutschen Ohren seltsam, ist
jedoch im Esperanto völlig legitim:

* Mi partoprenas en la aranĝo. - Mi partoprenas la aranĝon. (Ich nehme an der Veranstaltung teil.)
* Mi dankas al vi. - Mi dankas vin. (Ich danke dir.)
* Mi gratulas al vi. - Mi gratulas vin. (Ich gratuliere dir.)
* Ili sekvis post ŝi (oder: al ŝi). - Ili sekvis ŝin. (Sie folgten ihr. Sie folgten ihr nach.)

Wichtig ist, dass ein Verb nicht zwei direkte Objekte haben darf.

####  Das Prädikativum

Es gibt im Esperanto (wie im Deutschen) Sätze, in denen scheinbar zwei Satzglieder im
Nominativ (wer-Fall) stehen:

> Paŭl estas instruisto. - Paul ist Lehrer.

Da wir aus der 1. Lektion wissen, dass der Nominativ stets das Satzsubjekt bezeichnet
(Paul), wird das andere Satzglied (instruisto) als Teil des Prädikates (estas instruisto)
betrachtet. Das Wort „instruisto“ wird daher als Prädikativum bezeichnet. In der modernen
Literatur des Esperanto werden prädikative Formen gern benutzt und auf eine ganze
Reihe von Verben ausgedehnt:

*Li ŝajnas granda. - Er erscheint groß.
* Li ŝajnas esti la plej alta. - Er scheint der Größte zu sein.
* Ŝi sentas sin estrino de la grupo. - Sie fühlt sich als Chefin der Gruppe.
* Mi opinias la filmon bona. - Ich halte den Film für gut.

Diese im Deutschen ungewöhnlichen Formen können im Esperanto natürlich auch
umschrieben werden, durch Teilsätze oder durch das Wörtchen „kiel“:

* Ŝi sentas sin kiel la estrino de la grupo.
* Mi opinias ke la filmo estas bona.

Die Verwendung prädikativer Formen hilft, die Sprache rationell und eleganter zu
verwenden.

####  Wortbildung

Suffixe:
* -ism- (Lehre, Ideologie):
  * katolikismo
  * komunismo
  * esperantismo
* -obl- (das Vielfache einer Menge; -fach):
  * duoble (zweifach)
  * la duoblo (das Doppelte)
  * trioble kvar estas dek du (drei mal vier ist zwölf)
* -on- (der Bruchteil einer Menge)
  * la duono (die Hälfte),
  * kvin sesonoj (fünf Sechstel)
* -um- (wird verwendet, wenn keiner der anderen Suffixe Verwendung finden kann; -um- ist ohne feste eigene Bedeutung), z.B.:
  * aero (Luft) - aerumi (lüften)
  * proksima (nah) - proksimume (annähernd)
  * kolo (Hals) - kolumo (Kragen)
  * plena (voll) - plenumi (erfüllen)

Oft muss die Bedeutung der durch -um- gebildeten Wörter speziell gelernt werden, da sie
vom Wortstamm nur schwer ableitbar ist.

Hier noch die weitere wichtige Affixe, die bisher noch nicht in den Texten erschienen sind:

Präfix:

* mis- (falsch, irrtümlich, miss-): miskompreni (missverstehen), misuzi (missbrauchen)

Suffixe:

* -aĉ- (verächtliche Bezeichnung, häßlich): hundaĉo (Köter), ridaĉi (verächtlich lachen)
* -end- (Tätigkeit, die noch zu erledigen ist): skribenda letero (ein zu schreibender Brief), farendaĵoj (die noch zu erledigenden Dinge)
* -er- (Bestandteil): panero (Brotkrümel), programero (Programmpunkt)
* -ing- (Halter, Hülse): kandelo (Kerze) - kandelingo (Kerzenhalter), glavo (Schwert) glavingo (Schwertscheide)
* -uj- (Behältnis; Baum): paperujo (Papierkorb; Behälter mit Papier), panujo (Brotkorb), pomujo (Apfelbaum), ĉerizo (Kirsche) - ĉerizujo (Kirschbaum). Anstelle von pomujo, ĉerizujo usw. werden im modernen Esperanto (der Eindeutigkeit halber) häufig die zusammengesetzten Wörter pomarbo (pom-arb-o), ĉerizarbo (ĉeriz-arb-o) usw. benutzt.
* -ĉj- männliche Koseform: patro - paĉjo (Papa, Papi), Josef - Joĉjo (Jupp)
* -nj- weibliche Koseform: patrino - panjo (Mama, Mutti), Maria - Manjo (Mariechen)

---
title: "Grammatik"
---

Hier eine kurze Wiederholung von Sachen, die du in den Grammatikteilen der
vergangenen Lektionen gelernt hast:

### Die Wortarten und ihre Endungen:

* Substantiv: -o (floro)
* bestimmter Artikel: la (la floro)
* Adjektiv: -a (bela floro)
* abgeleitete Adverbien: -e (bele)
* ursprüngliche Adverbien ohne Endung: tro, hodiaŭ
* Pronomina (Fürwörter)
  * Personalpronomina: mi, vi, li, ŝi, ĝi, ni, vi, ili; oni
  * Possesivpronomina: mia, via, lia, ŝia, ĝia, nia, via, ilia; onia
  * Andere wichtige Pronomina findest Du unter 3.2!
* Verb (Zeitwort)
  * Infinitiv: -i (legi)
  * Gegenwart: -as (mi legas, vi legas, ...)
  * Vergangenheit: -is (mi legis, vi legis, ...)
  * Zukunft: -os (mi legos, vi legos, ...)
  * Wunschform: -us (mi legus, vi legus, ...)
  * Aufforderungsform: -u (legu!)
* Numerale (Zahlwörter)
  * Kardinalzahlen (Grundzahlen):
    * nul, unu, du, tri, kvar, kvin, ses, sep, ok, naŭ, dek,
    * dek unu, dek du, dudek, tridek, kvardek,
    * cent, cent unu, cent dek unu, ducent, ducent dudek du, trimil tricent tridek tri
  * Ordinalzahlen (Ordnungszahlen):
    * unua, dua, deka, dek unua, ducent dudek dua.
    * unue, due, deke.

### Tabellwörter

Eine ganze Reihe häufig gebrauchter Adverbien und Pronomina sind im Esperanto in einer
logisch erschließbaren Tabelle zusammengefasst. Jedes der darin enthaltenen Wörter
besteht aus zwei Bestandteilen. Die Bedeutung des ersten Wortteiles findest Du in der
Spalte, die Bedeutung des zweiten Wortteiles in der Zeile. Zum Beispiel:

* 3. Spalte + 4. Zeile = i- + -am = iam = unbestimmt + Zeit = irgendwann
* 1. Spalte + 3. Zeile = ki- + -e = kie = fragend + Ort = wo?

<table class="table"> <thead> <tr> <th>&nbsp;</th> <th> <em>ki-</em> <br> fragend </th> <th> <em>ti-</em> <br> hinweisend </th> <th> <em>i-</em> <br> unbestimmt </th> <th> <em>ĉi-</em> <br> allumfassend </th> <th> <em>neni-</em> <br> verneinend </th> </tr> </thead> <tbody> <tr> <th> <em>-o</em> <br> Sache </th> <td> <em>kio</em> –<br> was </td> <td> <em>tio</em> –<br> jenes, das </td> <td> <em>io</em> –<br> irgendwas </td> <td> <em>ĉio</em> –<br> alles </td> <td> <em>nenio</em> –<br> nichts </td> </tr> <tr> <th> <em>-u</em> <br> Person </th> <td> <em>kiu</em> –<br> wer, welcher </td> <td> <em>tiu</em> –<br> jener </td> <td> <em>iu</em> –<br> jemand </td> <td> <em>ĉiu</em> –<br> jeder </td> <td> <em>neniu</em> –<br> keiner </td> </tr> <tr> <th> <em>-am</em> <br> Zeit </th> <td> <em>kiam</em> –<br> wann </td> <td> <em>tiam</em> –<br> dann </td> <td> <em>iam</em> –<br> irgendwann </td> <td> <em>ĉiam</em> –<br> immer </td> <td> <em>neniam</em> –<br> nie </td> </tr> <tr> <th> <em>-a</em> <br> Eigenschaft </th> <td> <em>kia</em> –<br> was für ein </td> <td> <em>tia</em> –<br> solch ein </td> <td> <em>ia</em> –<br> irgendein </td> <td> <em>ĉia</em> –<br> jederlei </td> <td> <em>nenia</em> –<br> keinerlei </td> </tr> <tr> <th> <em>-e</em> <br> Ort </th> <td> <em>kie</em> –<br> wo </td> <td> <em>tie</em> –<br> dort </td> <td> <em>ie</em> –<br> irgendwo </td> <td> <em>ĉie</em> –<br> überall </td> <td> <em>nenie</em> –<br> nirgends </td> </tr> <tr> <th> <em>-el</em> <br> Art &amp; Weise </th> <td> <em>kiel</em> –<br> wie </td> <td> <em>tiel</em> –<br> so </td> <td> <em>iel</em> –<br> irgendwie </td> <td> <em>ĉiel</em> –<br> auf jede Weise </td> <td> <em>neniel</em> –<br> in keiner Weise </td> </tr> <tr> <th> <em>-om</em> <br> Menge </th> <td> <em>kiom</em> –<br> wieviel </td> <td> <em>tiom</em> –<br> soviel </td> <td> <em>iom</em> –<br> etwas, ein wenig </td> <td> <em>ĉiom</em> –<br> alles </td> <td> <em>neniom</em> –<br> nichts </td> </tr> <tr> <th> <em>-al</em> <br> Grund </th> <td> <em>kial</em> –<br> warum </td> <td> <em>tial</em> –<br> deshalb </td> <td> <em>ial</em> –<br> aus irgendeinem Grund </td> <td> <em>ĉial</em> –<br> aus jedem Grund </td> <td> <em>nenial</em> –<br> aus keinem Grund </td> </tr> <tr> <th> <em>-es</em> <br> Besitz </th> <td> <em>kies</em> –<br> wessen </td> <td> <em>ties</em> –<br> dessen </td> <td> <em>ies</em> –<br> irgend jemandes </td> <td> <em>ĉies</em> –<br> jedermanns </td> <td> <em>nenies</em> –<br> niemandes </td> </tr> </tbody> </table>

Die Tabellwörter können auch verändert werden. Z.B. kann man manche von ihnen in den
Plural setzen

tiuj belaj floroj - diese schönen Blumen

Und sie können eine Akkusativendung erhalten (bei Substantiven, Adjektiven, vielen
Pronomina und einigen Adverben):

Mi legis ĉi tiujn bonajn librojn - Ich habe diese guten Bücher gelesen.

Falsch wäre „Mi legis tion libron.“, da tio kein Demonstrativpronomen ist. Es muss „Mi
legis tiun libron.“ heißen.

### Wortbildung

Ein Wort besteht im Esperanto aus einer oder mehreren Wortwurzeln, Vor- und
Nachsilben und einer oder mehreren Endungen:* nur Wurzel: apud (neben)
* Wurzel + Endung: apuda (la apuda domo - das daneben befindliche Haus)
* 2 Wurzeln + Endung: leglibro / legolibro (Lesebuch)
* Affixe (Vor-/Nachsilben) + Wurzel + Endung:
* juna - jung, junulo - Jugendlicher, junularo - Jugend (junge Menschen)
* gejunuloj - Jugendliche (beiderlei Geschlechts), maljunulino - alte Frau

Bisher bekannte Präfixe (Vorsilben):

* ek- (plötzlicher Beginn): ekkanti
* ge- (beiderlei Geschlechts): gejunuloj
* mal- (Gegenteil): maljuna
* re- (zurück, noch einmal): redoni; relegi

Bisher bekannte Suffixe (Nachsilben):

* -ad- (Dauer; oft auch zur Substantivierung von Verben): la legado (das Lesen)
* -aĵ- (eine Sache): trinkaĵo (Getränk)
* -an- (Angehöriger, Anhänger): alilandano (ali-land-an-o)
* -ant- (eine Tätigkeit Ausführender): skribanto
* -ar- (Schar, Ansammlung): junularo
* -eg- (Vergrößerung, Verstärkung): maljunega (uralt)
* -et- (Abschwächung): boteleto
* -ej- (Ort): lernejo (Schule)
* -uj- oder -i- (Land): Germanujo/Germanio
* -iĝ- (zu etwas werden; Darstellung eines Vorganges): bluiĝi (blau werden)
* -il- (Werkzeug, Gerät, Mittel): laŭtparolilo (Lautsprecher)
* -in- (macht das Wort weiblich): virino (Frau)
* -ist- (Beruf; ständige Tätigkeit): skribisto (Schreiber)
* -ul- (Person, Träger einer Eigenschaft): malgrandulo (Kleiner)

### Einleitung eines Teilsatzes

Bei zusammengesetzten Sätzen kann der zweite Teilsatz eingeleitet werden duch:
1. kaj/aŭ/sed: Mi serĉis mian filon, sed (mi) ne trovis lin. - Ich suchte meinen Sohn, fand ihn aber nicht.
2. ke: Mi jam diris, ke mi forvojaĝos morgaŭ. - Ich habe schon gesagt, dass ich morgen wegfahren werde.
3. kiu(n)/kio(n): Diru, kiu vi estas! - Sag', wer du bist! Rakontu, kion vi vidis! - Erzähle, was du gesehen hast! Rakontu, kiun vi vidis! - Erzähle, wen du gesehen hast!
4. Präposition + kiu(n)/kio(n): Jen la knabo, kun kiu dancis via filino! Ĉu vi ne scias, sur kion li skribis la poemon? Sur la lernejan tablon!
5. Jedes der Fragewörter aus der Tabelle (3.2): Ĉu ŝi skribis, kiel ŝi fartas? Ĉu vi aŭdis, kiom da infanoj ili havas?


### Sechs Varianten des Satzaufbau (Vorschläge):

1. Subjekt - Prädikat: Li venis.
2. Ergänzung - Subjekt - Prädikat - Ergänzung (Adverb): Hieraŭ li venis tro malfrue.
3. Subjekt - Prädikat - Ergänzung (direktes Objekt, Akkusativ): Li skribas leteron.
4. Subjekt - Prädikat - Ergänzung (indirektes Objekt, Präpositiv): Ŝi sidas en la lernejo. Ŝi iras en la lernejon. (Richtungsangabe)
5. Unpersönliche Sätze: Prädikat (Verb; oder: Verb – Adverb): Pluvas. - Es regnet. Estas varme. - Es ist warm.
6. Der deutsche Infinitiv mit „zu“ wird im Esperanto einfach mit dem Infinitiv ausgedrückt: Ŝi ŝajnas esti la plej alta. - Sie scheint die Größte zu sein.

Alle Varianten können mit weiteren Ergänzungen gebildet werden. Aus stilistischen
Gründen oder durch den Einfluss der Muttersprache gibt es nicht selten Abweichungen
von dieser Reihenfolge.

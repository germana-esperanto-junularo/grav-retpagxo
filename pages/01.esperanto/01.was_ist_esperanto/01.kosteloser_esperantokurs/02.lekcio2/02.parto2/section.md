---
title: "Grammatik"
---

### Sätze mit Modalverben

Wenn ein Satz Modalverb und Vollverb aufweist, so erhält das Modalverb die Endung der
vorgesehenen grammatischen Zeit (z.B. -as, -is, os). Das Vollverb steht im Infinitiv.
Peter devas atendi. - Peter muss warten.
Modalverben sind zum Beispiel im Deutschen: wollen, können, müssen

### Der deutsche Dativ (wem-Fall) im Esperanto

Der deutsche Dativ (wem-Fall) wird im Esperanto oft mit Hilfe der Präposition „al“ gebildet:
Ŝi plaĉas al li. - Sie gefällt ihm.
Pardonu al mi! - Verzeih mir!

### Die Vergangenheit

Kennzeichen für die Vergangenheit der Verben ist die Endung -is.
Beispiel:

* esti - sein
* mi estis - ich war
* ni estis - wir waren
* vi estis - du warst; Sie waren
* vi estis - ihr wart; Sie waren
* li/ŝi estis - er/sie war
* ili estis - sie waren
* ĝi estis - (er, sie) es war

Ebenso:

* mi atendis - ich wartete
* vi devis - du musstest

usw.

Die deutschen Vergangenheiten Präteritum (ich ging) und Perfekt (ich bin gegangen)
werden auf Esperanto beide ohne Hilfsverb mit der Endung -is übersetzt. „Ich bin
gegangen“ heißt also einfach „Mi iris“ (und nicht z.B. „Mi estas iris“).

### Die Zukunft

Kennzeichen für die Zukunft der Verben ist die Endung -os:

* mi estos - ich werde sein
* vi estos - du wirst sein, ihr werdet sein, Sie werden sein (Höflichkeitsform)

usw.

Ebenso:

* mi venos - ich werde kommen
* vi havos - du wirst haben; ihr werdet haben, Sie werden haben

Anders als im Deutschen gibt es im Esperanto nur jeweils eine Form für Vergangenheit
und Zukunft. Kompliziertere Zeitbeziehungen können mit Partizipien ausgedrückt werden.
Dazu kommen wir in Lektion 7.


### Umstandswörter (Adverbien)

Adverbien beschreiben die Umstände von Tätigkeiten, Geschehnissen, Ereignissen,
Eigenschaften oder Verhältnissen genauer.
Sie enden meist auf -e:
malfrue - spät (vom Adjektiv malfrua)
bone - gut (vom Adjektiv bona)
kune - gemeinsam, zusammen (von der Präposition kun)
eble - vielleicht, möglicherweise (vom Adjektiv ebla - möglich)
Adverbien können sich beziehen auf* Verben: Ŝi venis malfrue. - Sie kam spät.
* Adjektive: Tio estas tute nova - Das ist völlig neu.
* andere Adverbien: Ili same bone lernas. - Sie lernen gleich gut.
* Teilsätze: Estas bele, iri kun vi al kinejo - Es ist schön, mit dir ins Kino zu gehen.

Außer diesen, von anderen Wortarten durch Veränderung der Endung abgeleiteten
Adverbien gibt es viele sogenannte ursprüngliche Adverbien, die endungslos sind:
tre, hieraŭ, hodiaŭ, morgaŭ, almenaŭ ...
Diese ursprünglichen Adverbien haben im Satz die gleiche Funktion wie alle anderen
Adverbien. Sie können zudem ebenfalls durch Anfügen von Endungen in andere
Wortarten verwandelt werden:
hieraŭa (von hieraŭ) - gestrig

### Fragewörter im Akkusativ

Die Fragewörter „kio“ (was?) und „kiu“ (wer?) erhalten die Akkusativendungen -n, wenn mit
Hilfe der Frage das direkte Objekt (Akkusativobjekt) erfragt werden soll:
Kion vi faras? (Was machst Du?) - Mi faras ordon.
Kiun vi vidas? (Wen siehst Du?) - Mi vidas la knabinon Klara.

### Das Wörtchen „ĉi“

Das Wörtchen „ĉi“ ist ein Beiwort. Es kann vor- oder nachgestellt werden. „Ĉi“ bringt die
Bedeutung näher heran:
tiu (jener), tiu ĉi oder ĉi tiu (dieser);
tie (dort), tie ĉi oder ĉi tie (hier).
Außerdem kann man es auch an Wörter anhängen:
ĉi-tage - an diesem Tag (wörtlich: diestägig)

### Der deutsche Genitiv (wes-Fall) im Esperanto

Der deutsche Genitiv (wes-Fall) wird häufig mit Hilfe der Präposition „de“ übertragen:
la adreso de la kinejo - die Adresse des Kinos.

### Wortbildungssilben:

Suffix:* -ul-: Person, Träger einer Eigenschaft
◦ junulo (von juna) - Jugendlicher
◦ novulo (von nova) - Neuling
* -et-: Verkleinerung, Abschwächung
◦ dometo (von domo) - Häuschen
◦ beleta (von bela) - hübsch (für schön reich es nicht)

Präfixe:* mal-: Gegenteil
◦ agrabla (angenehm) - malagrabla (unangenehm)
◦ bela (schön) - malbela (hässlich)
* ek-: Beginn
◦ iri (gehen) - ekiri (losgehen)

Wie bereits erwähnt, können alle Wortbildungssilben mit jedem beliebigen Wort
zusammengesetzt werden, wenn das neue Wort einen Sinn ergibt.

### Mehrere Wortbildungssilben in einem Wort

Mehrere Wortbildungssilben können, dem logischen Sinnaufbau des Wortes folgend,
aneinandergereiht werden:
juna - jung
junulo - junger Mensch, Jugendlicher (geschlechtsneutral)
junulino - junge Frau, Jugendliche

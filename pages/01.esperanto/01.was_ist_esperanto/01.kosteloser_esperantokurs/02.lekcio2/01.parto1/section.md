---
title: "Teksto - Peter devas atendi"
---

Nachdem wir in der ersten Lektion die Bekanntschaft von Klara und Peter gemacht haben,
wollen wir heute Peter ein zweites Mal in den Park begleiten. Erinnern wir uns: Gestern
hatte Klara nach anfänglichem Zögern zugesagt, heute mit ihm ins Kino zu gehen.
Hoffnungsvoll nähert sich daher Peter nun der Parkbank, wo er Klara gestern verließ:

![peter devas atendi](kek-2-1.mp3)

Peter venas en la parkon kaj atingas la benkon. Li ne vidas la knabinon. Ŝi ankoraŭ ne
estas ĉi tie. Do, li devas atendi, ĝis ŝi venos. Klara plaĉas al li. Ŝi estas ĝentila kaj bela
junulino. Hieraŭ, post la renkonto, li estis tre kontenta, ĉar ŝi akompanos lin en la kinejon.

> Klara venas.

* Bonan tagon, Peter.
* Saluton, Klara. Vi venas iom malfrue. Ni devos kuri al la kinejo.
* Jes, mi bedaŭras tion. Pardonu. La tuta tago estis malagrabla por mi.
* Kion vi faris dum la tago?
* Aĥ, nun mi ne diskutos pri tio. Almenaŭ nun mi estas ĉi tie.
* Ĉu vi konas la adreson de la kinejo „Amuzejo“?
* Jes, mi vidis tie interesan filmon. Ĝi nomiĝis „La komenco“.
* Ho jes, mi aŭdis pri ĝi. Do, ni nun devas ekiri.

2. NEUE WÖRTER
* adreso - Adresse
* akompani - begleiten
* almenaŭ - wenigstens, mindestens, zumindest
* amuzi - belustigen, amüsieren (jemanden)
* ankoraŭ - noch
* atingi - erreichen
* aŭdi - hören
* ĉar - weil
* ĉarma - charmant
* ĉarmulo - charmante Person
* ĉi tie - hier
* diskuti - diskutieren
* dum - während
* ekiri - losgehen
* fari - machen, tun
* ĝentila - höflich
* hieraŭ - gestern
* interesa - interessant
* iom - ein wenig, etwas

* juna - jung
* junulo - Jugendlicher
* komenco - Anfang
* koni - kennen
* kontenta - zufrieden
* kuri - laufen
* malagrabla - unangenehm
* malfrue - spät
* moderna - modern
* plaĉi (al) - gefallen
* por - für
* post - nach
* pri - über (nicht örtlich)
* renkonto - Treffen
* tago - Tag
* tre - sehr
* tuta - ganz, völlig
* veni - kommen
* vidi - sehen

**Redewendungen**:

* Bonan tagon! - Guten Tag!
* Mi bedaŭras (tion) - Tut mir leid.
* Kion vi faris?
* Was hast du (haben Sie, habt ihr) getan?

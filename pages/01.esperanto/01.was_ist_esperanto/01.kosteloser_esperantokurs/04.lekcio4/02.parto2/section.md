---
title: "Grammatik"
---

### Die Wochentage

* dimanĉo - Sonntag
* lundo - Montag
* mardo - Dienstag
* merkredo - Mittwoch
* ĵaŭdo - Donnerstag
* vendredo - Freitag
* sabato - Samstag

Die Adverbendung -e kann zum Ausdruck des Zeitpunktes verwendet werden:

* dimanĉe - am Sonntag, sonntags
* lunde - am Montag, montags
* oder die Akkusativendung -n, ebenfalls zum Ausdruck des Zeitpunktes:
* dimanĉon - am Sonntag
* lundon - am Montag
* la sekvan tagon - am nächsten Tag

Beispiele:

Hodiaŭ estas merkredo. - Heute ist Mittwoch.

Mi ekveturos merkredon. - Ich fahre am Mittwoch los.

Mi ekveturos merkrede. - Ich fahre am Mittwoch los.

Ĉiun lundon mi iras en la kinejon. - Jeden Montag gehe ich ins Kino.

Ĉiulunde (ĉiu-lund-e) mi iras en la kinejon. Jeden Montag (immer montags) gehe ich ins Kino.

de dimanĉo ĝis mardo - von Sonntag bis Dienstag

### Die Grundzahlwörter

* 11 - dek unu
* 12 - dek du
* 13 - dek tri
* 20 - dudek
* 21 - dudek unu
* 22 - dudek du
* 30 - tridek
* 40 - kvardek
* 99 - naŭdek naŭ
* 100 - cent
* 101 - cent unu
* 111 - cent dek unu
*222 - ducent dudek du
* 999 - naŭcent naŭdek naŭ
* 1000 - mil
* 2345 - dumil tricent kvardek kvin

Anwendung:

15 Tage - dek kvin tagoj

Die Zahlen werden in der Reihenfolge ihrer Ziffern gesprochen und ausgeschrieben.
Wenn die Zahl ausgeschrieben wird, werden die Ziffern (die Stellen der Zahl, d.h. die
Hunderter, Zehner, Einer) getrennt geschrieben. Eine Zahl mit vier Ziffern (1991) besteht
ausgeschrieben also aus vier Wörtern (mil naŭcent naŭdek unu).

### Ordnungszahlen

Wenn Ordnungszahlen wie Adjektive attributiv gebraucht werden, erhalten sie die
Adjektivendung -a (und damit alle Eigenschaften eines Adjektivs):

* la unua libro - das erste Buch
* la dek dua peco - das zwölfte Stück
* la naŭcent sepdek tria infano - das 973. Kind

Wie Du richtig bemerkst, erhält nur das letzte Glied der Ordnungszahl die Endung -a.

Beispiele:

* Donu al mi la unuan libron. - Gib mir das erste Buch. (Akkus. Sing.)
* Jen la unuaj libroj. - Hier sind die ersten Bücher. (Nomin. Pl.)
* Prenu la unuajn librojn. - Nimm die ersten Bücher. (Akkus. Pl.)

Wenn Ordnungszahlen als Beifügung für einen Satzteil, Satz oder Text gebraucht
werden, dann erhalten sie die Adverbendung -e:

**Beispiel**:

* unue - erstens, zuerst
* dek due - zwölftens; an zwölfter Stelle
* Unue li prenis mian libron, poste ankaŭ mian skribilon. - Zuerst nahm er mein Buch,
später auch meinen Stift.

Die Datumsangabe

Die Monatsnamen:

  * januaro, februaro, marto, aprilo, majo, junio, julio, aŭgusto, septembro, oktobro,
novembro, decembro.

### Das Datum:

* la unua de januaro mil naŭcent naŭdek naŭ - der 1. Januar 1999; Verkürzt geschrieben: la 1-a de januaro 1999
* am 1. Januar 1999: la unuan de januaro 1999; verkürzt: la 1-an de januaro 1999
* vom 1. Januar bis zum 2. Februar - de la 1-a de januaro ĝis la 2-a de februaro.

### Die Steigerung von Adjektiven und Adverben

Die erste Steigerungsstufe (Komparativ) wird durch „pli ... ol ...“, die zweite
Steigerungsstufe (Superlativ) durch „plej ...“ vor dem zu steigernden Wort gebildet. Beide
Wörter sind unveränderlich:

bela (Adjektiv) - schön
pli bela (ol ...) - schöner (als ...)
plej bela - am schönsten
la plej bela ... - der/die/das schönste …

bone (Adverb) - gut
pli bone (ol ...) - besser (als ...)
plej bone - am besten

Beispiele:

* Ŝi estas pli bela ol via amikino. - Sie ist schöner als deine Freundin.
* Li estas la plej alta (persono). - Er ist der größte (die größte Person).
* Ili lernas pli bone (ol vi). - Sie lernen besser (als ihr).
* Kiu lernas plej bone? - Wer lernt am besten?

### Sätze oder Teilsätze ohne Subjekt und das unpersönliche „es“

Sätze oder Teilsätze ohne Subjekt im Esperanto werden meist mit dem unpersönlichen 'es'
übersetzt:

* Estas bone. - Es ist gut.
* Hieraŭ estis pli hele ol hodiaŭ. - Gestern war es heller als heute.

Einfache Beifügungen zum Verb solcher Sätze haben im Esperanto logischerweise die
Adverbendung -e. Beachte deshalb die Unterschiede:

* Estis bone. - Es war gut.
* Tio estis bona. - Das war gut. (tio = Subjekt)
* Estis bona afero. - Es war eine gute Sache. (Hier bezieht sich das Adjektiv „bona“ auf „afero“: eine gute Sache.)


### Die Bedingungsform (Konditional) der Verben

Die Bedingungsform (Konditional) der Verben wird durch die Endung -us gekennzeichnet:
estus - (es) wäre

Estus bone, se li venus hodiaŭ. - Es wäre gut, wenn er heute käme (kommen würde).
3.8

Zur Wortbildung:

Suffixe

* -il- (Mittel, um etwas zu tun; Instrument; Werkzeug):
  * skribi - schreiben → skribilo - Schreibgerät, Stift
  * aliĝi - sich anmelden → aliĝilo - Anmeldeschein
* -ant- (Person, die gegenwärtig eine bestimmte Tätigkeit ausführt):
  * skribi → skribanto - ein Schreibender
  * komenci - anfangen → komencanto - Anfänger
* -iĝ- (Ergänzung zur 3. Lektion, 3.7.): Diese Nachsilbe dient dazu, ursprünglich transitive Verben (d.h. Verben, die ein direktes Objekt verlangen) intransitiv (nicht transitiv) zu machen:
  * komenci: Mi komencas la retpoŝton. - Ich beginne die E-Mail.
    komenciĝi: La tago komenciĝas. - Der Tag beginnt.
  * fini: Ni finis la ekskurson marde. - Wir beendeten den Ausflug am Dienstag.
    finiĝi: La ekskurso finiĝis marde. - Der Ausflug endete am Dienstag.
  * renkonti: Mi renkontis ŝin. - Ich traf sie.
    renkontiĝi: Ni renkontiĝis. - Wir trafen uns.

Aus einem aktiven Handlungsträger, der (grammatisch gesehen) auf ein bestimmtes
Objekt einwirkt, wird also mit Hilfe von -iĝ- eine quasi verselbständigte Handlung, ein
Vorgang.

### Grammatik-Exkurs: Transitive und intransitive Verben

Verben sind sehr wichtig. Sie sind sozusagen der Motor eines Satzes, von dem fast alles
andere abhängt. Im Esperanto sind sie besonders wichtig und benötigen beim Lernen
mehr Aufmerksamkeit als Adjektive, Substantive oder Adverbien. Es geht dabei nicht um
die Zeitformen, sondern um die so genannte Transitivität. Was ist das?

* Intransitiv: Li - iras.
* Transitiv: Mi - trinkas - teon.

Ein transitives Verb benötigt zwei Verbindungen, um zu funktionieren. Ein intransitives
Verb benötigt eine Verbindung, um zu funktionieren. Die Grafik soll das veranschaulichen.
Im ersten Satz „Li iras“ (Er geht) ist gehen das Verb. Im zweiten Satz „Mi trinkas teon“ (Ich
trinke Tee) ist das Verb trinken. Man kann etwas (eine Sache) trinken, aber man kann nicht
etwas (eine Sache) gehen. Verben, wie trinken, sehen, sagen, die ein direktes Objekt - wie
hier im Beispielsatz Tee - „aufnehmen“ können, sind transitiv. Verben, die kein direktes
Objekt aufnehmen können, wie zum Beispiel gehen, stehen, reisen, sind intransitiv.
Auf Esperanto ist jedes Verb entweder transitiv oder intransitiv. Um korrekt Esperanto zu
sprechen ist es wichtig, sich bei jedem Wort zu merken, ob es intransitiv oder transitiv ist.

Beispiel:

„Komenci“ ist ein transitives Verb und heißt soviel wie „etwas anfangen“.
Im Satz „La tago komencas“ fehlt dem transitiven Verb komenci die zweite Verbindung
zum Glücklichsein. In dem Fall kann man das z.B. mit der Nachsilbe -iĝ- reparieren:
„La tago komenciĝas“ - wörtlich: „Der Tag beginnt sich.“

Alle Verben, die -iĝ- enthalten, sind nämlich intransitiv (s. 3.8).
In vielen Sätzen, bei denen zwei Substantive und ein transitives Verb in einem Satz
stehen, muss eins der Substantive im Akkusativ stehen, da die Wortreihenfolge im
Esperanto frei ist:

Lernanto prenas libron. / Libron lernanto prenas - Ein Schüler nimmt ein Buch
Soweit zu diesem kleinen Grammatik-Exkurs. Keine Angst, bei den meisten Verben kann
man mit etwas Nachdenken und Erfahrung leicht darauf kommen, ob es intransitiv oder
transitiv ist. Und wenn du mal einen Fehler machst, ist das überhaupt nicht schlimm! Jetzt
geht es weiter mit ein paar Übungen.

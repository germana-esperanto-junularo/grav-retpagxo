---
title: Übung - Mi devos atendi tutan monaton!
---

Ergänze die Endungen folgender Sätze bzw. Wortgruppen

1. Hieraŭ estis mard___.
2. Li venos lund____
3. Ĉiumerkred____ ni iras al nia patrino.
4. La tri___ libro de dekstre.
5. Li vizitis mian unu___  loĝejon.
6. Hodiaŭ estas la 31-___.
7. Li alvenis la 2-___ de septembro.
8. Ĉu ĉi tie iu estas pli bela ____ mi?
9. Ĉi tie estas tre laŭt____
10. Tio estas laŭt____ strato.
11. Mi ege bedaŭrus, se vi rest____ hejme.

Übersetze mit Hilfe der neuen Suffixe folgende Wörter

Übersetze mit Hilfe der in 3.8 neu eingeführten Suffixe die nachfolgenden Wörter. Die zu
verwendenden Wortstämme stehen in Klammern:

1. Informationsblatt (inform-)
2. Teilnehmer (partopren-)
3. sich anziehen (vest-), vgl. auch 3. Lektion, 3.7
4. Bestellschein (mend-)
5. sich amüsieren (amuz-)
6. sich interessieren (interes-)
7. Fragebogen (demand-)
8. der Sehende (vid-)

5. „MI DEVOS ATENDI TUTAN MONATON!“

* Peter, ĉu mi devas kunporti nur mian valizon? Aŭ ĉu ankaŭ dorsosakon?
* Ĉu vi intencas forveturi por unu semajno aŭ por tuta jaro?
* Dankon pro via granda helpo! Vi estas denove tre ĉarma. Do, mi prenos nur la dorsosakon kaj mansaketon (man-sak-et-o-n).

Klara pakas la necesajn aĵojn (aĵ-o-j-n) en la grandan bluan dorsosakon.

* Nenion forgesu, Klara.
* Ne.
* Nek la fotilon (fot-il-o-n).
* Ne.
* Nek dikan puloveron nek jakon por pli malvarmaj tagoj.
* Certe ne.
* Nek la ombrelon por la pluvo.
* Tutcerte (tut-cert-e) ne, mi ĵus metis ĝin en la sakon.
* Nek la sapon nek la dentobroson.
* Peter!
* Kaj nek la dormsakon … Kaj ĉu la naĝkostumon kaj mantukon vi jam enpakis?
* Jes, Peter. Ĉesu paroli, mi petas! Tuj ĉesu paroli!
* Mi ja nur volis helpi vin ...
* Kial vi ne veturas kun mi? Tiam ankaŭ mi povus helpi vin prepari la pakaĵon. Aŭ malhelpi, kiel vi. Tian helpon mi ne bezonas. Dankon.
* Mi estas iom malgaja, ĉar vi forveturos, kaj mi restos hejme, sola. Mi devos atendi tutan monaton!
* Vi devos atendi nur unu semajnon. Ĉu vi kredas, ke mi nenion forgesis?
* Ho, mi vidas amason da aĵoj en via granda sako. Vi certe nenion forgesis. Kiam ekveturos la trajno?
* Post du horoj.
* Do ni rapide veturu al la stacidomo!

### NEUE WÖRTER

Die Bedeutung dieser Wörter findest Du sicher wieder selbst:

monato, jaro, foto, pulovero,varma.

* bezoni - brauchen, benötigen
* blua - blau
* broso - Bürste
* certe - sicher(lich), bestimmt
* ĉesi - aufhören
* dento - Zahn
* dorso - Rücken
* dorsosako - Rucksack
* forgesi - vergessen
* fotilo (fot-il-o) - Fotoapparat
* helpi (+ Akk.) - helfen (jemandem)
* intenci - beabsichtigen
* jako - Jacke
* jaro - Jahr
* ĵus - soeben, gerade
* kiam - wann
* mansaketo - Handtasche
* mantuko - Handtuch
* meti - stellen, setzen, legen
* naĝkostumo - Badeanzug
* nek ... nek ... - weder ... noch ...
* nek (allein) - auch nicht
* nenio - nichts
* ombrelo - Regenschirm
* pakaĵo (pak-aĵ-o) - Gepäck
* paki - (ein-, zusammen-)packen
* paroli - sprechen
* pasporto - Ausweis, Pass
* pluvo - Regen
* prepari - vorbereiten
* preta - fertig, bereit
* sapo - Seife
* stacidomo - Bahnhof
* tiam (ti-am) - dann; damals
* trajno - Zug
* tuj - sofort
* valizo - Koffer
* varma - warm

Lösungen zu 4.

4.1

1. Hieraŭ estis mardo.
2. Li venos lunde.
3. Ĉiumerkrede ni iras al nia patrino.
4. la tria libro de dekstre.
5. Li vizitis mian unuan loĝejon.
6. Hodiaŭ estas la 31-a de majo.
7. Li alvenis la 2-an de septembro.
8. Ĉu ĉi tie iu estas pli bela ol mi?
9. Ĉi tie estas tre laŭte .
10. Tio estas laŭta strato.
11. Mi ege bedaŭrus, se vi restus hejme.

1. Informilo
2. Partoprenanto
3. Vestiĝi
4. Mendilo
5. Amuziĝi
6. Interesiĝi
7. Demandilo
8. Vidanto

## KONTROLLÜBUNGEN FÜR DEINEN MENTOR

## Schreibe folgende Zahlen aus:

a. 19
b. 28
c. 76
d. 133
e. 845
f. 1001
g. 1998
h. der dritte
i. der einhundertsechzehnte
j. viertens
k. elftens

### Schreibe eine E-Mail auf Esperanto!

Du möchtest an einer Esperanto-Veranstaltung in Budapest (Budapeŝto) teilnehmen.
Schreibe eine kurze E-Mail an die Veranstalter. Beachte die Briefform (Anrede, Abschied)
und benutze die nachfolgenden Wörter:

> legi, interreto, intenci, partopreni, aranĝo, lerni, deziri, paroli, eksterlanda, amiko, sendi, aliĝilo, informi.

Du kannst die Wörter in beliebiger Reihenfolge verwenden und die Wortarten verändern.

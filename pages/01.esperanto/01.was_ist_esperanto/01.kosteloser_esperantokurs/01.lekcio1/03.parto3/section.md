---
title: "Grammatik"
---

### Adjektive und Substantive

Alle Hauptwörter (Substantive) enden in der Einzahl (Singular) auf -o:

> benko, parko, amikino.

Alle Eigenschaftswörter (Adjektive) enden im Singular auf -a:

> bona, bela, nova.

Wie im Deutschen können Adjektive direkt bei dem Substantiv stehen, das sie näher
beschreiben („Ĝi estas bela muziko.“) oder von ihm durch das Tätigkeitswort (Verb) und evtl.
andere Wörter getrennt stehen („La muziko estas bela.“)

### Der Artikel „la“

Es gibt nur einen bestimmten Artikel: la. Das bedeutet auch, dass es im Esperanto nur ein
grammatisches Geschlecht gibt: {der, die, das} = {la}. Der Artikel ist unveränderlich:

* La knabino - Das Mädchen
* La benko - Die Bank
* La parko - Der Park

kun la knabino - mit dem Mädchen.
Einen unbestimmten Artikel gibt es nicht. Der deutsche unbestimmte Artikel (ein, eine) wird nicht
mit übersetzt:
Ein schöner Tag - bela tago.
in einem Kurs - en kurso.

### Verben

Alle Tätigkeitswörter (Verben) enden in der Grundform (Infinitiv) auf -i:

> iri (gehen), sidi (sitzen), nomiĝi (heißen).

In der Gegenwart (Präsens) der Verben entfällt die Infinitivendung -i, und an den Wortstamm
wird dafür die Gegenwartsendung -as angehängt. Diese Endung gilt für alle Personen der
Einzahl wie auch der Mehrzahl. Entsprechend einfach ist die Beugung der Verben, hier am
Beispiel von esti (sein):

* **Einzahl**
  * mi estas - ich bin
  * vi estas - du bist; Sie sind
  * li/ŝi estas - er/sie ist (für Personen)
  * ĝi estas - (er, sie) es ist (für Sachen)
* **Mehrzahl**
  * ni estas - wir sind
  * vi estas - ihr seid; Sie sind
  * ili estas - sie sind

Ebenso werden alle anderen Verben gebeugt, also: konsenti (einverstanden sein): mi
konsentas (ich bin einverstanden), vi konsentas (du bist einverstanden) usw. ...
Damit hast du auch gleich alle persönlichen Fürwörter (Personalpronomen) kennen gelernt.
Beachte dabei bitte, dass li und ŝi nur für Personen benutzt werden, dagegen ĝi für Dinge,
Abstraktionen, Tiere.
Wie zum Beispiel im Englischen, gibt es auch im Esperanto für die Höflichkeitsform kein
besonderes Personalpronomen. Sie entspricht der zweiten Person Singular (vi) oder Plural
(ebenfalls: vi). So kann also der Satz „Ĉu vi konsentas?“ übersetzt werden durch:
Bist du einverstanden? / Seid ihr einverstanden? / Sind Sie einverstanden?
Dabei kann sich die letzte Übersetzung auf eine oder mehrere Personen beziehen.
Entscheidend ist immer der Sinnzusammenhang.

### Bitte

Für die Übersetzung des deutschen „bitte“ gibt es zwei Möglichkeiten:

1. bonvolu + Verb im Infinitiv: Bonvolu veni kun mi. - Bitte komm / kommt / kommen Sie mit mir.
2. Oder einfach: Mi petas. - Ich bitte.

### Fragen

Alle Fragesätze müssen ein Fragewort enthalten:
Kiu vi estas? - Wer bist du?
Bei Entscheidungsfragen (Ja/Nein-Fragen), die im Deutschen nicht durch ein Fragewort,
sondern durch die Wortstellung gekennzeichnet werden, z.B.:
Sind Sie Eva? - Ja. / Nein.
steht im Esperanto vor dem Fragesatz das Fragewörtchen „ĉu“ (wörtlich „ob“):
Ĉu vi estas Eva? - Jes. / Ne.
Ĉu vi estas Eva aŭ Klara? - Eva. / Klara.
„Ĉu“ als Fragewort wird wie du siehst nicht übersetzt. Sieh es einfach wie ein Fragezeichen.

### Reihenfolge der Wörter im Satz

Zum Aufbau eines einfachen Satzes gibt es keine bindenden Festlegungen. Die Satzstellung im
Esperanto ist frei. Gebräuchlich ist jedoch folgende:
Subjekt - Prädikat ( - Objekt)
Das heißt, das Verb steht in der Regel nach dem Subjekt:
Mi atendas. Kiu vi estas? Ĉu vi iras kun mi?

### Fälle im Esperanto

Im Esperanto können Adjektive, Substantive und deren Pronomen (Fürwörter) in zwei Fällen
stehen. Zum Vergleich, im Deutschen gibt es vier Fälle:
den 1. Fall (Wer-Fall oder Nominativ)
den 2. Fall (Wes-Fall oder Genitiv)
den 3. Fall (Wem-Fall oder Dativ)
den 4. Fall (Wen-Fall oder Akkusativ).

Die beiden Fälle im Esperanto sind:
1. der Nominativ: das Satzsubjekt, nach dem mit „Wer oder was?“ gefragt wird (kiu aŭ
kio?). Wie im Deutschen haben die Wörter, die im Nominativ stehen, keine spezielle
Endung:
La bela knabino ... - Das schöne Mädchen ...
2. der Akkusativ beschreibt das direkte Objekt. Der Akkusativ im Esperanto ist sehr häufig
(aber nicht immer) mit dem Akkusativ im Deutschen (wen-Fall) identisch. Die
Ergänzungen erhalten die Endung -n:
Mi atendas amikinon. - Ich erwarte eine Freundin. (wen?)
„Mi“ ist das Subjekt, „amikinon“ ist das direkte Objekt. Das Subjekt ist immer der
Handlungsträger im Satz, der etwas mit dem Objekt tut.
Mi havas bonegan ideon. - Ich habe eine hervorragende Idee. (wen oder was?)

### Präpositionen

Wörter nach Präpositionen stehen grundsätzlich im Nominativ:
kun la amikino - mit der Freundin
en la bela parko - in dem schönen Park
sur benko – auf einer Bank
Mit manchen Präpositionen können sowohl Orte als auch Richtungen angezeigt werden.
Im Kino (wo? - Ort)
Ins Kino (wohin? - Richtung)
Um diese zwei Bedeutungen zu unterscheiden, folgt nach der Präposition die Endung -n, also
der Akkusativ, wenn in der Ergänzung eine Richtung angezeigt werden soll:
en la belan parkon – in den schönen Park
en kinejon – in ein Kino
sur benkon - auf eine Bank.
Mit al (nach) und el (aus) kann man ausschließlich Richtungen ausdrücken, weshalb die
Unterscheidung nicht notwendig (und auch nicht erlaubt ist). Deshalb kommt trotz Richtung
kein Akkusativ nach diesen Präpositionen.

### Endungen für verschiedene Wortarten

Die eindeutigen Wortartendungen ermöglichen es, durch einfachen Austausch der Endungen
die Wortart (und damit den Sinn) eines Wortes zu verändern:
bona - gut → la bono - das Gute
konsenti - einverstanden sein → la konsento - das Einverständnis
nomo - Name → nomi - nennen, benennen

### Besitzanzeigende Pronomen

So kann man auch problemlos aus den Personalpronomen (mi, vi ...) durch Anhängen der
Adjektivendung -a besitzanzeigende Fürwörter (Possessivpronomen) bilden:
Einzahl

### Mehrzahl

* mia - mein(e)
* nia - unser(e)
* via - dein(e); ihr(e)
* via - euer(eure); Ihr(e)
* lia/ŝia - sein(e)/ihr(e)
* ilia - ihr(e)

ĝia - sein(e), ihr(e)
Zu beachten ist, dass diese neu gebildeten Wörter alle Eigenschaften „normaler“ Adjektive
besitzen und bei Bedarf wie diese verändert werden müssen:
Mi atendas mian amikinon. - Ich erwarte meine Freundin. (Akkusativ)

### Das Silbensystem im Esperanto

durch ein umfangreiches System an Vor- und Nachsilben (Präfixe und Suffixe), die man jeweils
an den Anfang eines Wortes setzt (Präfixe) oder zwischen Wortwurzel und Endung einfügt
(Suffixe), können im Esperanto ebenfalls schnell und unkompliziert völlig neue Wörter gebildet
werden. Bisher kennen wir diese Suffixe:
-eg- verstärkt die Grundbedeutung:
bonega - sehr gut (von bona - gut)
atendegi - sehnsüchtig warten (von atendi - warten)
-ej- ergibt einen Ort:
atendejo - Warteraum (von atendi)
kinejo - Kino (von der Wortwurzel kino - Kinematographie)
-in- macht das Grundwort weiblich:
amikino - Freundin (von amiko - Freund)
fratino - Schwester (von frato – Bruder)
Solche Wortbildungssilben können meist auch selbständig gebraucht werden. Wenn man sie
mit der Endung einer Wortart versieht, bilden sie eigenständige Wörter:
ega - groß,
ejo - Ort
ino - Frau, weibliches Wesen, Weibchen (bei Tieren)

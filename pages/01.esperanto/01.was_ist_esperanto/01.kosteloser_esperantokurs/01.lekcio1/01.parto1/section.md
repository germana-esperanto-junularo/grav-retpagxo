---
title: "Grundlegendes"
---

## Aussprache, Groß- und Kleinschreibung

Esperanto kennt 28 Buchstaben:

a, b, c, ĉ, d, e, f, g, ĝ, h, ĥ, i, j, ĵ, k, l, m, n, o, p, r, s, ŝ, t, u, ŭ, v, z.

Jeder Buchstabe entspricht einem Laut, das heißt, er wird immer gleich ausgesprochen, ganz
egal, an welcher Stelle im Wort er auftaucht. Die Aussprache entspricht der des deutschen
Alphabets, unter Beachtung folgender Hinweise:

#### Vokale

Die Selbstlaute (Vokale) a, e, i, o, u werden halblang, e und o immer offen ausgesprochen (wie
Fenster und morgen). Jeder Vokal bildet die Grundlage für eine Silbe. Das Wort ideo (Idee)
hat also drei Silben: i-de-o.

Die Halbvokale j und ŭ bilden keine eigene Silbe. Sie werden immer zusammen mit einem
anderen Vokal, als kurzer i- oder u- Laut gesprochen:

j: kinejo (Kino): ki-nej-o (drei Silben).

ŭ: ankaŭ (auch): an-kaŭ (zwei Silben),

Eŭropo (Europa): Eŭ-ro-po (drei Silben). Hier ist das eŭ nicht wie im Deutschen als oj,
sondern als offenes e (=ä) mit kurzem, nachfolgendem ŭ zu sprechen.

#### Konsonanten

* c: wird immer als ts gesprochen, also wie das deutsche Z (caro - Zar).
* ĉ: wird als tsch gesprochen, wie in Tschüss (ĉaro - Karren, Wagen).
* ĝ: dsch, stimmhaft, wie in Gentleman (ĝangalo - Dschungel).
* ĥ: ch wie in Bach (jaĥto - Jacht).
* ĵ: sch, stimmhaft, wie in Genre oder Gelee (ĵurnalo – Zeitschrift).
* s: immer stimmlos, wie in Fass (savi - retten)
* ŝ: sch, stimmlos (ŝviti - schwitzen).
* r: wer kann, spricht es gerollt (revolucio - Revolution)
* v: stimmhaft, etwa wie in Vase (vazo).
* z: wird immer wie ein stimmhaftes s gesprochen (rozo – Rose, zumi - summen).

#### Zusammenstehende Buchstaben bewahren immer ihren eigenen Klang.

-st- wird niemals zu -scht- : starti (starten) → s-tarti mit -st- wie in Post und nicht wie Stein.
Ebenso bei -sp-: sperto (Erfahrung) → s-perto mit -sp- wie in raspeln und nicht wie in spielen.
Im Esperanto gibt es keine Nasale: manki (fehlen) → man-ki:
0.4

#### Betonung immer auf der vorletzten Silbe

Die Hauptbetonung der mehrsilbigen Wörter liegt stets auf der vorletzten Silbe:
Eŭ-ro-po, laŭ-ta (laut), re-li-gi-o (Religion), in-ter-na-ci-a (international).
0.5

#### Groß und Kleinschreibung

Außer den Wörtern am Satzanfang und Eigennamen werden alle Wörter klein geschrieben.

Genug der Vorrede! Schauen wir uns nun an, wie Peter versucht, mit einem ihm unbekannten
Mädchen ins Gespräch zu kommen:

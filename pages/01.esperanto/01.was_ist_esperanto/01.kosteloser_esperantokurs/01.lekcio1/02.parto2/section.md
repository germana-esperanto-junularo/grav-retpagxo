---
title: "Teksto - La bona ideo"
---

![en la parko](kek-1-1.mp3)

Peter iras tra la parko. Sur benko en la parko sidas bela knabino.* Hej! Saluton, Eva! Kiel vi fartas?

* Mi ne estas Eva, mi estas ... Sed kiu vi estas?
* Pardonu! Mia nomo estas Peter. Ĉu vi ne estas Eva?
* Mi ripetas: Mi ne estas Eva.
* Sed kiun nomon vi havas?
* Mi nomiĝas Klara. Kaj nun mi atendas mian amikinon.
* Ĉu Eva?
* Ne, ankaŭ mia amikino ne nomiĝas Eva.
* Strange! Ha, mi havas bonegan ideon! Bonvolu iri kun mi en kinejon. Mi petas! La nova filmo estas belega. Kaj kun bona muziko!
* Ĉu mi?
* Jes, vi. Sola aŭ kun la amikino. Ĉu vi konsentas?

Bevor Klara sich entscheidet, schauen wir uns die neuen Wörter und einige grammatische
Grundregeln etwas genauer an:

#### NEUE WÖRTER

* amikino - Freundin
* ankaŭ - auch
* atendas - (er-)warte,
* atendi - (er-)warten (Infinitiv)
* aŭ - oder
* bela - schön
* belega - sehr schön
* benko - (Sitz-)Bank
* bona - gut
* bonega - hervorragend, sehr gut
* bonvolu - bitte, sei (seid, seien Sie) so freundlich
* ĉu - (Frageeinleitung, wörtlich "ob")
* en - in
* esti - sein (Infinitiv), estas
* fartas - (es) geht (sich befinden)
* farti - sich befinden (Infinitiv)
* filmo - Film
* ĝuste - richtig
* havas - habe, hast, hat ...
* havi - haben (Infinitiv)
* ideo - Idee
* iras - geht (gebeugte Form im Präsens)
* iri - gehen (Infinitiv)
* kaj - und
* kiel (ki-el) - wie (Art und Weise)
* kinejo - Kino
* kio (ki-o) - was
* kiu (ki-u) - wer, welcher
* knabino - Mädchen
* konsenti - einverstanden sein (Infinitiv)
* kun - mit, gemeinsam mit
* la - der, die, das (bestimmter Artikel)
* mi - ich
* mia - mein
* muziko - Musik
* ne - nein, nicht, kein
* nomiĝas - heiße, heißt
* nomiĝi - heißen, genannt werden
* nomo - Name
* nova - neu
* nun - jetzt, nun
* pardonu - Verzeihung!
* parko - Park
* petas - bitte, bittest, bittet
* peti - (er-)bitten (Infinitiv)
* ripetas - wiederhole, wiederholst, ...
* ripeti - wiederholen (Infinitiv)
* saluton - Hallo
* sed - aber, sondern, jedoch
* ŝi - sie (weiblich)
* sidi - sitzen (Infinitiv), sidas
* sola - allein
* strange - seltsam
* sur - auf
* tra - durch (örtlich)
* veni - kommen
* vi - du, Sie, ihr
* via - dein, ihr, euer

#### Redewendungen:

Saluton! - Hallo! (wörtlich: einen Gruß)
Kiel vi fartas? - Wie geht es dir? / Wie geht es Ihnen? / Wie geht es euch?
Kiel vi nomiĝas? - Wie heißt du? / Wie heißen Sie? / Wie heißt ihr?
De kie vi venas? - Woher kommst du? / Woher kommt ihr? / Woher kommen Sie?
(Antwort: Mi venas el Berlino. Ich komme aus Berlin.)

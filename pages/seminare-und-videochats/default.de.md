---
title: 'Online-Seminare und Videochats'
date: '09:24 20-06-2020'
hide_git_sync_repo_link: false
process:
    markdown: true
    twig: true
hero_image: videochat.jpg
hero_align: center
---

### Online-Seminare und Videochats

Seit März finden regelmäßig Videochats und seit Mai auch Online-Seminare statt. 
In den Videochats wird gequatscht. In den Webinaren mehr informiert.
Alle sind gern eingeladen. 

[Hier](https://meet.jit.si/DEJgrupalvoko) geht es zu den angekündigten Zeiten zum Chatraum.

![](videochat.jpg)

Termine zu den kommenden Webschaltungen werden bald hier veröffentlicht.

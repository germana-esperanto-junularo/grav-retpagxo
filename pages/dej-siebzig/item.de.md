---
title: 'Die DEJ wird 70! Feiert mit!'
date: '08:55 14-09-2021'
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Liebes Mitglied der DEJ, liebes Mitglied des DEB,

der Tag des 70-jährigen Bestehens der Deutschen Esperanto-Jugend kommt näher! Am 4. Oktober 1951 wurde unser Verein in Westdeutschland gegründet. Es dauerte dann fast 40 Jahre bis zur Vereinigung mit der Esperanto-Jugend der DDR und somit zur Entstehung eines gesamtdeutschen Vereins.

Wir wollen die Gelegenheit nicht verpassen das zu feiern! Deshalb planen wir ein kleines Online-Event am Wochenende genau vor dem Jubiläumstag, also am 02. (Sa) und 03. (So) Oktober. Zusätzlich zur entspannten virtuellen Zusammenkunft planen wir eine Reihe von Kurzvorträgen und Diskussionsrunden über die Geschichte der DEJ, Erfahrungen ehemaliger Mitglieder und Traditionen der Esperanto-Bewegung. Das ganze soll an beiden Tagen zwischen 13:30 und 16:00 Uhr stattfinden.

Also halte dir ein paar Stunden frei und sei dabei! Die Veranstaltung wird auf [meet.jit.si/70JahreDEJ](https://meet.jit.si/70JahreDEJ) stattfinden.

Hier ein Überblick über das Programm (auf Esperanto):

> SABATO (02 OKT)
> 
> 13:30-14:00 Libera babilado  
> 
> 14:00-14:30 Kvizo pri GEJ-historio (Annika)  
> 14:30-15:00 Prelegeto: Esperanto-Junularo en GDR (Torsten)  
> 15:00-15:30 Prelegeto: Reunuiĝo kaj fruaj 90aj jaroj (Nils & Adrian)  
> 15:30-16:00 Diskutrondo: Kion GEJ povas signifi? (moderigote de Torsten)
> 
> DIMANĈO (03 OKT)
> 
> 13:30-14:00 Libera babilado
> 
> 14:00-14:30 Prelegeto: Kunlaboro kun aliaj asocioj (Ulrich)  
> 14:30-15:00 Prelegeto: Kunlaboro inter GEJ kaj GEA (Rudolf)  
> 15:00-15:30 Prelegeto: Pri KKRen kaj ISoj (Jan Christof)  
> 15:30-16:00 Diskutrondo kaj adiaŭo: Kio pri la estonteco?

Bis bald, dein DEJ-Vorstand,
Michael, Michaela, Jonas, Jan

![](IMG_20210913_194146_299.png)
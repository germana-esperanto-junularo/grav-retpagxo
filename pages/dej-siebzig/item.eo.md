---
title: 'GEJ iĝas 70! Festu kun ni!'
date: '08:55 14-09-2021'
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Saluton kara membro de GEJ/GEA,

alproksimiĝas la 70-jariĝo de la Germana Esperanto-Junularo! La 4an de oktobro 1951 nia asocio estis fondita en Okcidenta Germanio. Tamen poste daŭris preskaŭ 40 jarojn ĝis la unuiĝo kun la EO-junularo de GDR kaj tiel la estiĝo de tutgermania organizo.

La okazon por festi tion ni ne volas maltrafi! Tial ni planas malgrandan retan eventon dum la semajnfino tuj antaŭ la jubilea tago, do la 2an (sabato) kaj 3an (dimanĉo) de oktobro. Krom trankvila reta kunumado, ni planas okazigi serion de etaj prelegoj kaj diskutrondoj ĝuste pri la historio de GEJ, spertoj de iamaj aktivuloj kaj tradicioj de la EO-movado. Ni intencas okazigi la kunvenon inter la 13h30 kaj 16h00 dum ambaŭ tagoj.

La evento okazos virtuale ĉe [meet.jit.si/70JahreDEJ](https://meet.jit.si/70JahreDEJ).

Jen skizo de la programo:

> SABATO (02 OKT)
> 
> 13:30-14:00 Libera babilado  
> 
> 14:00-14:30 Kvizo pri GEJ-historio (Annika)  
> 14:30-15:00 Prelegeto: Esperanto-Junularo en GDR (Torsten)  
> 15:00-15:30 Prelegeto: Reunuiĝo kaj fruaj 90aj jaroj (Nils & Adrian)  
> 15:30-16:00 Diskutrondo: Kion GEJ povas signifi? (moderigote de Torsten)
> 
> DIMANĈO (03 OKT)
> 
> 13:30-14:00 Libera babilado
> 
> 14:00-14:30 Prelegeto: Kunlaboro kun aliaj asocioj (Ulrich)  
> 14:30-15:00 Prelegeto: Kunlaboro inter GEJ kaj GEA (Rudolf)  
> 15:00-15:30 Prelegeto: Pri KKRen kaj ISoj (Jan Christof)  
> 15:30-16:00 Diskutrondo kaj adiaŭo: Kio pri la estonteco?

Amike, via GEJ-esrtraro
Michael, Michaela, Jonas, Jan 

![](IMG_20210913_194146_299.png)
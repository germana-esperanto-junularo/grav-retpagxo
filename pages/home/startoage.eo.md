---
title: Startseite
hide_git_sync_repo_link: false
process:
    markdown: true
    twig: true
cache_enable: false
debugger: false
recaptchacontact:
    enabled: false
hero_classes: 'text-light title-h1h2 parallax overlay-dark-gradient hero-large'
hero_image: jes-2017.jpg
hero_align: center
---

<div class="container">
  <div class="columns">
	<div class="column col-sm-12 col-md-8"> 
        	<h4>Aktualaĵoj</h4>
            {% include 'partials/blog-list-item.html.twig' with {blog: page, page: page.find('/unsere_treffen/kekso/anmeldung')} %}	
       		{% include 'partials/blog-list-item.html.twig' with {blog: page, page: page.find('/unsere_treffen/gej_semajnfino')} %}
       		{% include 'partials/blog-list-item.html.twig' with {blog: page, page: page.find('/dej-siebzig')} %}
	</div>
	<div class="column col-sm-12 col-md-4">
        <h4>Novaĵoj el la membrogazeto</h4>
        <p>Pliajn artikolojn vi trovos <a href="kune">ĉi tie</a></p>
        	{% for post in page.find('/kune').children.order('date', 'desc').slice(0, 3) %}
        <div class="card" style="margin-bottom: 10px">
            {% set image = post.media.images|first %}
            {% if image %}
            <div class="card-image">
                <a href="{{ post.url }}">{{ image.cropZoom(800,400).html|raw }}</a>
            </div>
            {% endif %}
            <div class="card-header">
                <div class="card-subtitle text-gray"></div>
            </div>
            <div class="card-title">
                {% if post.header.link %}
                <h5 class="p-name mt-1">
                    {% if post.header.continue_link is not same as(false) %}
                    <a href="{{ post.url }}"><i class="fa fa-angle-double-right u-url"></i></a>
                    {% endif %}
                    <a href="{{ post.header.link }}" class="u-url">{{ page.title }}</a>
                </h5>
                {% else %}
                <h5 class="p-name mt-1"><a href="{{ post.url }}" class="u-url">{{ post.title }}</a></h5>
                {% endif %}
            </div>
            <div class="card-body">
                {% if post.summary != post.content %}
                    {{ post.summary|raw }}
                {% else %}
                    {{ post.content|raw }}
                {% endif %}
            </div>
            <div class="card-footer">
                {% include 'partials/blog/taxonomy.html.twig' %}
            </div>
        </div>
        {% endfor %}
    </div>
  </div>
</div>
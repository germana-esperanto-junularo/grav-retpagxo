---
title: 'Ekde nun eblas aliĝi al JES 2021/''22 en Wittenberg'
date: '18:49 20-06-2021'
taxonomy:
    category:
        - Renkontiĝoj
    tag:
        - JES
        - esperanto
        - junularaesemajno
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Por pliaj informoj pri JES 2021/'22 vi povas viziti [la retejon de la evento](http://jes.pej.pl).

Se vi aliĝas jam nun, vi profitos de preze favora aliĝkotizo. Simple plenigu [la aliĝformularon](http://jes.pej.pl/eo/jes-2021/alighu/).
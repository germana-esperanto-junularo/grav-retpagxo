---
title: 'Du kannst dich jetzt zum JES 2021/''22 in Wittenberg anmelden!'
media_order: WittenbergLuthers_Hochzeit.jpg
date: '18:49 20-06-2021'
taxonomy:
    category:
        - Renkontiĝoj
    tag:
        - JES
        - esperanto
        - junularaesemajno
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Für weitere Infos zum JES 2021/'22 kannst du auf [der Webseite des Events](http://jes.pej.pl) vorbeischauen.

Wenn du dich schon jetzt anmeldest, profitierst du von einem günstigeren Teilnahmebeitrag. Füll dazu einfach [das Anmeldeformular](http://jes.pej.pl/eo/jes-2021/alighu/) aus.
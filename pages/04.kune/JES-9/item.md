---
title: JES impresoj
menu:  JES impresoj
date:  05-03-2018
slug:  jes-9
taxonomy:
    tag: [JES, Szczecin, renkontiĝoj]
    category: JES
author:
    name: [Jonas Marx, Saci, Alina Brücker, Cornelia Luszczek]
---

### Jonny M (Jonas Marx)

La 9a JES en sĉeĉino denove havis tiun senton, kiun mi amas. Multaj amikoj el diversaj landoj venis kaj mi bezonus monaton por resti sufiĉe da tempo kun ĉiuj amikoj, kiuj venis. Bedaŭrinde la programo estis sufiĉe ĥaosa, tiel gravaj programeroj estis samtempaj aŭ la homoj eĉ ne sciis pri ili. Mi ankaŭ volus plendi pri la manĝo, sed kiel mi skribis en unu de miaj kantoj: "...la manĝo ne taŭgas por la ventro, ne plendu sed helpu organizi la venontan eventon". Do se vi samopinias, ni komune certigu ke la manĝaĵo por la jubilea, deka JES en germanio estu bonkvalita! Mi ege antaŭĝojas jam venontan novjaron kaj deziras al ĉiuj bonan jaron , ĝis ni denove revidos.

![Grupo](image://jes-2017.jpg)

### Saci (Sarolta Bagó)

La ĉi-jara JES estis sufiĉe kaose organizita, tamen mi ĝuis. Eĉ se mi iom nervoziĝis pri manko de ĝustatempa informado, pri la post-lastmomente anoncitaj programoj, pri la translokiĝo al nova, maltaŭga ejo... kaj iom pli, la JES temis por mi pri renkonti miajn karajn amikojn. Kaj eĉ se ne ni ĉiuj sukcesis loĝi en la sama ejo, la plejparto ja jes, do ni ĝuis avantaĝojn de la "ĉio sub la sama tegmento". Malgraŭ mia plendemo pri la JES mi dankemas al la organiza teamo, ĉar ili kreis okazon por festi kun tiuj, kiuj gravas al mi. Certe estis mia plej bela novjarfesto.

### Alina Brücker

![Alina](image://alina.jpg)

Jen mia JES: Estis tre amuza renktontiĝo, mi ĝojis pri la multaj novuloj kiujn mi povis ekkoni, la bonega etoso kaj la diverseco de interesaj programeroj! Oni bonege povis festi, mi ankoraŭ bone memoras la antaŭsilvestran nokton, dum kiu mi festis ĝis la sesa. Bedaŭrinde mi ne estis tiom motivita dum la silvestra nokto mem, ĉar multaj aferoj prokrastiniĝis, la bongusta bufedo rapide malplenis kaj en la nokto la muziko ne vere plaĉis al mi kaj restis samstile... Mi proponas ŝanĝon de DĴo aŭ diversstilajn kantojn, ne malfacilus lasi la partoprenantojn kontribui al la kantarlisto. Krome mi ŝatis la ejon, kvankam estis iom malvarma kaj laŭta.

### JES 2017 estis malaĉa

> de Cornelia Luszczek

Dum la pasinta JES, mi estis samtempe partoprenantino en ERASMUS-trejnado. Ties celo estas eduki esperantoinstruistojn. Mi timegis la trejnadon antaǔe, sed mi rapide konstatis, ke tio tute ne necesis. Ĝi ne estas nur intensa, sed ankaǔ interesa, kaj dank al la kunpartoprenantoj sufiĉe amuza. Ni laboris, diskutis, kantis kaj ridis en agrabla atmosfero. La temoj estis ekzemple krei taskojn por gelernantoj, eraroj kiujn instruistoj povas fari, ,memfida apero kaj kiel konduti en iom malfacilaj situacioj.

La JESa programo (de kiu mi preskaǔ nenion faris pro la tempo-intensa trejnado) inkludis prelegojn ekz. pri genra egaleco, poemoj, promenon al la urbo centro, poemoj kaj danco lecionojn. Granda novaĵo estis patatajumado-kurso. Neniam mi aǔdis pri tio! Mi diros nenion ĉar oni devas vidi tion mem.

Tamen, mi supozas ke la plej bonaj momentoj kaj konversacioj ĉiam okazas aparte de la programo. Ĉefe malfrue vespere en la drinkejo aǔ gufujo. Dum tiu-ĉi JES, mi dormis en amasloĝejo por la unua fojo. Ni estis proksimume dek personoj en normala klasĉambro. Estas nerekomendebla por ĉiuj kiuj ŝatas bone kaj sufiĉe dormi. Sed ni amuziĝis kiam ni desegnis sur la tabulon, uzis plastajn butelojn kiel keglojn kaj havis profundajn konversaciojn.
Eĉ la plej grava tago, la novjarfesto kun la nokta bufedo, malaperis en la fono inter ĉiuj aferoj, kiuj mi lernis kaj travivis dum la semajno!

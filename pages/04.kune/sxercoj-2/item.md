---
title: 'Kune ridi'
date: '14:00 30-09-2020'
taxonomy:
    category:
        - Artikolo
    tag:
        - esperanto
        - ŝercoj
        - ridado
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

1.
Instruisto: Do Tom vi eliras la pordon nun, neniu zorgas pri via parolado!
Studento: Tiam vi povas sekvi tuj!
(Lehrer: So Tom du gehst jetzt vor die Tür dein Gelaber interessiert keinen! 
Schüler: Dann können sie ja gleich mitkommen!)

2.
La kronviruso estas malĝuste traktata. Ĝi devas esti diarea malsano. Kial alie homoj bezonas tiom da neceseja papero?
(Das Coronavirus wird falsch behandelt. Es muss sich hier um eine Durchfallerkrankung handeln. Warum sonst brauchen die Leute so viel Klopapier?)

3.
Kial abeloj zumas? Ĉar ili ne konas la tekston.
(Warum summen Bienen? Weil sie den Text nicht können.)

4.
Ne estu malfeliĉa!
Se birdo fekas sur vian kapon, ne malĝoju. Ĝoju, ke hundoj ne povas flugi.
(Wenn Dir ein Vogel auf den Kopf scheißt, sei nicht traurig. Freue Dich, dass Hunde nicht fliegen können.)

Aŭtoro: Jonas
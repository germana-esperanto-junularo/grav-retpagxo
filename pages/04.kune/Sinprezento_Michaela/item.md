---
title: Sinprezento - Michaela Stegmaier
menu:  Sinprezento - Michaela Stegmaier
date:  02-03-2017
slug:  sinprezento_michaela
taxonomy:
    tag: [personoj, esperantistoj]
    category: Personoj
author:
    name: Michaela Stegmaier
---

Saluton karaj membroj, mi nomiĝas Michaela, havas 23 jarojn kaj nun estas la prezidantino de GEJ. Mi naskiĝis kaj kreskis en Herzberg am Harz – la Esperanto-urbo, kie mi en la jaro 2007 komencis lerni Esperanton en libervola kurso en mia gimnazio. Post nur unu jaro da lernado mi vojaĝis al IIK en Roterdamo kaj partoprenis en SALO, la ĝemelurba interŝanĝo de junuloj el Herzberg kaj Góra en Pollando.

![Michaela](image://michaela.jpg)

Ekde tiam mi vojaĝis ĉiun jaron al renkontiĝoj por uzi la lingvon kaj ekkoni homojn el la tuta mondo. En IJK kaj JES mi unue partoprenis en 2015. Post longa tempo, en kiu la ICH (Interkultura Centro Herzberg) sendis min al aranĝoj, mi komencis mem organizi veturojn kun etaj grupoj de junuloj el Herzberg. Nun mi kunorganizas renkontiĝojn, ĉefe JES, por vi kaj junuloj el tuta Eŭropo kaj mi ĝojus ekkoni multajn de vi dum niaj aranĝoj.

En la jaro 2013 mi komencis studi naturajn sciencojn en Braunschweig. En Februaro mi finis mian bakelaŭran studperiodon kaj komencis la duan kun Erasmus+-semestro en Pollando. Tio ebligis viziti multajn etajn aranĝojn en la ĉirkaŭaĵo.Mi kontaktiĝis kun membroj de Varsovia vento, E@I, SKEJ, ĈEJ ktp. Mi ĝuas la kunlaboron kun la aliaj estraranoj, kaj mi volas danki al Annika kaj Jarno, kiuj multe laboris por GEJ antaŭe kaj daŭre ĉiam volonte subtenas nin. Se vi havas demandojn, ideojn aŭ ŝatus publikigi artikolon, sinprezenton aŭ vian plej ŝatatan recepton (germane aŭ esperante) vi povas [kontakti min](mailto:michaela[punkt]stegmaier[at]esperanto[punkt]de).

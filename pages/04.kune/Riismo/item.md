---
title: Riismo
menu:  Riismo
date:  21-12-2014
slug:  riismo
taxonomy:
    tag: [lingvo]
    category: Artikoloj
author:
    name: Redakcio
---

### Fantomo hantas en Esperantujo — la fantomo de riismo.

Kie estas tiu junulara renkontiĝo, kiu de siaj regantaj kontraŭuloj ne
estas misfamigita kiel feminisma? Kie estas la esperanta muzikisto, kiu
la stigmatizan riproĉon de egalecemismo ne reĵetis kaj al la pli
progresintaj opoziciuloj kaj al siaj reakciaj kontraŭuloj?

Estas urĝa tempo, ke la riistoj antaŭ la tuta mondo malkaŝe prezentu
siajn vidpunktojn, siajn celojn; siajn tendencojn kontraŭmetante al la
fantomfabelo pri la riismo.

### Kio do estas riismo?

Temas pri koncepto celanta forigi malneŭtralecon kaj malsimetrion el nia
lingvo por esti pli inkluziva.

### Kiel ĝi funkcias?

En riisma Esperanto oni uzas la genre neŭtralan pronomon *ri*.

Kelkaj riistoj opinias, ke la pronomoj *ŝi* kaj *li* tute ne estu
uzataj; pensa kaj lingva divido de la homaro en du grupojn ne necesas
kaj neeviteble kunportas genran hirarkion kaj pensajn limojn.

Aliaj homoj uzas *li* nur kiel vere viran formon kaj *ri* parolante pri
hipotezaj homoj (*se iu havas demandon al mi, ri sendu al mi botelan
mesaĝon)* aŭ* *en kazoj, en kiuj la genro de la priparolata persono ne
estas konata *(“mi enamiĝis” – “ĉu mi konas rin?”).* Homoj, kies genra
identeco estas alia ol ina aŭ vira, kutime deziras esti adresataj per la
pronomo *ri*.

Krome estas enkondukata la sufikso *-iĉ-,* kiu signifas viran genron kaj
estas uzata simetrie al *-in-*.

O-vortoj sen sufikso *-in-* aŭ *-iĉ-* ĝenerale ne indikas sekson.

“Ĉu vi havas fratojn?” – “Jes, unu grandan fratiĉon kaj du pli
malgrandajn fratinojn.”

### Kial uzi ĝin?

Genra malsimetrio estas unu el la plej kritikataj aspektoj de Esperanto.

Unue, ĝi kontraŭas la neŭtralecon kiel idealon de nia lingvo, miksante
ideojn de neŭtrala kaj vira genro. La vorto *esperantisto* do estas kaj
vira kaj neŭtrala, dum virina genro estas traktata kiel aldonaĵo. Uzi
virajn formojn neŭtrale estas klare rezulto de pensmaniero kiu vidas en
viroj pli gravan aŭ pli bazan genron.

Tamen, aliaj o-vortoj, ekz. *patro*, en neriisma Esperanto estas tute
viraj, aliaj neklaritaj, ekz. *kuracisto*. Tio povas kaŭzi
miskomprenojn, kiam unu persono komprenas la vorton *kuracisto* kiel
viran, alia persono kiel neŭtralan formon.

Per enkonduko de du neologismoj tiaj problemoj povas esti solvataj.

Krome la riismo lingve inkluzivas genrajn minoritatojn, ne devigante
ilin elekti aŭ virajn aŭ inajn formojn per kiuj ili estu aludataj.

### Bone, sed kiu uzas la riismon?

La koncepto de -*iĉ-* kaj *-ri-* estas proponata kaj uzata diversloke
jam delonge, ekzemple en la revuo “Sekso kaj Egaleco”(ekde 1979). En
1994 kelkaj esperantistoj interrete publikigis la dokumenton “Riisma
Esperanto”, kiu ofte estis nomata la manifesto de la riismo. Kelkaj
verkoj jam skribiĝis strikte laŭ riismaj reguloj, ekz. “Sur la linio” de
Jorge Camacho. Kelkaj muzikistoj kiel “La Perdita Generacio” kaj
Guillaume Armide skribas kantotekstojn laŭ la reformo.

La estraro de la GEJ ĉi-printempe decidis uzi la sufikson -*iĉ-* laŭ
propono de Paul Würtz.

Kaj, plej grave, pli kaj pli da homoj ekscias, interesiĝas kaj diskutas
pri ideoj - multaj jam parolas riisme.

Kion opinias vi? ĉu tro komplikas, senutilas, ĉu vi vidas konfliktojn
kun la fundamento, aŭ ĉu vi jam delonge uzas riismon? ĉu *ri* laŭ vi
estu uzata aldone al la nuna pronoma sistemo, aŭ alternative? Aŭ ĉu vi
havas tute alian proponon? Skribu al [kune](mailto:kune[at]esperanto[punkto]de)!

---
title: Donacetoj subtenas amikecon
menu:  Donacetoj subtenas amikecon
date:  21-11-2015
slug:  donacproponoj
taxonomy:
    tag: [kristnasko, donacoj]
    category: Artikoloj
author:
    name: GEJ Vorstand
---
Ĉu vi ankaŭ konas tion? Estas nelonga tempo ĝis kristnasko aŭ ĝis iu
naskiĝtago, kaj vi tute ne havas ideon kion doni al via Esperantista
amiko. Ne senesperiĝu! Ĉi tie vi trovos kelkajn proponojn kion donaci,
ne nur por la kristnaska festo.[^1]

1.  La klasikaĵoj

    Memfaritaĵoj kaj libroj ĉiam funkcias – eble speciale ĉe viaj verdaj
    amikoj kiuj aprezas diversecon, individuecon kaj, nu, lingvumadon.
    Ĉi tie mi ne proponu verajn specifaĵojn, ĉar kompreneble ege
    dependas de propraj ŝatoj kaj de via rilato al la donacricevonto: Vi
    povas pentri, brodi, triki, verki, faldi, kaj multe pli. Rilate al
    libroj, vi trovas kelkajn rekomendojn en la eldono 2015/4 de la
    *Esperanto aktuell*, kaj ekzemple la libroservo de la UEA ofertas
    specifan retpaĝon pri novaj varoj[^2], dum ĉe la libroservo de la
    GEA vi povas vidi liston de libroj haveblaj je reduktitaj
    prezoj[^3].

2.  La mondo

    Esperanto estas (kaj ĉiam estu pli) tutmonda lingvo. Kaj pro tio
    multaj Esperantistoj ŝatas vojaĝi al diversaj lokoj kaj informiĝi
    pri fremdaj landoj. Por tio, termapoj ĉiam estas bona ideo. Unu vere
    uzebla estas la *Gratmapo*[^4] (*Scratch Map*): Vojaĝemuloj povas
    forgrati aŭrofolian tavoleton de la jam vizititaj lokoj kaj aperigi
    la kolorojn sub tiu tavoleto, samtempe registradante kiom ili jam
    vidis de la tero.

    Dua mojosa mapo estas la *Tutmonda Metroomapo*[^5]* *(*World Tube
    Map*): Ĝi haveblas en diversaj grandecoj kaj montras la urbojn de la
    mondo kiel imagitan metroosistemon. Tio do estas bonkonata stilo
    bele aplikita.

3.  Lingvoj

    Eble viaj Eperantistaj amikoj ankaŭ ŝatas lerni aliajn lingvojn? Kio
    do pri aĉeti por ili lingvokurson aŭ lernolibron? Ĉi tie ekzistas
    ege multaj opcioj, se vi nur scias pri kiu lingvo temu ekzakte. ;)
    Tie helpas nur demandi!

    Mi opinias ke specife se la objekto de lernemo estas iu konstruita
    lingvo, vi povos ĝojigi homon per tiuspeca donaco – vidu ekzemple la
    *Vortaron de la Klingona*[^6] aŭ la libron *Vivanta Lingvo: La
    Dotrakia*[^7] (*Living Language: Dothraki*).

4.  ..Ĝirafoj?

    Tio eble ŝajnas senrilata, sed certe vi kaj la pridonacotulo konas
    ĉies plej ŝatatan Esperantan vortludon: *Kial ĝirafo neniam estas
    sola? ––– Ĉar ĝi havas kolegon!* Nia lingvo vere havas rilatojn al
    ĉiuj ajn aspektoj de l' vivo. Kaj troveblas multaj belaj aŭ utilaj
    donaceblecoj ĝirafoformaj! Specife mi povas proponi la *Trinkujon
    Ĝirafblekan*[^8] (*Giraffe Moo*) aŭ – se vi ne hezitos elspezi iom
    pli da mono – la *Ĝiraflibroapogilojn*[^9] (*Giraffe Bookends*).

5.  Kaj fine…

    …vi povas fari donacon tre specialan, pleje ĉar ĝi estas daŭra
    donaco: membreco en la Germana Esperanto-Junularo. Tiu mebreco
    havas, kiel vi verŝajne scias, kelkajn avantaĝojn; interalie la
    dumonatan ricevon de via *kune.* La kotizojn vi trovos
    interrete.[^10] Ĝojon do disdonu per tiu bonege Esperantisma faro!

Nun nur restas al mi deziri al vi bonajn tagojn kaj multan ĝojon al ĉiu
donacanto kaj donacricevanto!

[^1]: Kun la obligacia rimarko ke vere nur estas proponoj; nek mi nek iu
    alia el la *kune*-teamo profitas finance aŭ alie se vi aĉetas iun el
    la specife nomataj eksteraj artikloj. Nun ĝoju la legadon :)

[^2]: e-lingva: http://katalogo.uea.org/index.php?st=novaj

[^3]: e-lingva:
    http://www.esperanto-buchversand.de/kategorien/je\_reduktitaj\_prezoj.html

[^4]: anglalingva:
    https://www.luckies.co.uk/gift/scratch-map-personalised-world-map-poster

[^5]: anglalingva: http://www.firebox.com/product/6205/World-Tube-Map

[^6]: germanlingva:
    http://www.buecher.de/shop/star-trek/star-trek-das-offizielle-woerterbuch-ebook-epub/okrand-marc/products\_products/detail/prod\_id/38584910/

[^7]: anglalingva: http://www.livinglanguage.com/dothraki

[^8]: anglalingva:
    http://yas-ming.mysupadupa.com/collections/moo-cups/products/moo-cup--3

[^9]: anglalingva:
    https://www.etsy.com/listing/92351476/giraffe-metal-art-bookends-free-usa?ref=shop\_home\_active\_3

[^10]: germanlingva: http://www.esperanto.de/de/dej/mitgliedschaft

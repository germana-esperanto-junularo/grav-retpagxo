---
title: Esperanto-tendumado en la Domeyer-parko en Herzberg
menu:  Esperanto-tendumado en la Domeyer-parko en Herzberg
date:  01-04-2018
slug:  esperanto-tendumado-2018
taxonomy:
    tag: [tendumado, Herzberg, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Michaela Stegmaier
---

Skolta tendumado en Herzberg dum Pasko Karaj membroj, mi ŝatus inviti ĉiujn el vi, kiuj ne iras al IJF dum pasko, tiujn kiuj ŝatas tendumi kaj interkonatiĝi kun la Verdaj Skoltoj por fari mojosajn aktivaĵojn, sidi ĉirkaŭ fajro, kuiri mem kaj iam viziti la Esperanto-urbon Herzberg am Harz.

![Tendantoj](image://skoltoj.png)

Inter la 28-a kaj 30-a de marto okazos Esperanto-kurso kaj ekde la 31-a la Verdaj Skoltoj alvenos, kaj ni daŭrigos la tendumadon kune. Ili restos ĝis la 5-a de aprilo, sed ekde la 4-a rekomenciĝos la lernejo. Do se vi interesiĝas, bonvolu skribi al mi kaj diri, kiom longe vi povas partopreni. Ankaŭ se vi bezonas helpi plani vian vojaĝon, aŭ havas pliajn demandojn, kontaktu: michaela (punkt) stegmaier (at) esperanto (punkt) de

---
title: 'Pri mia vivstilo, klimatprotektado kaj ligo al Esperanto-movado'
media_order: 'fs_banner400x177.jpg,bemi2.gif,IFEF.jpg,FAIRKAUF-Logo.png'
date: '16:00 30-06-2021'
taxonomy:
    category:
        - Artikoloj
    tag:
        - vivstilo
        - klimato
        - daŭripovo
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
recaptchacontact:
    enabled: false
---

Grava kaj laŭ mi ĉiam pligraviĝanta temo estas, kiel la homaro povas vivi sur la terglobo sen komplete detrui ĝin por ontaj generacioj aŭ eĉ por nun kun ni vivantaj bestoj kaj plantoj.

En mia ĉiutaga vivo mi ofte pripensas, kiel eblas plibonigi proprajn kutimojn por daŭripove agi kaj vivi. Mi povas fiere diri, ke mi ne posedas aŭton kaj veturas per biciklo al mia laborejo. Kaj pro la amaso de rubaĵoj, kiujn mi preterpasas survoje, mi komencis ĉiutage kunporti kelkajn pecojn de rubaĵoj ĝis la sekva rubujo. Bone, mi estas skolto kaj mi diras: Unu bona ago ĉiutage helpas por postlasi la mondon pli bona ol mi trovis ĝin. Sed ne por ĉiuj ekzistas tiu ebleco loĝi sufiĉe proksime al la laborloko. Kaj mi ne nur konas la vivon en granda urbo, kie ĉiuj gravaj lokoj estas en bicikle atingeblaj distancoj, sed ankaŭ la vivon en eta urbo, kie viaj ebloj sen aŭto estas ilimigitaj.

![fs_banner400x177](fs_banner400x177.jpg "fs_banner400x177")

Krome mi pensas, ke mi estas en epizodo de mia vivo, en kiu ne bezonatas aŭto por povi rapide atingi multajn lokojn sinsekve ekz. por organizi hobiojn de familianoj. Komprebeble foje mi bezonas helpon de konatoj kun aŭto por povi transporti grandajn objektojn, sed por normalaj aĉetumadoj mi nur bezonas biciklajn sakojn. Espereble ankoraŭ longe mi ne bezonos aŭton, tio ja ankaŭ ŝparas monon, ne nur emisiojn.

Mia plej nova projekto estas “Foodsharing” (foodsharing.de) – asocio, kiu helpas al (super-)bazaroj, restoracioj, bakejoj ne forĵeti troajn ne venditajn manĝaĵojn, kiujn ankaŭ la helporganizoj por nepagipovaj ne povas plene disdoni. Mi rete registriĝis kaj, por povi savi manĝaĵojn, devis lerni kelkajn regulojn, trapasi kvizon kaj lerni pri la etiketo por rajti savi manĝaĵojn en superbazaro. Jam pli longe mi konis tiun movadon kaj profitis de amikino, kiu antaŭe jam komencis savi. Oni kelkope dividas la varojn, forportas ilin ekzemple per bicikla remorko, prenas de tio, kiom oni povas foruzi aŭ manĝi, antaŭ ol ĝi nemanĝebliĝas, kaj plu disdonas la reston. Ĉio devas senmone okazi kaj kiel savanto oni transprenas la respondecon pri la kvalito de la varoj. Ĉiukaze oni helpas malpligrandigi la amason de senmakulaj, bonkvalitaj kaj ankoraŭ freŝaj manĝaĵoj, kiuj pro miskalkuloj de aĉetantoj aŭ pro alveno de novaj varoj estus forĵetataj en la rubujon. Laŭ mi tre inda celo, por kiu mi kunaktivas.

![bemi2](bemi2.gif "bemi2")

Krome mia koramiko kaj mi planas dum niaj ferioj veturi bicikle dum pluraj tagoj kaj revoje uzi etajn trajnojn, kiel Esperanto-parolantoj kutimas karavane atingi ne tro forajn renkontiĝojn. Ĉu mi nun iĝu ano de BEMI (bemi.tejo.org) kaj IFEF (ifef.free.fr)? Indas pripensi tion, ĉu ne?

Kiel oni povas kombini klimatprotektadon kaj Esperanton, ni ankaŭ diskutis dum KEKSO. Paula opinias: “Klimata justeco kunligas klimatan kaj median protekton kun aliaj aferoj kiel homaj ratoj, socia justeco, mondvasta justeco inter la landoj, feminismo kaj paco. Ĉar oni ne povas trakti tiujn aferojn sole, oni devas trovi komunan solvon. Por atingi klimatan justecon, oni devas retiĝi, oni devas labori kune. Esperanto ebligas la konektadon de homoj mondvaste. Tiel la retiĝo inter la homoj facilas kaj labori por nia unusola mondo pli bone funkcias. Dum Esperanto-renkontiĝoj oni povas interŝangi ideojn, informojn kaj projektojn pri kiel atingi pli da klimata justeco.”

![IFEF](IFEF.jpg "IFEF")

Kaj Annika mencias: “Per Esperanto ankaŭ eblas havi pli rektajn informojn pri la konsekvencoj de klimatŝanĝiĝo. Oni povas rekte paroli kun Esperantistoj en aliaj mondregionoj, kie jam pli sperteblas la ŝanĝiĝo ol ĉi tie. Kaj por vojaĝi klimatneŭtrale oni povas per trajnkaravano iri al Esperanto-renkontiĝo. Tiel oni jam dum la vojaĝo renkontas Esperantistojn kaj povas ĝui la kongresecan etoson.” – “Ankaŭ eblas biciklokaravano de BEMI, ekzemple al IJK”, aldonas Paula.

Do kiam denove eblos viziti Esperanto-amikojn, mi klopodos uzi biciklon kaj trajnon por vojaĝi kaj dum parolado scivolos pri iliaj spertoj kun klimatŝanĝiĝo kaj iliaj kutimoj. Kaj ĉu ne estas tiel, ke en la Esperanto-movado (almenaŭ mi konas tion de junularaj aranĝoj) mutaj homoj manĝas vegetare aŭ vegane (ekz. dum KEKSO ni kutime manĝas vegane)? Eble ankaŭ ili nur mendas vegetaran manĝon de la ejo por ne ricevi viandaĵojn ĉiutage. Tiel mi ofte faris. Dum la fastotempo ĉi-jare mi manĝis tute vegetare kaj viando ne mankis al mi kaj mi ekkonis denove multajn bongustajn vegetarajn pladojn. Intertempe mi ne plu aĉetas viandaĵojn. Kelkfoje, kiam neniu volas preni de mi savatan viandon de Foodsharing, mi manĝas tion. Pli bone manĝi ol forĵeti! Kaj ankaŭ la viandon de sovaĝaj bestoj (aproj aŭ kapreoloj), kiujn ĉasis la patro de mia koramiko, mi ne rifuzas. Ĝi ja havas tute alian kvaliton kaj originon ol ekz. porkaĵo el amasproduktejo.

![FAIRKAUF-Logo](FAIRKAUF-Logo.png "FAIRKAUF-Logo")

Vestaĵojn mi prefere ne aĉetas en la kutimaj butikoj sed en socialaj vendejoj, kie jam portitaj vestaĵoj estas malmultekoste vendataj de handikapuloj (ekz. fairkauf). Ankaŭ meblojn el dua mano mi preferas. Kaj tio ĝuste nun okupas min, ĉar mi estas translokiĝanta kaj bezonas multan novan ekipaĵon. Do ĉiam kaj ĉie mi ankoraŭ povas lerni pri novaj ebloj, malpli foruzi resursojn kaj plibonigi mian ĉirkaŭaĵon. Ofte tio ne estas la plej simpla vojo kaj oni devas serĉi kaj legi pri tio, resti en dialogo kun aliaj homoj kaj ne heziti demandi pri ideoj aŭ helpo por tiaj projektoj. Kaj la temoj estas senfinaj: apartigo de rubaĵoj, reuzado de pakaĝoj, neaĉeto ne pakaĝoj, regionaj kaj sezonaj produktoj, kiom da kaj kiamaniere generitan kurenton mi uzas, kosmetikaĵoj, paperaĵoj, unufoje uzeblaj aŭ reuzeblaj produktoj, eble mi ĉesu. 

Ĉiu persono havas alian aliron al la ideo de klimat- aŭ naturprotektado kaj komreneble malsamajn eblojn de efektivigo, sed mi certas, ke ĉiu kiu volas ŝanĝi sin kaj siajn kutimojn povas iometo plibonigi la mondon kaj eble influi la pensmanieron de homojn en sia ĉirkaŭaĵo kaj iom post iom de la socio. Se vi interesiĝas pri interŝanĝo de spertoj aŭ havas bonan konsilon por mi, mi tre ĝojus pri komento de mia teksto al michaela.stegmaier(ĉe)esperanto.de kaj eble mi daurigos skribadon pri tiu ĉi temaro. Dankon, ke vi legis tiun ĉi tekston kaj tiel estimas mian mensan laboron kaj la klopodon pliboniĝi.

Aŭtoro: Michaela
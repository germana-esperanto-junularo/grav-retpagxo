---
title: Intervjuo kun egalecen
menu:  Intervjuo kun egalecen
date:  18-12-2014
slug:  intervjuo_egalecen
taxonomy:
    tag: [intervjuo, egealecen]
    category: Artikoloj
author:
    name: Redakcio
---

> Priskribu vian blogon per kvin vortoj.

Bunta, Aktuala, Kunlabora, Eduka, Pensiga

> Kiel vi ekhavis la ideon starigi blogon?

Mi ne memoras ekzakte la momenton kiam ekestis la ideo de la blogo.
Fakte ĝi aperis sufiĉe multe antaŭe, kompare al la efektiva apero de la
blogo. Ĝenerale en nia amika rondo ni komencis tre ofte diskuti pri
temoj rilataj al genra egaleco, GLAT-rajtoj, kaj simile, ni interŝanĝis
interesajn artikolojn kaj komentis ilin, aŭ simple babilis pri niaj
personaj spertoj. La blogo nur enkadrigas kaj videbligas tiun ĉi
interŝanĝon, por ke ĝi estu pliriĉiga por aliaj homoj, same kiel ĝi
estas por ni.

Ĝi temas ja pri inismo. Ĉu vi pensas ke en la Esperanto-movado la homoj
estas pli akceptemaj pri tiu temo ol en aliaj socioj?

Mi ne kredas tion, kaj ĝuste pro tio mi pensas ke tia blogo kiel
Egalecen estas bezonata. Tio ne signifas ke Esperantistoj estas
kontraŭ-inismaj, simple ni estas bunta medio, en kiu estas homoj kun
malsamaj interesoj kaj sentemo pri la diversaj temoj, inkluzive pri
genra egaleco, aŭ pri la aliaj temoj kiujn ni traktas.

> Egalecen vi strebas – kiel aspektas tiu egaleco laŭ vi?

La egaleco al kiu ni strebas ne kontraŭstaras, sed defendas la
diversecon. Ĝi estas situacio en kiu ĉiu individuo havu la eblon decidi
de si mem kiu kaj kia ri estas. Cetere, kvankam ne rekte pri tio temas
la blogo, gravas por ni ne nur egaleco laŭ genro kaj seksorientiĝo, sed
ankaŭ laŭ aliaj aferoj kiel aĝo, raso, klaso, ktp. *La tuta socio havus
profiton de tia egaleco, ĉar ĉiu individuo povos kaj rajtos plene montri
sian valoron, sen sperti barilojn en la sociaj influoj kaj antaŭjuĝoj.*

> Kiel vi imagas la mondon post 20 jaroj? Kiuj estas viaj revoj?

Nia revo certe estas ke estu plena egaleco en la socio. La demando estas
kiel realisma tio estas. Sed ni estas optimistoj. Nuntempe ni estas
longe for de la ideala stato, sed se ni ekzemple komparas la hodiaŭan
situacion kun tiu de antaŭ dudek jaroj, okazis multege da pliboniĝo, kiu
nun ne nur daŭras, sed eĉ progresas rapide. La novaj generacioj verŝajne
estos pli malfermitaj ol la antaŭaj, tamen ne sufiĉas nur esperi, sed
necesas ankaŭ aktive kontribui al la realigo de tio.

> Kio estis la plej agrabla reakcio kiun vi ricevis pri la blogo?

Ĝenerale ni estis plezure surprizitaj de la sukceso de la blogo, ĉar jam
de la komenco la statistikoj montris multe pli da sekvantoj kaj ŝatantoj
ol kiom ni atendis en tiom mallonga tempo. Por iuj el ni estis tre
agrable renkontiĝi dum kongresoj kun nekonataj esperantistoj, kiuj jam
konis la blogon kaj gratulis pri ĝi. Krome ni persone tre ĝojas kiam
niaj legantoj kundividas en siaj fejsbukaj aŭ tviteraj kontoj niajn
artikolojn, ĉar tio estas signalo ke homoj taksas ilin legindaj.

> Se homoj interesiĝas pri kunlabori – ĉu eblas? Kiel?

Kompreneble ni volonte akceptos ĉies kontribuojn, kaj ni kaptas tiun ĉi
okazon por instigi la legantojn verki aŭ traduki artikolojn (sed ankaŭ
videojn aŭ alispecan materialon) por Egalecen. Vi povas kontakti la
redakcion per la retadreso [egalecen](egalecen[at]gmail[punkto]com), aŭ ankaŭ pere de niaj
kontoj en Facebook kaj Twitter. Ni jam antaŭdankas ĉiujn kunlaborontojn,
same kiel ĉiujn kiuj ĉiusemajne sekvas la blogon kaj komentas ĝin.

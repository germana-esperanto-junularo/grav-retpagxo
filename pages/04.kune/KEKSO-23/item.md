---
title: KEKSO – La Kreema Esperanto-Kurso
menu:  KEKSO – La Kreema Esperanto-Kurso
date:  21-05-2018
slug:  kekso-23
taxonomy:
    tag: [KEKSO, Zweibrücken, renkontiĝoj]
    category: KEKSO
author:
    name: Michael Vrazitulis
---

De la 18-a ĝis la 21-a de majo, okazis en la urbeto Dupontoj (germane: Zweibrücken), kiu ne tro foras de la german-franca landlimo, modesta kaj ĉarma kunveno de esperantemaj junuloj: La Kreema Esperanto-Kurso, KEKSO!

![Keksoj](image://KEKSO-23.png)

Profitis ni de la samtempe kaj samloke okazanta Trilanda Kongreso de Germanio, Francio kaj Luksemburgio. Ni povis uzi apartan ĉambron de la kongresejo kaj ankaŭ kuirejeton, por memzorge fari nian propran, pli junularcelan esperantumadon.

Alvenis specife por KEKSO dek kaj iom da junaj homoj, venintaj de diversaj partoj de Germanio. Kompreneble en aro de preskaŭ nur denaskaj germanlingvanoj, kun sufiĉe da komencantoj, estas malfacile ĉiam teni la “disciplinon” ne krokodili. Tio estis eta manko; eble estintus alie, se ankaŭ kelkaj el niaj junaj samideanoj el Francio decidintus veni, por igi la grupon pli miksa kaj internacia. Ĉiuokaze tamen mi pensas, ke la formato de KEKSO estas tre taŭga por ekallogi freŝajn esperantemulojn, ankoraŭ ne tiom spertajn en la lingvo.

La kunan tempon ni pasigis per diversaj, plejparte spontanee planitaj kaj realigitaj aktivaĵoj, kiel promenado tra la apudurba arbaro kaj montetaro. Ni promenis eĉ tioman distancon, ke ni ŝanĝis de unu federacia lando de Germanio al alia! (Se vi tamen prenos mapon por vidi, kie troviĝas Dupontoj, vi rimarkos, ke tiu atingo ne estas tiom elstara.) Alian tagon parto de nia grupo iris al naĝejo en la urbo, kie ni povis ĝui naĝadon eble iom pli komfortan ol tiu dum Novjaro 2017 ĉe Waldheim am Brahmsee. Viziton al la loka, hazarde tiam okazanta urba popolfesto mi same tre bele memoras.

Danke al la paraleleco de nia aranĝo al la Trilanda Kongreso, ni bonŝancis povi ankaŭ partopreni kelkajn el ties programeroj. Mi memoras ekzemple tre interesajn prelegojn kaj diskutrondojn pri la ebloj de plievoluigo de Esperantujo en la hodiaŭa, digitalema mondo, aŭ pri la kreskanta movado poliglotula. Plie, estis ofertita Esperanto-kurso, celanta komencantojn. Same mi tre ĝojis pro abundeco de plej diversaj esperantlingvaj kaj priesperantaj libroj, kadre de la kongresa libroservo.

Tie fakte ekestis ankaŭ amuza situacio, kiam anoj de la regiona televidkanalo venis al la kongresejo por fari – ĉirkaŭ unuminutan – raportaĵon pri la okazanta Esperanto-kongreso. Oni petis iom el ni junuloj veni al la libroservo-ĉambro por ŝajnigi trafoliumi la librojn tre interesate. Do – ni eĉ eniĝis en televidon, ege famiĝis...

Mi pensas, ke ĉiuj KEKSO-partoprenintoj kunsentus kun mi, se mi diras, ke ni pasigis vere ĉarmajn tri tagojn kune. KEKSO estas bela ekzemplo, ke porjunula renkontiĝo ne bezonas ĉiam esti realigita je la dimensioj de JES aŭ IJK. Sufiĉas ja havi kelkajn malfermajn kaj kreemajn homojn, kiuj ĝojas kunpasigi iom da tagoj kaj vesperoj en verdula kuneco.

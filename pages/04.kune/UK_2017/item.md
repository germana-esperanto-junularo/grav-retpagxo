---
title: Esperanto-Welt - Kongress in Seoul, Korea
menu:  Esperanto-Welt - Kongress in Seoul, Korea
date:  08-08-2017
slug:  uk-2017
taxonomy:
    tag: [UK, Korejo, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Cornelia Luszczek
---

Im Frühling dieses Jahres stand es fest: mithilfe eines Stipendiums würde ich im Juli nach Südkorea fliegen, um am Universala Kongreso de Esperanto 2017 in Seoul teilzunehmen. is zum Abflug vergingen einige Monate der Vor- B freude und Aufgeregtheit, in denen es galt, die Reise vorzubereiten. Unabhängig davon, in welche Ecke der Erde es einen verschlägt, ist es zum Beispiel wichtig, sich über vorgeschriebene Impfungen zu informieren und die Dokumente zu sammeln, die für das Visum gebraucht werden; ein Touristenvisum bekommt man allerdings für die meisten Länder ohne viel Aufwand. Alles sollte rechtzeitig, und nicht erst kurz vor knapp, erledigt werden!

![IJF](image://uk_2017.png)

Am 21. Juli landete mein Flieger, nach Zwischenstops in Dubai und Kuala Lumpur, dann schließlich in Seoul. Das Abenteuer konnte beginnen...

Wer in einer kleinen Stadt, oder gar in einem Dorf, lebt, darf sich darauf gefasst machen, von all den Eindrücken und der schieren Größe der koreanischen Hauptstadt erschlagen zu werden. Seouls U-Bahnnetzwerk ist das weltweit größte, aber es ist viel weniger kompliziert, als ich vor Antritt der Reise be- fürchtet hatte. Wer der Landessprache nicht mächtig ist, wird für jedes Schild, auf dem eine Erklärung auf Englisch steht oder wenigstens die koreanischen Worte romanisiert sind, dankbar sein.

Dankbar bin ich auch den Koreanern, die ich kennengelernt habe, für ihre herzliche, großzügige und sehr hilfsbereite Art. Als ich der Frau, bei der ich zehn Tage lang untergebracht war, erzählte, dass ich noch eine SIM-Karte für mein Smartphone brauchte, griff sie zum Telefon und fragte ihre Freundin, wo es die günstigsten gibt. Außerdem hat sie öfters für mich mit- gekocht und mir alles aufgeschrieben, was ich mir in der Stadt ansehen muss.

Sightseeing und den Kongress zusammen konnte man nicht ohne Kompromisse in zehn Tagen packen. Schon für ei- nen „reinen“ Touristentrip nach Seoul empfehle ich, sich zwei Wochen Zeit zu nehmen. Es gibt nämlich sehr viel zu sehen: Kaiserpaläste, Tempel, traditionelle Märkte, ein schönes und belebtes Universitätsviertel, einen Turm, von dessen höch- ster Plattform aus man die Skyline betrachten kann, um die wichtigsten Attraktionen zu nennen. Und wehe dem, der die Gelegenheit verstreichen lässt, sich die Gegend um den Seoul Square bei Nacht anzusehen! Pflichtprogramm war für mich selbstverständlich der Uni- versala Kongreso. Der Austragungsort war die „Hankuk Uni-versity of Foreign Studies“. Von der nächsten U-Bahnstation wiesen große Esperantoflaggen an den Straßenseiten einem den Weg zum Kongressgebäude. Jedes Mal, wenn man es betrat, konnte man erstmal die kühle, klimatisierte Luft ge- nießen, die einem entgegenschlug. Die Sommer in Seoul sind sehr heiß und drückend schwül. Der Himmel war meistens komplett grau. Auf diese klimatischen Bedingungen sollte man sich einstellen, denn in Deutschland gibt es sie in dieser Intensität nicht.

Im Programmplan des Kongresses standen unter anderem Vorlesungen über verschiedene wissenschaftliche Aspekte des Landes (Geschichte, Geografie, Wirtschaft…) und Vor- führungen von traditioneller Musik, Kleidung und Tanz. Ge- nug Einheiten waren aber auch dazu ausgelegt, selber aktiv mitzumachen. So ist in einer 90-minütigen Einheit über die japanische Kampfkunst „Aikido“ die Theorie in den Hinter- grund gerückt, und wir haben uns im überfüllten Lehrzimmer an praktischen Übungen versucht.

Mein persönliches Highlight war das Theaterstück „El la vivo de insektoj“. Eine tschechische Gruppe führte ihre Inter- pretation des gleichnamigen literarischen Werkes von Karel Čapek auf, in welchem er satirisch und indirekt auf Fehler in der Gesellschaft und in menschlichen Moralvorstellungen auf- merksam macht.

Der letzte Tag des Kongresses ist traditionell, jedes Jahr, besonders feierlich. Es wird zusammen die Esperanto-Hymne gesungen, und die jungen Teilnehmer des Internacia Infana Kongreseto (IIK) präsentieren einen Beitrag, den sie während der Woche geübt und vorbereitet haben. Wenn Alles vorbei ist, heißt es, Abschied zu nehmen. Diejenigen, die noch Zeit haben, bleiben nach der offiziellen Schließung des Programms noch etwas im Gebäude. Es werden Adressen ausgetauscht und sich alles Gute gewünscht. Dass das ziemlich schwer werden kann, und dass dabei die Augen nicht bei jedem bleiben, habe ich bereits am eigenen Leib zu spüren bekommen. Wenn man alles richtig gemacht hat, hat man am Ende eines UK nämlich eventuell gute Freunde gewonnen, die weit weg, auf einem an- deren Kontinent, wohnen. Es gilt dann, die Freundschaft über eine große Distanz aufrechtzuerhalten und sich idealerweise im nächsten Jahr wiederzusehen, auf dem nächsten UK.

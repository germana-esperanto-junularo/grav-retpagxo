---
title: Sommerfest - Esperantoplatz, Berlin
menu:  Sommerfest - Esperantoplatz, Berlin
date:  21-09-2017
slug:  sommerfest-esperantoplatz-2017
taxonomy:
    tag: [sommerfest, Berlin, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Jonas Marx
---

Auch dieses Jahr fand am 20. September auf dem Esperantoplatz in Berlin wieder ein Sommerfest statt. Durch ein Konzertangebot der Organisatorenkonnte ich dieses Jahr mit dabei sein und habe direkt einmal dieBerliner Esperantoszene kennengelernt- In der Hauptstadt ist einiges loswas unsere Sprache anbetrifft. Wir feierten zusammen denfünfundzwanzigsten Geburtstag der Zamenhofeiche, die auf demEsperantoplatz gepflanzt wurde. Es gab ein breites Angebot für Kinder,unter anderem Schminken, Ballspielen und eine Pressmaschine für Buttons.Gut fand ich auch die Einbeziehung der Kiezbewohner durch einen Stand,an dem man Probleme und Ideen zu dem Platz äußern konnte. Nebenbei gabes auch eine Pflanz- und Begrünungsaktion des Projekts „Empowerment fürUmweltbewusstsein“, bei der die Grünflächen des Platzes mit Jungpflanzeneingedeckt wurden.

![Esperantoplatz](image://esperantoplatz-2017.jpg)

Leider kamen an diesem Tag nur rund einhundert feste Besucher plus Passanten, laut den Organisatoren waren es das letzte Jahr mehr. MeinAuftritt lief jedoch super- und auch die anderen Musiker belebten dieganze Kreuzung, namentlich QuerBeet – Klezmer und mehr, Hartmut Mittagund Alexander Danko. Glücklicherweise blieb es trotz ein paarNieseltropfen im Vorfeld trocken und so schmiedeten wir nach Ende desFestes bereits Pläne für nächstes Jahr...

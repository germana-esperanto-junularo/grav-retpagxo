---
title: Den 8. März repolitisieren – Frauen\*kampftag 2015
menu:  Den 8. März repolitisieren – Frauen\*kampftag 2015
date:  17-12-2014
slug:  anonco_frauenkampftag
taxonomy:
    tag: [renkontiĝoj, egealecen]
    category: Renkontiĝoj
author:
    name: Redakcio
---

Den 8. März repolitisieren – Frauen\*kampftag 2015

"Wir wollen mit euch eine gemeinsame neue feministische Offensive
organisieren! Unser Ziel ist es, den 8. März zu (re)politisieren,
Frauen\* untereinander zu solidarisieren und eine Öffentlichkeit für
unsere Anliegen und Forderungen zu schaffen."

Diesem Anspruch und Ziel ist das Bündnis für den Frauen\*kampftag in
diesem Jahr ein gutes Stück näher gekommen. Die zentrale Demonstration
in Berlin am 8. März 2014, dem Internationalen Frauentag, war
erfolgreich. Mehr als 5.000 Menschen waren auf der Straße, um gemeinsam
zu sagen „Still loving Feminism!“. Das Bündnis, das aus verschiedenen
linken Organisationen, Gewerkschaften, Parteien und Einzelpersonen
bestand hatte zum Ziel, den 8. März zu repolitisieren, Räume für einen
Austausch zu feministischer Theorie und Praxis zu schaffen gemeinsam
Forderungen zu entwickeln. Es war ein Anfang für eine neue feministische
Offensive. Noch immer sind Frauen\* auf der ganzen Welt ganz
unterschiedlich und dennoch auf ähnliche Weise ökonomisch und sozial
unterdrückt. Sie sind immer noch für den Hauptteil der Sorge- oder
Carearbeit verantwortlich, verdienen für die selbe Arbeit weniger Lohn,
arbeiten häufiger als Männer\* in prekären Jobs und werden häufiger
Opfer von sexualisierter Gewalt. Auch längst errungene Fortschritte wie
das Recht auf Abtreibung in Spanien, stehen immer wieder zur
Disposition. Die aktuelle Krise in Europa verschärft diese
Entwicklungen.

Das Bündnis hat deshalb zum Ziel, viele Frauen und solidarische Männer
zusammenzubringen und zu vernetzen, um gemeinsam feministisch
handlungsfähig zu sein. Deshalb soll es auch im nächsten wieder eine
gemeinsame Demonstration in Berlin geben. Die Vorbereitungen beginnen
bereits im Herbst. Auf der Auftaktveranstaltung Mitte November wurden
die ersten Planungsschritte eingeleitet und der Aufruftext für das
nächste Jahr diskutiert. Da das Bündnis jedoch keine alleinige
Orgatruppe für den 8. März sein will, wurden innerhalb das Bündnisses
Projektgruppen gegründet, die sich zu den Themen Sexuelle
Selbstbestimmung, Sexismus, Arbeit für ein gutes Leben und Widersprüche
im Bündnis vernetzen, austauschen und gemeinsame Aktionen planen wollen.
Der Raum zur Gründung neuer Projektgruppen steht jederzeit offen. Das
Bündnis hat den Anspruch Widersprüche in feministischer Politik sichtbar
zu machen und sich gleichzeitig dadurch nicht vereinzeln zu lassen,
sondern die verschiedenen Kämpfe zu vereinen. Nur so können wir
gemeinsam dafür kämpfen, den Kapitalismus und das Patriarchat, also die
männliche Vorherrschaft, gleichsam abzuschaffen. Für das Bündnis stehen
die kapitalistische Produktionsweise und die Geschlechterhierarchien
untrennbar zusammen. Die gegenwärtigen Zuspitzungen der Widersprüche
machen macht eine breite Bündnisarbeit umso notwendiger. Das Bündnis
will genau hier eingreifen.

Homepage www.frauenkampftag2015.de

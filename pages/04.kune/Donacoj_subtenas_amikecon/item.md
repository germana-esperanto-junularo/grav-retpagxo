---
title: Donacetoj subtenas amikecon
menu:  Donacetoj subtenas amikecon
date:  01-11-2018
slug:  donacetoj_subtenas_amikecon
taxonomy:
    tag: [kristnasko, donacoj]
    category: Artikoloj
author:
    name: Annika Förster
---

– oni diras. Tio precipe ĝustas kristnaske, sed ĉiam malfacilas trovi bonajn donacojn. Tial ni kolektis kelkajn ideojn (seriozajn aŭ malseriozajn – sed al ĉevalo donacita oni buŝon ne esploras):

**La Fundamento de Esperanto**: Tion oni povas uzi por multege da aferoj: fiksi ŝancelajn tabloj kaj seĝoj, uzi ĝin kiel kusenon aŭ eĉ legi en ĝi! Alifklanke: Ĉu persono, kiu ankoraŭ ne posedas La Fundamenton eĉ povas sin nomi Esperantisto?

**Ŝildo kiu montras „-n“**: Oni povas uzi ĝin mojosajn novajn vortojn kiel „embuski“ por helpi al homoj plibonigi siajn lingvokonojn. Kaj ankaŭ por ege ĝeni ilin kaze se oni volas Ne gravas, kiujn donacojn vi decidas esti sola dum Esperanto-renkontiĝo.

**Ĉokolado**: Fakte ne nur al esper- antistoj, sed al ĉiu vi povas donaci tion. Preskaŭ ĉiu ŝatas ĉokoladon. Adventan florkronon kun kvin kandeloj: Unu por ĉiu dimanĉo en advento kaj unu por la naskiĝtago de Onklo Zamĉjo. La kvina kandelo memkompreneble estu verda.

**Mondmapo** por indiki kiujn landojn vi jam vizitis: Tiel multe pli bone eblas plani, kiujn kongresojn viziti venontjare (aŭ kie organizi ilin). Kaj eblas facile fari: Vi

**Kartoj kontraŭ Esperantujo**: Vi povas ricevi la ludon senkoste [interrete](https:// timsk.wordpress.com/2014/02/05/kartoj-kontrau-esperantujo/), kaj ĝi estas bona donaco, ĉar vi povas printi kaj eltranĉi la kartojn. Vi ankaŭ povas elprovi la ludon antaŭ ol fordonaci ĝin, tiel vi ankaŭ ĝuos la donacon (se vi tre ĝuas ĝin, faru du por ke vi povu gardi unu).

**La Majstro de l’Ringoj de William Auld**: Ĝi ne nur estas bona libro, kiu ege legeblas, se vi legas ĝin, vi lernos mojosajn novajn vortojn kiel „embuski“ aŭ diversaj specoj de armiloj. Ne gravas, kiujn donacojn vi decidas transdoni, mi deziras al vi kaj via familio feliĉan kristnaskan feston.

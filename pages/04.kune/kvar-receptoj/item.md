---
title: 'Kvar facilaj, bongustaj receptoj por kompleta menuo: Bonan apetiton!'
media_order: 'florbrasika supo.jpg,Salato kaprese.jpg,ayayay.png'
date: '11:30 31-07-2020'
taxonomy:
    category:
        - receptoj
    tag:
        - manĝaĵoj
        - esperanto
        - bongusta
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Se ĝis JES vi enuiĝas, eble vi povas uzi vian tempon por prepari kelkajn esperantajn rezeptojn de apetito.ikso.net kaj surprizi amikojn aŭ vian familion. Cornelia kunigis por vi kvarpladan menuon!

![](Salato%20kaprese.jpg)

### 1. Salato Kaprese

Ingrediencoj por 4 personoj:

- 2 grandaj, freŝaj tomatoj
- 350 g da mocarelo
- 45 g da freŝaj folioj de bazilio
- 60 ml da olivoleo
- Salo kaj pipro

Preparado:

1. Tranĉu la tomatojn kaj la mocarelon. Faru diskajn tranĉaĵojn el ili, ĉirkaŭ 1 cm dikajn!
2. Metu la tranĉaĵojn en la formo de duonringo sur la rando de la telero. Alternu inter tomato kaj fromaĝo.
3. Garnu per bazilio, salo, kaj pipro, laŭ gusto.
4. Verŝu olivoleon sur la tutan salaton, kaj servu tuj.

### 2. Florbrasiko-Supo

Ingrediencoj por 4 personoj:

- 1/2 florbrasiko
- 1 karoto
- 2 pecoj da tofuo aŭ vegetara viando
- 1 pomo
- salo
- sukero

Preparado:

1. Tranĉu florbrasikon, karoton en konvenajn pecetojn.
2. Boligu la pomon kaj karoton dum 15 minutoj en akvo por havi buljonon, elprenu la pomon, metu la tranĉitan florbrasikon kaj la rostitan tofuon en tiun ĉi buljonon.
3. Daŭre boligu ĝin ĝis finkuiriteco.
4. Aldonu salon kaj sukeron laŭ gusto.
5. Servu la supon varme!

### 3. Nudeloj, Gorgonzola, Fungoj

Ingrediencoj:

- 250 g ŝampinjonoj 
- 250 g nudeloj
- 50 gr butero
- 200 g dolĉa gorgonzola 
- 150 ml laktokremo
- iom da pipro muelita 
- 50 g raspita parmezano

Preparado:

1. En fritilo metu iom da oleo kaj la ŝampinjonojn fritetu.
2. En la buteron fandiĝintan metu la gorgonzolon erigita kaj aldonu la laktokremon. Ĝi iĝos kiel kremo.
3. Aldonu la pipron muelitan, la fungojn kaj la duonon de la raspita fromaĝo.
4. La nudelojn boligitajn enmiksu en la bongustan saŭcon kaj surmetu la alian duonon de la raspita fromaĝo. Surtabligu ĝin ankoraŭ varmega!

### 4. Deserto: Nebakita Biskvit-Frukta Kuko

![](ayayay.png)

Ingrediencoj por 8 personoj:

- 300 aŭ 350 g da longaj biskvitoj (france _boudoirs_, itale _savoiardi_) 
- 200 ml da grasa (!) laktokremo 
- Fruktoj laŭ gusto: bananaj, framboj, mirteloj, fragoj, persikoj, abrikotoj, piroj, … 
- Likvaĵo por trempi la biskvitojn: eblas uzi la siropon de la fruktoj aŭ alian siropon (ekzemple ĉerizan). Ĉiukaze aldonu ankaŭ akvon. 
- Granda plasta bovlo

Preparado:

1. Batu la laktokremon per miksilo, ĝis ĝi fariĝos ŝaŭma.
2. Trempu biskvitojn kelkajn sekundojn. Ili devas moliĝi, sed ne disrompiĝi poste!
3. Kuŝigu malsekajn biskvitojn sur la fundon de la bovlo kaj starigu aliajn laŭ la randoj.
4. Aldonu tavolon da fruktoj.
5. Aldonu tavolon da batita kremo.
6. Aldonu tavolon da trempitaj biskvitoj.
7. Rekomencu: tavolon da fruktoj, tavolon da batita kremo kaj lastan tavolon da biskvitoj.
8. Kuŝigu teleron sur la bovlon kaj metu ĝin en fridujon.

Por servi la kukon, necesas simple turni ĝin kaj delikate forpreni la bovlon.

Aŭtoro: Cornelia
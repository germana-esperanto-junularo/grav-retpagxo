---
title: 'Scivolemo revenis'
media_order: scivolemo.jpg
date: '15:30 30-09-2020'
taxonomy:
    category:
        - Anoncoj
    tag:
        - scivolemo
        - edukado
        - youtube
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

La jutuba kanalo Scivolemo revenis!

En 2019 el la scienca blogo Scivolemo, teamo komencis adapti sciencajn artikolojn al videa formato en Esperanto. Post pli ol unujara paŭzo, la kanalo nun revenis kun nova koncepto: La plej granda parto de la videoj nun estas tradukoj de profesiaj sciencaj videoj kun liberaj licencoj, ekzemple de NASA aŭ ZDF. Ankaŭ originalaj videoj ankoraŭ estas publikigitaj (la unua temas pri katoj!!!). Uzi videojn kun liberaj licencoj permesas uzi la rezultojn kie ajn vi volas, ekzemple la teamo ankaŭ enmetas ilin en la esperanta Vikipedio. La kanalo publikigas novan videon ĉiusemajne.

La teamo de la kanalo ĉiam ĝojas pri konstruemaj kritikoj kaj proponoj. Ĉu vi havas sperton en sciencan fakon kaj volas krei videon pri ĝi? Ĉu vi konas bonan fonton de sciencaj videoj kun liberaj licencoj? Ĉu vi ŝatus helpi traduki? Tiam kontaktu la teamon!

[https://www.youtube.com/c/Scivolemo/](https://www.youtube.com/c/Scivolemo/)

![](scivolemo.jpg)
---
title: Anonco de la 25-a KEKSO en Neumünster
menu:  Anonco de la 25-a KEKSO en Neumünster
date:  17-10-2018
slug:  kekso-25-anonco
taxonomy:
    tag: [KEKSO, Neumünster, renkontiĝoj]
    category: KEKSO
author:
    name: Michaela Stegmaier
---

![Keksoj](image://KEKSO.jpg)

La 25-a KEKSO: de la 7-a ĝis 10-a de junio 2019 KEKSO okazos kune kun la Germana Esperanto-Kongreso (GEK) en Neumünster. ial ni bezonas vian kreemecon. Ni volas enmiksiĝi T en la programon de GEK kaj montri nian junulan energion. Vi povas veni kostumita, proponi ludojn aŭ aliajn programerojn al ni kaj ni prizorgos la reston. La ejo provizas por ni kuirejon, kie ni povas memstare prizorgi niajn veganajn manĝojn kaj interbabili kun partoprenantoj de la kongreso. Kun dormsakoj kaj matracetoj ni dormos en la amasloĝejo. Kotizoj por novuloj kaj eksterlandanoj: 0€. Kotizoj por membroj de GEJ: 30€. Kotizoj por ne­membroj de GEJ: 45€. Ni petas ĉiujn partoprenantojn aŭ kunporti keksojn aŭ fruktojn aŭ krompagi 10€. Ni antaŭĝojas vian ĉeeston! Aliĝi vi povos ekde nun en nia [retpaĝo]( www.esperanto.de/de/enhavo/anmeldung-zum-kekso-aliĝo-al-kekso)

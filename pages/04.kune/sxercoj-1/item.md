---
title: 'Kelkaj ŝercoj'
date: '12:30 31-07-2020'
taxonomy:
    category:
        - Artikolo
    tag:
        - esperanto
        - ŝercoj
        - ridado
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

1.	“Doktoro, mi sentas min tiel nekredeble superflua.” – Kuracisto: “La sekva bonvolu!”
2.	Kial la kapitano enprofundigis la submarŝipon? - Estis malferma tago.
3.	Mi ne povis kredi ĝin. Mia najbaro efektive sonoris ĉe la pordo je la 3-a horo nokte. Mia borilo preskaŭ falis el mia mano.
4.	Hodiaŭ mi vere freneziĝis pro mia navigilo. Poste mi kriis, ke ĝi iru en la inferon. Tiam, 25 minutojn poste, mi staris antaŭ la domo de Evildea.

Aŭtoro: Jonas
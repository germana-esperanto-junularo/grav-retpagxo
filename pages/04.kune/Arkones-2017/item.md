---
title: ARKONES kun plenplena programo
menu:  ARKONES kun plenplena programo
date:  24-09-2017
slug:  arkones-2017
taxonomy:
    tag: [ARKONES, Polando, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Jonas Marx
---

Esperantujo viglas - jen la resumo de mia septembro. Post la somera festo ĉe la esperantoplaco mi restis unu tagon en Berlino kaj jam vendrede daŭrigis la vojaĝon al Poznano, kie okazis la 33-a ARKONES. La „artaj konfrontoj en Esperanto“ estas unu el la plej fidindaj kaj grandaj renkontiĝoj en eŭropo kaj ĉijare mi havis la honoron partopreni kaj eĉ prezenti mian muzikon unuafoje.

![Centrejo](images://arkones-logo.jpg)

De Frankfurt (Oder) mi kaj tri aliaj Esperantistoj ekvojaĝis aŭte kaj dum la veturado aŭskultis E-muzikon kaj diskutis pri dio, fervojobiletaj prezoj kaj la mondo. Ni atingis nian celon ĝuste por esti akceptitaj kaj saluti amikojn, kiujn ni jam longtempe konas.

Je la sesa komencis la risko-ludo, kiu estas subtenita de edukado.net. Ĝi estas interesa ludo pri la esperantogramatiko, la movado kaj kulturo.

En vespero koncertis unue „La kumpanoj“ kaj poste Kimo. La tuta salono kunkantis la faman kanton „Sola“ (La version sen molaĵoj) kaj tiel finis la vespero.

La venontan tagon sekvis prezento pri esperantopoezio de Humphrey Tonkin, prezento de Irek Bobrzak pri esperantaj nomoj de plantoj kaj multege da aliaj programeroj.

Sed la plej granda surprizo por mi estis la koncerto de „Asocio Jazz Poznań“, poznana grupo kiu esperantigis siajn polajn kantojn kaj vigligis la tutan koncertejon per ĵazo kun rokaj elementoj. Eĉ pli maljunaj homoj ne sukcesis rezisti la melodiojn kaj fortecon de la programero. Poste, Suzana koncertis kaj per ŝia gitaro kaj dolĉa voĉo revigis la publikon.

Finis la sabaton mia koncerto je la deka horo. Feliĉe ankoraŭ multaj homoj restis en la koncertejo kvankam la tago estis plenplena. Mi kantis, provis movigi la homojn kaj invitis ilin kanti sukcese- je duono de mia koncerto ĉiu, kiu ankoraŭ havis forton en la korpo dancis apud la scenejo. Je la unua nokte, ni ankoraŭ sidis kaj babilis, ridis kaj drinkis. La sabato fakte estis la plej longa kaj mojosa tago de la aranĝo.

Matene, post sufiĉe da dormo, mi denove venis al la arkonesejo. Dimanĉe ankoraŭ koncertis Georgo Handzlik kaj Lena de ĴeLe. Mi ankoraŭ ne ĉeestis Handzlikan prezenton, tial mi ĝojis. Tagmeze iris la tempo por adiaŭi miajn amikojn kaj veni al la ĉefstacidomo. Atendis min trajnvojaĝo naŭhora, en kiu mi povis sendistre ĝui mian postkongresan sindromon.

Retpaĝo de ARKONES:[arkones.org/](http://arkones.org/)

---
title: 'Nova albumo de TIM'
media_order: 'Tim Gallego.jpg,Vaganto-TIM.jpg'
date: '15:00 30-09-2020'
taxonomy:
    category:
        - Anoncoj
    tag:
        - muziko
        - tim
        - vaganto
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

![](Vaganto-TIM.jpg)
Jen _Vaganto_, la unua albumeto de TIM; ĝi alvenas kiel rezulto de inspiro de vivo, de diversaj spertoj, amo, kaj libereco.
Ĉi tio estas ankaŭ la temoj pri kiuj TIM kantas en siaj kantoj, inter kiuj oni povas trovi kelkajn kiuj estis la unuaj verkitaj de li.

Alternativa roko / sendependa popo estas la ĝenroj kaj influoj sur kiuj TIM baziĝis por produkti albumeton kun belaj klavar-melodioj kaj gitar-aranĝoj, kiuj donas al la verko foje etan spacan kaj eĉ popan psikadelan sonon.

TIM komencis registri la unuan kanton dum decembro 2019, antaŭ sia koncerto dum JES en Pollando, kun la intenco montri ion al la publiko antaŭ la koncertado. Tial, la mondcivitana muzikisto kontaktiĝis kun Vinilkosmo por paroli pri eldono de unu titolo kaj muzikvideo - estis tiam kiam la demando aperis: kial ne registri 6-titolan albumeton kaj aperigi ĝin oficiale? Do, la decido ne estis malfacila; TIM enŝipiĝis en la registro-procezon per sia hejma studio, kiu en la unuaj tagoj estis nur komputilo, interfaco, kaj mikrofono. Post kelkaj semajnoj, TIM sendis la unuan demon al Vinilkosmo en Francio, de kie li ricevis pozitivajn rimarkojn por ke li daŭrigu la registradon por la bita albumeto (EP).

Jam naŭ monatoj pasis kaj hodiaŭ ni havas plezuron prezenti _Vaganto_ kiel aludo al tiu kiu senvoje vojaĝas, libera en sia mondo.

[Jen ligilo al la albumo!](https://www.vinilkosmo-mp3.com/eo/popo-roko-hiphopo-elektronik/tim/1-vaganto.html?fbclid=IwAR1YoP3TmLTFRX3FwYA2F54lg4jJoeb6q9hL1lbnApBNeNJ2o5R7OvkHsEM)

![](Tim%20Gallego.jpg)
TIM estas:
Timothy Gallego: aŭtoro kaj komponisto, voĉo, gitaroj, klavaro, drummaŝino, muzikaranĝoj. 
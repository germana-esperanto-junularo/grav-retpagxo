---
title: 'Picknick in Würzburg im Juli'
media_order: 'pikniko Würzburg.jpeg'
date: '13:00 31-07-2020'
taxonomy:
    category:
        - Renkontiĝoj
    tag:
        - pikniko
        - Würzburg
        - somero
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Nachdem wir uns coronabedingt seit dem Remote-Spielenachmittag nicht getroffen haben, gab es im Juli endlich wieder Esperantoaktivitäten in Würzburg: Wir haben gepicknickt.

Im Alten Park des mittlerweile nicht mehr ganz so neuen Geländes der Landesgartenschau trafen sich am späten Nachmittag sechs Leute bei bestem Wetter. Erstmal gab es natürlich viele Neuigkeiten zu besprechen (die Anmeldung zum JES ist online!) und Eindrücke auszutauschen, ob vom online-IJK oder vom Arbeiten im Chemielabor mit Corona-Lockdown. Außerdem waren wir mit Pizzaschnecken, Kekse und Kichererbsenchips, die überraschenderweise sehr zu empfehlen sind, verpflegt.

Danach haben wir "Nur unu" (in der deutschen Version Just One) gespielt, eine Art Mischung aus Tabu und Stadt-Land-Fluss, bei der Begriffe von der Gruppe beschrieben werden müssen. Dabei haben sich um uns herum immer mehr Sportgruppen versammelt, sodass wir von allen Seiten mit Entspannungsübungen beschallt wurden.

Es war sehr schön, alle mal wieder zu sehen und ein Picknick werden wir bestimmt bald mal wieder machen!

Autor\*in: Annika
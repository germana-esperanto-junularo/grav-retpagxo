---
title: Sinprezento - Severin Seehrich
menu:  Sinprezento - Severin Seehrich
date:  05-05-2017
slug:  sinprezento_severin
taxonomy:
    tag: [personoj, esperantistoj]
    category: Personoj
author:
    name: Severin Seehrich
---

Mi estas Severin‘, kaj mi venas el kaj studas en Germanio. Filozofion mi studas por kompreni la mondon ĉirkaŭe kaj ene, kaj ĉar nekredeble plezurigas min parkerigi gramatikajn librojn, mi studas la malnovgrekan. Mi komencis lerni Esperanton dum la aŭtuna KEKSO en 2014 kaj vizitis ĉiujn sekvajn JES-ojn kaj IJK-ojn ekde tiam. Do per Esperanto mi jam vojaĝis ene de Germanio, al Pollando kaj al Hungario.

![Severin](image://severin.jpg)

Malgraŭ dum la JES 2016/2017 mi okazigis novulan programon, mi ankoraŭ devas konatiĝi kun kaj parkerigi ĉiujn mallongigojn, kiuj jam ŝajnas esti baza parto de la ĉiutaga vivo de Konstanze.

Tial mi ankaŭ havas du ĉefajn celojn en la GEJ-estraro, pri kiuj mi tute bone kapablos okupiĝi jam antaŭ ol tiu lernado finintos: Unuflanke mi volas laŭeble plej bone bonvenigi kaj prizorgi komencantojn, kiuj konas eĉ malpli da Esperantujaj mallongigoj ol mi. Aliflanke mi celos prizorgi la kreeman reprezentadon de kaj klerigadon pri enhavecaj temoj kiel genra egaleco, rasismo, antisemitismo ktp. por eĉ pli mojosigi kaj sentemigi Esperantujon. Por la unua celo mi planos kunorganizi la jam antaŭe okazintajn KEKSOjn, kaj por la dua mi planas kune kun aliuloj okazigi specife feministan kaj GLAT\*an (GejanLesbanAmbaŭseksemanTransgenrulanKtp-an) Esperanto-kunvenon.

Mi antaŭĝojas frukto- kaj ideodonan kunlaboron (kaj eble kunteumadon?) kun vi ĉiuj!

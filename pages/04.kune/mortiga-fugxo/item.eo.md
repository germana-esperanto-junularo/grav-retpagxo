---
title: 'La Mortiga Fuĝo tra Spaco kaj Tempo – Die tödliche Flucht durch Raum und Zeit'
media_order: mortiga.png
date: '18:30 13-06-2021'
taxonomy:
    category:
        - Rakontoj
    tag:
        - personoj
        - filmetoj
        - ses
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

**22. Jahrhundert**  
„Morris, pafu! Pafu do!“ Atemlos versucht Kapitän Chesterfield, seinen Navigator Morris aus dessen Schockstarre zu befreien. Morris, der ebenso wie Kapitän Chesterfield und der Rest der Besatzung der SGW-2249 erst vor wenigen Stunden auf dem unerforschten Wüstenplaneten 3/5/20/9-1/12/16/8/1 V (منخر) gelandet ist, glaubt seinen durch eine Dior-Sonnenbrille geschützten Augen nicht trauen zu können: Eine riesige, wurmartige, dreiäugige Kreatur rast durch die endlosen Sandweiten auf ihn und Kapitän Chesterfield zu. Vor sich her treibt das mit einem riesigen, nach Fleisch gierenden, unterhalb der drei Augen auf- un zuschnappenden Maul jagende Geschöpf eine junge Frau. Wie durch Watte dringen die Rufe von Kapitän Chesterfield in Morris’ Bewusstsein. Als Chesterfield schon glaubt, dass Morris nicht mehr schießen wird, richtet dieser seine Strahlenwaffe mit ausgestrecktem Arm auf die Kreatur und schießt …

aus: “Epizodo IV: Sur la Planedo”
[plugin:youtube](https://www.youtube.com/watch?v=IUg5bvESgeM)

**Frühling 2014**  
„Die ersten drei Episoden unserer Serie sind fertig, die vierte Episode ist in Arbeit. Wir fahren zum ‚[Somera Esperanto-Studado](https://ses.ikso.net/2014/sk/)‘ in die Slowakei in die Stadt Nitra. Mein persönlicher Hintergedanke: Werbung machen! Mir sind keine aktuellen Produktionen exklusiv in Esperanto bekannt, schon gar nicht im Bereich der Science Fiction. Da muss es doch eine Art von (frenetischer?) Reaktion geben!?“

Andreas

**Die nahe Zukunft**  
Barbarellas Wagen lässt die sich nähernde Zombiehorde hinter sich und rast auf der einsamen Wüstenstraße dem Horizont entgegen. Dort zeichnet sich dichter Wald ab, aus dem, selbst aus dieser Entfernung sichtbar, ein militärischer Aufklärungsturm emporragt. Die Dunkelheit verschluckt Barbarellas Fahrzeug auf der in den Wald führenden Straße. Die nun herrschende Stille wird ohne Vorzeichen von erdbebenartigen, wiederkehrenden Intervallen durchbrochen. Ein ungefähr vierzig Meter hohes Wesen lässt, während es sich durch den Wald bewegt, eine Spur der Vernichtung aus metertiefen Abdrücken und zerborstenen Tannen zurück. Die Kreatur ist …

aus: „Epizodo III: Fuĝo“
[plugin:youtube](https://www.youtube.com/watch?v=rrZ06uAky7g)

**EINE SCHRECKLICHE …  
NOCH NICHT GESEHENE …  
GEISTESKRANKE …  
VERGEWALTIGUNG VON NATURGESETZEN!**

aus: „Atako de la Mortonuboj!“
[plugin:youtube](https://www.youtube.com/watch?v=VmW92vwNRWw)

**Rückblick Jesko I**  
Irgendwann sagte Andi dann so sinngemäß:

„Alter, dieses Esperanto, das du da immer quackelst ... Lass uns doch mal Bildungsurlaub machen und zu so einem Esperanto-Treffen fahren.“  
        „Bist du sicher?“  
 „Na klar!“  
        „Wirklich? Du? Und ich? Auf einem Esperanto-Treffen?“  
„Natürlich! Wieso denn nicht?“  
        „Na ja, nun. Du sprichst doch gar kein Esperanto ...“  
 „Lern ich da! Und wir können denen den Film zeigen! Wird bestimmt super!“  
        „Naaa schön ...“

**Sommer 2014 SES in der Slowakei**  
„Die einzige Frage, als ich mich endlich traue zu erwähnen, dass wir eine EIGENE(!) SERIE(!!) IN ESPERANTO(!!!) gedreht haben, lautet: ‚Wer hat euch die Übersetzung gemacht?‘“

**„Irgendwo im Weltraum … könnte genau dies geschehen!“**

Trailer Episode IV
[plugin:youtube](https://www.youtube.com/watch?v=VNVBEx1twpM)

**Rückblick SES 2014**  
„Dieser Moment ist mein persönlich erlebter Inbegriff für ‚Subkultur‘. ‚Subkultur‘, die anscheinend eine klare Abgrenzung von Mitwirkenden und Außenstehenden benötigt, um zu funktionieren.  
Die Reaktion auf den Vorschlag, einen Teil der Serie beim SES 2014 auf dem Kinoabend zu zeigen, war: Leute, ich glaube, so etwas passt hier nicht. Ich habe das Gefühl, dass es als unmöglich angesehen wurde, dass jemand, der nicht Teil der aktiven(?) Esperanto-Subkultur ist (was ja bei einer Subkultur die, soweit ich das mitbekommen habe, ‚nur‘ ihre Sprache als Alleinstellungsmerkmal hat, schon schwierig ist, da ja jeder, der die Sprache spricht, per se dazu gehören müsste), trotzdem einen Beitrag leisten darf/kann.“

Andreas

**„Die Erde ist der einzige der Menschheit bekannte Planet, auf dem Leben existiert, doch wie lange noch?“**  

aus: „Epizodo I: La Okulo de la Scienco“
[plugin:youtube](https://www.youtube.com/watch?v=ZWBKqHx7SC4)

**April 2021**  
„Versuche durch Versenden von E-Mails an Esperanto-bezogene Webseiten Aufmerksamkeit auf die finale(?), fünfte Episode (und damit hoffentlich die ganze Serie) zu ziehen. Eine der wenigen Antworten, die ich erhalte, ist von Michael Vrazitulis. Er bittet uns diesen Artikel zu schreiben.“  

Andreas

**AW: Kune Artikel – Einladung zum Bearbeiten
Von: Jesko**  
Ich glaube, es war genau genommen so:
> Die einzige Frage, als ich mich endlich traue zu erwähnen, dass wir eine EIGENE(!) SERIE(!) AUF ESPERANTO(!) gedreht haben, lautet: „Wer hat die Grammatik geprüft?“
… und ging dann noch (schlimmer?) weiter mit dem Hinweis: „Ich habe übrigens ein Grammatikbuch geschrieben, das ich euch empfehle.“

**Rückblick SES 2014 II**  
„Es gab da sehr freundliche, sympathische Leute. Dennoch habe ich das, was dort ablief, sehr distanziert wahrgenommen. Es herrschte eine (auf mich) manchmal schon fast überdreht wirkende Begeisterung, die eben (so deute ich das jedenfalls) maßgeblich darauf gerichtet war, dass man sich dort als Teil der Esperanto-Bewegung trifft. Das ist zumindest meine Erklärung dafür, wie die Teilnehmer:innen an sich normale (na gut, und teilweise auch etwas skurrile) ‚Ferienaktivitäten‘ zelebrierten. Und das ist natürlich super, aber, nun ja, ich bin ja nicht Teil der Esperanto-Bewegung. Das hat zu dem seltsamen Effekt geführt, dass ich mir dort zeitweilig wie ein Betrüger vorkam, vielleicht noch verstärkt dadurch, dass ich als halbwegs gut Esperanto Sprechender wahrgenommen wurde. Vielleicht ist das einfach der berühmte ‚etoso‘; dann ist das hier eine Schilderung, wie der auf einen Außenstehenden wirkt, der sich hineinverlaufen hat. Eine Abneigung gegen die Esperanto-Bewegung habe ich überhaupt nicht. Dass es eine solche Sprachgemeinschaft gibt, ist, soweit ich weiß, einmalig, und das ist schon faszinierend. Ich glaube aber, für mich bleibt da nur die Beobachterrolle, und das ist ja auch in Ordnung.“  

Jesko

![](mortiga.png)

_Andreas Niethmann dreht und veröffentlicht seit 2012 die esperantosprachige Serie „La mortiga fuĝo tra spaco kaj tempo“ auf dem YouTube-Kanal „Horizontalfilm“. Jesko Beyer unterstützt ihn dabei, unter anderem mit der Übersetzung der Dialoge ins Esperanto. Im Frühjahr 2021 erschien die finale(?) Episode: „Zurück auf der Erde“. Der serienbegleitende Blog erscheint auf [www.horizontalfilm.de](https://www.horizontalfilm.de). Beide besuchten 2014 das „Somera Esperanto-Studado“._
---
title: 'Pri kvartaga migrado en la Harc-montaro'
media_order: 'Aa_Abb3.jpg,Abb1.jpg,Abb2.jpg,Abb4.jpg,Abb5.jpg'
date: '17:00 30-11-2020'
taxonomy:
    category:
        - Rakontoj
    tag:
        - rakonto
        - montaro
        - migrado
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Jam de multaj jaroj mi revas pri kelktaga migrado kaj envias skoltajn amikojn, kiuj rakontas la plej mojosajn rakontojn pri tiaj travivaĵoj. Dormi sub stelplena ĉielo, aŭdi la venton en la arbopintoj kaj dumtage malkovri la montaran pejzaĝon, kiun oni normale nur vidas de for. Sed sole migri dum horoj, nokte frosti kaj iom timi pri la sovaĝaj bestoj por mi ne vere sonis aloga. Do mia celo estis organizi aventuran migradon kun homoj el diversaj skoltgrupoj de la Harc-regiono. Ĉar longe ne plu okazis tielnomata “Haik” (plurtaga ogientiĝa ludo devenanta el la sveda skoltmovado), en somero 2019 ĉirkaŭ mi tuj fondiĝis teamo de skoltestroj por organizi eventon ĉirkaŭ la 1-a de majo 2020, en kiu skoltoj povas montri sian eltenkapablon kaj bonhumuron dum malfacilaj situacioj. Sed la unua plano, migradi laŭ la 100-km-longa “Hexenstieg” (sorĉistina vojo) baldaŭ evidentiĝis neebla por junuloj, des pli, ke ni efektive nur havis tri plenajn tagojn por atingi nian celon. Estis elektita alia itinero, sed finfine la kronvirusaj restriktoj tute detruis la planojn. Mi ne multe plendis, ĉar ĉiukaze mi havis akcidenton en januaro kaj operacion en februaro por fiksi mian piedartikon, pro kio mi ne povintus kunmigri.

Sed la deziro restis en mia kapo. Nepre mi volis realigi tiun-ĉi revon ankoraŭ en la jaro 2020. Kaj okazis, ke mi trovis kunmigranton tre simpatian kaj sufiĉe spertan pri la arbaro kaj ties enloĝantoj, kiu ekflamiĝis pri mia ideo. Lia nomo estas Malte, li ĵus antaŭe migris de Garmisch-Patenkirchen al Zugspitze kaj li estas mia koramiko. Mojose! Do mi tuj malfaldis mian tripartan mapon de la Harco kaj montris al li la vojon por povi plani, kie ekzistas dormeblecoj. Ni rimarkis, ke ni tute ne havis ideon, ĉu 25 km tage fareblas por ni, do ni daŭrigis nian diskuton pri kion ni devas kunporti kaj kion pli bone lasi hejme por ne devi porti tro da pezo. Se iu planas fari similan aferon, ne hezitu demandi pri miaj nunaj spertoj. Plej gravis ja havi sufiĉe da akvo kaj manĝaĵoj, krome manĝilaro kaj dormsako ktp. Kio ankoraŭ necesis, eble evidentiĝos dum la rakonto.

Nia aventuro komencis dum la plej varma semajno en septembro. Ni ne kaptis la plej varmajn tagojn, sed tamen estis la perfekta vetero por resti en la naturo. Dum la tuta tempo ne pluvis, nur dum la unua nokto estis iom malvarma kaj la matena humideco foriris, post kiam la suno leviĝis super la montoj. Sed ekde la komenco:

La 16-an de septembro vespere ni pertrajne veturis al Herzberg, kie ni tranoktis ĉe mia familio. La sekvan matenon Janette kunportis nin al Osterode, kie troviĝas kaj ŝia lernejo kaj la komenco de la sorĉistina vojo. La antaŭtagmezo de la unua tago estis karakterizita per suprenirado, kio estas logika, kiam oni volas transiri montaron. Sed ni venis de la pli dekliva flanko, do ni jam ŝvitis post kvin minutoj kaj ne ĉesis ĝis nia tagmeza paŭzo. Jam 16 km estis marŝitaj, sur kiuj ni vidis jam multegajn laboristojn, kiuj per kamjonoj fortransportas mortajn piceojn el la arbaro, verŝajne ĉefe al havenoj, de kie ili pluveturas ŝipe ĝis Ĉinio. Kaj ankaŭ la unuajn stampojn de la “Harzer Wandernadel” ni jam kolektis. Ni ambaŭ havas kajeron, en kiu oni povas kolekti 222 stampojn, kiuj troviĝas dise en la tuta montaro. Posttagmeze ni trapasis multajn informtabulojn pri “Oberharzer Wasserregal”, kiu estas sistemo de kanaletoj, kiu estis konstruita dum la tempo de ercminado por mastrumi la akvon kaj per helpo de akvoradoj levi ercujojn kaj laboristojn el la minejoj. Ankaŭ hodiaŭ tiu historiaĵo servas por produkti energion kaj krome al la retenado de alta akvo, kaj al la akirado de trinkakvo. Kaj la avantaĝo por ni estas, ke sekvante la kanaletojn oni havas ebenan, nedeklivan vojeton kun freŝa aero kaj interesa flaŭro. Sekve ni iomete perdis la vojoj kaj eriris valon kun lago, en kiu mi decidis baniĝi por momento. Sekigtukon mi ja havis en mia pakaĵo. Refreŝigite ni grimpis la lastan monton antaŭ la urbo Altenau, kie ni aĉetis panon kaj supon.

![](Abb1.jpg)  
_Sur la lasta monto antaŭ Altenau. Inter niaj kapoj videblas la plej alta pinto de la montaro nomata “Brocken”._

Ni manĝetis, reŝtopis la manĝaĵojn en la dorsosakojn kaj rigardante sur la mapon decidis akiri plian sufiĉe proksiman stampon. Jam nun la marko de 25 km estis superigita kaj ni ankoraŭ ne trovis lokon por dormi. Sur la mapo montriĝis kabaneto kun fajroloko, al kiu ni direktis. Ĝojatendante la alvenon en tiu loko okazis, ke ni ne tuj trovis ja ĝustan vojon kaj poste devis konstati, ke ĝi estis kabano de ski-klubo, kiu ne provizis dormlokon al sovaĝaj migrantoj kiel ni. Do kun lacaj gamboj ni daŭrigis ĝis la sekva kabano, finfine irinte 32 km. Same kiel ni, du knabinoj planis resti tie. Bonŝance estis sufiĉe da spaco por ni. Do ni interkonatiĝis, kune vespermanĝis (per gaskuirilo ni varmigis la supon en poto), brosis dentojn kaj tuj endormiĝis, kiam ni kuŝis en la dormsakoj.

La dua tago komenciĝis per matenmanĝo, pakado kaj adiaŭo de la knabinoj, kiuj pro malbonfaro devis ĉesi sian migradon. Kun pli da energio ol vespere, sed malpli ol en la antaŭa mateno, ni daŭrigis nian vojon. Ĝis la plej alta pinto restis sufiĉe deklivaj vojoj, des pli ke barita vojo devigis nin malrimpi kaj regrimpi sufiĉe longan kromvojon. Survoje en Torfhaus ni replenigis niajn kvin akvobotelojn. La aldonitan pezon oni tuj rimarkis. Ĝis Brocken restis nur ĉirkaŭ 10 km kaj kiel dum la antaŭa tago kantado multe helpis resti bonhumura kaj plibeligi la tempon. Tri stampojn kaj ankaŭ tri riverfontojn pli poste ni estis sur la plej alta punkto de la sorĉistina vojo. Brocken ankau nomiĝas  Blocksberg / Blokmonto kaj laŭ legendoj estas la loko, kie sorĉistinoj kolektiĝas en la Valpurga Nokto kaj sur siaj flugbalailoj ĉirkaŭas grandegan fajron. Perfekta loko por manĝi pizan supon kun kolbaso kaj iomete ripozi. Sed ne tro longe. Pro la fortaj ventoj, kiuj regas la lokan klimaton oni prefere ne restu tro longe, aŭ oni malvarmiĝas.

Ĝis la fino de la taga etapo ni nur mapgrimpis la deklivon kaj havis energion por niaj cerboj, kiuj nun des pli rimarkis la mortajn arbojn ĉirkaŭe, kiujn ni jam vidis dum la tuta tago.

![](Aa_Abb3.jpg)  
_La matena vido el la kabano._

![](Abb2.jpg)  
_Ĉie en la Harco videblas mortaj piceoj. Tristige._

Sed sciante, ke la piceoj plantitaj en la 19-a jarcento, kiam pro la ercminado plejparte de la Harco estis senarbigita, originas en alia regiono, ne estas surprizo, ke ili malpli sukcese rezistas al la streso per neĝo, glacio kaj en la lastaj jaroj kreskantaj periodoj de sekeco. Pro tio la piceomonokulturo ankoraŭ estas pli vundiĝema per bostrik-atakoj. En la naturprotekta areo de Harco oni ne rajtas forpreni arbojn (escepte apud vojoj) kaj nun komencos periodo, en kiu novaj arboj havos la ŝancon kreski tie, kie nun ĉio ŝajnas esti morta.

Ni finis nian tagon en pli eta kabano jam ekster la naturprotekta areo. Ĉiuj mortaj arboj, kiuj falante povus detrui la kabanon jam estis forprenitaj. Oni do devis iomete pli marŝi kun la necesejpapero en la mano por ne esti vidata malantaŭ arbo. Sed post “nur” 23 km ankoraŭ tio eblis.

![](Abb4.jpg)  
_Jen kiel oni aspektas matene post du tre lacigaj tagoj de migrado._

La trian matenon vekis nin la sono de motorsegilo. Nun la piedoj jam matene doloras, sed ankoraŭ la duono de la vojo estis farendaj. Komence ni elektis vojon, sur kiu kuŝis multaj falintaj arboj. Nur je unu mi vundiĝis kaj ni baldaŭ atingis ejon, kie ni povis necesejumi, ankoraŭfoje replenigi la botelojn kaj trankvile glui plastron super la vundeton. Ni jam pripensis, kie aĉeti ankoraŭfoje manĝaĵojn, sed dum la tago evidentiĝis, ke ne plu estas ebleco survoje. Ankaŭ evidentiĝis, ke ni ne plu nur malgrimpos la monton, sed tiu fakto trafis nin nur je la lasta parto de la vojo. Intertempe ni estis trapasantaj kelkajn vilaĝojn, kolektantaj kelkajn stampojn kaj forlasante la altecon, super kiu preskaŭ nur kreskas piceoj. 

![](Abb5.jpg)  
_Unu el la vidoj, kiuj rekompecis la penon grimpi monton._

Do ni nun paŝas tra foliarbo, supren kaj suben, preter akvorezervujoj kaj la rivero Bode. Dum tiu tago ni ofte renkontis alian migranton, kiu komencis sian vojaĝon en Hamburgo kaj preskaŭ la tutan vojon piediris ĝis la Harco. Neimagable por ni, kiuj jam sopiris la atingon de Thale, la finon de la itinero. Sed bone, ankoraŭ la kantado donis sufiĉe da forto por atingi ankoraŭ pli foran kabanon jam post la sunsubiro. 31 km estis la atingo de la tago, entute jam 86 km. Tio signifas, ke por la lasta tago nur restis ĉ. 17 km (pro kromvojetoj iom pli ol 100 km).

Antaŭ ol endormiĝi ni havis la bonŝancon povi aŭskulti la konversacion inter du cervoj, kiuj kriante defendis siajn areojn. La lastan tagon pro doloroj ni ne plu vere povis ĝui la pejzaĝon de la Bode-valo, ĉar laŭ nia gusto estis tro da montetoj genantaj la trankvilon. Nur la penso pri kotleto kun frititaj terpomoj lasis nin plumarŝi ĝis la fino. Atingante Thale ni estis tre feliĉaj kaj fieraj pri tio, kion ni faris kaj vidis kaj aŭdis dum la lastaj kvar tagoj. Mi vere rekomendas tiajn travivaĵojn al homoj, kiuj ne havas altan demandon je komforto kaj povas ĝui la naturon. Min kaj Malte tre kunigis la spertoj kaj eble iam (ne tuj) ni refaros similajn komunajn feriojn.

Aŭtoro: Michaela
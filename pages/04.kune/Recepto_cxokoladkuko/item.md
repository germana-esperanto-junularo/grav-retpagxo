---
title: Ĉokoladkuko
date: '14-05-2019 00:00'
taxonomy:
    category:
        - receptoj
    tag:
        - recepto
hide_git_sync_repo_link: false
menu: Ĉokoladkuko
slug: cxokoladkuko
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
author:
    name: 'Michaela Stegmaier'
---

Laste okazis la 51-a naskiĝtago de mia patrino kaj mi decidis baki ĉokoladan kukon. Ĝi estis tre bongusta kaj pro tio mi ŝatus dividi kun vi tiun recepton, kiu nomiĝas „morto pro ĉokolado“.

![](image://recepto_cxokoladkuko.jpg)

Vi kunigu 124 g da butero, 150 g de sukero, 8 g (tekulereto) de vanila sukero kaj kirlu ĝis vi havas homogenan cremaĵon. Poste aldonu du ovojn unu post la alia, 80 g de konfitaĵo de nigraj riboj kaj malrapide ankoraŭ 250 g de buterlakto. Sekve tra kribrilo enmetu la farunon (150 g), kakaopulvoron (60 g), bakpulvoron (8 g) kaj natron (8 g). Kune kun 80 g de hakita malhela ĉokolado vi miksu la paston.

Nun via forno povas esti varmigita je 175°C kaj vi buterumigas vian formilon. Verŝu vian paston en ĝin kaj baku la kukon dum 45 minutoj. Poste vi lasas ĝin plimalvarmigi dum minimume 15 minutoj, dum kiuj vi povas okupiĝi pri la ĉokoladgazuro.
Ĝi konsistas el malhela ĉokolado (100 g), butero (25 g), kremo (100 g) kaj pulvorsukero (kulero da). Atente varmigu ĉion en poteto kaj kirlu, ĝis kiam ĉio kuniĝis.

La kukon vi prenu el la formilo kaj metos sur grandan teleron por plibeligi ĝin per via glazuro kaj sukerornamaĵoj laŭ via plaĉo. Jen vi povas vidi mian ekzempleron de la kuko. Se mi farus ĝin denove, tiam mi prenus pli da konfitaĵo kaj malpli da sukero. 

Bonan apetiton!


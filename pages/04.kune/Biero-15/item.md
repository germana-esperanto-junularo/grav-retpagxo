---
title: 15a BIERo
menu:  15a BIERo
date:  27-04-2019
slug:  biero-15
taxonomy:
    tag: [BIERo, Ĉeĥio, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Paul Würtz
---

Mi vojaĝis aŭte al Prago pere de petveturo, la kondukisto demandis pri mia celo en Ĉeĥio kaj mi diris 'Ĉoteborŝ'... Li: "Ĉoteborŝ, ne, kie estas?" Mi: "Du kaj duona horo oriente de Prago per trajno." Li: "Ĉoteborŝ, Ĉoteborŝ, ne, ne havas ideon. Kiel vi skribas ĝin?" Mi:"C-H-O-T-Ě-B-O-Ř, Ĉoteborŝ" Li: "Ah, vi volas al Ĥotjeboŝĉ!"

Tiel estis mia unu lekciono de ĉeĥa prononco dum unua parto de vojaĝo al mia unua BIERo. Mi jam venis malfrue kaj maltravis la promenado tra la urbo kiam mi alvenis en Prago. Post la unua trajno mi malfeliĉe ne trovis mian kajon pro mia nekompreno de ĉeĥaj trajnstacioj (se vi estas en la sama situacio, ili havas numerojn por kajo KAJ platformo, kontrolu antaŭe se vi ne kiel mi volas atendi du pliajn horojn ;) )

<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
      <img class="img-responsive" src="/grav/gej/user/images/biero2.jpg">
      <figcaption class="figure-caption text-center">Post bierumado</figcaption>
   </figure>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
      <img class="img-responsive" src="/grav/gej/user/images/biero4.jpg">
      <figcaption class="figure-caption text-center">BIERa naskiĝtago</figcaption>
   </figure>
  </div>
</div>

Kiam mi alvenis finfine je la 23a en Ĥotjeboŝĉ, mi tra la mallumo de la bohemia urbo promenis al la ejo, kiu por la biero estis internuleja lernejo, kie ni povis dormi en la ĉambroj de la lernantojn, kiuj por la semajnfino estis for je siaj familioj. La bieruloj dum mia alveno jam estis en serioza studo, pri la memmiksita citrona likvoro de unu de la partprenantoj. Mi por la duona parto de ĝi gustumis, kaj ankaŭ donis poentojn kaj opiniojn. La variantoj estis diversaj je partoj de citrona ĵeleo, alkoholo kaj acideco.

<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <figure class="figure">
      <img class="img-responsive" src="/grav/gej/user/images/biero3.jpg">
      <figcaption class="figure-caption text-center">Ĉevala manĝigado</figcaption>
   </figure>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <figure class="figure">
      <img class="img-responsive" src="/grav/gej/user/images/biero1.jpg">
      <figcaption class="figure-caption text-center">Bierfabriko</figcaption>
   </figure>
  </div>
</div>

La mateno komencis bierotipe kun memorganizita bufedo de la partoprenantoj. Poste ni prenis trajnon al apuda urbo. Je la stacidomo ni ankoraŭ festis la naskiĝtagon de unu de la partprenantoj kun kuko kaj ŝaŭmvino. La programo gvidis nin por rajdi ĉevalojn kaj nutri ilin kaj porkojn al bieno. Poste ni vizitis lokan bierofabrikon, kie ni estis gvidataj tra la proceso de bierfardo kun sekva gustumado. La resto de la tago manĝis, kantis kaj ludis ĝis la nokto fin'

La forveturmateno ni ankoraŭ uzis por festi la nakiĝtago de la renkontiĝo mem en kafejo kaj kun grupo de kvin aliaj mi refoje iris al prago, kie mi prenis buson hejme.

Al ĉiuj, kiuj ankoraŭ ne konas la BIERon, [E-mental](http://www.e-mental.cz/archiv/12961) aŭ ties aliajn renkontiĝojn, nepre viziti ĝies paĝaro kaj pripensu! Ekzemple VIANDO en Julio, au se vi volas post la SES turismumi en Prago, iru al ĜUPR. Eble vi eĉ akiros ministropostenon ;)

---
title: '5 bonaj ludoj por videoalvokoj'
date: '12:00 31-07-2020'
taxonomy:
    category:
        - Renkontiĝoj
    tag:
        - grupalvoko
        - ludoj
        - amuziĝo
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Nuntempe estas malfacila renkontiĝi persone, sed bonŝance ekzistas la interreto kaj tiel eblecoj por pasigi la tempon kaj eĉ amuziĝi samtempe. Jen kelkaj ideoj kion vi povas fari en viaj lokaj kluboj aŭ kun viaj (esperantlinvgaj) amikoj:

### Homlupoj
Homlupoj verŝajne estas la plej vaste konata kaj ofte ludata ludo en grupoj. Kaj sufiĉe facile eblas ludi ĝin interrete! Sur la retpaĝo https://play.werwolfonline.eu/Werwolf.php vi povas fari vian propran ĉambron por ludi kun viaj geamikoj. Simple kreu novan ludejon kaj disdonu la kodon por inviti aliajn homojn. Bedaŭrinde la retpaĝo estas en la germana, sed tamen eblas diskuti kaj ludi interrete

### Just one
"Nur unu" (aŭ angle Just One) estas kooperativa ludo kiu iom similas al tabuo. El la grupo de ludantoj unu persono estas elektita por esti divenulo. La aliaj homoj vidas vorton kiun ili devas priskribi al la divenulo - sed ili rajtas uzi nur unu vorton! Kaj iĝas eĉ pli malfacila: La sama vorto ne rajtas esti dirita de pli ol unu homo... 
Por tiu ludo ne ekzistas aparta retpaĝo, sed eblas bone ludi ĝin per kamerao kaj babilejo. Vi simple bezonas stokon da vortoj kiuj taŭgas por estis priskribitaj per nur unu vorto.

### Spyfall
Spyfall iomete similas al homlupoj. Vi estas grupo da homoj en iu loko, sed inter vi troviĝas spiono - kaj vi devas eltrovi kiu estas! Ĉiu persono ricevas rolon kaj lokon, ekzemple astronaŭto sur kosmostacio kaj devas respondi demandon pri ili (“Ĉu vi estas for de via hejmo?”, “Ĉu vi ricevas altan salajron?”, …). Unu homo estas la spiono (kiu ne scias ion ajn pri la loko en kiu troviĝas la aliaj kune) kaj devas plejeble bone respondi la demandojn evitante ke la aliaj iĝas suspektemaj... Post difinita tempo la grupo devas malkaŝigi la spionon. La ludon eblas ludi (eĉ parte en Esperanto!) sur https://spyfall.adrianocola.com/

### Skribbl.io
Eble kelkaj de vi konas tiun ludon ankoraŭ de la fama televidserio "Montagsmaler" - unu persono desegnas vorton kaj la aliaj devas diveni pri kio temas. Kaj eblas ludi ĝin Esperante kaj kune per la interreto sur skribbl.io! Simple enmetu propran liston da vortoj kaj invitu viajn amikojn! Ne gravas ĉu vi estas la plaj sperta artisto, ĝi ĉiukaze estos amuza!

### Codenames
Codenames ankaŭ estas divenludo. La ludantoj faras du teamojn kaj en ĉiu teamo estas unu gvidanto. Kiam vi komencis la ludon sur la paĝo https://www.horsepaste.com/ kaj invitis viajn amikojn, en la mezo de la (virtuala) tablo vi povas vidi 25 vortojn de kiuj kelkaj apartenas al la ruĝa kaj aliaj al la blua teamo. La gvidantoj de la teamoj devas doni indicojn al siaj teamanoj por ke tiuj eltrovu la vortojn apartenantajn al la propra teamo. Sed atentu ke vi ne neintence helpas al la alia teamo malkaŝigi vortojn!

Kiujn ludojn vi trovas plej taŭgaj por ludi en grupoj dum videoalvokoj? Skribu viajn konsilojn al ni!

Aŭtoro: Annika
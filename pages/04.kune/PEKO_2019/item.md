---
title: 'PEKO 2019'
date: '19-11-2019 00:00'
taxonomy:
    category:
        - 'Verdaj skoltoj'
    tag:
        - renkontiĝoj
        - PEKO
        - 'Verdaj skoltoj'
hide_git_sync_repo_link: false
menu: 'PEKO 2019'
slug: peko-2019
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
author:
    name:
        - Linko
        - Garano
        - Agutio
        - Mevo
        - Orcino
        - Ideal
---

#### Tendumado

![](image://PEKO_grupfoto.jpg)

Kontraŭ la kutimo de la lastaj jaroj, dum tiu-ĉi PEKO ĉiuj verdaj skoltoj (krom Ideal) dormis en tendoj. Por skoltoj tio estas pli aŭtentika kaj krome lasas pli da litoj por aliaj partoprenantoj de nia kongreso. Kaj tio estis necesa, ĉar ĉiuj litoj estis okupitaj. Por ni skoltoj estis ankaŭ bele esti iom apartaj por plifortigi la grupan spiriton. La starigado de la tendoj bezonas teaman kunlaboron kaj dormante kune restas tempo babili dum la vesperoj kaj dumnokte aŭskulti la ronkadon de la aliaj. Krom belega vetero sen pluvo dum la tuta tempo unu skolto havis plendojn: Linko havis tro varman dormsakon kaj ankaŭ la grundo sub la ŝia mato estis multe tro komforta! Tio ne estas vere defia tendumado! Kroma plendo pri tio, ke mankis piano, kio malplaĉis al ni.
Dum niaj skoltaj kunvenoj ni decidis kunveni pli ofte en la reto por ne devi sopiri la aliajn skoltojn tiom longe inter la maloftaj kunvenoj en la jaro. Krome ni ricevis de Korsako eŭropajn skoltotukojn, por montri nian internaciecon kaj ke ni subtenas la ideon de la Eŭropa Unio.

#### Manĝaĵoj

La skoltoj multe helpis al nia kara kuiristino Ella en la kuirejo. Ni tranĉis legomojn, lavis manĝilaron, purigis la tablojn kaj kuirejon kaj kreis bonan etoson per muziko, dancado kaj profundtemaj babiladoj pri dio kaj la mondo. Tamen foje estis bezonata la helpo de kelkaj bonvolaj partoprenantoj, al kiuj ni volas danki. Eĉ pli grandan dankon al Ella, kiu tiom pacience kaj bone planis kaj organizis la tutan manĝadon bongustegan!

#### Tragedioj

Sed bedaŭrinde ankaŭ ĉe la Verdaj Skoltoj ne ĉiam ĉio bone funkcias. Ni bedaŭras anonci, ke vundiĝis Ideal kaj origine vegana supo iĝis vianda. Pli granda bedaŭro estis la preskaŭa rompo de longa tradicio. De multaj PEKOj la skoltoj ĉiam malkovris groton en la ĉirkaŭaĵo de la ejo. Ĉi-foje troveblis nur iama ŝtonrompejo en la vilaĝeto. Ni iris tien pro voko de homlupoj, kiujn ni aŭdis. Tio estis eraro, ĉar ĝi mortigis ĉiujn skoltajn vilaĝanojn. Ni petas momenton de silento pro tiu tragedio.

#### Fajro

Ĉiu bona skolta renkontiĝo devas havi kelkajn gravajn aferojn: 
(1) skoltojn evidente (2) fajron kaj (3) kantadon.
Ni decidis kombini ĉion kaj kantis ĉe bele memfarita paska fajro. Aliĝis eĉ kelkaj (neskoltaj) partoprenantoj, kiuj bele kunkantis. Inter alie ni kantis la skoltan himnon, aron de belaj nostalgiaj REF-kantoj kaj plurajn kanonojn. Ankaŭ ni rostis bastonpanon kaj sukerpufaĵojn super la fajro. Kiom belegaj kaj plaĉaj estas vesperoj ĉe fajro!

Skoltamike salutas vin
Linko, Garano, Agutio, Mevo, Orcino, Ideal

![](image://PEKO_paska_fajro.jpg)

---
title: 'Recepto por baki verdan bulko-stelon'
media_order: IMG_20201116_091355.jpg
date: '15:00 30-11-2020'
taxonomy:
    category:
        - receptoj
    tag:
        - kristnasko
        - KEKSO
        - recepto
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Beligu bufedojn per bongustaĵo kun esperanta etoso!

Ingrediencoj:  
400 g da tritika faruno  
100 g da sekala faruno (tipo 1150)  
½ kubo da gisto  
340 ml da akvo  
1-2 kuleretoj da salo  
Se vi volas verdigi ĝin: 80 g da mola butero kaj manpleno da hakitaj verdaj herboj (eblas freŝaj aŭ frostitaj aŭ sekigitaj – mi uzis petroselon)

Instrukcioj:  
Miksu kaj knedu ĉiujn ingrediencojn dum kelkaj minutoj. Se la pasto gluadas ĉe viaj manoj, aldonu iom pli da faruno. Se ĝi estas tro seka, aldonu iom pli da akvo.
Sur farunigita submetaĵo dispecigu la paston al 21 buloj. Iom knedu ĉiujn bulojn kaj inter viaj manoj rondigu ilin.  
Preparu bakpleton kun bakpapero aŭ graso kaj metu la bulojn sur ĝin kun iom da distanco, ĉar ili ankoraŭ ŝveliĝos: Metu unu pastobulon en la mezon. Ĉirkaŭ ĝi aranĝu kvin pliajn pastobulojn. En rektaj vicoj aldonu po du pliajn bulojn al ĉiu de la kvin ĵus metitaj buloj. Kaj laste vi metu la kvin restantajn pastobulojn en la liberajn spacojn inter la branĉoj.  
Jen via stelo!  
Por verdigi ĝin miksu la buteron kun la herboj kaj ŝmiru tion sur vian stelon.  
Lasu ŝveliĝi ĉion dum 1 horo kaj baku la tuton dum 20 minutoj ĉe 200° en forno sen ŝalti la ventumilon.  
Lasu iom malvarmiĝi kaj ĝuu sola aŭ kun kunloĝantoj (en pandemio) aŭ kun amaso da esperantaj amikoj (post pandemio). Bonan apetiton!

Aŭ elprovu tion:  
**Verda pasto**  
Miksu spinacon kun la akvo antaŭ knedi la paston por verdigi ankaŭ la paston.  
**Abio vi, abio vi**  
Anstataŭ fari 21, faru nur 16 bulojn kaj metu abion:  
1 pinta bulo, sub ĝi du buloj, sub ili tri bulojn, sub ili kvar bulojn, sub ili kvin bulojn, kaj fine unu en la mezo kiel trunkon.

Ne forgesu afiŝi fotojn de via bakaĵo: #bakustelon

![](IMG_20201116_091355.jpg)

Aŭtoro: Carolin

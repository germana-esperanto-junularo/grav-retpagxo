---
title: 'Malnovaĵoj de GEJ – Antaŭ 10 jaroj'
media_order: colleg.jpg
date: '17:33 19-06-2021'
taxonomy:
    category:
        - Artikolo
    tag:
        - renkontiĝoj
        - malnovaĵoj
        - kune
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Da die DEJ dieses Jahr 70 Jahre alt wird, haben wir für euch mal in den alten Mitgliederzeitschriften gewühlt und werden in jeder weiteren Ausgabe in diesem Jahr Auszüge aus Artikeln von vor ca. 10, 20, 30 usw. Jahren veröffentlichen. Fangen wir mit der jüngsten Vergangenheit an. Die _kune_ als Teil der _Esperanto aktuell_ gibt es seit 2011, also jetzt genau 10 Jahre. Die Themen waren gar nicht so anders als jetzt, aber lest selbst. Die vollständigen Artikel findet ihr im Zeitschriften-Archiv auf esperanto.de.  

Eure kune-Redakion 2021 :)

**Kune vor 10 Jahren**

„Damals“ vor 10 Jahren war der erste Text darüber, dass die kune nach einer längeren Pause nun wieder erschienen ist. Dieser Bericht erläutert die Umstände der kune Anfang 2011, denn sie war nicht immer Teil der Esperanto aktuell!

> **Wo bleibt denn diese kune?**
> 
> Wo bleibt denn jetzt endlich diese kune? Und warum bekomme ich denn jetzt Esperanto aktuell? Was ist hier eigentlich los? Wenn ihr euch das auch gefragt habt, hier ein paar klärende Worte: Wie ihr sicherlich gemerkt habt, hat es seit längerer Zeit keine kune mehr gegeben. Mancher fragte sich gar, ob es sie überhaupt noch gibt. Nun, an sich schon. Doch da wir, Caroline und Miriam, uns seit anderthalb Jahren und noch bis zum Sommer durch die sächsische Abiturstufe quälen müssen, sind wir nicht in der Lage, genügend Zeit aufzubringen, um den ganzen Prozess des Sammelns von Artikeln, deren Zusammenstellung und, vor allem, das Layout zu bewältigen. Und auch die Zeit danach lässt sich schwierig einschätzen, so dass wir diesbezüglich für nichts garantieren können. Aber wir wollen die kune auch nicht DEJ-Geschichte werden lassen. Deshalb haben wir uns zusammen mit dem Vorstand Gedanken gemacht, wie wir die kune mit kleinerem Zeitaufwand trotzdem weiterführen könnten. Es wurden verschiedene Ideen geäußert, und eine kristallisierte sich schließlich heraus: Man könnte die kune mit Esperanto aktuell, der Mitgliederzeitschrift des Deutschen EsperantoBundes (also das DEJ-Äquivalent für alle über 27) zusammenlegen, und so insbesondere die Layoutarbeit abgeben. Dieser Vorschlag traf auch bei der EA-Redaktion auf offene Ohren, denn gerne will man ja auch die Mitglieder des DEB informieren, was in der Jugend so passiert, und auch für die Jüngeren kann es ja nicht schaden, einen etwas weiteren Blick in den Verein zu erhaschen. Und wenn ihr anderer Meinung seid, dann blättert einfach bis zu den kune-Seiten durch. Das Ergebnis der ersten Zusammenarbeit liegt nun in euren Händen. Bitte, lasst uns wissen, wie euch diese neue Entwicklung gefällt oder was gar nicht geht. Erreichbar sind wir per email: kune@esperanto.de. Auch Artikel und andere Beiträge (auf Deutsch oder Esperanto) sind jederzeit willkommen! Aber jetzt wünschen wir erstmal ein frohes, neues Jahr und viel Spaß mit den neuen kune-Seiten!
> 
> Caroline und Miriam

**JES vor 10 Jahren**

Auch schon zum Jahreswechsel 2010/2011 gab es das JES. Damals fand es in Burg (Spreewald) zum zweiten mal (zum ersten mal in Deutschland) statt.

> **JES 2010/2011 in Burg (Spreewald)**
> 
> Den Jahreswechsel in internationaler Atmosphäre im verschneiten Spreewald feiern? Mal ein bisschen die brandenburgische Provinz aufmischen und nebenbei noch so viel Kultur wie möglich mitnehmen? Hört sich doch nach einem Plan an, oder? Da gab es auch bei den rund 252 vorwiegend jungen Esperantisten, die sich vom 27. 12. 2010 bis zum 02. 01. 2011 im Örtchen Burg trafen, keinen Zweifel. Sie alle zog es zur JES (Junulara E-Semajno). Auf die Teilnehmer aus rund 30 verschiedenen Ländern wartete ein umfangreiches Programm, welches unter anderem Vorträge, Sprachkurse, Konzerte, Theateraufführungen beinhaltete, alles unter der Überschrift „Estinteco, Estanteco, Estonteco“. [...]  
> Für mich selbst sollte das JES das erste größere, das heißt internationale, Treffen sein. Meine bisherigen Esperantoerfahrungen beschränkten sich auf das Internet und den zweimaligen Besuch des KEKSO-Treffens. So wagte ich also am 27. Dezember meine ersten unsicheren Schritte in der Esperantowelt. Zum Glück traf ich in Cottbus am Bahnhof auf eine große Gruppe aus der Ukraine, an die ich mich nur noch hinten anhängen musste. So gelangte ich ohne Probleme in den richtigen Bus, fand die „junulargastejo“ (Jugendherberge) und damit die „akceptejo“ (Anmeldung). Anschließend machte ich mich auf den Weg zum „amasloĝejo“ (Massenunterkunft), um mir dort meine Schlafstatt für die folgenden sieben Tage zu suchen. Da traf ich dann auch gleich auf Keksofreunde, so dass ich mich gleich wie zu Hause fühlte. […] Im Programmheft waren all diese Veranstaltungen unter Überschriften zu finden, wie „Kleiner Schnellkurs der niederländischen Sprache“, „Journalistisches Trainingsseminar“ oder „Politische Perspektiven für Esperanto in der Europäischen Union“ und so weiter. Des Nachts konnte man gemeinsam im „gufujo“ (Uhuland) sitzen, eine gemütliche Teestube zum spätabendlichen Pläuschchen. Die meisten schliefen im amasloĝejo, welches in der örtlichen Turnhalle untergebracht war. Dort machten wir es uns alle mit Schlafsäcken, Decken, Iso- und Sportmatten bequem. Das junulargastejo war Schlafstatt für die Luxusliebenden und der Ort zum Essen für diejenigen, die sich nicht über die Woche selbst versorgen wollten. Den Höhepunkt des Treffens stellte der Silvestra Balo (Silvesterball) in der Aula dar. Es wurde gefeiert, getanzt, getrunken, gespielt, und auch ich hätte gerne die gesamte Nacht durchgefeiert, wenn ich nicht bereits am nächsten Morgen hätte früh aufbrechen müssen. […]  
> Wer einmal die überwältigende Erfahrung eines Esperantotreffens gemacht hat, vergisst das so schnell nicht. Und so fiebre ich schon dem nächsten aranĝo (Treffen) entgegen. Ich hoffe, wir sehen uns dann dort.
> 
> Salutas vin amike  
> Carl Bauer

**KEKSO vor 10 Jahren**

Auch das KEKSO gab es 2011 schon. Hierzu nochmal ein Bericht aus der Esperanto aktuell 3/2011:

> **KEKSO**
> 
> Schon beim Abendessen war er Chrissy und mir aufgefallen. Dieser merkwürdige Name. Er prangte dick und fett auf den Namensschildern, der etwa 15 Leute an unserem Nachbartisch. KEKSO. Als wir nachfragten, was denn dieses ominöse Wort zu bedeuten habe, erklärte man uns, KEKSO stehe für Kreativa Esperanto-KurSO und sei ein Treffen der Deutschen Esperanto-Jugend. Esperanto... Das war doch diese Plansprache... oder vielleicht doch eine Kaffeemarke? Noch bevor wir weiter grübeln konnten, hatten uns die KEKSO-Menschen schon eingeladen, doch mal in den Kursen vorbeizuschauen. […] Als wir die Tür zum Tagungsraum 3 öffneten, blickten uns viele freundliche Augenpaare aus dem Halbdunkel entgegen. Uns wurden zwei Stühle hingeschoben und schon richtete sich aller Aufmerksamkeit wieder auf die Leinwand. Darüber flackerten Bilder von fröhlichen Menschen, die lachten, tanzten, redeten, Spaß hatten. Doch wir verstanden kaum etwas. Einige Wörter kamen uns entfernt bekannt vor, doch wirklich entschlüsseln konnten wir das Wirrwarr an Wörtern nicht. Im Anschluss an den Film gab es eine große Schwarzwälder Kirschtorte zum zehnten Jubiläum des KEKSOs. Während wir versuchten den Berg an Sahne, Biskuitteig, Schokolade und Kirschen zu bewältigen, konnten wir uns mit ein paar Leuten unterhalten. Da war zum Beispiel Leo, der Organisator des Treffens und Esperanto-Muttersprachler. Brandon, ein Gast aus Irland, der im Moment in der Weltgeschichte herumreiste. Alina, die neben Esperanto auch noch Chinesisch sprach. Oder Armin aus Berlin, der mich beim anschließenden Spaziergang zu einer Bar in der Innenstadt über die Grundregeln der Esperanto-Grammatik aufklärte. Ich war verblüfft, wie einfach diese Sprache war. Natürlich wusste ich, dass es der Sinn von Esperanto ist, möglichst leicht erlernbar zu sein. Aber es war trotzdem überraschend, wie weit man Grammatik vereinfachen kann. [...] Unseren Abend verbrachten wir mit Werwölfe erlegen, Menschen von ihren Psychosen heilen, Musik machen und tanzen. Viel zu schnell war der Sonntag und somit der Abschied gekommen. Es fiel uns nicht leicht, uns von den Menschen zu trennen, die irgendwie unsere Freunde geworden waren. Doch wir sind neugierig auf Esperanto geworden und fest entschlossen, diese faszinierende Sprache zu lernen.
> 
> Ĝis la venonta KEKSO! ;)
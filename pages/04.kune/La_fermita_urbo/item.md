---
title: 'La fermita urbo'
date: '09-05-2019 00:00'
taxonomy:
    category:
        - Libroj
    tag:
        - libro
hide_git_sync_repo_link: false
menu: 'La fermita urbo'
slug: la-fermita-urbo
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
author:
    name: 'Annika Förster'
---

“La fermita urbo” estas scienc-fikcia romano de István Nemere el la jaro 1982, eldonita de la Hungara Esperanto-Asocio kaj origine en Esperanto verkita.

![la fermita urbo](image://la_fermita_urbo.jpg)

La ĉefrolulo estas la juna sciencisto Mark Bellor, kiu komencas novan laboron en psikosociologa instituto en Thorlund en la jaro 2533. En la instituto oni priesploras fermitajn sociojn, ekzemple kosmonaŭtojn en malproksimaj spacbazoj. Sed ankaŭ ekzistas tie la “Adapta Parto” kie devas loĝi krimfarintoj kondamnitaj al eterna izolo.
Baldaŭ Mark trovas amikon en sia nova laborejo, la intendanton Erni. Kune ili ekrimarkas ke io strangas en la instituto: La energifakturoj estas tro altaj, ofte venas nokte iuj helikoptoroj por alporti grandan aron da varoj pri kiu neniu havas detalajn informojn kaj en la apuda arbaro troviĝas granda ŝakto kiu enhavas aerpumpilon. 
Kiam Mark decidas subengrimpi la ŝakton por eltrovi kio estas tie kaŝita, li malkovras gravan sekreton: Sub la instituto troviĝas fermita urbo kvazaŭ mezepoka, kun multe da enloĝantoj kiuj ne scias ke ankaŭ ekzistas alia mondo ekstere. Oni uzas tie nur primitivan teknologion, preskaŭ neniu havas sufiĉe da manĝaĵo kaj estas brutaj bataloj inter du grupoj: La kredantoj de la Sankta Cirklo, gvidataj de la Pastro kaj la soldatoj de la Princo.
Mark estas ŝokita: Kiuj estas tiuj homoj? Kial ili loĝas subtere en primitiva urbo? Kiel oni povas helpi al ili solvi la konfliktojn? Kaj ĉu la institutestro entute scias kio okazas sub la instituto?
La lingvostilo de István Nemere estas, kvankam ne tro simpligita, facile legebla. La du unue ŝjane neligitaj rakontoj – tiu de Mark kaj tiu de la homoj en la urbo – interesigas la libron, ĉar oni volas eltrovi kiel ili estas kunligitaj. Tamen jam frue al la leganto estas sufiĉe klara kiel la okazaĵo progresos, kaj almenaŭ mi neniam estis vere surprizata dum la legado. La problemsolvon je la fino mi estimas tro simpla.
Malgraŭ tio la temoj traktataj – kiel sin kondutas homoj en fermitaj socioj, kion oni rajtas fari al brutaj murdistoj – estas gravaj kaj interesaj. 
Entute la romano estas leginda, se ne pro inspirita enhavo, des pli pro la signifo de István Nemere por la Esperanto-literaturo. Li skribis 23 originajn verkojn pri diversaj temoj el kiuj multaj (interalie “La fermita urbo”) estas jam reeldonitaj.
Laŭ mi la verko bone taŭgas por iomete-sed-ne-tro-progresinto de Esperanto. Plejparton de la teksto oni komprenas per kono de bazaj radikoj kaj eblas lerni kelkajn novajn vortojn kiuj estas bone aplikeblaj en la ĉiutaga vivo. Kio krome agrabligas la komprenon estas la fakto ke nek la frazoj nek la ĉapitroj estas tre longaj.
Resume mi rekomendas legi “La fermita urbo” se oni volas plibonigi sian lingvokonon aŭ legokapablon aŭ simple por konatiĝi kun grava reprezentanto de la Esperanta beletristiko.

Bazaj faktoj:

Titolo: La fermata urbo
Aŭtoro: István Nemere
Eldonejo: Hungara Esperanto-Asocio
Paĝoj: 158
ISBN: 3-901752-19-6

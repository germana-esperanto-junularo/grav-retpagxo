---
title: Sinprezento - David Mamsch
menu:  Sinprezento - David Mamsch
date:  14-03-2018
slug:  sinprezento_david
taxonomy:
    tag: [personoj, esperantistoj]
    category: Personoj
author:
    name: David Mamsch
---

Saluton,
mia nomo estas David kaj origine mi venas el Lepsiko (germane Leipzig). Mia "Esperanto-vivo" komenciĝis en 2015 ĉe la SES (somera Esperanto studado) en Slovakio, kiu estis mia unua internacia evento.

![David](image://david.jpg)

Nun mi eĉ loĝas en Slovakio, kie mi nuntempe pasigas unujaran volontuladon (EVS - european voluntary service) en la organizo "edukado@interreto". Tiel mi havas la ŝancon lerni de la homoj kiuj ĉiujare organizas la SES-on kaj kuj organizis la UK-on 2016 en Nitra (Slovakio) kaj la Polyglot Gathering en Bratislava. Dum mia volontulado mi ĉefe okupiĝas pri tradukoj kaj la organizado kaj planado de aranĝoj.
 
En la estraro de GEJ mi ŝatus helpi al la organizado de la JES2018 (junulara E-semajno) kaj eventuale transdoni sciojn kaj spertojn al estontaj organizantoj.
 
Krom Esperanton mi ŝatas vojaĝi kaj pli-malpli ĉion, kio iel rilatas al muziko.

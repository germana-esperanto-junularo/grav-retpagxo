---
title: Neuer Jugendstammtisch in Herzberg
menu:  Neuer Jugendstammtisch in Herzberg
date:  24-05-2017
slug:  stammtisch-herzberg
taxonomy:
    tag: [Herzberg, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: Michaela Stegmaier
---

![Stammtisch](image://stammtisch.jpg)

Meine Freunde der Herzberger Esperanto-Jugend folgten am Sonntagabend,
den 12.02.17 meiner zweiten Einladung zum Stammtisch mit dem Wunsch,
dass alle nochmal zusammen an einen Tisch kommen, bevor ich mein
Auslandssemester in Polen antrete. Ich selbst kam auf Umwegen von Sande
nach Herzberg und außer meiner Mama und meiner Schwester Janette wartete
schon mein erster Gast auf mich. Rieke, die ich vor mittlerweile sieben
Jahren auf dem Jugendaustausch SALO der Städtepartnerschaft
Herzberg-Góra kennengelernt habe kam aus Braunschweig, um alte Freunde
wiederzusehen. Wir fingen an Schnitten und Getränke vorzubereiten, als
es klingelte und Felix vor der Tür stand. Ihn kenne ich schon seit
langem vom Turnen und die letzten zwei Jahre hat er mich zum JES
begleitet. Ich hatte eine Überraschung für ihn: In Sande, wo ich übers
Wochenende an einem Siebdrucklehrgang der Niedersächsischen Turnerjugend
teilnahm, hatte ich Aufkleber mit dem Logo der DEJ gedruckt, weil Felix
sich bei unserem letzten Treffen Aufkleber gewünscht hatte. Durch das
Druckverfahren waren etwa 20 Aufkleber auf einem großen Bogen und ich
gab Felix die Aufgabe, sie mit einem Schneidebrett in das richtige
Format zu bringen. Es stellte sich heraus, dass diese Aufgabe den Abend
über mehrere Leute beschäftigte. Als nächstes kam Cornelia zu uns. Sie
studiert mittlerweile in Germersheim und ist gerade für die
Semesterferien in die Heimat gekommen, wo sie für das Esperanto-Zentrum
eine große Unterstützung darstellt und immer fleißig und fröhlich
mithilft. Sie übernahm Felix Aufgabe am Schneidebrett und er kümmerte
sich um die Resteverwertung der Aufkleber. Als nächstes kam Mike, den
wir alle schon lange nicht mehr gesehen hatten. Bis 2014 nahm auch er
regelmäßig am SALO teil. Er ist kürzlich 18 Jahre alt geworden und lernt
jetzt Sozialpädagogik. Anna komplettierte unsere Runde, sie brachte mir
ein schönes violettes Notizbuch mit, und da ich tolle Freunde habe, war
das nicht mein einziges Geschenk. Dann aßen wir zusammen Abendbrot,
erzählten von alten Zeiten, sortierten Aufkleber, diskutierten Riekes
Liebesangelegenheiten, sprachen über die Organisation des SALO und wie
es mit dem Stammtisch weitergeht, wenn ich weg bin.

Sogar Zsófia Kóródy kam vorbei, freute sich über unsere Zusammenkunft,
schenkte uns eine Packung polnischer Süßigkeiten und machte ein
„grupfoto“ mit uns. Wir hatten viel Spaß und Cornelia fasste den Abend
folgendermaßen zusammen: „Wir dekorieren eine Flasche, hören
Esperanto-Reggae und sind total entspannt und ausgelassen“. Mike, der um
„halb zwanzig Uhr“ nach Hause fuhr, um mit seiner Familie Pizza zu essen
sagte: „Es war total schön, alte Freunde und Bekannte wiederzusehen“. Es
lag Liebe in der Luft und alle waren gerührt von dieser Aussage. Felix
beschrieb den Abend in einem Wort: „Gluema!“, ihm klebte dabei ein rotes
Herz auf der Wange. „Bei so viel Energie und Lust, werden sich gute
Erfolge einstellen.“, sagte Zsófia, die sich über so viel Unterstützung
bei der Organisation des nächsten SALO freut. Meine Mama lobte uns für
die gute Arbeit mit den Aufklebern. Bevor alle gingen nahmen sie sich
auch ein paar Früchte unserer Arbeit mit. Zum Schluss möchte ich euch
noch die Weisheit des Abends mitgeben: „Lachgummi ist der Konter zu
Weingummi“ (Rieke). Und obwohl Felix vergessen hat, sich Aufkleber
mitzunehmen, gilt natürlich: „Fine ĉio estas en ordo“.

Ich vermisse euch und freue mich auf den SALO Anfang Juli, Eure Michaela

Wenn ihr in der Nähe von Herzberg wohnt und andere Jugendliche
kennenlernen wollt, die sich für Esperanto interessieren, dann meldet
euch [bei mir](mailto:michaela[punkt]stegmaier[at]esperanto[punkt]de) oder beim
[Esperanto-Zentrum](mailto:esperanto-zentrum[at]web[punkt]de) und ich schicke euch
einfach eine Einladung zum nächsten Treffen per Mail. Oder Aufkleber per
Post

---
title: 'La nova vero'
date: '14:30 30-09-2020'
taxonomy:
    category:
        - Poemo
    tag:
        - lernado
        - poezio
        - vero
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

### La nova vero

Ĝi estas en ĉiu gazeto:  
Milito neniam fariĝis!  
Plu nia paseo klariĝis  
post nova kritika enketo

Scienco nur nun malkonfuzas  
Erare taksitan suferon  
kaj miskomprenitan mizeron:  
Simpligi saĝuloj rifuzas

Aŭtoro: Henri (eklernis Esperanton antaŭ tri monatoj)
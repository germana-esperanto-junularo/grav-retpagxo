---
title: Ĉiusemajna demando
menu:  Ĉiusemajna demando
date:  01-05-2018
slug:  cxiusemajna_demando
taxonomy:
    tag: [demando, wikipedio]
    category: Artikoloj
author:
    name: Zam
---

Ekde la mezo de aprilo aperas ĉiusemajne nova demando en nia Facebook-paĝo. Vi povas komenti kaj plusendi por scii kion pensas viaj amikoj pri tio. Jen ekzemplo kaj ebla respondo:

![Demando](image://cxiusemajna_demando.png)

La Fluganta Spagetmonstro aŭ ankaŭ Spaflumo estas la Dio de la religio nomata “flugantspagetmonstrismo” (FSM) aŭ “pastafarismo” (Flying Spaghetti Monsterism aŭ Pastafarianism angle). Bobby Henderson komencis la flugantspagetmonstrismon interrete kiel religiansatiron, ĉar regiona estraro de edukado en Kansaso (usona ŝtato) decidis, ke dum la leciono pri biologio, la teorion de evoluado oni instruu nur kune kun tiu de “inteligenta dezajno” (ID). Pro tio Henderson sendis oficialan leteron al la Kansasa Komitato de Edukado. En la letero li skribis, ke ankaŭ la Fluganta Spagetmonstro devos esti instruata en la lernejo. Li ankaŭ diris, ke lia letero pruvas, ke la Fluganta Spagetmonstro estas almenaŭ tiel bona teorio pri la komenco de la universo kiel tiu de inteligenta dezajno.

Esence, la tiel nomataj pruvoj de Henderson paralelas kaj tiel celas moki la argumentojn de la proparolantoj de inteligenta dezajno kaj kreitismo.

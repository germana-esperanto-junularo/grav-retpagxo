---
title: Riismo – reagoj
menu:  Riismo – reagoj
date:  21-01-2015
slug:  riismo_reagoj
taxonomy:
    tag: [lingvo]
    category: Artikoloj
author:
    name: Legantoj
---

Koncernante la artikolon pri riismo kiun vi povis legi en la antaŭa
eldono de -kune- ricevis ni multajn reagojn. Kelkajn el ili ni volas ĉi
tie presi, la aliajn vi trovos ĉe http://www.esperanto.de/dej/blog

Ni ĉiam ĝojas pri reagoj al artikoloj kaj invitas vin esprimi vian
opinion pri la temo kaj sendu retpoŝton al kune@esperanto.de

Hallo, da mein Esperanto noch sehr dürftig ist, schreibe ich auf
Deutsch. Euren Artikel zu Riismo im kune fand ich sehr interessant, ich
hatte zuvor noch nie von dieser Möglichkeit gehört. Ich finde das Thema
sehr wichtig, die Umsetzung sehr einfach und praktikabel und würde mir
wünschen, dass sich ri als Pronomen durchsetzt und -icx- als Spiegelbild
zu -in-. Wie wunderbar einfach geschlechtergerechte Sprache doch sein
kann! Diese Chance darf Esperanto sich nicht entgehen lassen. Gruß,
Martin

Martin Kutzner

---

Saluton,\
havu jen mian opinion pri riismo kaj rilataj temoj.

Ja estas bona ideo plibonigi nian lingvon. Tamen oni ne baru al si la
vojon al taŭga futuro.\
Enkonduki novajn sufiksojn, neologismojn ktp., kiuj portas ĉapelitajn
literojn, estas dekomence due plej bona solvo. Estas apenaŭ imageble, ke
la UN, la EU aŭ aliaj similaj internaciaj organizaĵoj, se iam entute,
akceptos internacian neŭtralan lingvon kun alfabeto pli komplika ol la
Angla.\
Eĉ se inter la nun ekzistaj-uzataj oficialaj laborlingvoj de ĉi
organizaĵoj estas etnaj lingvoj kun supersignoj, tio ja ne signifas, ke
oni laŭ simpla racieco ne rezignu pri diakritsignaj alfabetoj, se oni
fine decidiĝas havi duan lingvon por ĉiuj.\
La Nova Helpalfabeto (NHA) aŭ RALFo (Racia Alfabeto) intertempe
prezentas taŭgan alternativon, kiu konsekvence modernigas la Esperantan
alfabeton. Ankaŭ la uzado den "na" kaj "den" estas bonaj pliampleksigo
den la lingvaj resursoj en nia komuna lingvo. Oni ankaŭ ne forgesu, ke
Zamenhof mem en sia Lingvaj Respondoj jam dubis pri la fina taŭgeco de
la "alfabeta diakriteco" (vd. §§ 58).\
Estas ja verŝajne, ke iom inerta ĉirilata agado de la Akademio, iom
inerta konservativismo en la Movado kaj iom troa konfido al la
ĉiopotenco de la elektronika maŝinaro kondukas al nenecesa barado de la
propra vojo. Krom la idea malforteco de la organizita kaj organizanta
esperantismo (ekz. manko de enstatutiigita ekolgie daŭripova bazo de la
propra laboro) ĉi neprogresemo fine malhelpas nepotaŭgan persepktivon de
la Movado.

Wolfgang Günther

---
title: 'Esploru la jidan'
media_order: '(g) duo jida.jpg'
date: '17:00 30-06-2021'
taxonomy:
    category:
        - Artikoloj
    tag:
        - lingvo
        - duolingo
        - jida
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
recaptchacontact:
    enabled: false
---

Post ses jaroj da atendado ĝi finfine alvenis: La kurso de la jida lingvo en Duolingo. Neniam daŭris la evoluigo de Duolongo-kurso tiom longe. La kialoj por tio estas diversaj: Sur la organiza flako estis multaj ŝanĝoj en la teamo de kontributantoj, kaj sur la praktika flanko estis defiaj teknikaj demandoj solvendaj: Kiun dialekton oni instruu? Kiun skribmanieron oni uzu? Al kiu oni entute celu la korson? Ĉu specife al homoj kun jida heredaĵo aŭ ĉu al pli ĝenerala grupo?

La unuaj du demandoj estis responditaj per kompromiso: La kurso instruas per la elparolmaniero de la ĥasida dialekto, kiu estas denaska al la plimulto de la hodiaŭa parolantaro. Tamen, kiel skribsistemo oni elektis la normigitan YIVO-ortografion. Nekutima elekto, sed ne malsaĝa! Ĉar Duolingo ĝenerale celas alparoli kiel eble plej multe da lernuntoj, la lastaj demandoj respondas sin mem.

![%28g%29%20duo%20jida](%28g%29%20duo%20jida.jpg "%28g%29%20duo%20jida")

El alia vidpunkto, el la vidpunkto de multokupata lingvolernanto, oni sin nature demandas: Kial indas investi tempon en lingvo (ŝajne) apenaŭ kaj dise parolata? Tion mi ŝatus iom esplori, kaj mi taksos min sukcesa se mi kapablas doni al vi inspireton mem eltrovi la diversajn facetojn de tiu lingvo. Malfacilas diri, kiam kaj kie ekzakte la jida lingvo estiĝis, sed la hodiaŭ plej vaste kredata teorio estas tiu, ke ĝi formiĝis en la 10a jarcento, kiam judoj el Italio kaj Francio migris en la rejn-valon. Tie ili intermiksis iliajn kunprenitajn lingvojn kun la mezaltgermana de la novaj najbaroj. Dum la sekvaj jarcentoj, pro setlo-movadoj kaj persekutado, la lingo disvastiĝis al orienta Eŭropo, kaj de tie ĝi migris al Usono, Israelo, Kanadao kaj aliaj partoj de la mondo.

Antaŭ la dua mondmilito, proksimume 80% el la tuta juda popolo parolis la jidan lingvon, en nomroj do inter 11 kaj 13 milionoj. Por doni al vi ideon kiom multe tio estas: Tio signifas, ke en tiu tempo la Jida estis - malantaŭ la angla kaj germana - la plej vaste parolita ĝermana lingvo. Oni facile imagas ke tiom granda nombro da parolantoj kaŭzis grandan eligon da kulturaj produktaĵoj: dum la tiel nomata ora tempo de la jida filmo, pli ol 130 filmoj estis produktitaj, ekzistis amaso da jidaj teatroj, kaj pro Isaac Singer (1902-1991) la jida literaturo povas eĉ drapiri sin kun unu nebel-premiito pri literaturo.

Kvankam la oraj tempoj por la jida kulturo estas for, hodiaŭ ĝi travivas renaskiĝeton: En 2017 “Menashe” estis la unua jidlingva filmo el Usono de la pasintaj 70 jaroj, kaj la grandparte jidlingva mini-serio “Unorthodox” de Netflix eĉ ricevis Emmy-premion. Impona estas ankaŭ la laboro de Aaron Lansky, kiu en la 70aj jaroj komencis savi alikaze forĵetitajn jidajn librojn, ja tutajn bibliotekojn. Tiom da homoj kontaktadis lin, ke li decidis dediĉi sian tutan vivon al la savado de jida literaturo kaj fondi la “Jidan Libro-Centron” (YBC). La kolektitajn librojn la YBC ciferecigas kaj havebligas, tiel ke nun pli ol 11.000 libroj povas esti legataj sur sia retpaĝo: originaj verkoj kaj ankaŭ tradukoj de ĉiuj imageblaj verkoj kiel tiuj de Shakespeare, Nietzsche, Remarque aŭ Heine.

La tradicia jida muziko jam antaŭ longe eniris la popularan kulturon pro la revivigo de “klezmer” in la 70aj jaroj. Artistoj kiel Daniel Kahn, Psoy Korolenko aŭ Isabel Frey evoluigas ĝin ĝis hodiaŭ per originaj kantoj kaj novaj interpretoj de klasikaĵoj. Fakte, dum la sekva JES vi havos la ŝancon sperti klezmer-koncerton de la germana grupo “Vagabondoj”.

Finfine ankaŭ Esperanto iasence estas historio jida. Zamenhof, kies denaska lingvo la jida estis, verkis la “novan judan lingvon” en provo modernigi la jidan por ke ĝi unuigu la judan popolon. Poste li abandonis tiun projekton favore al Esperanto. Kvankam Esperanto ricevis relative malmulte da jidaj vortoj, ĝi naskiĝis kun jida spirito.

Aŭtoro: Jan
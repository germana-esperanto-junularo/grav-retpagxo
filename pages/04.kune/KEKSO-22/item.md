---
title: 22. KEKSO in Herzberg
menu:  22. KEKSO in Herzberg
date:  21-05-2017
slug:  kekso-22
taxonomy:
    tag: [KEKSO, Herzberg, renkontiĝoj]
    category: KEKSO
author:
    name: Michaela Stegmaier
---

Der erste KEKSO des Jahres fand wie in der letzten Ausgabe zu lesen war in Freiburg parallel zum GEK statt. Wann aber sollte die zweite Auflage des Jahres passieren?

![Keksoj](image://kekso-22.jpg)

Aufgrund mangelnder freier Wochenenden seitens des Vorstandes, der mit der Durchführung eines Erasmus-projektes und der Vorbereitung der nächsten beiden JES schon ganz gut ausgelastet ist, bediente ich mich meiner guten Kontakte nach Herzberg am Harz und es kam
die Idee auf, einen KEKSO parallel zum TORPEDO zu organisieren. So weit so gut. Also plante ich einige Programmpunkte, andere ergaben sich von selbst. Übrigens war es auch für mich der erste KEKSO, also schon ein bisschen aufregend. Es meldeten sich Kamil aus Warschau, Marco aus Stuttgart und 5 weitere Jugendliche an, von denen 4 kurzfristig wieder absagten. Zum Glück aber hatte ich Luisa und Anna aus Herzberg dabei, meine Schwester kam auch einmal zu Besuch.

Am ersten Abend, es war schon relativ spät, lernten wir uns alle kennen und steckten ein paar Ziele für die nächsten Tage ab, der Konsens war: „Ni devas multe paroli!“. Dann machten wir es uns so bequem, wie nur irgend möglich, in dem hölzernen Gartenhaus, denn eine andere Übernachtungsmöglichkeit gab es nicht. Aber am nächsten Morgen mussten wir erstmal einkaufen gehen, denn außer Keksen und Obst, die von den Teilnehmern Mitgebracht wurden, hatten wir nicht viel. Zwischen Frühstück und Mittagessen gab es die erste Esperanto-Lektion zu „tabelvortoj“ und „afiksoj“ und danach gingen wir in die Stadtbibliothek, wo wir zu einem Vortrag von Sabine Fiedler zum Thema „frazeologio“ in Esperanto eingeladen waren. Mit uns saßen dort Teilnehmer eines Erasmusprojektes, das ebenfalls parallel in Sieber, einem Stadtteil von Herzberg stattfand und weitere interessierte. Am Abend haben wir uns prächtig amüsiert, indem wir 20-sekündige Videos für einen Wettbewerb drehten. Darin haben wir Figuren sagen lassen, warum wir eine bestimmte Sprache gerne mögen, auf Esperanto und vielen anderen
Sprachen. Dann stand noch ein Besuch zum Abendessen in Sieber an, es wurde das Ende des Projektes gefeiert, der 80. Geburtstag von Halina Komar und nur an dem Tag fanden wir heraus, dass auch unser Teilnehmer Marco Geburtstag hat. Deshalb gab es auch Kuchen und kleine Geschenke für ihn. Wir mussten allerdings vor Mitternacht noch unsere Filmchen hochladen, denn es war der letzte Tag dafür. Glücklich und zufrieden zogen wir uns in unsere Gartenhütte zurück. Am nächsten Tag stand Kekse backen an, denn wir hatten ja noch nicht genug. Des Weiteren gab es noch eine Stadtrallye mit allen wichtigen Esperanto-Sehenswürdigkeiten. Nach dem Abendessen zogen wir ins Gästezimmer im Haus um, das nun frei wurde. Dort fanden wir eine Schreibmaschine, auf der sogleich eine Geschichte und ein Tagebucheintrag entstanden. Am letzten Tag haben wir noch einmal die Zeit genutzt, um uns einigen Sprachspielen, einem Gedicht und Beispielen für Filme auf Esperanto zu widmen. Wir resümierten den KEKSO und machten noch einen Filmabend zusammen. Ganz spät abends gab es noch eine Überraschung: Cornelia, die den gesamten Sommer in Korea verbracht hatte, war wieder in Deutschland angekommen und besuchte uns nur einige Stunden nach ihrer Ankunft.

Der nächste morgen wurde nur noch zum Aufräumen und Verabschieden genutzt. Ich glaube, dass ich für alle Teilnehmer sprechen kann, wenn ich sage, dass wir eine Menge Spaß hatten. Außerdem möchte ich an dieser Stelle noch einmal unsere neuen Mitglieder Luisa Deppe und Marco Watermeier herzlich in der DEJ begrüßen. Wenn ihr den KEKSO mal in eure Stadt holen wollt, machen wir es gerne möglich. Ansonsten sind wir auch sicherlich mit einigen Vertretern wieder beim GEK in Zweibrücken anzutreffen. Macht es gut! Bis dahin!

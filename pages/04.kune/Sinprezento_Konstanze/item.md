---
title: Sinprezento - Konstanze Schönfeld
menu:  Sinprezento - Konstanze Schönfeld
date:  03-05-2017
slug:  sinprezento_konstanze
taxonomy:
    tag: [personoj, esperantistoj]
    category: Personoj
author:
    name: Konstanze Schönfeld
---

Mia nomo estas Konstanze, origine mi venas el Italio sed translokiĝis al Germanio antaŭ pli ol kvar jaroj por iĝi studento de la Universitato de Hajdelbergo. Nuntempe mi loĝas en Japanio, kie mi partoprenas en ŝanĝprogramo kun la Kjuŝu Universitato en Fukuoka. 

![Konstanze](image://konstanze.png)

Esperanton mi lernis en Germanio dum majo 2014; tuj poste mi partoprenis
la Poliglotan Renkontiĝon en junio 2014 kaj FESTOn en aŭgusto 2014 per
subvencio de IEJ. Dum ambaŭ renkontiĝoj mi konatiĝis por la unua fojo
kun esperantoparolantoj kaj amikiĝis kun pluraj movadanoj, kiuj tuj
enkondukis al mi agadon de TEJO kaj landaj sekcioj. Ekde tiam mi
okupiĝis pri diversspecaj projektoj, ĉefe en TEJO, interalie pri
kunordigo de la tradukado de la retpaĝo kaj EVS-subvencipetado 2015.
Plie, finon de 2015 mi iĝis grupestro kadre de la partnerado kun FoJE
por realigi EJOn (European Youth Event) 2016.

Kvankam mi nuntempe loĝas iom for de Eŭropo, mi nun estas estrarano de
GEJ por povi daŭre kontribui al Esperanta movado. Precipe, mi
respondecos pri projektoj kun TEJO kadre de subvencioj de Eŭropa Unio
kaj mi volas kontribui al la organizado de KEKSOj kaj JES.

---
title: Bonvenon arbon
menu:  Bonvenon arbon
date:  08-11-2015
slug:  bonvenon_arbon
taxonomy:
    tag: [rakonto, renkontiĝoj]
    category: Rakontoj
author:
    name: GEJ Vorstand
---

Ĉi tio estas traduko de la Norvega kristnaskkanto *Du grønne, glitrende
tre,* kies tekston verkis Johan Krohn. Ekzistas diversaj melodioj; mi
mem plej ŝatas tiun verkitan de Edvard Grieg.

    Bonvenu, arbo en verda bril',
    vi bonŝatato kun ĝoja helo,
    sur ĉiu branĉo kristnasklumil'
    kaj supre staras glimanta stelo!

    Ĝi linde oru, ke ni memoru
    pri nia di'.
    Dum kuŝis mondo en granda ve',
    ĝi lumon donis fidanto-ronden:

    Al la homaro ĝi montris, ke
    Jesuo eta senditis monden;
    brilĉarmon havis, l' anĝelojn ravis
    en Betlehem'.

    Pri Jesueto en rakontar'
    de panjo hejme ni aŭdis ofte;
    La lum' pri ĉiu pacema far'
    kaj vorto nin memorigos softe:
    La stelo helas kaj daŭre belas
    sur l' kristnaskarb'.

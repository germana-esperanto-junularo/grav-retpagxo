---
title: Raporto pri Tuluzo - KUNE
menu:  Raporto pri Tuluzo - KUNE
date:  15-09-2019
slug:  semeo-19
taxonomy:
    tag: [SemEO, Francio, Tuluzo, renkontiĝoj]
    category: SemEO
author:
    name: Konstanze Schönfeld
---

Meze de marto, mi havis la privilegion partopreni nekutiman esperantan aranĝon: la Esperanto-Semajnon de Tuluzo (mallongigo: SemEO), en Francio, organizita de la Tuluza Esperanto-Centro.

Kial nekutima? Ĉar SemEO ne luas tranoktejon, ne pagigas kotizon, ne plenigas viajn tagojn per prelegoj. Kaj fakte, ĝi daŭras multe pli ol unu semajnon, kaj ne limigas siajn eventojn al unu loko. Tute kontraŭe, ĝi proponas buntan, libere alireblan programon tra la tuta urbo dum pli ol dek tagoj, dum kiuj estas organizita io interesa por ĉiuspeca Esperanto-ŝatanto. Kadre de SemEO, atingis Tuluzon kaj esperantistoj el la vasta okcitana kaj kataluna regionoj kaj eksterlandanoj. Tamen, ne nur Esperantistoj reprezentas la celon de la eventaro: ĉiu evento rezultiĝas el kunlaboro inter la Tuluza Esperanto-Centro kaj aliaj instancoj, kiel la universitatoj de Tuluzo, kulturcentroj, lokaj organizoj kaj kvartalaj renkontiĝejoj.
 
Ene de kelkaj tagoj, mi povis malkovri plurajn kvartalojn de Tuluzo simple partoprenante en la diversspecaj programeroj. Tuj post mia alveno komenciĝis lingva festivalo en universitata centro, kie eksterlandaj studentoj pretigis dekminutajn prezentaĵojn pri siaj denaskaj lingvoj. La tagon poste, mi havis la ŝancon registri epizodon por loka radio en profesia studio. En la intervjuo mi paroladis ĉefe pri la agado de TEJO kaj pri la rolo de junuloj ene de la esperanta movado. Al radioelsendo sekvis entuziasmiga koncerto de La Kompanoj kun kantoj de Jacques Brel tradukitaj al Esperanto, informo-tablo pri la Esperanto-movado, promenado tra la historiaĵoj de la urbo, vizito al la Naturhistoria Muzeo kun loka esperantista gvidanto, ateliero pri Vikipedio kaj komuna vespertempo en tipaj manĝejoj kaj trinkejoj. Kompletigis mia vizito unutaga pertrajna ekskurso al apuda urbeto Albi kaj partopreno en la protesto kadre de la “Vendredoj por la Estonteco” kune kun francaj studentoj.

Konklude, frapis min la bunteco de la programo kaj ĉefe de la medio, en kiun vi povas plonĝi partoprenante SemEOn. Ne nur Francion, ne nur Esperantujon vi povas malkovri en Tuluzo: Okcitanio kaj okcitan-kataluna kulturo bonvenigos vin kore kun mirindaj pejsaĝoj, manĝoj kaj muziko, el kiuj transpiras la varmeco kaj komplekseco de siaj pluraj identecoj.

Aparte mi kaptas la okazon danki la ĉefkunordigantojn de SemEO, Marion kaj Christophe, kaj miajn gastigantojn Rikardon kaj Jeannette. Estis plezuro konatiĝi kun vi!
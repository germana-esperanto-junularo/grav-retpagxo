---
title: 24-a KEKSO en Heidelberg
menu:  24-a KEKSO en Heidelberg
date:  14-10-2018
slug:  kekso-24
taxonomy:
    tag: [KEKSO, Heidelberg, renkontiĝoj]
    category: KEKSO
author:
    name: Paul Würtz
---

Dum sufiĉe somera oktobra semajnfino ni ĉiuj alvojaĝis Heidelbergon.

![Keksoj](image://KEKSO-24.jpg)

Tiu urbo estas por tri partoprenantoj ilia hejmurbo, kio havis la agrablan efekton, ke ĉiuj aliaj partoprenantoj havis spertajn urbogvidantojn. Dank‘ al la subteno de la BAVELO-teamo ni ja ankaŭ havis luksajn lit- kaj manĝkondiĉojn kaj ŝancojn partopreni en pli profesiaj prelegoj, ol normale okazas en keksujo. Tiel la spertuloj povis diskuti pri internacia politiko, kaj havi enkondukon al kiel organizi sukcesan internacian helpprojekton pere de Esperanto. Sed ankaŭ por la komencantoj klare ekzistis enkondukaj kursoj, organizataj de internaciaj instruistoj.

Eble ja la aktive kreema parto de la KEKSO mankis ĉifoje, sed fabelaj elvidoj de la kastelo, la kastela ĝardeno kaj en la nokto al la trankvile fluanta rivero lasis tiun parton al la imago de ĉiuj mem. Sekvan fojon ni organizu pli bonan propran komencantan kurson, ĉar mi pensas, ke laŭ stilo kaj rapideco ni povus havi iun pli celgruptaŭgan.

---
title: Esperanto en SpongeBob Squarepants
menu:  Esperanto en SpongeBob Squarepants
date:  21-01-2015
slug:  esperanto_spongebob
taxonomy:
    tag: [lingvo]
    category: Artikoloj
author:
    name: Amerie Lommen
---

En julio de tiu ĉi somero mi partoprenis NASK-on (Nord-Amerika somera
kursaro) en Viktorio, Kanado. Unu vesperon mi kaj mia samĉambrano
decidis veturi al la urbocentro por aĉeti la plej novan volumon de
“Saga” (komikso en kiu estas raso de aliplanedanoj kiuj parolas
Esperanton) kaj en la komikso-vendejo ni parolis iomete kun la kasisto
pri Esperanto. Subite li menciis: “Estas ankaŭ viro en la plej nova
volumo de “SpongeBob” kiu parolas Esperanton, ĉu ne?”

Mi estis suprizita, ĉar unue mi ne sciis ke ekzistas bildstrio de
SpongeBob SquarePants, kaj due mi neniam pensis ke Esperanto estas en
tiu serio. Ni ekscitite petis lin pri pli da informo, sed tiu volumo ne
estis en la vendejo. Do li telefonis al iu, demandis nin pri niaj nomoj,
kaj diris ke ni povos aĉeti ĝin morgaŭ.

Je la venonta tago, ni (ambaŭ 20-jaraj virinoj) devis iri al publika
loko kaj diri al fremdulo: “Mi estas tie ĉi por aĉeti mian kopion de
“SpongeBob” kaj ni estis kompreneble iom embarasitaj, sed ankaŭ
ekscititaj vidi Esperanton en tiu popularega serio. La kvin-parta
rakonto temas pri heroo nomata “Mermaid Man” kaj lia batalo kun “Viro
Reganto”, kiu estas muskola, verda estro de malnova submara civilizacio,
kies anoj parolas Esperanton. SpongeBob aŭdis pri la batalo kaj volas
scii kiu venkis – kaj Mermaid Man kaj Viro Reganto atribuas la venkon al
si mem, sed la legantoj devas atendi la novajn volumojn por ekscii la
veron.

La Esperanto en la rakonto ne estas bona. Plejparte, Viro Reganto uzas
kelkajn Esperantajn vortojn inter anglaj frazoj. Kiam li uzas kompletajn
frazojn, evidentas ke ĉiu vorto estis tradukita unuope. Ekzemple, li
diris “Fari vi paroli Esperanto?” anstataŭ “Ĉu vi parolas Esperanton?”
En la angla, “Ĉu vi parolas Esperanton?” estas “Do you speak Esperanto?”
Sed, “Do” en Esperanto estas “Fari”, “you” estas “vi”, “speak” estas
“paroli”, kaj “Esperanto” estas “Esperanto”. Do “Do you speak Esperanto”
iĝas “Fari vi paroli Esperanto?”

Malgraŭ ĉio ĉi, mi ĝojas ke infanoj povas lerni iom pri Esperanto de
SpongeBob, kaj la listo de Esperantaj vortoj kaj frazoj ĉe la fino de la
libro estas tre amuza. Mi opinias ke ĉiu devus scii kiel diri “Vi
mortos! Mi mortigos vin!” en Esperanto.

(kopio de unu paĝo tranĉebla laŭvole)

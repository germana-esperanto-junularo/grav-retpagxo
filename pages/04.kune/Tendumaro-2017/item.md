---
title: Tendumado de la verdaj skoltoj
menu:  Tendumado de la verdaj skoltoj
date:  01-04-2017
slug:  esperanto-tendumado-2017
taxonomy:
    tag: [tendumado, skoltoj, renkontiĝoj]
    category: Renkontiĝoj
author:
    name: [Michaela Stegmaier, Janette Stegmaier]
---

![Skoltoj](image://verdajskoltoj1-2017.jpg)

skoltoj, franca lingvo, mangao, mors-kodo, tendarfajro, tendoturo,
renato la unua, floso, ruldomo, ruĝa lumo, La dua duono de Yasna,
Promeso!, amikoj, puDINGO de Korsako,

Diese Wörter kommen mir in den Kopf, wenn ich an das Zeltlager der
Verdaj Skoltoj in Rosée, Belgien Ende August denke. Wir waren 25
Jugendliche, die eine Woche lang zusammen gebaut, gemalt, gerätselt und
Spaß gehabt haben. Auf dem Gelände des Vaters unseres Pfadfinderleiters
Lupo mit einem großen Teich, einem Wohnwagen und einem Holzschuppen
lernten wir eine Menge über das Pfadfinden, wie man Säge und Axt nutzt,
kochten mit selbst geernteten Kräutern, spülten viel Geschirr und
überstanden singend einige Regenschauer gemeinsam. Aufgrund des Wetters
entstand eine neue Hymne für die Verdaj Skoltoj:

La aŭto de la ĉef‘ estas kaptita en la kot‘ (3x) Ni retiru ĝin pere de
la traktor.

![Skoltoj](image://verdajskoltoj2-2017.jpg)

Skoltoj, franca lingvo, mangao, puDINGO de Korsako, renato la unua,
mors-kodo, tendarfajro, tendoturo, ruĝa lumo, floso, ruldomo, la dua
duono de Yasna, Promeso!, amikoj, ktp…

Diese Wörter kommen uns in den Kopf, wenn wir an das Zeltlager der
Verdaj Skoltoj in Rosée, Belgien Ende August sprechen. Wir waren 25
Jugendliche, die eine Woche lang zusammen gebaut, gemalt, gerätselt und
Spaß gehabt haben. Auf dem Gelände des Zeltlagers mit einem großen
Teich, einem Wohnwagen und einem Holzschuppen lernten wir eine Menge
über das Pfadfinden, wie man Säge und Axt nutzt, kochten mit selbst
geernteten Kräutern, spülten viel Geschirr und überstanden singend
einige Regenschauer gemeinsam. Aufgrund des Wetters entstand eine neue
Hymne für die Verdaj Skoltoj:

La aŭto de la ĉef‘ estas kaptita en la kot‘ (3x) Ni retiru ĝin pere de
la traktor.

![Skoltoj](image://verdajskoltoj3-2017.jpg)

Wir freuen uns schon auf die nächste Gelegenheit unser Halstuch
umzulegen und uns in ein Abenteuer zu stürzen.

Korajn Salutojn al ĉiuj bestoj kaj tiuj, kiuj volas iĝi.

https://verdajskoltoj.wordpress.com

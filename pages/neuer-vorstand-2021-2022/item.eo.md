---
title: 'Nova estraro 2021–2022'
media_order: 'Nova Estraro.png'
date: '15:14 09-01-2021'
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
---

Dum nia membrokunsido en 2020-12-30 ni elektis novan estraron! Prezidanto por la mandatperiodo 2021/'22 estas Michael Vrazitulis. La postenon de kasisto transprenas Jan Raring. Vicprezidanto estas Jonas Scheerschmidt. Michaela Stegmaier, antaŭa prezidanto, restas en la nova estraro kiel kroma ano.
Je bona kaj fruktodona kunlaborado dum la venontaj du jaroj! 💚💪

![](Nova%20Estraro.png)
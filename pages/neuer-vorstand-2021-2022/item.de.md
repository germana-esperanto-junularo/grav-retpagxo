---
title: 'Neuer Vorstand 2021–2022'
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
bricklayer_layout: true
display_post_summary:
    enabled: false
feed:
    limit: 10
---

Auf unserer Mitgliederversammlung am 30.12.20 haben wir einen neuen Vorstand gewählt! Vorsitzender für die Amtsperiode 2021/'22 ist Michael Vrazitulis. Das Amt des Kassenwarts übernimmt Jan Raring. Stellvertretender Vorsitzender ist Jonas Scheerschmidt. Michaela Stegmaier, die bisherige Präsidentin, bleibt als Beisitzerin im neuen Vorstand.
Auf eine gute und erfolgreiche Zusammenarbeit in den nächsten zwei Jahren! 💚💪

![](Nova%20Estraro.png)
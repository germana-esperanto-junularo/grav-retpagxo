---
title: Dokumentoj
media_order: '2019_01_02_dejsatzung.pdf,Fahrtkostenformular.pdf,Freistellungsbescheid_2014-16.pdf,Kontrakto_IEJ-GEJ-PEJ.pdf,Finanzordnung.pdf,Ortsgruppenleitfaden.pdf'
hide_git_sync_repo_link: false
menu: Dokumentoj
recaptchacontact:
    enabled: false
---

## Asociaj dokumentoj

Jen kelkaj gravaj dokumentoj de GEJ …

… en la germana:

  1. [Statut / Satzung](satzung/default.de.md) oder als [PDF](./2019_01_02_dejsatzung.pdf)
  2. [Geschäftsordnung der Hauptversammlung](go_vorstand/default.de.md)
  3. [Geschäftsordnung des Bundesvorstands](go_jhv/default.de.md)
  4. [Beitragsordnung](./Finanzordnung.pdf)
  5. [Formular zur Fahrtkostenbeantragung](./Fahrtkostenformular.pdf)
  6. [Regelung über die Teilnahme an Veranstaltungen]()
  7. [Regelungen über Fahrtkostenzuschüsse]()
  8. [Regelung für Veranstaltungen]() durch Unterorganisationen
  9. [Freistellungsbescheid](./Freistellungsbescheid_2017-19.pdf) (2017-2019)
  10. [Ortsgruppenleitfaden](./Ortsgruppenleitfaden.pdf)

… en Esperanto:

  1. [Regularo pri repagoj, rabatoj kaj honorarioj]()
  2. [Regularo pri monsubteno al la suborganizoj]()
  3. [Specimena tagordo de la jarĉefkunveno]()
  4. [Kontrakto pri unua Junulara E-semajno]() inter Germana kaj Pola Esperanto-Junularoj
  5. [Kontrakto pri interŝanĝo JES-IJF](./Kontrakto_IEJ-GEJ-PEJ.pdf) inter Germana, Itala kaj Pola Esperanto-Junularoj
 
---
title: Dokumente
media_order: '2019_01_02_dejsatzung.pdf,Kontrakto_IEJ-GEJ-PEJ.pdf,Finanzordnung.pdf,Ortsgruppenleitfaden.pdf,Fahrtkostenformular.pdf,1_EO_in_DE.pdf,2_sich_einbringen.pdf,3_Vorstandsämter.pdf,4_kleine_Treffen.pdf,5Orts-und_Unigruppen.pdf,6_kune.pdf,7_internationale_Veranstaltungen.pdf,8_TEJO_und_Projekte.pdf,9_Werbung.pdf,11_Geschichte_der_DEJ.pdf,organizi_retseminarion.pdf,Freistellungsbescheid_2017-19.pdf,10_Protokoll_neue_DEJ-Website.pdf'
hide_git_sync_repo_link: false
menu: Dokumente
recaptchacontact:
    enabled: false
---

## Vereinsdokumente

Hier einige wichtige Dokumente der DEJ …

… auf Deutsch:

  1. [Statut / Satzung](satzung/default.de.md) oder als [PDF](./2019_01_02_dejsatzung.pdf)
  2. [Geschäftsordnung der Hauptversammlung](go_vorstand/default.de.md)
  3. [Geschäftsordnung des Bundesvorstands](go_jhv/default.de.md)
  4. [Beitragsordnung](./Finanzordnung.pdf)
  5. [Formular zur Fahrtkostenbeantragung](./Fahrtkostenformular.pdf)
  6. [Regelung über die Teilnahme an Veranstaltungen]()
  7. [Regelungen über Fahrtkostenzuschüsse]()
  8. [Regelung für Veranstaltungen]() durch Unterorganisationen
  9. [Freistellungsbescheid](./Freistellungsbescheid_2017-19.pdf) (2017-2019)

… auf Esperanto:

  1. [Regularo pri repagoj, rabatoj kaj honorarioj]()
  2. [Regularo pri monsubteno al la suborganizoj]()
  3. [Specimena tagordo de la jarĉefkunveno]()
  4. [Kontrakto pri unua Junulara E-semajno]() inter Germana kaj Pola Esperanto-Junularoj
  5. [Kontrakto pri interŝanĝo JES-IJF](./Kontrakto_IEJ-GEJ-PEJ.pdf) inter Germana, Itala kaj Pola Esperanto-Junularoj
  
  ##Nützliches
  
 1. [Ortsgruppenleitfaden](./Ortsgruppenleitfaden.pdf)
 2. [Esperanto in Deutschland](./1_EO_in_DE.pdf)
 3. [Mitarbeiten im Verein](./2_sich_einbringen.pdf)
 4. [Vorstandsämter DEJ und DEB](./3_Vorstandsämter.pdf)
 5. [kleine Treffen organisieren](./4_kleine_Treffen.pdf)
 6. [Orts- und Unigruppen](./5Orts-und_Unigruppen.pdf)
 7. [die kune (Mitgliederzeitschrift)](./6_kune.pdf)
 8. [Organisation internationaler Veranstaltungen](./7_internationale_Veranstaltungen.pdf)
 9. [Projekte und Zusammenarbeit mit TEJO](./8_TEJO_und_Projekte.pdf)
 10. [Werbung in Sozialen Netzwerken](./9_Werbung.pdf)
 11. [Unsere neue Website](./10_Protokoll_neue_DEJ-Website.pdf)
 12. [Geschichte der DEJ](./11_Geschichte_der_DEJ.pdf)
 13. [Organizi retan seminarion (EO)](./organizi_retseminarion.pdf)


  
  


---
title: Satzung
taxonomy:
    tag:
    category:
---

## Satzung der Deutschen Esperantojugend

###  § 1 – Name und Sitz

Der Verein trägt den Namen »Deutsche Esperanto-Jugend« (DEJ), in Esperanto
»Germana Esperanto-Junularo« (GEJ) und ist der Jugendverband des Deutschen
Esperanto-Bundes e. V. mit eigener Verwaltung. Die DEJ ist im Vereinsregister
mit dem Sitz in Berlin eingetragen.

###  § 2 – Ziele und Mittel

  1. Der Schwerpunkt der Arbeit liegt auf dem Gebiet der internationalen Jugendarbeit. Die DEJ pflegt die Persönlichkeitsbildung junger Menschen auf kulturellem, sozialem und politischem Gebiet unter besonderer Berücksichtigung internationaler Probleme. Die DEJ erstrebt die Erziehung der Jugend zu verantwortungsbewußten Staatsbürgern und zur Achtung vor den Werten und Leistungen der anderen Völker, die Herstellung einer engen Verbindung der deutschen Jugend mit der des Auslands auf allen kulturellen Gebieten und eine internationale Zusammenarbeit in allen die Jugend berührenden Fragen.
  2. Die DEJ bekennt sich zum Grundgesetz der Bundesrepublik Deutschland und orientiert sich in ihrer Arbeit am Leitbild des Kinder- und Jugendhilfegesetzes.
  3. Den Kern der Jugendarbeit bilden die Veranstaltung von internationalen Jugendtreffen, Seminaren und Kursen, das Fördern von internationalen Briefpartnerschaften, der Jugendaustausch auf privater Ebene, um einen möglichst engen Kontakt mit anderen Völkern, ihren Anschauungen und ihren Sitten zu erreichen, gemeinsame Projekte mit in- und ausländischen Partnerverbänden und die Pflege des Gedankenaustausches mit Jugendlichen anderer Länder.
  4. Die DEJ verwendet für ihre internationale Arbeit die internationale Sprache Esperanto und fördert deren Verbreitung unter der Jugend. Die DEJ ist überzeugt, daß die Verwendung der neutralen Sprache Esperanto einen wertvollen und wichtigen Beitrag zur Schaffung einer vertrauensvollen und friedlichen Atmosphäre unter den Völkern leistet.

###  § 3 – Gemeinnützigkeit

Die DEJ ist selbstlos tätig und verfolgt ausschließlich und unmittelbar
gemeinnützige Zwecke im Sinne der Abgabenordnung in der jeweils gültigen
Fassung. Die Verfolgung wirtschaftlicher, parteipolitischer, religiöser oder
weltanschaulicher Ziele ist ausgeschlossen. Mittel des Vereins dürfen nur für
die satzungsgemäßen Zwecke verwendet werden. Die Mitglieder erhalten keine
Gewinnanteile und in ihrer Eigenschaft als Mitglieder auch keine sonstigen
Zuwendungen aus Mitteln des Vereins. Es darf keine Person durch Ausgaben, die
den Zwecken der DEJ fremd sind, oder durch unverhältnismäßig hohe Vergütungen
begünstigt werden.

###  § 4 – Mitgliedschaft

  1. Ordentliche bzw. fördernde Mitglieder der DEJ können Jugendliche werden, die zum 1. 1. des Kalenderjahres höchstens 26 Jahre alt sind. Mit dem Beitritt zur DEJ ist ein Beitritt zum Deutschen Esperanto-Bund e. V. verbunden; ordentliche bzw. fördernde Mitglieder des Deutschen Esperanto-Bundes e. V. bis zu dieser Altersgrenze gelten als Mitglieder der DEJ. Mitglieder von Organen der DEJ sowie durch Organe oder Unterorganisationen eingesetzte Amtsinhaber können älter sein, müssen aber ordentliche Mitglieder sein. Mitglieder des Bundesvorstandes dürfen zum Zeitpunkt ihrer Wahl nicht älter als 30 Jahre sein.
  2. Die DEJ kann Ehrenmitglieder unabhängig von ihrem Alter aufnehmen.
  3. Über die Aufnahme von ordentlichen und fördernden Mitgliedern im DEJ-Alter entscheidet der DEJ-Bundesvorstand.
  4. Jugendverbände, deren Ziele mit denen der DEJ in Einklang stehen, können Unterorganisation der DEJ werden. Alle Mitglieder der Unterorganisationen werden durch die Aufnahme Mitglieder der DEJ. Das gilt insbesondere für Landes-, Kreis- und Ortsverbände.
  5. Ordentliche Mitglieder zahlen einen Beitrag, dessen Höhe die Hauptversammlung festsetzt. Fördernde Mitglieder unterstützen die DEJ durch Geld- oder Sachspenden oder auf andere Weise, gegebenenfalls durch ihre bloße Mitgliedschaft.
  6. Die Mitgliedschaft in der DEJ endet mit Ablauf des Geschäftsjahres,
    * durch schriftlich erklärten Austritt,
    * durch Überschreiten des Höchstalters,
    * durch Tod,
    * durch Ausschluß (mit sofortiger Wirkung).
  7. Ein Mitglied kann wegen groben oder wiederholten Verstoßes gegen die Ziele der DEJ ausgeschlossen werden.

###  § 5 – Organe

Die Organe der DEJ sind

  * die Hauptversammlung,
  * der Bundesvorstand.

###  § 6 – Hauptversammlung

  1. Die Hauptversammlung ist das höchste beschlußfassende Organ.
  2. Die Hauptversammlung besteht aus ordentlichen Mitgliedern. Details zum Stimmrecht regelt die Geschäftsordnung der Hauptversammlung.
  3. Die Hauptversammlung tritt mindestens einmal im Geschäftsjahr zusammen. Sie wird von der Hauptversammlung oder vom Bundesvorstand einberufen. Sie ist vom Bundesvorstand einzuberufen, wenn dies von mindestens einem Zehntel der ordentlichen Mitglieder verlangt wird. Sie ist beschlußfähig, wenn sie den ordentlichen Mitgliedern unter Nennung von Ort, Zeit und Tagesordnung spätestens 4 Wochen vor dem Termin durch Veröffentlichung in der Mitgliederzeitschrift bekanntgegeben wurde.
  4. Die Hauptversammlung ist insbesondere zuständig für:
    * Wahl, Abberufung und Entlastung des Bundesvorstandes,
    * Wahl und Entlastung der Rechnungsprüfer,
    * Einsetzung und Abberufung von Beauftragten und Ausschüssen,
    * Beschlußfassung über den Jahresabschluß und den Haushaltsplan der DEJ,
    * Festsetzung von Mitgliedsbeiträgen,
    * Festlegung von Richtlinien des Arbeitsprogramms,
    * Ausschluß von Mitgliedern,
    * Satzungsänderungen,
    * Auflösung der DEJ.
  5. Über die Beschlüsse der Hauptversammlung wird ein Protokoll geführt, das vom Bundesvorsitzenden zu unterzeichnen ist.

###  § 7 – Der Bundesvorstand

  1. Der Bundesvorstand wird in jedem zweiten Jahr von der Hauptversammlung gewählt. Er besteht aus dem Bundesvorsitzenden und mindestens zwei weiteren Mitgliedern. Jedes Mitglied des Bundesvorstandes ist für seinen Tätigkeitsbereich der Hauptversammlung direkt verantwortlich.
  2. Bundesvorsitzender und Stellvertreter sind je einzelvertretungsberechtigt als Vorstand im Sinne von § 26 BGB. Sie regeln untereinander die Wahrnehmung der Vertretungsbefugnis.
  3. Der Bundesvorstand ist insbesondere zuständig für:
    * Durchführung der Beschlüsse der Hauptversammlung,
    * Ernennung und Entlassung der von ihm eingesetzten Beauftragten und Ausschüsse,
    * Führung der laufenden Geschäfte.

###  § 8 – Ausschüsse

  1. Die Ausschüsse sind der Hauptversammlung verantwortlich.
  2. Jeder Ausschuß arbeitet entweder unter einem aus seiner Mitte gewählten Leiter oder unter dem fachlich zuständigen Bundesvorstandsmitglied oder einem Beauftragten des Bundesvorstands. Die Entscheidung trifft der Ausschuß.

###  § 9 – Abstimmungen und Wahlen

  1. Grundsätzlich fasst die Hauptversammlung ihre Beschlüsse mit einfacher Mehrheit der abgegebenen Stimmen, der Bundesvorstand mit einfacher Mehrheit seiner Mitglieder.
  2. Beschlüsse der Hauptversammlung über Satzungsänderungen, Auflösung der DEJ sowie über Ausschlüsse von Mitgliedern sind nur zulässig, wenn die Antragstexte den Stimmberechtigten mindestens 4 Wochen vor Beginn der Hauptversammlung bekanntgegeben wurden. Für Satzungsänderungen ist eine Zweidrittelmehrheit, für die Auflösung eine Vierfünftel-Mehrheit der abgegebenen Stimmen erforderlich.
  3. Schriftliche Abstimmungen außerhalb der Tagungen sind gültig, wenn allen Stimmberechtigten der Abstimmungsgegenstand bekanntgemacht und eine Frist von mindestens 4 Wochen gesetzt worden ist. Der Bundesvorstand kann für seine schriftlichen Abstimmungen eine von dieser Bestimmung abweichende Frist setzen.
  4. Die Abstimmungen sind grundsätzlich offen. Geheime Abstimmung erfolgt auf Antrag eines Fünftels der Stimmberechtigten.

###  § 10 – Öffentlichkeit

Die Sitzungen der Organe sind öffentlich. Die Öffentlichkeit kann mit
einfacher Mehrheit der abgegebenen Stimmen ausgeschlossen werden.

###  § 11 – Geschäftsjahr und -sprachen

  1. Das Geschäftsjahr ist das Kalenderjahr.
  2. Geschäftssprachen sind Deutsch und Esperanto.

###  § 12 – Zugehörigkeit zu anderen Verbänden

Die DEJ ist dem Weltbund der Esperanto-Jugend (Tutmonda Esperantista Junulara
Organizo) als dessen Landesverband in der Bundesrepublik Deutschland
angeschlossen.

###  § 13 – Geschäftsordnungen

Die Organe und Ausschüsse können sich Geschäftsordnungen geben.

###  §14 – Vermögensregelung im Falle der Auflösung

Die Auflösung oder Aufhebung der Deutschen Esperanto-Jugend e.V. oder bei
Wegfall steuerbegünstigter Zwecke fällt das Vermögen der Deutschen Esperanto-
Jugend e.V. an den gemeinnützigen Deutschen Esperanto-Bund e.V., der es
unmittelbar und ausschließlich für gemeinnützige Zwecke zu verwenden hat..

* * *

Bonn und Berlin, den 31.12.1991. Letzte Änderungen durch Beschlüsse der
Hauptversammlung am 31.12.2006, 31.12.2011 und 31.12.2012

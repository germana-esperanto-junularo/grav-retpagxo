---
title: Membreco
menu: Membreco
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
---

Indas membri en la GEJ! Kiu ŝatas niajn agadojn devus aliĝi, ĉar vi ricevos
multe da bonaĵoj:

###  Gazeto [/·kune·/](http://esperanto.de/dej/blog)

La dulingva gazeto [/·kune·/](../../kune) estas parto de la
"Esperanto Aktuell", la gazeto de la GEA. Per tio vi ricevos regule novaĵojn
pri renkontiĝoj, kursoj kaj aliaj Esperanto aktivaĵoj. La gazeto estas sendata
al via hejmo dumonate.

###  Malmultkoste vojaĝi

La GEJ estas subtenata de la "Bundesministerium für Frauen, Senioren, Familie
und Jugend". Vi povas ekzemple rericevi monon por viaj [vojaĝkostoj](../fahrtkosten) al grandaj
internaciaj renkontiĝoj. Tiamaniere vi povas ŝpari iom da mono.

###  Rabato por la JES

Por GEJ-membroj normale malplikostas partopreni la "Junulara Esperanto
Semajno", la novjarrenkontiĝo organizata de la pola kaj german
esperantojunularo.

##  Kotizoj kaj kategorioj

###  Kategorio A (junuloj inter 16-26):

Tio estas la normala kategorio por ĉiuj pli aĝa ol dekses. Lernejanoj,
studentoj, "Azubi"-oj, homoj kiu faras libervolan servon en organizo aŭ estas
senlabora, pagas 30€. Junuloj, kiuj rivecas propran salajron, pagas 50€.

###  Kategorio B (junloj sub 16 jaroj):

Junuloj sub 16 pagendas nur 20€.


###  Kategorio F (Familimembreco):

Kiam familio estas GEA-registrata, ĉiuj envivantoj de la loĝejo sub 27 membras
en la GEJ aŭtomate. Ĉiuj familioj ricevas unu gazeton por ĉiuj.
[Familimembreckotizojn](https://www.esperanto.de/de/enhavo/gea/mitgliedskategorien) vi trovas ĉe GEA.

###  Kategorio P (Patronoj):

Homoj, kiuj volas subteni la GEJ speciale povas patronigi sin, pro tio oni
pagas jare minimume 50€.

###  Kategorio S (Ekkonmembreco):

Ĉiuj sub 27 povas ricevi ekkonmembrecon, post la partopreno je laŭ la GEJ
valida [Esperantlingvokurso](http://esperanto.de/ereignisse) aŭ KEKSO. La
membreco tute samas normalan A aŭ B membrecon, sed senkostas por la plej unua
jaro.

Post la senkosta jaro vi povas igi pagantan membron aŭ nemembriĝi. GEJanoj
estas aŭtomate membroj de la GEA.

**Subten- aŭ donacpagoj estas diferencebla je impostoj. Se estas volanta, vi
ricevos donackvitanco de tio.**

**Por membrigi sube vi trovos aliĝilon, kiu vi povas plenumi rete. Sub tio
estas plidetalaĵoj pri la pagmanieroj por la membreckotizoj.**

  * [Aliĝilo](../dej-beitritt)
  * [Pagmanieroj](http://esperanto.de/dej/dej_beitrag_zahlen)


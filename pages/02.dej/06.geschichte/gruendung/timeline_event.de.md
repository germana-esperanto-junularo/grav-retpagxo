
---
title: Gruendung
order:
    by: date
    dir: asc
date: '18-07-1947 12:15'
taxonomy:
    tag:
    category:
---

**Geschichtlicher Abriss**

In den dreißiger Jahren wurden alle [Esperanto-Organisationen
verboten](http://www.esperantoland.org/de/plu.php?msgid=814). Daher war nach
dem Krieg ein Neuanfang nötig.

* 1947
     * Wiedergründung des Deutschen Esperanto-Bunds in Frankfurt
     * Horst Kliemann (Hannover) beginnt die jungen Esperantisten der 4 Zonen in Deutschland zu vereinen.
     * Zusammenkunft der Gruppen-Vorsitzenden und -Stellvertreter in Hannover – behandelt wird die Organisation der Jugend-Sektion (JS) des DEB und die Vorbereitung des internationalen Jugend-Zeltlagers.
* 1948
     * Die Jugend-Sitzung während des 26sten Deutschen Esperanto-Kongresses in München erwählt Horst Kliemann zum Vorsitzenden der Jugend-Sektion und akzeptiert ein Statut (ein Vorschlag der Gruppe aus Hannover).
     * Internationales Jugend-Zeltlager in Garmisch-Partenkirchen mit 360 Teilnehmern, Haupt-Organisator: Hermann Heiß, München.
     * DEB-JS hat mehr als 100 Jugend-Gruppen in allen Zonen Deutschlands. Diverse Kinder-Gruppen (Berlin, Bremen, Hannover, Frankfurt, Stuttgart).
* 1949 - K. Kaifer wird Vorsitzender.
* 1950
     * A. Hopfenmüller (Aschaffenburg) wird Vorsitzender.
     * Flugblatt „Wir rufen Euch, Jungen und Mädel in Deutschland“.
     * Sechster IJK in Konstanz.
* 1951 - Eine JS-Vorsitzenden-Versammlung (im Rahmen des internationalen Treffens in Urfeld/Walchensee?) entscheidet die JS zu verselbständigen.
* 04.10.1951 - Geburt der selbständigen Deutschen Esperanto-Jugend

###  Zeitzeugenbericht

1945, nach einem Jahrzehnt des Verbots, erblühte Esperanto wieder und zog auch
junge Menschen an, die eine Jugendabteilung im DEB bildeten. Sie organisierten
u. a. ein großes Treffen in Kochel, direkt nach dem UK 1951 in München.

Uns jungen Aktiven war klar, dass ein selbstständiger Jugendverband
(eingebunden in den allgemeinen Verband) mehr Möglichkeiten hat, auf Esperanto
aufmerksam zu machen. Auf örtlicher Ebene konnten wir in offiziellen
Institutionen wie den Stadtjugendringen mitarbeiten und etwaige Vorteile wie
kostenlose Gruppenräume nutzen. Auch auf Landesebene konnten wir mitmischen.
Und die Selbstständigkeit als echter Jugendverband eröffnete uns auch den
Zugang zu finanzieller Unterstützung vom Staat.

Damals waren Studenten der Universität Freiburg der aktive Kern der DEJ. 1952
reisten der Vorsitzende und die Kassiererin der DEJ von Freiburg in die
Hauptstadt Bonn. Sie erhielten die gewünschte Hilfe. Aber als sie eine
Erstattung ihrer Fahrtkosten beantragten, protestierte ein Mitglied des DEB-
Vorstandes - vor allem weil ein Mann und eine Frau gemeinsam gereist waren
(die später heirateten). Eine erste Spannung zwischen DEB und DEJ.

Ein Verband, der öffentliche Mittel beantragten wollte, musste in allen
Bundesländern vertreten sein. Der DEJ-Vorstand versucht also, überall genügend
Mitglieder nachzuweisen. Das Zentrum der DEJ befand sich im damaligen
Bundesland Südbaden, ich hingegen in Flensburg, denkbar weit entfernt. So weit
zu fahren, war damals nicht üblich, also lief der Kontakt per Post. Trotzdem
gelang es uns, dass die DEJ bundesweit vertreten war. Eine wichtige
Veranstaltung der DEJ war schon damals das IS. Die Vorträge hielten erfahrene
Fachleute; die jungen Leute waren nur Zuhörer. Ein häufiger Redner war Prof.
Lapenna. (Ich hielt meinen ersten Vortrag mit 31 Jahren, 1962 in Rüsselsheim.)
Erst später durften die jungen Leute selber Vorträge halten.

Es gab viele Kontakte von DEJ-Mitgliedern mit den Nachbarverbänden, was bei
einem Esperanto-Verband selbstverständlich ist. Aber die DDR war für uns
vollkommen unerreichbar. Auch die Aufnahme des DDR-Esperanto-Verbandes in den
Welt-Esperanto-Bund UEA 1977 änderte daran wenig. Nur unter Wissenschaftlern
war ein wenig Kontakt möglich, unter jungen Leuten nicht. Die
Wiedervereinigung brachte auch die Verschmelzung der Esperanto-Verbände mit
sich.

_Dr. Werner Bormann, im DEB seit 1949, bis 1953 im DEJ-Vorstand, verfasst im
Juni 2004_

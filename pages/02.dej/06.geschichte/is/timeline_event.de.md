
---
title: Internacia Semajno
order:
    by: date
    dir: asc
date: '18-07-1960 12:15'
taxonomy:
    tag:
    category:
---

**Internacia Seminario** (IS), auch unter seinem deutschen Namen
**Internationale Woche** bekannt, war lange Zeit die wichtigste Veranstaltung
der Deutschen Esperanto-Jugend e.V.

Das internationale Jugendtreffen fand jährlich zum Jahreswechsel an
unterschiedlichen Orten in Deutschland statt. Von 1957 bis 2008 wurde es
insgesamt 52 mal veranstaltet.

Jede Internationale Woche wurde unter einem allgemeinen Thema veranstaltet,
das das Programm maßgeblich bestimmte. Neben den themenbezogenen Vorträgen und
Diskussionsrunden fanden aber auch regelmäßig Konzerte, Exkursionen ins Umland
und natürlich der große Silvesterball mit Tanzwettbewerb statt.

Seit dem Jahr 2009 wurde die Internationale Woche durch die Jugend-Esperanto-
Woche ersetzt, die seitdem jährlich von der Deutschen Esperanto-Jugend
mitorganisiert wird.

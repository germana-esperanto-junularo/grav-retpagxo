
---
title: Geschichte nach 1990
order:
    by: date
    dir: asc
date: '18-07-1990 12:15'
taxonomy:
    tag:
    category:
---

Nach der Wiedervereinigung und dem Fall des Eisernen Vorhangs begann ein neuer
Abschnitt in der Geschichte der DEJ. Die vormals geteilte Welt erschien nun
greifbarer. Die Erfolge auf dem Gebiet der internationalen Jugendarbeit
belohnte das [Bundesministerium für Familie, Senioren, Frauen und
Jugend](http://www.bmfsfj.de/) 1993 mit einer Anerkennung der DEJ als Träger
der freien Jugendhilfe nach § 75 KJHG. Mit Mitteln aus dem Kinder- und
Jugendplan des Bundes konnte die DEJ fortan ihr Angebot für Mitglieder
erweitern und verbessern.

Umzüge der Bundesgeschäftsstelle geschahen in den 90ern sehr häufig. Nach der
Vereinigung der beiden Esperanto-Jugenden gab es zunächst eine Zeit lang zwei
Geschäftsstellen in Bonn und Berlin. Seit 1994 ging es komplett nach Berlin,
wo man von Unter den Linden über die Chausseestraße und Grellstraße eine
Bleibe im Esperanto-Haus in der Einbecker Straße 36 fand.

Die rasche Verbreitung des Internets nutzte die DEJ 1996 mit dem Erwerb der
Domain esperanto.de, die seitdem zusammen mit dem [Deutschen Esperanto-
Bund](http://esperanto.de/deb/) betrieben wird. Seit 2003 sind die neuen
Ausgaben der Vereinszeitschrift **kune** auch vollständig im Internet lesbar.
Alle bisher erschienenen Exemplare liegen in unserer
[Bundesgeschäftsstelle](http://esperanto.de/dej/bero) im Archiv zum Schmökern
bereit. 2010 wurde die Vereinszeitschrift in Papierform eingestellt, da sie
seit mehreren Jahren nur noch unregelmäßig erschien und junge Leute lieber im
Internet Neuigkeiten erfuhren und sich vernetzten.

Mittlerweile erscheint die kune wieder regelmäßig in Papierform in sechs
Ausgaben pro Jahr und zwar als integraler Bestandteil der Zeitschrift des
Deutschen Esperanto-Bundes, der **Esperanto aktuell**.

Zum Jahreswechsel 2008/09 fand die letze [Internationale
Woche](http://esperanto.de/dej/geschichte/is) statt. Seit 2009 findet
stattdessen in Zusammenarbeit mit der Polnischen Esperanto-Jugend die Jugend-
Esperantowoche statt, die abwechselnd auf deutscher und polnischer Seite
organisiert wird.

Weiterlesen: Ausführliche Artikel zur Geschichte von Esperanto hat die
Zeitschrift “La Ondo de Esperanto” zusammengestellt.


---
title: BRD
order:
    by: date
    dir: asc
date: '18-07-1969 12:15'
taxonomy:
    tag:
    category:
---

Die erste [Internationale Woche](http://esperanto.de/dej/geschichte/is) (IS)
fand sechs Jahre nach der Vereinsgründung in Mainz mit dem Thema “Kulturelle,
politische und wirtschaftliche Fragen der europäischen Einigung” statt. Mit
verschiedenen Themen wurde sie seitdem jedes Jahr wiederholt, mehr
Informationen dazu liefert die vollständige Liste aller IS-Orte und Themen auf
Esperanto. In den späten 70ern begann die DEJ ein Programm zu organisieren,
das es jungen Deutschen aus der BRD ermöglichte, hinter den eisernen Vorhang
nach Polen zu fahren, wobei diese sogar vom obligatorischen Devisenumtausch
befreit wurden. Generell neigt man bei der Lektüre von Dokumenten aus dieser
Zeit zu der Ansicht, dass in der DEJ der siebziger und achtziger Jahre
deutlich mehr über politisches Engagement abseits von Sprachenfragen
diskutiert wurde als heute.

Forum dafür wurde vor allem die Vereinszeitschrift “GEJ-gazeto”, die 1979 von
Thomas Bormann und anderen gegründet wurde. Der Name war zunächst ein
Arbeitstitel, zusammengesetzt aus der Esperantosprachigen Abkürzung des
Vereins (“Germana Esperanto-Junularo”) und dem Wort “gazeto” - Zeitung – er
hielt sich aber sehr lange. Ein ausführliches Interview mit ihm erschien in
der Mitgliedszeitschrift “Kune” 4/2004.

“Kune” und “GEJ-gazeto” - hat der Verein jetzt zwei Zeitschriften? Nein. Die
GEJ-gazeto hat aufgrund der Ähnlichkeit von “gej” zum englischen Wort “gay”
(schwul) (und dem entsprechenden Esperanto-Wort "geja") oft Missverständnisse
innerhalb und außerhalb der Esperanto-Welt ausgelöst und wurde deswegen 1999
nach einer Idee von [Gunnar R. Fischer](http://www.muenster.de/%7Ekunar/) und
Till Schönberner umbenannt in “Kune”, was soviel heißt wie “gemeinsam”.

1985 erschien das Lehrbuch [Tesi, la testudo](http://tesitestudo.de/), dass
seitdem in unzähligen Kursen eingesetzt wurde und 2008 in einer Neuauflage
erschien.

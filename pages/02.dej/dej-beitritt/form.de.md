---
title: 'Anmeldung Deutsche Esperanto-Jugend'
hide_git_sync_repo_link: false
form:
    name: join-form
    fields:
        -
            name: name
            label: 'Vorname / Antaŭnomo:'
            placeholder: 'Dein Name / Via nomo'
            type: text
            validate:
                required: true
        -
            name: familia_name
            label: 'Name / Nomo:'
            placeholder: 'Dein Name / Via nomo'
            type: text
            validate:
                required: true
        -
            name: email
            label: Email
            placeholder: 'Deine Email-Adresse / Via retpoŝtadreso'
            type: email
            validate:
                required: true
        -
            name: naskigxtago
            type: date
            label: 'Dein Geburtsdatum / Via naskiĝdato'
            default: '2015-01-01'
            validate:
                min: '1985-01-01'
                max: '2015-13-13'
                required: true
        -
            name: adreso
            type: text
            placeholder: 'Deine Adresse / Via adreso'
            label: 'Straße und Hausnummer / Strato kaj domnumero:'
            validate:
                required: true
        -
            name: adresaldonajxo
            type: text
            label: 'Adresszusatz / Adresaldonaĵo:'
        -
            name: plz_urbo
            type: text
            placeholder: 'PLZ, Name deiner Stadt / Poŝtokodo, nomo de via urbo'
            label: 'PLZ und Stadt / Poŝtkodo kaj urbo:'
            validate:
                required: true
        -
            name: lingonivelo
            label: 'Deine Sprachniveau / Via lingvonivelo'
            placeholder: Auswählen/Elektu
            type: select
            options:
                - 'fließend / flue'
                - 'geht so / meze'
                - 'ein bisschen / iomete'
                - 'Anfänger / komencanto'
            validate:
                required: true
        -
            name: laboro
            label: 'Berufstätig? / Ĉu vi laboras?:'
            type: radio
            options:
                - 'Ja / Jes'
                - 'Nein / Ne'
            validate:
                required: true
        -
            name: datumoj
            type: radio
            label: 'Zustimmung zur Datenschutzerklärung (s. oben)? / Konsento kun la deklaro pri datumprotektado (v. supre)? '
            options:
                - 'Ja / Jes'
                - 'Nein / Ne'
            validate:
                required: true
    buttons:
        -
            type: submit
            value: Jetzt Beitreten
    process:
        -
            phpemail:
                from: gej.fe@esperanto-jugend.de
                to: '{{ form.value.email }}'
                subject: '[Mitgliedsanmeldung] {{ form.value.name|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            phpemail:
                to: michael.vrazitulis@esperanto.de
                subject: '[Mitgliedsanmeldung] {{ form.value.name|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            phpemail:
                to: jan.raring@esperanto.de
                subject: '[Mitgliedsanmeldung] {{ form.value.name|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            phpemail:
                to: gej.fe@esperanto-jugend.de
                subject: '[Mitgliedsanmeldung] {{ form.value.name|e }}'
                body: '{% include ''email/esperanto.html.twig'' %}'
        -
            save:
                fileprefix: '{{ form.value.name|e }}-{{ form.value.familia_name|e }}-feedback-'
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Danke für deine Anmeldung in der Deutschen Esperantojugend! Wir melden uns bald bei dir!'
recaptchacontact:
    enabled: false
---

## Beitrittsformular zur Deutschen Esperanto-Jugend

Hiermit beantrage ich die Aufnahme in die Deutsche Esperanto-Jugend.

Ĉi tiel mi petas membriĝon en la Germana Esperanto-Junularo.

[Datenschutzerklärung / Deklaro pri datumprotektado](https://www.esperanto.de/sites/default/files/esperanto.de/deb/dokumente/2018-05-25%20D.E.B.-Datenschutzerklärung.pdf)

---
title: 'Aktiv werden'
media_order: 'Michaela.jpg,Lars.jpg,Konstanze.jpg,Paul.jpg,David1.jpg,Jan.jpeg,Jonny1.jpg,Jani2.jpg,Annika1.jpg,Cornelia1.jpg,Jan Raring.jpg,Jonas.jpg,Ortsgruppenleitfaden.pdf,Oda.jpg,Anastasiia.png,Michael.jpg,Tobias.jpg'
hide_git_sync_repo_link: false
menu: 'Aktiv werden'
recaptchacontact:
    enabled: false
---

## Engagier dich für Esperanto

Jedes neue Mitglied bringt eine Vielfalt von Talenten und Interessen mit sich und genauso divers sind die Möglichkeiten, diese Fähigkeiten in der Vereinsarbeit einzubringen. Du schreibst gerne Texte oder hast an redaktionellen Tätigkeiten Interesse? Dann komm doch ins <em>kune</em>-Team! Soziale Medien und Grafikdesign sind dein Ding? Dann unterstütze uns bei der Öffentlichkeitsarbeit! Oder hast du Spaß daran, internationale Treffen mit 200 Teilnehmenden zu organisieren? Was auch immer es ist, für das du brennst, wir können dich gebrauchen! 

 Wenn du auch motiviert bist dich für die Verwendung und Verbreitung von Esperanto einzusetzen melde dich gerne bei uns mit deiner Idee auf [Telegram](https://t.me/joinchat/DcGzfk89ndwgNPsXvFUSQA) oder [kontaktiere](../../kontakt) uns!

## Engagierte Mitglieder

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Michael</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Michael.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
             <li>JES-Organisation</li> 
            <li>Social Media</li>
            <li>TEJO-Komitatano</li>
            <li>Ortsgruppe Saarbrücken</li>
            <li>Webseiten-Redakteur</li>
		    <li>KEK und Mentor:innen</li> 
            <li>DEB-Jugendvertreter</li>
      </div>
    </div>
  </div>
 
 <div class="column col-6 col-md-12 extra-spacing">
        <h4>Annika</h4>
        <div class="columns">
            <div class="column col-6 col-md-12 extra-spacing">
              <figure class="figure">
		<img class="img-responsive" src="aktivigxu/Annika1.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>JES-Helferin</li> 
            <li>Unterricht</li>
            <li>Ortsgruppe Würzburg</li>
            <li>Spieletreffen</li>
            <li>Kassenprüferin</li>
            </div>
          </div>
      </div>
  </div>    
    
<div class="columns">
 <div class="column col-6 col-md-12 extra-spacing">
    <h4>Jani</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Jani2.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>KEKSO-Organisation</li>
        </div>
      </div>
  </div>

 <div class="column col-6 col-md-12 extra-spacing">
      <h4>Jan</h4>
      <div class="columns">
          <div class="column col-6 col-md-12 extra-spacing">
            <figure class="figure">
		<img class="img-responsive" src="aktivigxu/Jan Raring.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>KasKo</li> 
	<li>Mitgliederverwaltung</li>
          </div>
        </div>
    </div>
  </div>

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Jonas</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Jonas.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>-kune- Helfer</li> 
            <li>KasKo</li>  
        </div>
      </div>
  </div>

 <div class="column col-6 col-md-12 extra-spacing">
    <h4>Oda</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Oda.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>KEK-Beauftragte</li>
            <li>KINo</li>
      </div>
    </div>
  </div>
</div>
    
<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Cornelia</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
           <figure class="figure">
          <img class="img-responsive" src="aktivigxu/Cornelia1.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>-kune- Helferin</li> 
      </div>
    </div>
  </div>

  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Paul</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Paul.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>Webseiten-Administrator</li>
            <li>Öffentlichkeitsarbeit</li>
      </div>
    </div>
  </div>
</div>

 <div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Tobias</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
           <figure class="figure">
          <img class="img-responsive" src="aktivigxu/Tobias.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>TEJO-Komitatano</li> 
      </div>
    </div>
  </div>

  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Ana</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Ana.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>Social Media</li>
      </div>
    </div>
  </div>
 </div>
 
<div class="column col-6 col-md-12 extra-spacing">
  <h4>und du... ?</h4>
  <div class="columns">
    <div class="column col-6 col-md-12 extra-spacing">
        <li>Weitere Infos zu möglichen Aufgabenbereichen und Kontaktpersonen findest du weiter unten</li>     
    </div>
  </div>
</div>


## Weitere Unterstützende (27+)

<div class="columns">
 <div class="column col-6 col-md-12 extra-spacing">
    <h4>Michaela</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
	<img class="img-responsive" src="aktivigxu/Michaela.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
             <li>Social Media</li> 
            <li>Redakteurin -kune-</li>
            <li>Ortsgruppe Braunschweig</li>
            <li>Öffentlichkeitsarbeit</li>
        </div>
      </div>
  </div>

    
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Lars</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
            <figure class="figure">
         <img class="img-responsive" src="aktivigxu/Lars.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>KasKo</li>
            <li>Subventionen</li>
            <li>JES-Budget</li>
      </div>
    </div>
  </div>
</div>

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>David</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/David1.jpg">
              </figure>
            </div>
            <div class="column col-6 col-md-12 extra-spacing">
                <li>JES-Helfer</li> 
                <li>Internationale Zusammenarbeit</li> 
        </div>
    </div>
  </div>


  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Anastasiia</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Anastasiia.png">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>eventaservo.org</li>
      </div>
    </div>
  </div>
</div>

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Konstanze (Tuŝka)</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
	<img class="img-responsive" src="aktivigxu/Konstanze.jpg">
            </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
              <li>Zusammenarbeit mit TEJO</li>
              <li>Erasmus+ Projekte</li>           
        </div>
    </div>
  </div>
 
 
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Jonny</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Jonny1.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>Kunst und Kultur</li>            
        </div>
    </div>
  </div>
</div>



<!--
<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Rolf</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Rolf.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>Kassenprüfer</li> 
      </div>
    </div>
  </div>
    
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Christine</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Christine.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>Kassenprüferin</li>
			<li>JES Helferin</li>
        </div>
    </div>
  </div>
</div>
-->
<br>


Laut Beschluss des Vorstands im Mai 2020 haben engagierte Mitglieder die Möglichkeit, sich Fahrtkosten zu Treffen auch innerhalb Deutschlands erstatten zu lassen, sowie einen Zuschuss für die Teilnahme am JES (oder an anderen internationalen Treffen) zu bekommen. Dafür ist Rücksprache mit dem Vorstand zu halten.

## Kommissionen

Du interessierst dich dafür, eine kleine Aufgabe im Verein zu übernehmen, um selbstverantwortliches Arbeiten in einem netten und lustigen Team zu lernen? Dann kontaktiere doch den Vorstand oder die jeweils angegebene Kontaktperson und mache Esperanto auch zu deinem Projekt!

### KasKo und SuPo

Finanzen, den Kassenwart beim Geschäftsbetrieb oder bei Budgets für Treffen oder Projekte unterstützen (KasKo) 
- Jan, Jonas, Lars, Christine, Rolf, Annika, Jarno (gej.kasko@esperanto.de)


Subventionen (SuPo)
- Andreas Diemel, Lars

### KIR und KonKERo
    
Internationales z.B. Beziehungen zu TEJO und PEJ (KIR)
- Vorstand (Michael, Jan, Jonas, Michaela)
- Komitatanoj bei TEJO (Tobias und Michael)


Außenbeziehungen zu anderen deutschen Jugendverbänden (KonKERo)
- vakant


### IReK und KAPRi

Internet, Website (IReK)
- Paul, Michael, Michaela (gej.retejo@esperanto.de)


Öffentlichkeitsarbeit in sozialen Medien, Zeitungen, auf Sprachenmessen usw. (KAPRi)
- Michael, Michaela, Paul, Ana

### MA

Mitgliederverwaltung (MA)
- Jan

### KoLA

Regionales, Ortsgruppen (KoLA)
Wir sind jeweils in unserer Stadt der Ansprechpartner der Ortsgruppe, unterstützen dich aber auch gerne, wenn du selbst eine Ortsgruppe gründen möchtest. Hier findest du auch unseren [Ortsgruppenleitfaden](./Ortsgruppenleitfaden.pdf).
- Michaela: Braunschweig 
- Annika: Würzburg
- Michael: Saarbrücken
- Wo wohnst du? Hast du Lust, eine Ortsgruppe zu gründen?

### KKRen

Treffen, JES und KEKSO (KKRen)
- Michael, David, Annika und Lars (JES)
- Jani, Paul, Michaela (KEKSO)

### Kune

Mitgliederzeitschrift, Artikel zu interessanten Themen veröffentlichen, Mitglieder vorstellen, Interviews führen uvm. (Kune)
- Michael, Cornelia, Jonas (kune@esperanto.de)

### KINo

Unterricht und Neulingsbetreuung, z.B. unser Kostenloser Esperanto-Kurs, alternative Kurse (KINo)
- Paul, Annika, Oda


KEK-Mentoren zuweisen
- Michael (kek@esperanto.de)

### BerO

Geschäftsstelle (BerO)
Katzbachstraße 25, 10965 Berlin
- Sibylle Bauer (bero@esperanto.de)

### Jugendvertreter

Zusammenarbeit mit DEB (Jugendvertreter): 
- Michael (michael.vrazitulis@esperanto.de)
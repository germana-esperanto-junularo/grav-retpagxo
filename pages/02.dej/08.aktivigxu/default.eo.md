---
title: Aktiviĝu
media_order: 'David1.jpg,Jan.jpeg,Jani2.jpg,Jonny1.jpg,Michaela.jpg,Paul.jpg,Annika1.jpg,Konstanze.jpg,Lars.jpg,Cornelia1.jpg,Jan Raring.jpg,Jonas.jpg,Ortsgruppenleitfaden.pdf,Oda.jpg,Anastasiia.png,Michael.jpg,Tobias.jpg,Ana.jpg'
hide_git_sync_repo_link: false
menu: Aktiviĝu
recaptchacontact:
    enabled: false
---

## Engaĝiĝu por Esperanto

En nia asocio multaj homoj aktivas diversmanjere, por povi pliproksimigi Esperanton al aliaj. Se ankaŭ vi estas motivigita engaĝigi por la uzado kaj disvastigo de Esperanto, [kontaktu](../../kontakt) nin kun via ideo!

## Engaĝitaj membroj

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Michael</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Michael.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>JES-kunordiganto</li> 
            <li>sociaj retoj</li>
	    <li>redaktoro de -kune-</li>
            <li>loka grupo Saarbrücken</li>
            <li>retpaĝo-helpanto</li>
	    <li>KEK kaj mentoroj</li> 
            <li>GEJ-reprezentanto ĉe GEA</li>
      </div>
    </div>
  </div>
 
 <div class="column col-6 col-md-12 extra-spacing">
        <h4>Annika</h4>
        <div class="columns">
            <div class="column col-6 col-md-12 extra-spacing">
              <figure class="figure">
		<img class="img-responsive" src="aktivigxu/Annika1.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>JES-konsilanto</li> 
            <li>kursoj</li>
            <li>loka grupo Würzburg</li>
            <li>ludkunvenoj</li>
            <li>kaspruvisto</li>
            </div>
          </div>
      </div>
  </div>    
    
<div class="columns">
 <div class="column col-6 col-md-12 extra-spacing">
    <h4>Jani</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Jani2.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>KEKSO-organizado</li> 
        </div>
      </div>
  </div>

 <div class="column col-6 col-md-12 extra-spacing">
      <h4>Jan</h4>
      <div class="columns">
          <div class="column col-6 col-md-12 extra-spacing">
            <figure class="figure">
		<img class="img-responsive" src="aktivigxu/Jan Raring.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>KasKo</li> 
	    <li>membroadministrado</li>
          </div>
        </div>
    </div>
  </div>

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Jonas</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Jonas.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>-kune- helpanto</li> 
            <li>KasKo</li>  
        </div>
      </div>
  </div>

  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Oda</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Oda.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>KEK-respondeculo</li>
            <li>KINo</li>
      </div>
    </div>
  </div>
 </div> 

 <div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Cornelia</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
           <figure class="figure">
          <img class="img-responsive" src="aktivigxu/Cornelia1.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>-kune- helpanto</li> 
      </div>
    </div>
  </div>

  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Paul</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Paul.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>retpaĝo-administrado</li>
            <li>publikaj rilatoj</li>
      </div>
    </div>
  </div>
 </div>

 <div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Tobias</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
           <figure class="figure">
          <img class="img-responsive" src="aktivigxu/Tobias.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>TEJO-komitatano</li> 
      </div>
    </div>
  </div>

  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Ana</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Ana.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>sociaj retoj</li>
      </div>
    </div>
  </div>
 </div>
    
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>kaj vi... ?</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
            <li>Pliajn informojn pri eblaj taskoj en komisionoj kaj kontaktpersonojn vi trovos sube</li>     
        </div>
    </div>
  </div>


### Pliaj subtenantoj (27+)

<div class="columns">
 <div class="column col-6 col-md-12 extra-spacing">
    <h4>Michaela</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
	<img class="img-responsive" src="aktivigxu/Michaela.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
             <li>sociaj retoj</li>
            <li>-kune- helpanto</li>
            <li>loka grupo Braunschweig</li> 
            <li>publikaj rilatoj</li>
        </div>
      </div>
  </div>

    
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Lars</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
            <figure class="figure">
         <img class="img-responsive" src="aktivigxu/Lars.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>KasKo</li>
            <li>subvencioj</li>
            <li>JES buĝeto</li>
      </div>
    </div>
  </div>
</div>

<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>David</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/David1.jpg">
              </figure>
            </div>
            <div class="column col-6 col-md-12 extra-spacing">
                <li>JES-helpanto</li>  
        </div>
    </div>
  </div>


  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Anastasiia</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Anastasiia.png">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>eventaservo.org</li>
      </div>
    </div>
  </div>
</div>

<div class="columns">
<div class="column col-6 col-md-12 extra-spacing">
    <h4>Konstanze (Tuŝka)</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
	<img class="img-responsive" src="aktivigxu/Konstanze.jpg">
            </figure>
          </div>
          <div class="column col-6 col-md-12 extra-spacing">
              <li>kunlaboro kun TEJO</li>
              <li>Erasmus+ projektoj</li>           
    </div>
  </div>
</div>
 
 
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Jonny</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
           <img class="img-responsive" src="aktivigxu/Jonny1.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>arto kaj kulturo</li>            
        </div>
    </div>
  </div>
</div>
    
 

<!--
<div class="columns">
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Rolf</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Rolf.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>Kassenprüfer</li> 
      </div>
    </div>
  </div>
    
  <div class="column col-6 col-md-12 extra-spacing">
    <h4>Christine</h4>
    <div class="columns">
        <div class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="aktivigxu/Christine.jpg">
          </figure>
        </div>
        <div class="column col-6 col-md-12 extra-spacing">
            <li>Kassenprüferin</li>
			<li>JES Helferin</li>
        </div>
    </div>
  </div>
</div>
-->
<br> 



En majo 2020 la estraro decidis pli subteni la engaĝitajn membrojn. Ili nun havas la eblecon ricevi repagojn de vojaĝkotizoj por enlandaj renkontiĝoj kaj rabatojn por la partopreno en JES (aŭ aliaj internaciaj renkontiĝoj). Bezonatas konsento kun la estraro.

##Komisionoj

###KasKo kaj SuPo

Finanzen, den Kassenwart beim Geschäftsbetrieb oder bei Budgets für Treffen oder Projekte unterstützen (Kasa Komisiono) 
- Jan, Jonas, Lars, Christine, Rolf, Annika, Jarno (gej.kasko@esperanto.de)


Subvencipetoj (SuPo)
- Andreas Diemel, Lars

###KIR kaj KonKERo
Internationales z.B. Beziehungen zu TEJO und PEJ (KIR)
- Vorstand (Michael, Jan, Jonas, Michaela)
- Komitatanoj bei TEJO (Tobias und Michael)


Außenbeziehungen zu anderen deutschen Jugendverbänden (KonKERo)
- vakant


###IReK kaj KAPRi

Internet, Website (IReK)
- Paul, Michael, Michaela (gej.retejo@esperanto.de)


Öffentlichkeitsarbeit in Sozialen Medien, Zeitungen, auf Sprachenmessen usw. (KAPRi)
- Michael, Michaela, Paul, Ana

###MA

Membroadministrado
- Jan

###KoLA

Regionales, Ortsgruppen (KoLA)
Wir sind jeweils in unserer Stadt der Ansprechpartner der Ortsgruppe, unterstützen dich aber auch gerne, wenn du selbst eine Ortsgruppe gründen möchtest. Hier findest du auch unseren [Ortsgruppenleitfaden](./Ortsgruppenleitfaden.pdf).
- Michaela: Braunschweig 
- Annika: Würzburg
- Michael: Saarbrücken
- Wo wohnst du? Hast du Lust, eine Ortsgruppe zu gründen?

###KKRen

Treffen, JES und KEKSO (KKRen)
- Michael, David, Annika und Lars (JES)
- Jani, Paul, Michaela (KEKSO)

###Kune

Mitgliederzeitschrift, Artikel zu interessanten Themen veröffentlichen, Mitglieder vorstellen, Interviews führen uvm. (Kune)
- Michael, Cornelia, Jonas (kune@esperanto.de)

###KINo

Unterricht und Neulingsbetreuung, z.B. unser Kostenloser Esperanto Kurs, alternative Kurse (KINo)
- Paul, Annika, Oda


KEK-Mentoren zuweisen
- Michael (kek@esperanto.de)

###BerO

Geschäftsstelle (BerO)
Katzbachstraße 25, 10965 Berlin
- Sibylle Bauer (bero@esperanto.de)

###Jugendvertreter

Zusammenarbeit mit DEB (Jugendvertreter): 
- Michael (michael.vrazitulis@esperanto.de)
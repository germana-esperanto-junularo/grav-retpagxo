var landaj_organizoj = {
    Argentino: "<b>Argentino</b><b><a href=\"https://www.facebook.com/jeaeo\" target=\"_blank\" rel=\"nofollow\">Juna Esperantistaro Argentina (JEA)</a></b>",
    Auxstrio: "<b>A\u016Dstrio</b><b><a href=\"http://aej.esperanto.at/\" target=\"_blank\" rel=\"nofollow\">A\u016Dstria Esperanto Junularo (AEJ)</a></b>",
    Benino: "<b>Benino</b><b><a href=\"http://tejo-benino.webs.com/\" target=\"_blank\" rel=\"nofollow\">Benina Organizo de Junaj Esperantistoj (BOJE)</a></b>",
    Brazilo: "<b>Brazilo</b><b><a href=\"http://bejo.esperanto.org.br\" target=\"_blank\" rel=\"nofollow\">Brazila Esperantista Junulara Organizo (BEJO)</a></b>",
    Britio: "<b>Britio</b><b><a href=\"http://jeb.org.uk/\" target=\"_blank\" rel=\"nofollow\">Junularo Esperantista Brita (JEB)</a></b>",
    Bulgario: "<b>Bulgario</b><b>Bulgara Esperanto-Junularo (BEJ)</b>",
    Burundo: "<b>Burundo</b><b>Junulara Esperantista Burunda Organizo (JEBUO)</b>",
    Danio: "<b>Danio</b><b><a href=\"http://dejo.dk/\" target=\"_blank\" rel=\"nofollow\">Dana Esperantista Junulara Organizo (DEJO)</a></b>",
    Filipinoj: "<b>Filipinoj</b><b><a href=\"https://sites.google.com/site/esperantoph/\" target=\"_blank\" rel=\"nofollow\">Filipina Esperanto-Junularo (FEJ)</a></b>",
    Finnlando: "<b>Finnlando</b><b><a href=\"http://www.esperanto.fi/fejo\" target=\"_blank\" rel=\"nofollow\">Finnlanda Esperantista Junulara Organizo (FEJO)</a></b>",
    Francio: "<b>Francio</b><b><a href=\"http://esperanto-jeunes.org/\" target=\"_blank\" rel=\"nofollow\">Junulara Esperantista Franca Organizo (JEFO)</a></b>",
    Germanio: "<b>Germanio</b><b><a href=\"http://esperanto.de/dej\" target=\"_blank\" rel=\"nofollow\">Germana Esperanto-Junularo (GEJ)</a></b>",
    Grekio: "<b>Grekio</b><b>Junulara Sekcio de HEA</b>",
    Haitio: "<b>Haitio</b><b>Haita Esperanto Junulara Asocio</b>",
    Hispanio: "<b>Hispanio</b><b><a href=\"http://www.esperanto.es/wordpress/eo/\" target=\"_blank\" rel=\"nofollow\">Hispana Esperantista Junulara Societo (HEJS)</a></b>",
    Hungario: "<b>Hungario</b><b><a href=\"http://www.esperanto-junularo.hu/\" target=\"_blank\" rel=\"nofollow\">Hungara Esperanto-Junularo (HEJ)</a></b>",
    Irano: "<b>Irano</b><b><a href=\"http://www.irejo.ir\" target=\"_blank\" rel=\"nofollow\">Irana Esperantista Junulara Organizo (IREJO)</a></b>",
    Israelo: "<b>Israelo</b><b><a href=\"http://www.esperanto.org.il/jeli.html\" target=\"_blank\" rel=\"nofollow\">Junulara Esperanto-Ligo en Israelo (JELI)</a></b>",
    Italio: "<b>Italio</b><b><a href=\"http://iej.esperanto.it/\" target=\"_blank\" rel=\"nofollow\">Itala Esperantista Junularo (IEJ)</a></b>",
    Japanio: "<b>Japanio</b><b><a href=\"http://japana-esperanto-junularo.com/\" target=\"_blank\" rel=\"nofollow\">Japana Esperanto-Junularo (JEJ)</a></b>",
    Kanado: "<b>Kanado</b><b><a href=\"http://www.esperanto.ca/jek/\" target=\"_blank\" rel=\"nofollow\">Junularo Esperantista Kanada (JEK)</a></b>",
    Kolombio: "<b>Kolombio</b><b><a href=\"http://esperanto.co\" target=\"_blank\">KolombEJO</a></b>",
    "Kongo (Dem. Resp.)": "<b>Kongo (Dem. Resp.)</b><b>Demokratia Kongolanda E-ista Junulara Organizo</b>",
    "Korea Resp.": "<b>Korea Resp.</b><b><a href=\"https://www.facebook.com/kejesperanto\" target=\"_blank\" rel=\"nofollow\">Korea Esperanto-Junularo (KEJ)</a></b>",
    Kroatio: "<b>Kroatio</b><b><a href=\"http://www.keja.hr/\" target=\"_blank\" rel=\"nofollow\">Kroatia Esperanto-Junulara Asocio (KEJA)</a></b>",
    Kubo: "<b>Kubo</b><b><a href=\"https://kejo.tejo.org/\" target=\"_blank\" rel=\"nofollow\">Kuba Esperantista Junulara Organizo (KEJO)</a></b>",
    Litovio: "<b>Litovio</b><b>Litova Esperantista Junulara Ligo (LEJL)</b>",
    Madagaskaro: "<b>Madagaskaro</b><b>Junulara Esperantista Organizo de Madagaskaro (JEOM)</b>",
    Meksiko: "<b>Meksiko</b><b><a href=\"http://www.facebook.com/pages/MEJ-Meksika-Esperanto-Junularo/254023401308818\" target=\"_blank\" rel=\"noopener nofollow\">Meksika Esperanto-Junularo (MEJ)</a></b>",
    Nederlando: "<b>Nederlando</b><b><a href=\"http://esperanto-jongeren.nl/\" target=\"_blank\" rel=\"nofollow\">Nederlanda Esperanto-Junularo (NEJ)</a></b>",
    Nepalo: "<b>Nepalo</b><b><a href=\"http://esperanto.org.np/organizo-2.html\" target=\"_blank\" rel=\"nofollow\">Nepala Esperanto-Junulara Organizo (NEJO)</a></b>",
    Norvegio: "<b>Norvegio</b><b><a href=\"http://www.esperanto.no/nje/\" target=\"_blank\" rel=\"nofollow\">Norvega Junularo Esperantista (NJE)</a></b>",
    Pollando: "<b>Pollando</b><b><a href=\"http://www.pej.pl/\" target=\"_blank\" rel=\"nofollow\">Pola Esperanto-Junularo (PEJ)</a></b>",
    Portugalio: "<b>Portugalio</b><b>Portugala Esperanto-Junularo (PEJ)</b>",
    Rumanio: "<b>Rumanio</b><b>Rumana Esperantista Junulara Asocio (RumEJA)</b>",
    Rusio: "<b>Rusio</b><b><a href=\"https://vk.com/rejm_mojosas\" target=\"_blank\" rel=\"nofollow\">Rusia Esperantista Junulara Movado (REJM)</a></b>",
    Serbio: "<b>Serbio</b><b><a href=\"http://www.yurope.com/org/eos\" target=\"_blank\" rel=\"nofollow\">Serbia Esperanto-Junulara Organizo (SerbEJO)</a></b>",
    Slovakio: "<b>Slovakio</b><b><a href=\"http://skej.esperanto.sk/\" target=\"_blank\" rel=\"nofollow\">Slovakia Esperanta Junularo (SKEJ)</a></b>",
    Svedio: "<b>Svedio</b><b>Sveda Esperantista Junulara Unuiĝo (SEJU)</b>",
    Togolando: "<b>Togolando</b><b>Junulara Organizo de Togolandaj Esperantistoj (JOTE)</b>",
    Ukrainio: "<b>Ukrainio</b><b><a href=\"http://ulej.retejo.net/\" target=\"_blank\" rel=\"nofollow\">Ukrainio Ligo Esperantista Junulara (ULEJ)</a></b>",
    Usono: "<b>Usono</b><b><a href=\"http://www.esperanto.org/us/USEJ/xmethod/bonvenon.html\" target=\"_blank\" rel=\"nofollow\">Usona Esperantista Junularo (USEJ)</a></b>",
    Vjetnamio: "<b>Vjetnamio</b><b><a href=\"https://www.facebook.com/Vejovjetnamio\" target=\"_blank\" rel=\"nofollow\">Vjetnama Esperanto-Junularo (VEJO)</a></b>",
    Cxehxio: "<b>\u0108e\u0125io</b><b><a href=\"http://cxej.esperanto.cz\" target=\"_blank\" rel=\"nofollow\">\u0108e\u0125a Esperanto-Junularo (\u0108EJ)</a></b>",
    Cxinio: "<b>\u0108inio</b><b>\u0108ina Junulara Esperanto-Asocio (\u0108JEA)</b>",
    "Katalunio": "<b>– Katalunio</b><b><a href=\"https://retejo.esperanto.cat/?lang=eo\" target=\"_blank\" rel=\"nofollow\">Kataluna Esperanto-Junularo (KEJ)</a></b>",
};

var code_2_eo = {
    ar:"Argentino" ,
    at:"Auxstrio",
    bj:"Benino",
    br:"Brazilo",
    gb:"Britio",
    bg:"Bulgario",
    bi:"Burundo",
    dk:"Danio",
    ph:"Filipinoj",
    fi:"Finnlando",
    fr:"Francio",
    de:"Germanio",
    gr:"Grekio",
    ht:"Haitio",
    es:"Hispanio",
    hu:"Hungario",
    ir:"Irano",
    il:"Israelo",
    it:"Italio",
    jp:"Japanio",
    ca:"Kanado",
    co:"Kolombio",
    cd:"Kongo (Dem. Resp.)",
    kr:"orea Resp.",
    hr:"Kroatio" ,
    cu:"Kubo" ,
    lt:"Litovio" ,
    mg:"Madagaskaro" ,
    mx:"Meksiko" ,
    nl:"Nederlando" ,
    np:"Nepalo" ,
    no:"Norvegio" ,
    pl:"Pollando" ,
    pt:"Portugalio" ,
    ro:"Rumanio" ,
    ru:"Rusio" ,
    rs:"Serbio" ,
    sk:"Slovakio" ,
    se:"Svedio" ,
    tg:"Togolando" ,
    ua:"Ukrainio" ,
    us:"Usono" ,
    vn:"Vjetnamio",
    cz:"Cxehxio",
    zh:"Cxinio"
    };

let landoj_uea = [
    "al", "dz", "ad", "ao", "ag", "ar", "am", "aw", "ai", "au", "at", "az", "bs", "bd", "bb",
    "bh", "be", "bz", "by", "bj", "bm", "mm", "bw", "bo", "ba", "br", "gb", "bn", "bg", "bf", 
    "bi", "bt", "cf", "td", "cz", "cl", "cn", "dk", "do", "dm", "ci", "eg", "ec", "gq", "ee", 
    "er", "et", "ph", "fi", "fj", "fr", "pf", "ga", "gm", "gh", "de", "gr", "gd", "gl", "gp", 
    "gt", "gy", "gn", "gw", "gi", "dj", "ht", "in", "es", "hn", "hu", "hk", "id", "iq", "ir", 
    "ie", "is", "il", "it", "jm", "jp", "ye", "jo", "cv", "cm", "kh", "ky", "ca", "ge", "qa", 
    "kz", "ke", "kg", "ki", "cy", "co", "km", "cd", "cg", "kr", "kp", "cr", "hr", "cu", "ck", 
    "cw", "kw", "la", "lv", "ls", "lb", "lr", "ly", "li", "lt", "lu", "mg", "mo", "my", "mw", 
    "mv", "ml", "mt", "mh", "ma", "mq", "mu", "mr", "mx", "fm", "md", "mc", "ms", "mn", "me", 
    "mz", "na", "nr", "nl", "np", "ng", "ne", "ni", "mk", "no", "nc", "nz", "tl", "om", "pk", 
    "pw", "ps", "pa", "pg", "py", "pe", "pl", "pr", "pt", "re", "rw", "ro", "ru", "sv", "sb", 
    "ws", "kn", "lc", "vc", "sm", "st", "sa", "sc", "rs", "sn", "sl", "sg", "sy", "sk", "si", 
    "so", "lk", "za", "sd", "ss", "sr", "sz", "se", "ch", "tj", "th", "tw", "tz", "tg", "to", 
    "tt", "tn", "tr", "tc", "tm", "tv", "ug", "ua", "ae", "uy", "us", "uz", "vu", "va", "ve", 
    "vn", "zm", "zw"
];
---
title: 'Organizo kaj Estraro'
hide_git_sync_repo_link: false
menu: 'Organizo kaj Estraro'
recaptchacontact:
    enabled: false
---

## Organiza strukturo de GEJ

La laboro de GEJ estas distribuita al homoj en pluraj laborgrupoj, komisionoj kaj projektoj.
Por rapida klarigado jen ĉi-tiu organiza skemo.

<div class="columns">
    <div id="item" class="column col-12 col-mx-auto col-md-12 extra-spacing">
      <figure class="figure">
        <img class="img-responsive" src="/retideoj/gej/user/pages/images/GEJ_organiza_skemo.png">
   <!--     <figcaption class="figure-caption text-center">Organiza skemo de GEJ</figcaption>   -->
      </figure>
    </div>
</div>

## Estraro

Jen la plej gravaj adresoj:

<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Prezidanto</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="organisation_und_vorstand/michael.jpg">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <b>Michael Vrazitulis</b> <br> <br>
           <p>E-Mail:
              <a href="mailto:gej.prezidanto _at_ esperanto _punkt_ de">gej.prezidanto<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Kasisto</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="organisation_und_vorstand/jan.jpg">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <b>Jan Raring</b> <br> <br>
           <p>E-Mail:
              <a href="mailto:gej.kasko _at_ esperanto _punkt_ de">gej.kasko<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>
</div>

##  Kromaj Estraranoj


<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="organisation_und_vorstand/jonas.jpg">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
            <b>Jonas Michel Scheerschmidt</b>
            <br> <i>Vicprezidanto</i> <br>
           <p>E-Mail:
              <a href="mailto:jonas.scheerschmidt _at_ esperanto _punkt_ de">jonas.scheerschmidt<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="organisation_und_vorstand/michaela.jpg">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
            <b>Michaela Stegmaier</b> <br> <br>
           <p>E-Mail:
              <a href="mailto:michaela.stegmaier _at_ esperanto _punkt_ de">michaela.stegmaier<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>
</div>

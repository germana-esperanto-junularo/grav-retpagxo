---
title: 'Organisation und Vorstand'
media_order: 'GEJ_rilatoj6.png,jan.jpg,jonas.jpg,michael.jpg,michaela.jpg'
hide_git_sync_repo_link: false
menu: 'Organisation und Vorstand'
recaptchacontact:
    enabled: false
---

##  Bundesvorstand

<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Vorsitzender</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="organisation_und_vorstand/michael.jpg">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <b>Michael Vrazitulis</b> <br> <br>
           <p>E-Mail:
              <a href="mailto:gej.prezidanto _at_ esperanto _punkt_ de">gej.prezidanto<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <h2>Finanzen (Schatzmeister)</h2>
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="organisation_und_vorstand/jan.jpg">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
           <b>Jan Raring</b> <br> <br>
           <p>E-Mail:
              <a href="mailto:gej.kasko _at_ esperanto _punkt_ de">gej.kasko<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>
</div>

##  Weitere Vorstandsmitglieder


<div class="columns">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="organisation_und_vorstand/jonas.jpg">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
            <b>Jonas Michel Scheerschmidt</b>
            <br> <i>Stellvertretender Vorsitzender</i> <br>
           <p>E-Mail:
              <a href="mailto:jonas.scheerschmidt _at_ esperanto _punkt_ de">jonas.scheerschmidt<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>

  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <div class="columns">
        <div id="item" class="column col-6 col-md-12 extra-spacing">
          <figure class="figure">
            <img class="img-responsive" src="organisation_und_vorstand/michaela.jpg">
          </figure>
        </div>
        <div id="item" class="column col-6 col-md-12 extra-spacing">
            <b>Michaela Stegmaier</b> <br> <br>
           <p>E-Mail:
              <a href="mailto:michaela.stegmaier _at_ esperanto _punkt_ de">michaela.stegmaier<img src="/retideoj/gej/user/pages/images/at_symbol.gif" style="height: 16px;">esperanto.de</a>
            </p>
        </div>
      </div>
  </div>
</div>

## Aufbau der Deutschen Esperanto-Jugend

Die Arbeit der DEJ ist in mehreren Kommisionen, Arbeitsgruppen und Projekten organisiert.
Um einen kleinen Überblick zu bekommen hier das Wichtigste in einem Bild.

<div class="columns">
    <div id="item" class="column col-12 col-mx-auto col-md-12 extra-spacing">
      <figure class="figure">
        <img class="img-responsive" src="organisation_und_vorstand/GEJ_rilatoj6.png">
      <!--  <figcaption class="figure-caption text-center">Organigramm der GEJ</figcaption>  -->
      </figure>
    </div>
</div>


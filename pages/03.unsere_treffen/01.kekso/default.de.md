---
title: KEKSO
hide_git_sync_repo_link: false
menu: KEKSO
recaptchacontact:
    enabled: false
---

## KEKSO

KEKSO steht für "Kreema Esperanto-KurSO". Wenn du dich für Esperanto interessierst und dir einfach mal anschauen willst, wie die Sprache funktioniert, dann hast du hier die Chancem dich mit anderen Jugendlichen zu treffen. Wir bieten für euch einen Sprachkurs für AnfängerInnen an.
Das KEKSO ist auch ein Treffpunkt für Mitglieder der Deutschen Esperanto-Jugend, die wechselnde Workshops zur Umsetzung konkreter Projekte mitgestalten können, in denen neu erlernte Kenntnisse schnell Anwendung finden. Wir nehmen ein Hörspiel auf Esperanto auf oder drehen einen kleinen Film. Auch kleine Theaterstücke sind denkbar. Vielleicht spielt ein Keks die Hauptrolle?

<div id="item" class="column col-6 col-mx-auto col-md-12 extra-spacing">
<figure class="figure">
    <img class="img-responsive" src="/retideoj/gej/user/pages/images/KEKSO_Logo.png">
    <figcaption class="figure-caption text-right"><small>© Logotipo kreita de <a href="https://jonny-m.org/">Jonny M</a></small></figcaption>
 </figure>
</div>

Mit etwa 10 bis 20 Teilnehmenden ist das KEKSO ein eher kleines Esperanto-Treffen; perfekt also für deinen Einstieg in die bunte Esperanto-Welt.
 
Der Spaß steht hier im Vordergrund! Es wird gemeinsam gespielt und gekocht, Musik gemacht, etc. Die oft sehr unterschiedlichen Teilnehmenden aus allen Ecken Deutschlands (und darüber hinaus) lernen sich so besser kennen. 

Du warst noch nie auf einem Esperanto-Treffen? Dann laden wir dich kostenlos zum KEKSO ein! Ansonsten belaufen sich die Kosten auf 45€ pro TeilnehmerIn bzw. 30€ für Mitglieder der Deutschen Esperanto-Jugend.

Die Organisation des KEKSOs übernehmen die Mitglieder der Deutschen Esperanto-Jugend selbst. Mindestens zweimal jährlich, meist an Wochenenden im Frühjahr und im Herbst, wird es angeboten. Der Veranstaltungsort wechselt jedes Mal, sodass die Treffen bunt gestreut in ganz Deutschland stattfinden. Bisherige Veranstaltungsorte waren zum Beispiel: Neumünster, Görlitz, Erfurt, Nürnberg, Zweibrücken und Hameln. Wenn das KEKSO auch mal in deine Stadt kommen soll, dann schreib uns über das Kontaktformular. Wenn du wissen willst, was die Leute denken, die auf den letzten KEKSOs waren dann schau mal [hier](../../../kune/tag:KEKSO) rein.

Wann und wo das nächste KEKSO stattfindet, kannst du hier nachlesen.

## Nächstes KEKSO beim DEK in Oldenburg 03.-06.06.2022

**_ACHTUNG: Bitte melde dich bis spätestens zum 14.04.2022 an, wenn du einen Schlafplatz in der gemeinsamen Unterkunft haben möchtest!_**

Lieber Neuling, liebes DEJ-Mitglied,

Vom 03. bis 06. Juni 2022 findet das 29. KEKSO in Oldenburg statt. Unsere Unterkunft ist das [Boardinghouse Oldenburg.](https://www.boardinghouse-ol.de)

Es wird sowohl einen AnfängerInnen-Sprachkurs als auch Weiterbildungsangebote für Fortgeschrittene im Programm geben. Außerdem freuen wir uns auf eure Beiträge, die ihr im Anmeldeformular angeben könnt (oder gerne auch in unserem Kontaktformular). Außerdem haben wir die Möglichkeit, die vielen Angebote des 99. Deutschen Esperanto-Kongresses (DEK) zu nutzen und mitzugestalten. Das Thema des Kongresses ist Nachhaltigkeit.

Kosten für Erstteilnehmende an einem Esperantotreffen: 0€

Kosten für Mitglieder der DEJ: 30€

Kosten für Nicht-DEJ Mitglieder: 45€

Im Teilnahmebeitrag sind Unterkunft und die Mahlzeiten an dem Wochenende mit inbegriffen. Wir bitten dich trotzdem, etwas Obst oder Kekse (so viel, wie du selbst essen würdest) und vielleicht das Rezept deines Lieblingsessens mitzubringen. Wer nichts mitbringt, zahlt 10€ mehr.

Anmelden könnt ihr euch ab sofort hier. 
<a class="btn btn-success" href="./kekso/anmeldung" style="color: white !important;">Jetzt zur Anmeldung!</a>

Eine Woche vor Beginn des KEKSOs bekommst du die letzen Infos per Mail!

Wir freuen uns auf deine Teilnahme!
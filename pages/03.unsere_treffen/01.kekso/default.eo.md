---
title: KEKSO
hide_git_sync_repo_link: false
menu: KEKSO
recaptchacontact:
    enabled: false
---

## KEKSO

KEKSO estas la mallongigo de Kreema Esperanto-KurSO. Ĝi estas eta renkontiĝo, kiu donu ŝancon al junuloj, kiuj interesiĝas pri Esperanto, ekkoni aliajn junulojn, kiuj volas scii pli pri la internacia lingvo. Pro tio dum KEKSO okazas kurso por komencantoj.
Sed ankaŭ membroj de la Germana Esperanto-Junularo renkontiĝas dum KEKSO, kreas atelierojn kaj realigas etajn projektojn ankaŭ kun ĵus akiritaj konoj aŭ kapabloj. Eble ni registras podkaston, rakonton, aŭ eĉ filmeton. Ankaŭ teatraĵojn eblas elpensi, ĉu kun kekso kiel ĉefrolulo?

<div id="item" class="column col-6 col-mx-auto col-md-12 extra-spacing">
<figure class="figure">
    <img class="img-responsive" src="/retideoj/gej/user/pages/images/KEKSO_Logo.png">
    <figcaption class="figure-caption text-right"><small>© Logotipo kreita de <a href="https://jonny-m.org/">Jonny M</a></small></figcaption>
 </figure>
</div>

Kun ĉirkaŭ 10-20 partoprenantoj la KEKSO estas unu de la etaj renkontiĝoj, do perfekte por la unuaj paŝoj en esperantujon.

Amuziĝo plej gravas! Kune ni ludas kaj kuiras, faras muziukon ktp. La bunta partoprenantaro tiel pli bone interkonatiĝas.

Vi ankoraŭ ne partoprenis en Esperanto-renkontiĝo aŭ venas el alia lando ol Germanio? Do ni invitas vin senpage partopreni! Membroj de GEJ normale pagas 30€ kaj nemembroj el Germanio 45€ por ĉeesta semajnfino.

La organizadon de KEKSO transprenas GEJ-membroj mem. Minimume dofoje jare, en semajnfinoj en printempo kaj aŭtuno, ĝi okazas en diversaj germanaj urboj. Se vi volas, ke KEKSO okazu en via urbo, kontaktu nin per la formularo. Se vi ŝatus havi impresojn de rakontoj fare de homoj kiuj partoprenis pasintajn KEKSOjn, esploru [ĉi tie](../../../kune/tag:KEKSO).

Kaj kie okazos la sekva KEKSO, vi vidos ĉi-sube.

## Sekva KEKSO dum GEK en Oldenburg 03.-06.06.2022

**_ATENTU: Bonvolu nepre aliĝi ĝis plej malfrue la 14-an de aprilo 2022, se vi volas havi dormlokon en la komuna tranoktejo!_**

Kara novulo, kara membro de GEJ,

De la 03-a ĝis 06-a de junio 2022 la KEKSO okazos en Oldenburg. Ni loĝos en la tranoktejo [Boardinghouse Oldenburg.](https://www.boardinghouse-ol.de)

En la programo ekzistos kaj kurso por komencantoj kaj trajnado por progresantoj. Krome ni antaŭĝojas viajn programproponojn, kiujn vi povas indiki en la aliĝilo (aŭ per nia kontaktformularo). Ankaŭ ni povos profiti de la programo de la 99-a Germana Esperanto-Kongreso (GEK) kaj pliriĉigi ties programon. La temo de la kongreso estos daŭripoveco.

Kotizoj por novuloj kaj eksterlandanoj: 0€

Kotizoj por membroj de GEJ: 30€

Kotizoj por ne-membroj de GEJ: 45€

La prezo enhavas loĝadon kaj manĝadon dum la semajnfino. Ni petas ĉiujn partoprenantojn aŭ kunporti keksojn aŭ fruktojn (nur tiom, kiom vi povus mem manĝi) aŭ krompagi 10€.

Aliĝi vi povos ekde nun ĉi-tie. <a class="btn btn-success" href="./kekso/anmeldung" style="color: white !important;">Ek al la aliĝilo!</a>

Unu semajno antaŭ la aranĝo venos lastaj informoj per retpoŝto.

Ni antaŭĝojas pri via ĉeesto!
---
title: 'Internationale Treffen'
hide_git_sync_repo_link: false
menu: 'Internationale Treffen'
recaptchacontact:
    enabled: false
---

## Internationale Treffen

Falls du noch nicht weißt, was du in deinen Ferien oder Freizeit machen sollst und neue Leute und dir vielleicht ein paar neue Ecken in Deutschland oder Europa erschließen möchtest sind hier ein paar Esperantotreffen aufgeführt, die sich dafür anbieten. Dies sind insbesondere etabliertere und größere Veranstalltungen, die mit internationalen Besucherspektrum, bunten Programm und bester Atmosphäre punkten können (insbesondere um sich authentisch auf Esperanto verständigen und Esperanto-Kultur erleben zu können). 

Wenn du eines dieser oder auch andere Treffen besuchen solltest, informiere dich über die <a href="../dej/fahrtkosten">Fahrtkostenzuschüsse</a> die es dieses Jahr gibt! Wenn du deine Erfahrungen auf einem Esperantotreffen teilen möchtest ist immer Platz für einen kleinen Bericht in der <a href="../kune">kune</a>, schreib einfach an kune(ĉe)esperanto.de.

### Internacia Junulara Kongreso (IJK)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>Das Internacia Junulara Kongreso (<a href="https://www.tejo.org/cause/internacia-junulara-kongreso/">IJK</a>, Internationale Jugendtreffen) ist der größte jährliche Esperanto Jugendkongress organisiert von der Weltesperantojugend <a href="tejo.org">TEJO</a>. Es wird jedes Jahr in einem anderen Land organisiert und findet für gewöhnlich zwischen Juli und August statt, oft kurz vor dem Esperanto-Weltkongress. 
      </p>
      <p>Hier geht es zur <a href="https://ijk.tejo.org/">Anmeldung!</a></p>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
    <a href="https://www.tejo.org/cause/internacia-junulara-kongreso/">
      <img class="img-responsive" src="internaciaj_renkontigxoj/ijk.png">
    </a>
   </figure>
  </div>
</div>

<div class="columns text-center">
<div class="col-12" style="margin-top:20px;margin-buttom:20px;">
<a href="../kune/tag:IJK">Ließ mehr Erfahrungsberichte zum IJK in der kune</a>
</div>
</div>

### Somera Esperanto-Studado (SES)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
      <a href="https://ses.ikso.net/">
        <img class="img-responsive" src="internaciaj_renkontigxoj/ses.png">
      </a>
   </figure>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>Das Somera Esperanto-Studado (Esperanto Sommerschule, kurz SES) ist das größte jährliche internationale Esperantolernevent. Seit 2007, findet es im jeden Sommer in der Slovakai statt. Neben Konzerten, Sprachkursen und Ausflügen gibt auch eine Vielzahl von teilnehmerorganisierten Programmpunkten. Mit Teilnehmern aus meist über 30 Ländern erwarten dich viele interessante Menschen und Kulturbeiträge.</p>
      <p>Hier geht es zur <a href="https://ses.ikso.net/">Anmeldung</a>!</p>
  </div>
</div>

### Internacia Junulara Semajno (IJF)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>Das Internacia Junulara Festivalo (IJF) ist das Esperanto-Jugendtreffen der italienischen Esperantojugend, was jedes Jahr in einer anderen Region Italiens stattfindet. Für gewöhnlich findet es während der Pfingstferien statt. </p>
      <p>Seit 1977 kommen so jährlich um die circa 100 Teilnehmer zusammen (das größte IJF hatte 325 Teilnehmer). </p>
      <p>Hier geht es zur <a href="http://iej.esperanto.it/wordpress/eo/la-festivalo/">Anmeldung</a>!</p>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
    <figure class="figure text-right">
    <a href="http://iej.esperanto.it/wordpress/eo/la-festivalo/"> 
      <img class="column img-responsive" style="float: right;" src="https://www.tejo.org/wp-content/uploads/2020/01/ijf1.jpg">
    </a>
  </figure>
  </div>
</div>

<div class="columns text-center">
<div class="col-12" style="margin-top:20px;margin-buttom:20px;">
<a href="../kune/tag:IJF">Ließ mehr Erfahrungsberichte zum IJF in der kune</a>
</div>
</div>

### Internacia Junulara Semajno (IJS)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
    <a href="http://ijs.hu/">
      <img class="img-responsive" src="internaciaj_renkontigxoj/ijs.png">
    </a>
  </figure>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      <p>Das Internacia Junulara Semajno, (Internationale Jugendwoche - kurz IJS und auf ungarisch Nemzetközi Ifjúsági Eszperantó Fesztivál), ist das Esperanto-Jugendtreffen ungarischen Esperantojugend (HEJ). </p>
      <p>Hier geht es zur <a href="http://ijs.hu/">Anmeldung</a>!</p>
  </div>
</div>

### Artaj Konfrontoj en Esperanto (ARKONES)

<div id="item" class="columns col-md-12 extra-spacing">
  <div id="item" class="column col-6 col-md-12 extra-spacing">
      Das Arkones (kurz für Artaj Konfrontoj en Esperanto) ist ein regionales Esperantotreffen, dass jedes Jahr in Poznano stattfindet. Es wird von E-SENCO anschließend an eine Vorlesungsreihe der Interlinguistik der Adam Mickiewicz Universität im September organisiert. 
      <p>Hier geht es zur <a href="http://arkones.org/">Anmeldung</a>!</p>
  </div>
  <div id="item" class="column col-6 col-md-12 extra-spacing">
  <figure class="figure">
    <a href="http://arkones.org/">
      <img class="img-responsive" style="float: right;" src="https://www.tejo.org/wp-content/uploads/2019/09/ArkEve.jpg">
    </a>
  </figure>
  </div>
</div>
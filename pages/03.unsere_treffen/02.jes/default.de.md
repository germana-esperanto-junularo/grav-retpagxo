---
title: 'Junulara E-Semajno (JES)'
hide_git_sync_repo_link: false
menu: 'Junulara E-Semajno (JES)'
recaptchacontact:
    enabled: false
---

## Junulara E-Semajno

Die Jugend E-Woche (JES) ist eine Jugend Esperanto-Veranstaltung, die von der Polnischen Esperanto-Jugend (PEJ) und der Deutschen Esperanto-Jugend (DEJ) organisiert wird. Sie findet jedes Jahr in den letzten Dezember- und den ersten Januartagen statt, dauert etwa eine Woche und wird in Mitteleuropa ausgerichtet. Vom Typ, nicht aber von der Jahreszeit her, ähnelt sie dem Internationalen Jugend-Kongress (IJK).

<div id="item" class="column col-6 col-mx-auto col-md-12 extra-spacing">
<figure class="figure">
<img class="img-responsive" src="/retideoj/gej/user/pages/images/jes.png">
</figure>
</div>

JES wurde zusammengelegt aus den Vorherigen Silvestertreffen AS der PEJ [1] und IS der DEJ und gibt es seit dem Jahreswechsel 2008/2009. [2]

> vikipedio

**Wann und wo das nächste JES stattfindet, kannst du [hier](http://jes.pej.pl/de/) nachlesen.**

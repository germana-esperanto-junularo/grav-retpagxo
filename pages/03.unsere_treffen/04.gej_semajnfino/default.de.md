---
title: DEJ-Wochenende
date: '19:01 25-02-2022'
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
media_order: 'GEJ-Semajnfino (largha).png'
---

## Das DEJ-Wochenende in Halbe

Vom 22. bis zum 24. April 2022, also exakt ein halbes Jahr nach dem letzten [KEKSO](../kekso), veranstalten wir als Deutsche Esperanto-Jugend ein Mitglieder- und Aktiven-Wochenende in der einzigartigen [Esperanto-Stacio](https://www.esperantostacio.com) in Halbe bei Berlin.

Auf diesem Treffen erwarten dich unter anderem spannende Workshops rund um die Themen Esperanto und gesellschaftliches Engagement. Auch ein paar Ausflüge in der Umgebung stehen an, etwa zum Waldfriedhof Halbe, einer der größten Kriegsgräberstätten des Landes, oder zum Esperanto-Laden in Berlin-Kreuzberg. Für die abendliche Unterhaltung ist natürlich ebenfalls gesorgt!

#### Anreise

Anreisen kannst du bequem per Zug: Der Bahnhof heißt **Halbe**, und unsere Unterkunft befindet sich unmittelbar an den Gleisen! Von Berlin Ostkreuz aus dauert die Fahrt mit dem **RE 2** oder der **RB 24** nur 38 Minuten.

Falls du mit dem Auto unterwegs bist, such einfach direkt die Adresse der Unterkunft:  
**Bahnhofstraße 30  
15757 Halbe**

#### Teilnahmegebühren

Um beim DEJ-Wochenende teilzunehmen, benötigen wir von dir eine Teilnahmegebühr, mit der deine Übernachtung (2 Nächte), deine Verpflegung und deine Teilnahme am Programm abgedeckt ist.

Als Mitglied der DEJ bis 27 Jahre zahlst du **40€**, wenn du kein Einkommen hast, bzw. **50€**, wenn du berufstätig bist.  
Als Nichtmitglied der DEJ zahlst du **55€**, wenn du kein Einkommen hast, bzw. **65€**, wenn du berufstätig bist.  
Falls du **nicht** in der gemeinsamen Unterkunft übernachtest, zahlst du lediglich **15€** für Verpflegung und Programm.

#### Anmeldung

_**ACHTUNG: Bitte melde dich bis spätestens zum 02.04.2022 an, wenn du einen Schlafplatz in der gemeinsamen Unterkunft haben möchtest!**_

[Anmelden kannst du dich ab sofort hier!](https://forms.gle/nZKoJ2nCtaUbbyaY8)

#### Zahlung

Du kannst deinen Teilnahmebeitrag (s. o.) bereits vorab per Überweisung an uns bezahlen:

**Kontoinhaberin:** Deutsche Esperanto-Jugend  
**IBAN:** DE64 2512 0510 0008 4249 00  
**BIC/SWIFT:** BFSWDE33HAN

![GEJ-Semajnfino%20%28largha%29](GEJ-Semajnfino%20%28largha%29.png "GEJ-Semajnfino%20%28largha%29")

---
title: DEJ-Wochenende
date: '19:01 25-02-2022'
hide_git_sync_repo_link: false
recaptchacontact:
    enabled: false
media_order: 'GEJ-Semajnfino (largha).png'
---

## La GEJ-Semajnfino en Halbe

De la 22-a ĝis la 24-a de aprilo, do ekzakte duonan jaron post la lasta [KEKSO](../kekso), ni kiel Germana Esperanto-Junularo organizos membro- kaj aktivulo-semajnfinon en la unika [Esperanto-Stacio](https://www.esperantostacio.com) en Halbe ĉe Berlino.

Dum tiu renkontiĝo vin atendos interalie pensigaj atelieroj pri la temoj Esperanto kaj ensocia engaĝiĝo. Same sur la tagordo estas ekskursoj al la ĉirkaŭaĵo, kiel al la foresta tombejo Halbe, unu el la plej grandaj militaj tombejoj de la lando, aŭ al la Esperanto-Butiko en Berlin-Kreuzberg. La vesperojn ni pasigos per distra programo!

#### Alveno

Vi povas facile alveni per trajno: La stacio nomiĝas **Halbe**, kaj nia ejo troviĝas tuj apud la kajo! De Berlin Ostkreuz la veturado per la linio **RE 2** aŭ **RB 24** daŭras nur 38 minutojn.

Kaze ke vi venos aŭte, provu trovi la lokon per la adreso de la ejo:  
**Bahnhofstraße 30  
15757 Halbe**

#### Partoprenkotizoj

Por partopreni la GEJ-Semajnfinon, ni postulas kotizon, per kiu vi kovros vian tranoktadon (2 noktojn), manĝadon kaj partoprenadon de la programo.

Kiel membro de GEJ ĝis-27-jaraĝa vi pagas **40€**, se vi ne havas regulan enspezon, sed **50€**, se vi ja laboras.  
Kiel ne-membro de GEJ vi pagas **55€**, se vi ne havas regulan enspezon, sed **65€**, se vi ja laboras.  
Se vi **ne** tranoktos en la komuna ejo, vi nur pagas **15€** entute por manĝado kaj programo.

#### Aliĝo

_**ATENTU: Bonvolu nepre aliĝi ĝis plej malfrue la 2-an de aprilo 2022, se vi volas havi dormlokon en la komuna tranoktejo!**_

[Aliĝi vi jam tuj povas ĉi tie!](https://forms.gle/nZKoJ2nCtaUbbyaY8)

#### Pago

Vi povas pagi vian kotizon (v. supre) jam antaŭe per ĝirado al nia konto:

**Posedanto:** Deutsche Esperanto-Jugend  
**IBAN:** DE64 2512 0510 0008 4249 00  
**BIC/SWIFT:** BFSWDE33HAN

![GEJ-Semajnfino%20%28largha%29](GEJ-Semajnfino%20%28largha%29.png "GEJ-Semajnfino%20%28largha%29")
